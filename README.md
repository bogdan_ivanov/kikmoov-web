To synchronizing thumbnails for the provider sonata.media.provider.image in the workspace context:
https://sonata-project.org/bundles/media/master/doc/reference/command_line.html

bin/console sonata:media:sync-thumbnails sonata.media.provider.image workspace

# Install `wkhtmltopdf` lib for generating pdf 
### Please run this commands on your server:
```
1. sudo apt-get update
2. sudo apt-get install xvfb libfontconfig wkhtmltopdf
```
###