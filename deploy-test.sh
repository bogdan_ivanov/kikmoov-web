#!/bin/bash
$(aws ecr get-login --no-include-email)
echo y | docker system prune

## API ##
echo "Building admin container"
cp $WORKSPACE/secret-test/.env-api $WORKSPACE/api/app/.env
cd $WORKSPACE/api
docker build -t kikmoov-api .
cd $WORKSPACE
docker tag kikmoov-api:latest 853707894985.dkr.ecr.eu-west-2.amazonaws.com/kikmoov-api:latest
docker push 853707894985.dkr.ecr.eu-west-2.amazonaws.com/kikmoov-api:latest
###########

### UPDATE ECS SERVICES
echo "Updating ecs services"
# Declare an array of string with type
declare -a services=("kikmoove-api")
 
# Iterate the string array using for loop
for service in ${services[@]}; do
    aws ecs update-service --cluster kikmoov  --service $service --force-new-deployment
    echo "Service $service updated successfully"
done
