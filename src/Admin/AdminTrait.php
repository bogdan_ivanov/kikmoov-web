<?php

namespace App\Admin;

use App\Model\BuyerInternalNotificationModelManager;
use App\Notifications\NotificationService;
use App\Services\RouterWrapper;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

trait AdminTrait
{
    /**
     * @return ContainerInterface
     */
    protected function getContainer() : ContainerInterface
    {
        return $this->getConfigurationPool()->getContainer();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return NotificationService
     */
    protected function getNotificationService() : NotificationService
    {
        return $this->getConfigurationPool()->getContainer()->get(NotificationService::class);
    }

    /**
     * @return RouterWrapper
     */
    protected function getRouter() : RouterWrapper
    {
        return $this->getConfigurationPool()->getContainer()->get(RouterWrapper::class);
    }

    /**
     * @return BuyerInternalNotificationModelManager|mixed
     */
    protected function getBuyerInternalNotificationModelManager()
    {
        return $this->getContainer()->get(BuyerInternalNotificationModelManager::class);
    }
}
