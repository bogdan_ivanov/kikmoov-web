<?php

namespace App\Admin;

use App\Provider\AreaMediaProvider;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * Class AreaAdmin
 * @package App\Admin
 */
class AreaAdmin extends AbstractAdmin
{

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('status')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('slug')
            ->add('status')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
        ;

        if (!is_null($this->getSubject()->getId())) {
            $formMapper->add('slug');
        }

        $formMapper
            ->add(
                'boroughs',
                ModelType::class, [
                    'label'         => 'Boroughs',
                    'required'      => true,
                    'multiple'      => true,
                    'by_reference'  => false,
                    'btn_add' => false
                ]
            )
            ->add(
                'status',
                CheckboxType::class,
                [
                    'label' => 'Active',
                    'required' => false
                ]
            )
            ->add(
                'image',
                ModelListType::class,
                [
                    'required' => false,
                    'btn_list' => null,
                    'btn_edit' => null,
                ],
                ['link_parameters' => ['provider' => AreaMediaProvider::class, 'context' => 'area']]
            )
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('slug')
            ->add('status')
            ->add('locations')
            ->add('image')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}