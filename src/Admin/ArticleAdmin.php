<?php

namespace App\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class ArticleAdmin
 * @package App\Admin
 */
final class ArticleAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('title')
			->add('description')
			->add('slug')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('title')
			->add('description')
			->add('slug')
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
        ;

        if (!is_null($this->getSubject()->getId())) {
            $formMapper->add('slug');
        }

        $formMapper
            ->add('published')
            ->add('category')
            ->add('description')
            ->add('body', CKEditorType::class, [
                'config' => [
                    'uiColor' => '#ffffff'
                ]
            ])
            ->add('coverImageUrl')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('title')
			->add('description')
			->add('body')
			->add('slug')
            ->add('coverImageUrl')
			->add('metaRobots')
			->add('createdAt')
			->add('updatedAt')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
