<?php

namespace App\Admin;

use App\Entity\Booking;
use App\Entity\BuyerInternalNotification;
use App\Entity\Location;
use App\Entity\User;
use App\InternalNotification\InternalNotificationType;
use App\InternalNotification\PushNotificationType;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationsEvent;
use App\Services\CalendarFileGenerator;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * Class BookingAdmin
 * @package App\Admin
 */
final class BookingAdmin extends AbstractAdmin
{
    use AdminTrait;

    /**
     * @return CalendarFileGenerator
     */
    protected function getCalendarFileGenerator() : CalendarFileGenerator
    {
        return $this->getConfigurationPool()->getContainer()->get(CalendarFileGenerator::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add(
			    'startTime',
                'doctrine_orm_date',
                [
                    'field_type' => DatePickerType::class
                ]
            )
            ->add(
                'location',
                'doctrine_orm_string',
                ['label' => 'Locations'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getLocations()
                ]
            )
            ->add(
                'seller',
                'doctrine_orm_string',
                ['label' => 'Seller'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getSellers()
                ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
            ->add(
                'workspace.location',
                'string',
                [
                    'label' => 'Location',
                    'template' => 'admin/booking/location_link_row.html.twig'
                ]
            )
            ->add('workspace', 'string', ['template' => 'admin/booking/workspace_link_row.html.twig'])
            ->add('units')
			->add('startTime')
			->add('endTime')
			->add('bookingType')
			->add('phone')
			->add('name')
			->add('email')
			->add('duration')
			->add('tenantType')
			->add('status')
			->add('amount')
			->add('amountVAT')
			->add('billingAddress')
			->add('city')
			->add('postCode')
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                    'download' => [
                        'template' => 'admin/booking/list__action_download.html.twig'
                    ],
                ],
            ]);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('download', $this->getRouterIdParameter().'/download');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General')
                    ->add(
                        'status',
                        ChoiceType::class,
                        [
                            'choices' => Booking::adminStatuses()
                        ]
                    )
                    ->add(
                        'bookingType',
                        ChoiceType::class,
                        [
                            'choices' => Booking::adminBookingTypes()
                        ]
                    )
                    ->add(
                        'units',
                        IntegerType::class,
                        [
                            'attr' => [
                                'min' => 1
                            ]
                        ]
                    )
                    ->add(
                        'startTime',
                        DateTimePickerType::class
                    )
                    ->add(
                        'endTime',
                        DateTimePickerType::class,
                        ['required' => false]
                    )
                    ->add('billingAddress')
                    ->add('city')
                    ->add('postCode')
                ->end()
            ->end()
            ->tab('Tenant')
                ->with('Tenant')
                    ->add('name')
                    ->add(
                        'tenantType',
                        ChoiceType::class,
                        [
                            'choices' => Booking::adminTenantTypes()
                        ]
                    )
                    ->add('phone')
                    ->add('email')
                    ->add('information')
                    ->add('duration')
                ->end()
            ->end()
            ->tab('Payment')
                ->with('Payment')
                    ->add(
                        'paymentType',
                        ChoiceType::class,
                        [
                            'choices' => Booking::adminPaymentTypes()
                        ]
                    )
                    ->add('paymentLog')
                    ->add('amount')
                    ->add('customerId')
                    ->add(
                        'attempts',
                        NumberType::class,
                        [
                            'attr' => [
                                'min' => 0
                            ]
                        ]
                    )
                ->end()
            ->end()
			;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('startTime')
			->add('endTime')
			->add('bookingType')
			->add('phone')
			->add('name')
			->add('email')
			->add('information')
            ->add('billingAddress')
            ->add('city')
            ->add('postCode')
			->add('duration')
			->add('tenantType')
			->add('status')
			->add('paymentType')
			->add('paymentLog')
			->add('amount')
			->add('amountVAT')
			->add('customerId')
			->add('attempts')
			->add('attendees')
			->add('createdAt')
			->add('updatedAt')
			;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * @param Booking $booking
     */
    public function prePersist($booking)
    {
        $this->checkForStatusChanges($booking);
    }

    /**
     * @param Booking $booking
     */
    public function preUpdate($booking)
    {
        $this->checkForStatusChanges($booking);
    }

    /**
     * @param Booking $booking
     * @throws \Exception
     */
    private function checkForStatusChanges(Booking $booking) : void
    {
        $original = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($booking);

        if (isset($original['status']) && $original['status'] !== $booking->getStatus()) {
            $this->changeInternalNotificationStatus($booking, $booking->getStatus());
            $this->sendNotification($booking, $booking->getStatus());
            return;
        }
    }

    /**
     * @param Booking $booking
     * @param string $status
     */
    protected function changeInternalNotificationStatus(Booking $booking, string $status): void
    {
        if (in_array($status, Booking::mapStatusesInternalNotification()) && $booking->getBookingType() === Booking::BOOKING_REQUEST) {
            $params = [
                'time' => $booking->getStartTime()->format('h:ia'),
                'date' => $booking->getStartTime()->format('l, jS F'),
                'workspace_info' => $booking->getWorkSpace()->getWorkspaceInfo(),
                'location_name' => $booking->getWorkSpace()->getLocation()->getName()
            ];

            $event = array_search($status, Booking::mapStatusesInternalNotification());

            ['message' => $message] = $this->getInternalNotificationType()->findEvent($event, $params);
            $booking->getInternalNotification()->setStatus($event);
            $booking->getInternalNotification()->setMessage($message);
        }
    }

    /**
     * @param Booking $booking
     * @param string $status
     * @throws \Exception
     */
    protected function sendNotification(Booking $booking, string $status) : void
    {
        $workSpace = $booking->getWorkSpace();

        if (is_null($workSpace)) {
            return;
        }

        $buyer = $booking->getUser();

        if (is_null($buyer)) {
            return;
        }

        $location = $workSpace->getLocation();

        if (is_null($location)) {
            return;
        }

        $seller = $location->getUser();

        if (is_null($seller)) {
            return;
        }

        if ($status === Booking::ACCEPTED) {

            $pushParams = [
                'startTime' => $booking->getStartTime()->format('H:i'),
                'startDate' => $booking->getStartTime()->format('d.m.Y'),
                'address' => $workSpace->getLocation()->getAddress(),
                'user' => $booking->getUser()->getFullname()
            ];

            $this->getBuyerInternalNotificationModelManager()->create(
                $booking->getUser(),
                $pushParams,
                PushNotificationType::BOOKING_ACCEPTED,
                $booking->getId(),
                BuyerInternalNotification::BOOKING
            );

            $this->getNotificationService()->sendEmail(
                $booking->getUser(),
                NotificationsEvent::BOOKING_APPROVED,
                [
                    'location' => $location,
                    'workspace' => $workSpace,
                    'seller' => $seller,
                    'buyer' => $buyer,
                    'booking' => $booking,
                    'link' => $this->getRouter()->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workSpace->getId()))
                ],
                $location->getUser()
            );
        } elseif ($status === Booking::DECLINED || $status === Booking::CANCELED) {
            $this->getBuyerInternalNotificationModelManager()->delete(
                $booking->getUser(),
                $booking->getId(),
                BuyerInternalNotification::BOOKING
            );
        }
    }

    /**
     * @return array
     */
    protected function getLocations()
    {
        $list = [];

        $locations = $this->getEntityManager()->getRepository(Location::class)->findAll();

        if (!empty($locations)) {
            /** @var Location $location */
            foreach ($locations as $location) {
                $list[$location->getName()] = $location->getId();
            }
        }

        return $list;
    }

    /**
     * @return array
     */
    protected function getSellers()
    {
        $list = [];

        $users = $this->getEntityManager()->getRepository(User::class)->findAll();

        if (!empty($users)) {
            /** @var User $user */
            foreach ($users as $user) {
                if (!$user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                $list[$user->getFullname() . ' ( ' . $user->getCompanyName() . ' ) '] = $user->getId();
            }
        }

        return $list;
    }

    /**
     * @param string $context
     * @return QueryBuilder
     * @throws \Exception
     */
    public function createQuery($context = 'list')
    {
        $filters = $this->getRequest()->get('filter');
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($context === 'list') {
            //collect all params from filter request
            $params = [];

            if (isset($filters['location']['value']) && $filters['location']['value']) {
                $query
                    ->innerJoin('o.workSpace', 'w')
                    ->innerJoin('w.location', 'l')
                    ->andWhere('l.id = :locationId')
                    ;

                $params['locationId'] = $filters['location']['value'];
            }

            if (isset($filters['seller']['value']) && $filters['seller']['value']) {
                $query
                    ->innerJoin('o.workSpace', 'w')
                    ->innerJoin('w.location', 'l')
                    ->innerJoin('l.user', 'u')
                    ->andWhere('u.id = :sellerId')
                ;

                $params['sellerId'] = $filters['seller']['value'];
            }

            if (!empty($params)) {
                $query->setParameters($params);
            }
        }
        return $query;
    }

    /**
     * @return mixed
     */
    protected function getInternalNotificationType()
    {
        return $this->getContainer()->get(InternalNotificationType::class);
    }
}
