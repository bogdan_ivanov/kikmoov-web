<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class ArticleAdmin
 * @package App\Admin
 */
final class BuyerInternalNotificationAdmin extends AbstractAdmin
{

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
        $collection->remove('create');
        $collection->remove('edit');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('actionType')
			->add('actionGroup')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('subject')
			->add('message')
			->add('actionType')
			->add('actionGroup')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('subject')
			->add('message')
			->add('actionType')
			->add('actionGroup')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
