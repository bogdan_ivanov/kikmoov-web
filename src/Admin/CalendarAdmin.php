<?php

namespace App\Admin;

use App\Admin\Type\CalendarEventType;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
//use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class CalendarAdmin
 * @package App\Admin
 */
final class CalendarAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('name')
            ->add('status')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('name')
            ->add('status')
            ->add('type')
            ->add('data')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if (is_null($this->getSubject()) || is_null($this->getSubject()->getId())) {
            $formMapper
                ->add(
                    'name',
                    TextType::class
                )
                ->add('status')
                ->add(
                    'type',
                    TextType::class,
                    [
                        'disabled' => true,
                        'attr' => [
                            'value' => Calendar::getCustomTypeLabel()
                        ]
                    ]
                )
                ->add(
                    'events',
                    CollectionType::class,
                    [
                        'required' => false,
                        'by_reference' => false,
                        'prototype' => true,
                        "allow_delete" => true,
                        'entry_type' => CalendarEventType::class,
                        'allow_add' => true,
                    ],
                    [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                    ]
                );
        } else {
            $formMapper
                ->add(
                    'name',
                    TextType::class
                )
                ->add('status')
                ->add(
                    'type',
                    ChoiceType::class,
                    [
                        'choices' => Calendar::getTypes(),
                        'disabled' => true
                    ]
                );
            if ($this->getSubject()->getType() === Calendar::CUSTOM_CALENDAR) {
                $formMapper->
                add(
                    'events',
                    CollectionType::class,
                    [
                        'entry_type' => CalendarEventType::class,
                        'allow_add' => true,
                        "allow_delete" => true,
                        'by_reference' => false,
                    ]
                );
            } else {
                $formMapper->
                add(
                    'events',
                    CollectionType::class,
                    [
                        'entry_type' => CalendarEventType::class,
                        'by_reference' => false,
                    ]
                );
            }
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('name')
            ->add('status')
            ->add('type')
            ->add('data')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
