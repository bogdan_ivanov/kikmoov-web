<?php

namespace App\Admin;

use App\Provider\CategoryProvider;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;

final class CategoryAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('name')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('name')
			->add('slug')
            ->add('active')
			->add('description')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add('name')
			->add('active')
        ;

        if (!is_null($this->getSubject()->getId())) {
            $formMapper->add('slug');
        }

        $formMapper
			->add('description')
            ->add(
                'icon',
                ModelListType::class,
                [
                    'required' => false,
                    'btn_list' => null,
                    'btn_edit' => null,
                ],
                ['link_parameters' => ['provider' => 'sonata.media.provider.image', 'context' => 'category']]
            )
        ;

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('name')
            ->add('active')
			->add('slug')
			->add('description')
            ->add('icon');
    }
}
