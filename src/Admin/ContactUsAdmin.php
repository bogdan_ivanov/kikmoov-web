<?php

namespace App\Admin;

use App\Entity\ContactUs;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class ContactUsAdmin
 * @package App\Admin
 */
final class ContactUsAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
        $collection->remove('create');

        if ($this->hasParentFieldDescription()) {
            $collection
                ->remove('create')
                ->remove('add')
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'status',
                'doctrine_orm_string',
                ['label' => 'Status'],
                ChoiceType::class,
                [
                    'choices' => ContactUs::statusesList()
                ]
            )
            ->add('email')
            ->add(
                'createdAt',
                'doctrine_orm_date_range',
                [
                    'field_type' => DateRangePickerType::class
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->addIdentifier('id')
			->add('name')
			->add('email')
			->add('status')
            ->add('createdAt')
            ->add('updatedAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add(
			    'status',
                ChoiceType::class,
                [
                    'choices' => ContactUs::statusesList()
                ]
            )
			->add('name')
			->add('email')
			->add('message')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('name')
			->add('email')
			->add('message')
            ->add('status')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
