<?php

namespace App\Admin;

use App\Entity\Facility;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;

/**
 * Class FacilityAdmin
 * @package App\Admin
 */
final class FacilityAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('name')
			->add('slug')
			->add('isActive')
			;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('name')
			->add('slug')
			->add('isActive')
			->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name');

        if (!is_null($this->getSubject()->getId())) {
            $formMapper->add('slug');
        }

        $formMapper
			->add('isActive')
            ->add(
                'webIcon',
                ModelListType::class,
                [
                    'required' => false,
                    'btn_list' => null,
                    'btn_edit' => null
                ],
                ['link_parameters' => ['provider' => 'admin.provider.facility', 'context' => 'facility']]
            )
            ->add(
                'appIcon',
                ModelListType::class,
                [
                    'required' => false,
                    'btn_list' => null,
                    'btn_edit' => null,
                    'help' => 'Only image/png mime type'
                ],
                ['link_parameters' => ['provider' => 'sonata.media.provider.image', 'context' => 'app_facility']]
            )
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
