<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class InternalNotificationAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
        $collection->remove('create');
        $collection->remove('edit');

        if ($this->hasParentFieldDescription()) {
            $collection
                ->remove('create')
                ->remove('add')
                ->remove('edit')
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('id')
			->add('message')
			->add('status')
			->add('type')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('user')
			->add('message')
			->add('status')
			->add('type')
			->add('createdAt')
			->add('updatedAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
            ->add('user')
			->add('message')
			->add('status')
			->add('type')
			->add('createdAt')
			->add('updatedAt')
			;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
