<?php

namespace App\Admin;

use App\Entity\LeaseholdEnquiry;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class LeaseholdEnquiryAdmin
 * @package App\Admin
 */
final class LeaseholdEnquiryAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
        $collection->remove('create');

        if ($this->hasParentFieldDescription()) {
            $collection
                ->remove('create')
                ->remove('add')
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('numberOfPeople')
			->add('fullName')
			->add('companyName')
			->add('email')
			->add('phone')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('area', "string", ["template" => "admin/enquiry/area_types_row.html.twig"])
			->add('numberOfPeople')
			->add('fullName')
			->add('companyName')
			->add('email')
			->add('phone')
			->add('createdAt')
			->add('updatedAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add(
			    'area',
                ChoiceType::class,
                [
                    'choices' => [
                        LeaseholdEnquiry::EAST => LeaseholdEnquiry::EAST,
                        LeaseholdEnquiry::NORTH => LeaseholdEnquiry::NORTH,
                        LeaseholdEnquiry::WEST => LeaseholdEnquiry::WEST,
                        LeaseholdEnquiry::SOUTH => LeaseholdEnquiry::SOUTH
                    ],
                    'multiple' => true,
                    'required' => false
                ]
            )
			->add('numberOfPeople')
			->add('fullName')
			->add('companyName')
			->add('email')
			->add('phone')
			;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('numberOfPeople')
			->add('fullName')
			->add('companyName')
			->add('email')
			->add('phone')
			->add('createdAt')
			->add('updatedAt')
			;
    }
}
