<?php

namespace App\Admin;

use App\Entity\Location;
use App\Notifications\NotificationsEvent;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class LocationAdmin
 * @package App\Admin
 */
final class LocationAdmin extends AbstractAdmin
{
    use AdminTrait;

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('show');

        if ($this->hasParentFieldDescription()) {
            $collection
                ->remove('show')
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('name')
			->add('address')
			->add('optionalAddress')
			->add('town')
			->add('postcode')
            ->add('area')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
            ->add('user')
			->add('name')
            ->add('company')
			->add('address')
			->add('town')
            ->add('area')
            ->add('nearby', 'string', ['template' => 'admin/location/nearby_row.html.twig'])
			->add('postcode')
			->add('workSpaceTypes', "string", ["template" => "admin/location/workspace_types_row.html.twig"])
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'name',
                null,
                [
                    'attr' => [
                        'maxlength' => 22
                    ]
                ]
            )
            ->add('address')
            ->add('optionalAddress')
            ->add('company')
            ->add('area', null, ['required' => true])
            ->add('user', null, ['label' => 'Seller', 'required' => true])
            ->add('latitude')
            ->add('longitude')
            ->add('town')
            ->add('postcode')
            ->add('description')
			->add('videoLink')
            ->add('isVisited')
			;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * @param $location
     */
    public function prePersist($location)
    {
        $this->checkVisitedWorkspaces($location);
    }

    /**
     * @param $location
     */
    public function preUpdate($location)
    {
        $this->checkVisitedWorkspaces($location);
    }

    /**
     * @param Location $location
     */
    public function checkVisitedWorkspaces(Location $location)
    {
        $original = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($location);

        if (isset($original['isVisited']) && $original['isVisited'] !== $location->getisVisited()) {
            $this->setIsVisitedWorkspaces($location);
        }

        if ( !empty($location->getVideoLink()) && isset($original['videoLink']) && $original['videoLink'] !== $location->getVideoLink()) {

            $this->setIsVisitedWorkspaces($location);

            $location->setIsVisited(true);
        }
    }

    /**
     * @param Location $location
     */
    public function setIsVisitedWorkspaces(Location $location)
    {
        $workspaces = $location->getWorkspaces();

        if (!empty($workspaces)) {
            foreach ($workspaces as $workSpace) {
                $workSpace->setVisited($location->getisVisited());
            }
        }
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'admin/location/edit.html.twig';
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
