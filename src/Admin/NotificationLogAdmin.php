<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class NotificationLogAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('add');
        $collection->remove('create');
        $collection->remove('edit');

        if ($this->hasParentFieldDescription()) {
            $collection
                ->remove('create')
                ->remove('add')
                ->remove('edit')
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('id')
			->add('status')
			->add('event')
			->add('toEmail')
			->add('subject')
			->add('log')
			->add('createdAt')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('status', "string", ["template" => "admin/notificationLog/status_list_field.html.twig"])
			->add('event')
			->add('toEmail')
			->add('subject')
			->add('log')
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('status')
			->add('event')
			->add('toEmail')
			->add('subject')
			->add('body', "string", ["template" => "admin/notificationLog/body_row.html.twig"])
			->add('log')
			->add('createdAt')
			->add('updatedAt')
			;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
