<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;

final class TestimonialsAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('fullName')
			->add('companyName')
			;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('fullName')
			->add('position')
			->add('companyName')
			->add('published')
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add('fullName')
			->add('position')
			->add('companyName')
			->add('feedback')
			->add('published')
			->add(
			    'photo',
                ModelListType::class,
                [
                    'required' => false,
                    'btn_list' => null,
                    'btn_edit' => null,
                ],
                ['link_parameters' => ['provider' => 'sonata.media.provider.image', 'context' => 'testimonials']]
            )
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('fullName')
			->add('position')
			->add('companyName')
			->add('feedback')
			->add('published')
			->add('createdAt')
			->add('updatedAt')
			;
    }
}
