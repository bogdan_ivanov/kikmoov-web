<?php

namespace App\Admin\Type;

use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Class CalendarEventType
 * @package App\Admin\Type
 */
class CalendarEventType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CalendarEvent::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'summary',
                TextType::class,
                [
                    'label' => 'Summary',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ])
            ->add(
                'start',
                DateTimePickerType::class,
                [
                    'label' => 'Start Time',
                    'required' => false
                ]
            )
            ->add(
                'end',
                DateTimePickerType::class,
                [
                    'label' => 'End time',
                    'required' => false
                ]
            )
        ;
    }
}