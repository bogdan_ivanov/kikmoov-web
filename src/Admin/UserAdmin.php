<?php

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormTypeInterface;

/**
 * Class UserAdmin
 * @package App\Admin
 */
abstract class UserAdmin extends BaseUserAdmin
{
    use AdminTrait;

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        // define group zoning
        $formMapper
            ->tab('User')
                ->with('Profile', ['class' => 'col-md-6'])->end()
                ->with('Invoicing Contact Details', ['class' => 'col-md-6'])->end()
//                ->with('Social', ['class' => 'col-md-6'])->end()
            ->end()
            ->tab('Security')
                ->with('Status', ['class' => 'col-md-6'])->end()
                ->with('Keys', ['class' => 'col-md-6'])->end()
                ->with('Roles', ['class' => 'col-md-12'])->end()
            ->end()
        ;

        $genderOptions = [
            'choices' => \call_user_func([$this->getUserManager()->getClass(), 'getGenderList']),
            'required' => true,
            'translation_domain' => $this->getTranslationDomain(),
        ];

        // NEXT_MAJOR: Remove this when dropping support for SF 2.8
        if (method_exists(FormTypeInterface::class, 'setDefaultOptions')) {
            $genderOptions['choices_as_values'] = true;
        }

        $formMapper
            ->tab('User')
                ->with('Profile')
                    ->add('email')
                    ->add('plainPassword', TextType::class, [
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    ])
                    ->add('firstname', null, ['required' => false])
                    ->add('lastname', null, ['required' => false])
                    ->add('phone', null, ['required' => false])
                ->end()
                ->with('Invoicing Contact Details')
                    ->add('companyName', null, ['required' => false])
                    ->add('address', null, ['required' => false])
                    ->add('optionalAddress', null, ['required' => false])
                    ->add('town', null, ['required' => false])
                    ->add('postcode', null, ['required' => false])
                    ->add('invoiceEmail', null, ['required' => false])
                ->end()
                /*->with('Social')
                    ->add('facebookUid', null, ['required' => false])
                    ->add('facebookName', null, ['required' => false])
                    ->add('gplusUid', null, ['required' => false])
                    ->add('gplusName', null, ['required' => false])
                    ->add('linkedInUid', null, ['required' => false])
                    ->add('linkedInData', null, ['required' => false])
                ->end()*/
            ->end()
            ->tab('Security')
                ->with('Status')
                    ->add('enabled', null, ['required' => false])
                    ->add(
                        'status',
                        ChoiceType::class,
                        [
                            'choices' => User::getStatusesList()
                        ]
                    )
                ->end()
                ->with('Roles')
                    ->add('realRoles', SecurityRolesType::class, [
                        'label' => 'Roles',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => false,
                    ])
                ->end()
                ->with('Keys')
                    ->add('token', null, ['required' => false])
                    ->add('socialPassword', null, ['required' => false])
                ->end()
            ->end()
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
