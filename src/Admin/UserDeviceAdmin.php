<?php

namespace App\Admin;

use App\Entity\UserDevice;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class UserDeviceAdmin
 * @package App\Admin
 */
class UserDeviceAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('deviceName')
            ->add('deviceOS')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('deviceName')
            ->add('deviceModel')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'users',
                ModelType::class, [
                    'label'         => 'Users',
                    'required'      => true,
                    'multiple'      => true,
                    'by_reference'  => false,
                ]
            )
            ->add('deviceName')
            ->add('deviceToken')
            ->add('deviceModel')
            ->add('deviceOS')
            ->add('deviceIdentifier')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('deviceName')
            ->add('deviceToken')
            ->add('deviceOS')
            ->add('deviceModel')
            ->add('deviceIdentifier')
        ;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }

}