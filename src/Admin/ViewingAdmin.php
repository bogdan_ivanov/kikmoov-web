<?php

namespace App\Admin;

use App\Entity\BuyerInternalNotification;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\Viewing;
use App\InternalNotification\InternalNotificationType;
use App\InternalNotification\PushNotificationType;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationsEvent;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class ViewingAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'startTime',
                'doctrine_orm_date',
                [
                    'field_type' => DatePickerType::class
                ]
            )
            ->add(
                'location',
                'doctrine_orm_string',
                ['label' => 'Locations'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getLocations()
                ]
            )
            ->add(
                'seller',
                'doctrine_orm_string',
                ['label' => 'Seller'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getSellers()
                ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
            ->add(
                'workspace.location',
                'string',
                [
                    'label' => 'Location',
                    'template' => 'admin/viewing/location_link_row.html.twig'
                ]
            )
            ->add('workspace', 'string', ['template' => 'admin/viewing/workspace_link_row.html.twig'])
			->add('startTime')
			->add('endTime')
			->add('phone')
			->add('email')
			->add('status')
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add('startTime', DateTimePickerType::class)
			->add('endTime', DateTimePickerType::class, ['required' => false])
			->add('phone')
			->add('email')
			->add(
			    'status',
                ChoiceType::class,
                [
                    'choices' => Viewing::adminStatuses()
                ]
            )
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
			->add('id')
			->add('startTime')
			->add('endTime')
			->add('phone')
			->add('email')
			->add('status')
			->add('createdAt')
			->add('updatedAt')
			;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * @param $viewing
     * @throws \Exception
     */
    public function prePersist($viewing)
    {
        $this->checkForStatusChanges($viewing);
    }

    /**
     * @param $viewing
     * @throws \Exception
     */
    public function preUpdate($viewing)
    {
        $this->checkForStatusChanges($viewing);
    }

    /**
     * @param Viewing $viewing
     * @throws \Exception
     */
    private function checkForStatusChanges(Viewing $viewing) : void
    {
        $original = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($viewing);

        if (isset($original['status']) && $original['status'] !== $viewing->getStatus()) {
            $this->changeInternalNotificationStatus($viewing, $viewing->getStatus());
            $this->sendNotification($viewing, $viewing->getStatus());
            return;
        }
    }

    /**
     * @param Viewing $viewing
     * @param string $status
     */
    protected function changeInternalNotificationStatus(Viewing $viewing, string $status) : void
    {
        if (in_array($status, Viewing::mapStatusesInternalNotification())) {
            $params = [
                'time' => $viewing->getStartTime()->format('h:ia'),
                'date' => $viewing->getStartTime()->format('l, jS F'),
                'workspace_info' => $viewing->getWorkSpace()->getWorkspaceInfo(),
                'location_name' => $viewing->getWorkSpace()->getLocation()->getName()
            ];

            $event = array_search($status, Viewing::mapStatusesInternalNotification());

            ['message' => $message] = $this->getInternalNotificationType()->findEvent($event, $params);
            $viewing->getInternalNotification()->setStatus($event);
            $viewing->getInternalNotification()->setMessage($message);
        }
    }

    /**
     * @param Viewing $viewing
     * @param string $status
     * @throws \Exception
     */
    protected function sendNotification(Viewing $viewing, string $status) : void
    {
        $workSpace = $viewing->getWorkSpace();

        if (is_null($workSpace)) {
            return;
        }

        $buyer = $viewing->getUser();

        if (is_null($buyer)) {
            return;
        }

        $location = $workSpace->getLocation();

        if (is_null($location)) {
            return;
        }

        $seller = $location->getUser();

        if (is_null($seller)) {
            return;
        }

        if ($status === Viewing::ACCEPTED) {
            $pushParams = [
                'startTime' => $viewing->getStartTime()->format('H:i'),
                'startDate' => $viewing->getStartTime()->format('d.m.Y'),
                'address' => $workSpace->getLocation()->getAddress(),
                'user' => $viewing->getUser()->getFullname()
            ];

            $this->getBuyerInternalNotificationModelManager()->create(
                $viewing->getUser(),
                $pushParams,
                PushNotificationType::VIEWING_ACCEPTED,
                $viewing->getId(),
                BuyerInternalNotification::VIEWING
            );

            $this->getNotificationService()->sendEmail(
                $viewing->getUser(),
                NotificationsEvent::VIEWING_APPROVED,
                [
                    'location' => $location,
                    'workspace' => $workSpace,
                    'seller' => $seller,
                    'buyer' => $buyer,
                    'viewing' => $viewing,
                    'link' => $this->getRouter()->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workSpace->getId())),
                    'downloadFileLink' => $this->getRouter()->generateAdminLink('download_viewing_icalendar_file', ['id' => $viewing->getId()])
                ],
                $location->getUser()
            );
        } elseif ($status === Viewing::DECLINED) {
            $this->getBuyerInternalNotificationModelManager()->delete(
                $viewing->getUser(),
                $viewing->getId(),
                BuyerInternalNotification::VIEWING
            );
        }
    }

    /**
     * @return array
     */
    protected function getLocations()
    {
        $list = [];

        $locations = $this->getEntityManager()->getRepository(Location::class)->findAll();

        if (!empty($locations)) {
            /** @var Location $location */
            foreach ($locations as $location) {
                $list[$location->getName()] = $location->getId();
            }
        }

        return $list;
    }

    /**
     * @return array
     */
    protected function getSellers()
    {
        $list = [];

        $users = $this->getEntityManager()->getRepository(User::class)->findAll();

        if (!empty($users)) {
            /** @var User $user */
            foreach ($users as $user) {
                if (!$user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                $list[$user->getFullname() . ' ( ' . $user->getCompanyName() . ' ) '] = $user->getId();
            }
        }

        return $list;
    }

    /**
     * @param string $context
     * @return QueryBuilder
     * @throws \Exception
     */
    public function createQuery($context = 'list')
    {
        $filters = $this->getRequest()->get('filter');
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($context === 'list') {
            //collect all params from filter request
            $params = [];

            if (isset($filters['location']['value']) && $filters['location']['value']) {
                $query
                    ->innerJoin('o.workSpace', 'w')
                    ->innerJoin('w.location', 'l')
                    ->andWhere('l.id = :locationId')
                ;

                $params['locationId'] = $filters['location']['value'];
            }

            if (isset($filters['seller']['value']) && $filters['seller']['value']) {
                $query
                    ->innerJoin('o.workSpace', 'w')
                    ->innerJoin('w.location', 'l')
                    ->innerJoin('l.user', 'u')
                    ->andWhere('u.id = :sellerId')
                ;

                $params['sellerId'] = $filters['seller']['value'];
            }

            if (!empty($params)) {
                $query->setParameters($params);
            }
        }
        return $query;
    }

    /**
     * @return mixed
     */
    protected function getInternalNotificationType()
    {
        return $this->getContainer()->get(InternalNotificationType::class);
    }
}
