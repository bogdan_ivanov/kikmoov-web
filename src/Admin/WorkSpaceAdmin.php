<?php

namespace App\Admin;

use App\Entity\Calendar;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\Model\FacilityModelManager;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationsEvent;
use App\Provider\WorkspaceMediaProvider;
use App\Traits\YieldTrait;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

final class WorkSpaceAdmin extends AbstractAdmin
{
    use AdminTrait;
    use YieldTrait;

    /**
     * @return WorkspaceMediaProvider
     */
    protected function getWorkspaceMediaProvider() : WorkspaceMediaProvider
    {
        return $this->getConfigurationPool()->getContainer()->get(WorkspaceMediaProvider::class);
    }

    /**
     * @return FacilityModelManager
     */
    protected function getFacilitiesModelManager() : FacilityModelManager
    {
        return $this->getConfigurationPool()->getContainer()->get(FacilityModelManager::class);
    }

    /**
     * @param ShowMapper $showMapper
     * @throws \Exception
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /** @var WorkSpace $workspace */
        $workspace = $this->getSubject();

        $images = $this->getWorkspaceMediaProvider()->getPublicLinks($workspace, true);

        $facilities = $this->getFacilitiesModelManager()->getListOfFacilities($workspace);

        $location = $workspace->getLocation();

        $showMapper
            ->add('id')
            ->add(
                'location',
                'string',
                [
                    'template' => 'admin/workspace/show/location_link_row.html.twig'
                ]
            )
            ->add(
                'nearBy',
                'string',
                [
                    'template' => 'admin/workspace/show/nearbystation.html.twig',
                    'mapped' => false,
                    'data' => $location->getNearby()
                ]
            )
            ->add(
                'locationInfo',
                'string',
                [
                    'template' => 'admin/workspace/show/location_info_row.html.twig',
                    'mapped' => false,
                    'data' => [
                        'name' => $location->getName(),
                        'address' => $location->getAddress(),
                        'latitude' => $location->getLatitude(),
                        'longitude' => $location->getLongitude(),
                        'description' => $location->getDescription()
                    ]
                ]
            )
            ->add('type')
            ->add(
                'deskType',
                'string',
                [
                    'template' =>  'admin/workspace/show/desktype.html.twig'
                ]
            )
            ->add('description')
            ->add('status')
            ->add('price')
            ->add('minContractLength')
            ->add('size')
            ->add('quantity')
            ->add('capacity')
            ->add('availableFrom')
            ->add('opensFrom')
            ->add('closesAt')
            ->add('visited')
            ->add(
                'duration',
                'string',
                [
                    'template' => 'admin/workspace/show/duration_row.html.twig',
                    'mapped' => false
                ]
            )
            ->add(
                'coverImage',
                'string',
                [
                    'template' => 'admin/workspace/show/coverImage.html.twig',
                    'mapped' => false
                ]
            )
            ->add('createdAt')
            ->add(
                'images',
                'string',
                [
                    'template' => 'admin/workspace/show/images.html.twig',
                    'mapped' => false,
                    'data' => $images
                ]
            )
            ->add(
                'facilities',
                'string',
                [
                    'template' => 'admin/workspace/show/facilities.html.twig',
                    'mapped' => false,
                    'data' => $facilities
                ]
            )
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add(
			    'status',
                'doctrine_orm_string',
                ['label' => 'Status'],
                ChoiceType::class,
                [
                    'choices' => WorkSpace::getListOfStatuses()
                ]
            )
			->add('price')
            ->add(
                'location',
                'doctrine_orm_string',
                ['label' => 'Locations'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getLocations()
                ]
            )
            ->add(
                'seller',
                'doctrine_orm_string',
                ['label' => 'Seller'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => $this->getSellers()
                ]
            )
            ->add(
                'duration',
                'doctrine_orm_string',
                ['label' => 'Duration'],
                ChoiceType::class,
                [
                    'mapped' => false,
                    'choices' => [
                        'Hourly'  => 'hourly',
                        'Monthly' => 'monthly',
                    ]
                ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('location', 'string', ['template' => 'admin/workspace/location_link_row.html.twig'])
			->add('type')
			->add('status')
			->add('price')
			->add('size')
            ->add('quantity')
            ->add('capacity')
			->add('availableFrom')
			->add('duration', 'string', ['template' => 'admin/workspace/duration_row.html.twig', 'mapped' => false])
			->add('createdAt')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Main')
                ->with('Main')
                    ->add('location', null, ['required' => true])
                    ->add(
                        'type',
                        ChoiceType::class,
                        [
                            'choices' => [
                                'Desk' => WorkSpace::DESK,
                                'Meeting Room' => WorkSpace::MEETING_ROOM,
                                'Private Office' => WorkSpace::PRIVATE_OFFICE
                            ]
                        ]
                    )
                    ->add(
                        'status',
                        ChoiceType::class,
                        [
                            'choices' => WorkSpace::getListOfStatuses()
                        ]
                    )
                    ->add(
                        'deskType',
                        ChoiceType::class,
                        [
                            'choices' => [
                                'Hourly Hot Desk' => WorkSpace::HOURLY_HOT_DESK,
                                'Monthly Hot Desk' => WorkSpace::MONTHLY_HOT_DESK,
                                'Monthly Fixed Desk' => WorkSpace::MONTHLY_FIXED_DESK,
                                'Daily Desk' => WorkSpace::DAILY_HOT_DESK,
                                'Hourly Meeting Room' => WorkSpace::HOURLY_MEETING_ROOM,
                                'Daily Meeting Room' => WorkSpace::DAILY_MEETING_ROOM,
                            ],
                            'required' => false,
                            'help' => 'Required if workspace type is Desk'
                        ]
                    )
                    ->add(
                        'quantity',
                        IntegerType::class,
                        [
                            'attr' => [
                                'min' => 1
                            ]
                        ]
                    )
                    ->add(
                        'price',
                        NumberType::class,
                        [
                            'scale' => 4,
                            'attr' => [
                                'min' => 1
                            ]
                        ]
                    )
                    ->add(
                        'size',
                        IntegerType::class,
                        [
                            'required' => false,
                            'attr' => [
                                'min' => 1
                            ]
                        ]
                    )
                    ->add(
                        'capacity',
                        IntegerType::class,
                        [
                            'required' => false,
                            'attr' => [
                                'min' => 1
                            ]
                        ]
                    )
                    ->add('description')
                    ->add('minContractLength')
                    ->add('coverImageUrl')
                    ->add(
                        'availableFrom',
                        DateTimePickerType::class,
                        [
                            'required' => false
                        ]
                    )
                    ->add('visited',
                        null,
                        [
                            'required' => false
                        ]
                    )
                    ->add('topPick',
                        null,
                        [
                            'required' => false
                        ]
                    )
                    ->add(
                        'opensFrom',
                        TextType::class,
                        ['required' => false, 'help' => 'Format H:i, eg 08:30']
                    )
                    ->add(
                        'closesAt',
                        TextType::class,
                        ['required' => false, 'help' => 'Format H:i, eg 18:00']
                    )
                    ->add(
                        'facilities',
                        ModelType::class,
                        [
                            'required' => false,
                            'multiple' => true,
                            'btn_add' => false
                        ]
                    )
                    ->add('notes', TextareaType::class, ['required' => false])
                    ->add(
                        'googleCalendarId',
                        null,
                        [
                            'label' => 'Google Calendar Id',
                            'required' => false
                        ]
                    )
                    ->add(
                        'outlookCalendarId',
                        null,
                        [
                            'label' => 'Outlook Calendar Id',
                            'required' => false
                        ]
                    )
                    ->add(
                        'outlookCalendarName',
                        null,
                        [
                            'label' => 'Outlook Calendar Name',
                            'required' => false
                        ]
                    )
                ->end()
            ->end()
            ->tab('Calendars')
                ->with('Calendars')
                    ->add(
                        'calendars',
                        CollectionType::class, [
                            'label' => 'Calendars',
                            'by_reference' => false,

                        ],
                        [
                            'edit' => 'inline',
                            'inline' => 'table',
                        ]
                    )
                ->end()
            ->end()
        ;
    }

    /**
     * @param WorkSpace $workspace
     * @throws \Doctrine\ORM\ORMException
     */
    public function prePersist($workspace)
    {
        $this->checkDeskType($workspace);
        $this->handleFacilities($workspace);
        $this->checkForStatusChanges($workspace);
        $this->processCalendars($workspace);
    }

    /**
     * @param WorkSpace $workspace
     * @throws \Doctrine\ORM\ORMException
     */
    public function preUpdate($workspace)
    {
        $this->checkDeskType($workspace);
        $this->handleFacilities($workspace);
        $this->checkForStatusChanges($workspace);
        $this->processCalendars($workspace);
    }

    /**
     * @param WorkSpace $workspace
     */
    public function checkDeskType($workspace)
    {
        if ($workspace->getType() === WorkSpace::MEETING_ROOM && (
            empty($workspace->getDeskType()) || !in_array($workspace->getDeskType(), WorkSpace::getMeetingRoomTypes()))
        ) {
            $workspace->setDeskType(WorkSpace::HOURLY_MEETING_ROOM);
        }

        if ($workspace->getType() === WorkSpace::DESK && (
            empty($workspace->getDeskType()) || !in_array($workspace->getDeskType(), WorkSpace::getDeskTypes())))
        {
            $workspace->setDeskType(WorkSpace::HOURLY_HOT_DESK);
        }

        if ($workspace->getType() === WorkSpace::PRIVATE_OFFICE) {
            $workspace->setDeskType(null);
        }
    }

    /**
     * @param WorkSpace $workspace
     * @throws \Doctrine\ORM\ORMException
     */
    private function handleFacilities(WorkSpace $workspace)
    {
        $facilities = clone $workspace->getFacilities();
        $workspace->getFacilities()->clear();

        foreach ($facilities as $facility) {
            $workspace->addFacility($facility);
            $this->getEntityManager()->persist($workspace);
        }
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * @param WorkSpace $workSpace
     */
    private function checkForStatusChanges(WorkSpace $workSpace) : void
    {
        $original = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($workSpace);

        if (isset($original['status']) && $original['status'] !== $workSpace->getStatus()) {
            $this->sendNotification($workSpace, $workSpace->getStatus());
            return;
        }
    }

    /**
     * @param WorkSpace $workSpace
     */
    private function processCalendars(WorkSpace $workSpace)
    {
        if (!empty($workSpace->getCalendars())) {
            foreach ($workSpace->getCalendars() as $calendar) {
                $calendar->setWorkspace($workSpace);
                if (is_null($calendar->getId()))
                {
                    $calendar->setType(Calendar::CUSTOM_CALENDAR);
                }
            }
        }
    }

    /**
     * @param WorkSpace $workSpace
     * @param string $status
     */
    protected function sendNotification(WorkSpace $workSpace, string $status) : void
    {
        if (is_null($workSpace->getLocation())) {
            return;
        }

        $location = $workSpace->getLocation();

        $user = $location->getUser();
        if (is_null($user)) {
            return;
        }

        if ($status === WorkSpace::ACTIVE) {
            $this->getNotificationService()->sendEmail(
                $location->getUser(),
                NotificationsEvent::WORKSPACE_APPROVED,
                [
                    'location' => $location,
                    'workspace' => $workSpace,
                    'seller' => $user,
                    'linkPortal' => $this->getRouter()->generateFrontLink(FrontUrls::MAINPORTAL)
                ]
            );

            if ($user->getStatus() !== User::HAS_SUCCESSFULLY_UPLOADED) {
                $user->setStatus(User::HAS_SUCCESSFULLY_UPLOADED);
                try {
                    $this->getEntityManager()->flush($user);
                } catch (\Exception $exception) {}
            }
        } elseif ($status === WorkSpace::UNAVAILABLE) {
            $this->getNotificationService()->sendEmail(
                $location->getUser(),
                NotificationsEvent::WORKSPACE_UNAVAILABLE,
                [
                    'seller' => $location->getUser(),
                    'location' => $location,
                    'workspace' => $workSpace,
                    'adminLink' => $this->getRouter()->generateAdminLink('admin_app_workspace_edit', ['id' => $workSpace->getId()]),
                    'linkPortal' => $this->getRouter()->generateFrontLink(FrontUrls::MAINPORTAL)
                ]
            );
        }
    }

    /**
     * @return array
     */
    protected function getLocations()
    {
        $list = [];

        $locations = $this->getEntityManager()->getRepository(Location::class)->findAll();

        if (!empty($locations)) {
            /** @var Location $location */
            foreach ($locations as $location) {
                $list[$location->getName() . ' (id: ' . $location->getId() . ' )'] = $location->getId();
            }
        }

        return $list;
    }

    /**
     * @return array
     */
    protected function getSellers()
    {
        $list = [];

        $users = $this->getEntityManager()->getRepository(User::class)->findAll();

        if (!empty($users)) {
            /** @var User $user */
            foreach ($users as $user) {
                if (!$user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                $list[$user->getFullname() . ' ( ' . $user->getCompanyName() . ' ) '] = $user->getId();
            }
        }

        return $list;
    }

    /**
     * @param string $context
     * @return QueryBuilder
     * @throws \Exception
     */
    public function createQuery($context = 'list')
    {
        $filters = $this->getRequest()->get('filter');
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($context === 'list') {
            //collect all params from filter request
            $params = [];

            if (isset($filters['location']['value']) && $filters['location']['value']) {
                $query
                    ->innerJoin('o.location', 'l')
                    ->andWhere('l.id = :locationId')
                ;

                $params['locationId'] = $filters['location']['value'];
            }

            if (isset($filters['seller']['value']) && $filters['seller']['value']) {
                $query
                    ->innerJoin('o.location', 'l')
                    ->innerJoin('l.user', 'u')
                    ->andWhere('u.id = :sellerId')
                ;

                $params['sellerId'] = $filters['seller']['value'];
            }

            if (isset($filters['duration']['value']) && $filters['duration']['value']) {
                if ($filters['duration']['value'] === 'hourly') {
                    $query->andWhere('((o.type = :type AND o.deskType = :hourlyDeskType) OR o.type = :meeting)');
                    $params['type'] = WorkSpace::DESK;
                    $params['hourlyDeskType'] = WorkSpace::HOURLY_HOT_DESK;
                    $params['meeting'] = WorkSpace::MEETING_ROOM;
                } else {
                    $query->andWhere('((o.type = :type AND (o.deskType = :hotDeskType OR o.deskType = :fixedDeskType)) OR o.type = :office)');
                    $params['type'] = WorkSpace::DESK;
                    $params['hotDeskType'] = WorkSpace::MONTHLY_HOT_DESK;
                    $params['fixedDeskType'] = WorkSpace::MONTHLY_FIXED_DESK;
                    $params['office'] = WorkSpace::PRIVATE_OFFICE;
                }
            }

            if (!empty($params)) {
                $query->setParameters($params);
            }
        }
        return $query;
    }

    /**
     * @return array
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        $actions['approve_workspaces'] = [
            'label'            => 'Approve',
        ];

        return $actions;
    }
}
