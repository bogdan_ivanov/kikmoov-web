<?php

namespace App\Calendar;

/**
 * Interface CalendarInterface
 * @package App\Calendar
 */
interface CalendarInterface
{

    /**
     * @return mixed
     */
    public function processAuthorization();

    /**
     * @return mixed
     */
    public function getCalendarList();

    /**
     * @param string $calendarId
     * @return mixed
     */
    public function getEventListByWorkspace(string $calendarId);

    /**
     * @param string $calendarId
     * @param int $startTime
     * @param int $endTime
     * @param string $title
     * @param string $summary
     * @return mixed
     */
    public function addEvent(string $calendarId, int $startTime, int $endTime, string $title, string $summary);

}