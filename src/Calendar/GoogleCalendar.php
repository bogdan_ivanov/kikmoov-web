<?php

namespace App\Calendar;

use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use App\Validators\Constraints\Time;

class GoogleCalendar implements CalendarInterface
{
    /**
     * @var string
     */
    private $authorizeFile;

    /**
     * GoogleCalendar constructor.
     * @param string $authorizeFile
     */
    public function __construct
    (
        string $authorizeFile
    )
    {
        $this->authorizeFile = $authorizeFile;
    }

    /**
     * @return Google_Client
     */
    public function processAuthorization()
    {
        $client = new Google_Client();

        // by https://developers.google.com/api-client-library/php/auth/service-accounts#authorizingrequests
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $this->authorizeFile);

        $client->useApplicationDefaultCredentials();
        $client->setApplicationName("test_calendar");
        $client->setScopes([Google_Service_Calendar::CALENDAR]);
        $client->setAccessType('offline');

        return $client;
    }

    /**
     * @return \Google_Service_Calendar_CalendarList|mixed
     */
    public function getCalendarList()
    {
        $client = $this->processAuthorization();

        $service = new Google_Service_Calendar($client);

        $calendarList = $service->calendarList->listCalendarList();

        return $calendarList;
    }

    /**
     * @param string $calendarId
     * @return \Google_Service_Calendar_Events|mixed
     */
    public function getEventListByWorkspace($calendarId)
    {
        $client = $this->processAuthorization();

        $service = new Google_Service_Calendar($client);

        $eventsList = $service->events->listEvents($calendarId);

        return $eventsList;
    }

    /**
     * @param string $calendarId
     * @param int $startTime
     * @param int $endTime
     * @param string $title
     * @param string $summary
     * @return mixed|void
     */
    public function addEvent($calendarId, int $startTime, int $endTime, string $title, string $summary)
    {
        $client = $this->processAuthorization();

        $service = new Google_Service_Calendar($client);

        $event = new Google_Service_Calendar_Event([
            'summary' => $title,
            'description' => $summary,
            'start' => array(
                'dateTime' => (new \DateTime())->setTimestamp($startTime)->format( \DateTimeInterface::ISO8601)
            ),
            'end' => array(
                'dateTime' => (new \DateTime())->setTimestamp($endTime)->format( \DateTimeInterface::ISO8601)
            )
        ]);

        return $service->events->insert($calendarId, $event);
    }
}