<?php

namespace App\Calendar;

use Microsoft\Graph\Graph;

/**
 * Class OutlookCalendar
 * @package App\Calendar
 */
class OutlookCalendar implements CalendarInterface
{

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $tenantId;

    /**
     * @var string
     */
    private $accountEmail;

    /**
     * @var
     */
    private $link;

    /**
     * OutlookCalendar constructor.
     * @param string $clientId
     * @param string $clientSecret
     * @param string $tenantId
     * @param string $accountEmail
     */
    public function __construct
    (
        string $clientId,
        string $clientSecret,
        string $tenantId,
        string $accountEmail
    )
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->tenantId = $tenantId;
        $this->accountEmail = $accountEmail;
    }

    /**
     * @return mixed
     */
    public function processAuthorization()
    {
        $guzzle = new \GuzzleHttp\Client();

        $url = 'https://login.microsoftonline.com/' . $this->tenantId . '/oauth2/v2.0/token';
        $client = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'scope' => 'https://graph.microsoft.com/.default',
                'grant_type' => 'client_credentials',
            ],
        ])->getBody()->getContents());

        return $client->access_token;

    }

    /**
     * @return mixed
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getCalendarList()
    {
        $accessToken = $this->processAuthorization();

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $calendarsRequest = $graph
            ->createRequest("GET", "/users/" . $this->accountEmail . "/calendars/")
            ->execute();
        $calendars = $calendarsRequest->getBody();
        $calendarsList = $calendars['value'];
        if (isset($calendars['@odata.nextLink'])) {
            $this->link = $calendars['@odata.nextLink'];
        }

        while ($this->link) {
            $newCalendars = $graph->createRequest('GET', $this->link)->execute();
            $calendarsList = array_merge($calendarsList, $newCalendars->getBody()['value']);
            if (isset($newCalendars->getBody()['@odata.nextLink'])) {
                $this->link = $newCalendars->getBody()['@odata.nextLink'];
            } else {
                $this->link = false;
            }
        }

        return $calendarsList;
    }

    /**
     * @param $calendarId
     * @return mixed
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getCalendar($calendarId)
    {
        $accessToken = $this->processAuthorization();

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $calendar = $graph
            ->createRequest("GET", "/users/" . $this->accountEmail . "/calendars/" . $calendarId)
            ->execute();

        return $calendar->getBody();
    }

    /**
     * @param string $calendarId
     * @return mixed
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getEventListByWorkspace($calendarId)
    {
        $accessToken = $this->processAuthorization();

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $eventList = $graph
            ->createRequest("GET", "/users/" . $this->accountEmail . "/calendars/" . $calendarId . "/events")
            ->execute();

        return $eventList->getBody();
    }

    /**
     * @param string $calendarId
     * @param int $startTime
     * @param int $endTime
     * @param string $title
     * @param string $summary
     * @return mixed|void
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function addEvent(string $calendarId, int $startTime, int $endTime, string $title, string $summary)
    {
        $accessToken = $this->processAuthorization();

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $data = [
            'Subject' => $title,
            'Body' => [
                'ContentType' => 'HTML',
                'Content' => $summary,
            ],
            'Start' => [
                'DateTime' =>  (new \DateTime())->setTimestamp($startTime)->format( \DateTimeInterface::ISO8601),
                'TimeZone' => 'Pacific Standard Time',
            ],
            'End' => [
                'DateTime' => (new \DateTime())->setTimestamp($endTime)->format( \DateTimeInterface::ISO8601),
                'TimeZone' => 'Pacific Standard Time',
            ],
        ];

        $response = $graph->createRequest("POST", "/users/" . $this->accountEmail . "/calendars/" . $calendarId . "/events")
            ->attachBody($data)
            ->execute();

        return $response->getBody();
    }
}