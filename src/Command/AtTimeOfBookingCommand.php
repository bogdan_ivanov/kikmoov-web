<?php

namespace App\Command;

use App\Entity\Booking;
use App\Entity\User;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Repository\BookingRepository;
use App\Services\RouterWrapper;
use App\Services\UserStatusChecker;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AtTimeOfBookingCommand extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('booking:at-time');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return BookingRepository
     */
    protected function getRepository() : BookingRepository
    {
        return $this->getEntityManager()->getRepository(Booking::class);
    }

    /**
     * @return NotificationService
     */
    protected function getNotificationService() : NotificationService
    {
        return $this->getContainer()->get(NotificationService::class);
    }

    /**
     * @return RouterWrapper
     */
    protected function getRouter() : RouterWrapper
    {
        return $this->getContainer()->get(RouterWrapper::class);
    }

    /**
     * @return UserStatusChecker
     */
    protected function getUserStatusChecker() : UserStatusChecker
    {
        return $this->getContainer()->get(UserStatusChecker::class);
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $output->writeln(sprintf("Find bookings by date %s", $date->format('Y-m-d H:i')));

        $bookings = $this->getRepository()->getBookingsForCronTask($date);

        if (empty($bookings)) {
            $output->writeln("No bookings records \n");

            return 0;
        }

        /** @var Booking $booking */
        foreach ($this->yieldCollection($bookings) as $booking) {
            $user = $booking->getUser();

            if (is_null($user)) {
                $output->writeln(sprintf("No User was found for booking with id %d \n", $booking->getId()));
                continue;
            }

            $workspace = $booking->getWorkSpace();

            if (is_null($workspace)) {
                $output->writeln(sprintf("No Workspace was found for booking with id %d \n", $booking->getId()));
                continue;
            }

            $location = $workspace->getLocation();

            if (is_null($location)) {
                $output->writeln(sprintf("No Location was found for booking with id %d \n", $booking->getId()));
                continue;
            }

            $seller = $location->getUser();

            if (is_null($seller)) {
                $output->writeln(sprintf("No Seller was found for booking with id %d \n", $booking->getId()));
                continue;
            }

            $this->getNotificationService()->sendEmail(
                $seller,
                NotificationsEvent::BOOKING_AT_TIME,
                [
                    'seller' => $seller,
                    'workspace' => $workspace,
                    'location' => $location,
                    'buyer' => $user,
                    'booking' => $booking,
                    'link' => $this->getRouter()->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
                ]
            );

            $this->getUserStatusChecker()->forceChangeStatus($user, User::BOOKING_COMPLETED);
        }

        $output->writeln("Mission Complete");

        return 0;
    }
}
