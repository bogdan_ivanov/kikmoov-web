<?php

namespace App\Command;

use App\Calendar\GoogleCalendar;
use App\Calendar\OutlookCalendar;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use App\Entity\WorkSpace;
use App\Repository\CalendarEventRepository;
use App\Repository\CalendarRepository;
use App\Repository\WorkSpaceRepository;
use App\Traits\FetchAvailableDataTrait;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FecthAvailabilityDataFromCalendar
 * @package App\Command
 */
class FetchAvailabilityDataFromGoogleCalendar extends ContainerAwareCommand
{
    use YieldTrait, FetchAvailableDataTrait;

    protected function configure()
    {
        $this->setName('fetch-google-calendar-availability');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return WorkSpaceRepository
     */
    protected function getRepository() : WorkSpaceRepository
    {
        return $this->getEntityManager()->getRepository(WorkSpace::class);
    }

    /**
     * @return CalendarRepository
     */
    protected function getCalendarRepository() : CalendarRepository
    {
        return $this->getEntityManager()->getRepository(Calendar::class);
    }

    /**
     * @return GoogleCalendar
     */
    protected function getGoogleCalendar() : GoogleCalendar
    {
        return $this->getContainer()->get(GoogleCalendar::class);
    }

    /**
     * @return CalendarEventRepository
     */
    private function getCalendarEventRepository() : CalendarEventRepository
    {
        return $this->getEntityManager()->getRepository(CalendarEvent::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->isProcessRunning() && !$this->checkPreviousProcess()) {
            return null;
        }

        $this->toggleProcessStatus(true);
        $workspaces = $this->getRepository()->findBy(['status' => WorkSpace::ACTIVE]);

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $workspace) {

            if (empty($workspace->getGoogleCalendarId())) {
                continue;
            }

            if ($workspace->getGoogleCalendarId()) {
                $calendar = $this->getGoogleCalendar()->getEventListByWorkspace($workspace->getGoogleCalendarId());
                $events = $calendar->getItems();

                if (empty($events)) {
                    continue;
                }

                /** @var Calendar $googleCalendarEvents */
                $googleCalendarEvents = $this->getCalendarRepository()->findOneBy([
                    'workspace' => $workspace,
                    'type' => Calendar::GOOGLE_CALENDAR
                ]);

                if (empty($googleCalendarEvents)) {
                    $googleCalendar = new Calendar();
                    $googleCalendar->setType(Calendar::GOOGLE_CALENDAR);
                    $googleCalendar->setStatus(false);
                    $googleCalendar->setName($calendar->getSummary());
                    $googleCalendar->setWorkspace($workspace);

                    foreach ($events as $event) {
                        $calendarEvent = new CalendarEvent();
                        $calendarEvent->setSummary($event['summary']);
                        $calendarEvent->setHtmlLink($event['htmlLink']);
                        $calendarEvent->setEventId($event['id']);
                        if (!is_null($event['start']['dateTime']) && !is_null($event['end']['dateTime'])) {
                            $calendarEvent->setStart((new \DateTime($event['start']['dateTime'])));
                            $calendarEvent->setEnd((new \DateTime($event['end']['dateTime'])));
                        } elseif (!is_null($event['start']['date']) && !is_null($event['end']['date'])) {
                            $calendarEvent->setStart((new \DateTime($event['start']['date'])));
                            $calendarEvent->setEnd((new \DateTime($event['end']['date'])));
                        }
                        $calendarEvent->setCalendar($googleCalendar);
                        $this->getEntityManager()->persist($calendarEvent);
                    }

                    $this->getEntityManager()->persist($googleCalendar);
                    $this->getEntityManager()->flush();
                } else {
                    foreach ($events as $event) {

                        $calendarEvent = $this->getCalendarEventRepository()->findOneBy(['eventId' => $event['id']]);

                        if (empty($calendarEvent)) {
                            $calendarEvent = new CalendarEvent();
                        }

                        $calendarEvent->setSummary($event['summary']);
                        $calendarEvent->setHtmlLink($event['htmlLink']);
                        $calendarEvent->setEventId($event['id']);
                        if (!is_null($event['start']['dateTime']) && !is_null($event['end']['dateTime'])) {
                            $calendarEvent->setStart((new \DateTime($event['start']['dateTime'])));
                            $calendarEvent->setEnd((new \DateTime($event['end']['dateTime'])));
                        } elseif (!is_null($event['start']['date']) && !is_null($event['end']['date'])) {
                            $calendarEvent->setStart((new \DateTime($event['start']['date'])));
                            $calendarEvent->setEnd((new \DateTime($event['end']['date'])));
                        }
                        $calendarEvent->setCalendar($googleCalendarEvents);
                        $this->getEntityManager()->persist($calendarEvent);
                    }

                    $this->getEntityManager()->flush();
                }
            }
        }
        $this->toggleProcessStatus(false);
    }
}