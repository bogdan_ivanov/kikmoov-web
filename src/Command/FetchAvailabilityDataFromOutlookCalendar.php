<?php

namespace App\Command;

use App\Calendar\OutlookCalendar;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use App\Entity\WorkSpace;
use App\Repository\CalendarEventRepository;
use App\Repository\CalendarRepository;
use App\Repository\WorkSpaceRepository;
use App\Traits\FetchAvailableDataTrait;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FecthAvailabilityDataFromCalendar
 * @package App\Command
 */
class FetchAvailabilityDataFromOutlookCalendar extends ContainerAwareCommand
{
    use YieldTrait, FetchAvailableDataTrait;

    protected function configure()
    {
        $this->setName('fetch-outlook-calendar-availability');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return WorkSpaceRepository
     */
    protected function getRepository() : WorkSpaceRepository
    {
        return $this->getEntityManager()->getRepository(WorkSpace::class);
    }

    /**
     * @return CalendarRepository
     */
    protected function getCalendarRepository() : CalendarRepository
    {
        return $this->getEntityManager()->getRepository(Calendar::class);
    }

    /**
     * @return OutlookCalendar
     */
    protected function getOutlookCalendar() : OutlookCalendar
    {
        return $this->getContainer()->get(OutlookCalendar::class);
    }

    /**
     * @return CalendarEventRepository
     */
    private function getCalendarEventRepository() : CalendarEventRepository
    {
        return $this->getEntityManager()->getRepository(CalendarEvent::class);
    }
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->isProcessRunning() && !$this->checkPreviousProcess()) {
            return null;
        }

        $this->toggleProcessStatus(true);
        $workspaces = $this->getRepository()->findBy(['status' => WorkSpace::ACTIVE]);

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $workspace) {

            if (empty($workspace->getOutlookCalendarName())) {
                continue;
            }

            if ($workspace->getOutlookCalendarId() || $workspace->getOutlookCalendarName()) {

                if (empty($workspace->getOutlookCalendarId())) {
                    $calendars = $this->getOutlookCalendar()->getCalendarList();

                    foreach ($calendars as $calendar) {
                        if ($calendar['name'] === $workspace->getOutlookCalendarName()) {
                            $workspace->setOutlookCalendarId($calendar['id']);
                            $this->getEntityManager()->flush($workspace);
                        }
                    }
                }

                if (empty($workspace->getOutlookCalendarId())) {
                    continue;
                }

                $calendar = $this->getOutlookCalendar()->getCalendar($workspace->getOutlookCalendarId());

                $events = $this->getOutlookCalendar()->getEventListByWorkspace($workspace->getOutlookCalendarId())['value'];

                if (empty($events)) {
                    continue;
                }

                /** @var Calendar $outlookCalendarEvents */
                $outlookCalendarEvents = $this->getCalendarRepository()->findOneBy([
                    'workspace' => $workspace,
                    'type' => Calendar::OUTLOOK_CALENDAR
                ]);

                if (empty($outlookCalendarEvents)) {
                    $outlookCalendar = new Calendar();
                    $outlookCalendar->setType(Calendar::OUTLOOK_CALENDAR);
                    $outlookCalendar->setStatus(false);
                    $outlookCalendar->setName($calendar['name']);
                    $outlookCalendar->setWorkspace($workspace);

                    foreach ($events as $event) {
                        $calendarEvent = new CalendarEvent();
                        $calendarEvent->setSummary($event['subject']);
                        $calendarEvent->setHtmlLink($event['webLink']);
                        $calendarEvent->setEventId($event['id']);
                        $calendarEvent->setStart((new \DateTime($event['start']['dateTime'], new \DateTimeZone($event['start']['timeZone']))));
                        $calendarEvent->setEnd((new \DateTime($event['end']['dateTime'], new \DateTimeZone($event['end']['timeZone']))));
                        $calendarEvent->setCalendar($outlookCalendar);
                        $this->getEntityManager()->persist($calendarEvent);
                    }

                    $this->getEntityManager()->persist($outlookCalendar);
                    $this->getEntityManager()->flush();
                } else {

                    foreach ($events as $event) {
                        $calendarEvent = $this->getCalendarEventRepository()->findOneBy(['eventId' => $event['id']]);

                        if (empty($calendarEvent)) {
                            $calendarEvent = new CalendarEvent();
                        }

                        $calendarEvent->setSummary($event['subject']);
                        $calendarEvent->setHtmlLink($event['webLink']);
                        $calendarEvent->setEventId($event['id']);
                        $calendarEvent->setStart((new \DateTime($event['start']['dateTime'], new \DateTimeZone($event['start']['timeZone']))));
                        $calendarEvent->setEnd((new \DateTime($event['end']['dateTime'], new \DateTimeZone($event['end']['timeZone']))));
                        $calendarEvent->setCalendar($outlookCalendarEvents);
                        $this->getEntityManager()->persist($calendarEvent);
                    }

                    $this->getEntityManager()->flush();
                }
            }
        }
        $this->toggleProcessStatus(false);
    }
}