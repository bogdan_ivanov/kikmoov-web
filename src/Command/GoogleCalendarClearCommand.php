<?php

namespace App\Command;

use App\Entity\Calendar;
use App\Entity\WorkSpace;
use App\Repository\WorkSpaceRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GoogleCalendarClearCommand
 * @package App\Command
 */
class GoogleCalendarClearCommand extends ContainerAwareCommand
{

    use YieldTrait;

    protected function configure()
    {
        $this->setName('clear-google-calendars');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return WorkSpaceRepository
     */
    protected function getRepository() : WorkSpaceRepository
    {
        return $this->getEntityManager()->getRepository(WorkSpace::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $workspaces = $this->getRepository()->findAll();

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $workspace) {

            $workspace->setGoogleCalendarId(null);
            if ($workspace->getCalendars()) {
                /** @var Calendar $calendar */
                foreach ($workspace->getCalendars() as $calendar) {
                    if ($calendar->getType() === Calendar::GOOGLE_CALENDAR) {
                        $workspace->removeCalendar($calendar);
                    }
                }
            }
            $this->getEntityManager()->persist($workspace);
            $this->getEntityManager()->flush();
        }

    }
}