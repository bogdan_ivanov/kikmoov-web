<?php

namespace App\Command;

use App\Entity\Viewing;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Repository\ViewingRepository;
use App\Services\RouterWrapper;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OneHourAfterViewingCommand extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('viewing:one-hour-after');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return ViewingRepository
     */
    protected function getRepository() : ViewingRepository
    {
        return $this->getEntityManager()->getRepository(Viewing::class);
    }

    /**
     * @return NotificationService
     */
    protected function getNotificationService() : NotificationService
    {
        return $this->getContainer()->get(NotificationService::class);
    }

    /**
     * @return RouterWrapper
     */
    protected function getRouter() : RouterWrapper
    {
        return $this->getContainer()->get(RouterWrapper::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime("-1 hour");
        $output->writeln(sprintf("Find viewing by date %s", $date->format('Y-m-d H:i')));

        $viewings = $this->getRepository()->getViewingsForCronTask($date);

        if (empty($viewings)) {
            $output->writeln("No viewing records \n");

            return 0;
        }

        /** @var Viewing $viewing */
        foreach ($this->yieldCollection($viewings) as $viewing) {
            $user = $viewing->getUser();

            if (is_null($user)) {
                $output->writeln(sprintf("No User was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $workspace = $viewing->getWorkSpace();

            if (is_null($workspace)) {
                $output->writeln(sprintf("No Workspace was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $location = $workspace->getLocation();

            if (is_null($location)) {
                $output->writeln(sprintf("No Location was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $seller = $location->getUser();

            if (is_null($seller)) {
                $output->writeln(sprintf("No Seller was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $this->getNotificationService()->sendEmail(
                $user,
                NotificationsEvent::VIEWING_ONE_HOUR_AFTER,
                [
                    'seller' => $seller,
                    'workspace' => $workspace,
                    'location' => $location,
                    'viewing' => $viewing,
                    'buyer' => $user,
                    'link' => $this->getRouter()->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
                ],
                $seller
            );

        }

        $output->writeln("Mission Complete");

        return 0;
    }
}
