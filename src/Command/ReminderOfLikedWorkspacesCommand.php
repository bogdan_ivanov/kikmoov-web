<?php

namespace App\Command;

use App\Entity\BuyerInternalNotification;
use App\Entity\User;
use App\InternalNotification\PushNotificationType;
use App\Model\BuyerInternalNotificationModelManager;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Traits\YieldTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ReminderOfLikedWorkspacesCommand
 * @package App\Command
 */
class ReminderOfLikedWorkspacesCommand extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('reminder:liked-workspaces');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return NotificationService
     */
    protected function getNotificationService(): NotificationService
    {
        return $this->getContainer()->get(NotificationService::class);
    }

    /**
     * @return BuyerInternalNotificationModelManager
     */
    protected function getBuyerInternalNotificationModelManager(): BuyerInternalNotificationModelManager
    {
        return $this->getContainer()->get(BuyerInternalNotificationModelManager::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->getEntityManager()->getRepository(User::class)->findBy(['enabled' => true]);

        if (empty($users)) {
            $output->writeln("No users \n");
            return 0;
        }

        /** @var User $user */
        foreach ($this->yieldCollection($users) as $user) {
            $likedWorkspaces = $user->getLikedWorkspaces();

            if ($likedWorkspaces->isEmpty()) {
                continue;
            }

            $this->collectLikedWorkspaces($user, $likedWorkspaces);
        }

        $output->writeln("Mission Complete");
        return 0;
    }

    /**
     * @param User $user
     * @param Collection $likedWorkspaces
     * @throws \Exception
     */
    private function collectLikedWorkspaces(User $user, Collection $likedWorkspaces)
    {
        $max = 3;
        $workspaces = [];

        for ($i = $likedWorkspaces->count() - 1; $i >= 0; $i--) {
            $workspaces[] = $likedWorkspaces->offsetGet($i);

            $max--;
            if ($max === 0) {
                break;
            }
        }

        $this->getBuyerInternalNotificationModelManager()->create(
            $user,
            [],
            PushNotificationType::EXISTING_SAVED_WORKSPACES,
            null,
            BuyerInternalNotification::LIKED
        );

        $this->getNotificationService()->sendEmailToUser(
            $user,
            NotificationsEvent::REMINDER_OF_LIKED_WORKSPACES,
            [
                'workspaces' => $workspaces,
                'buyer' => $user
            ]
        );
    }
}
