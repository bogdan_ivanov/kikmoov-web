<?php

namespace App\Command;

use App\Entity\Booking;
use App\Entity\BuyerInternalNotification;
use App\Entity\User;
use App\Entity\Viewing;
use App\InternalNotification\PushNotificationType;
use App\Model\BuyerInternalNotificationModelManager;
use App\Repository\BookingRepository;
use App\Repository\ViewingRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ReminderOfNotUsingApp
 * @package App\Command
 */
class ReminderOfNotUsingApp extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('reminder:not-using-app');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }


    /**
     * @return BuyerInternalNotificationModelManager
     */
    protected function getBuyerInternalNotificationModelManager(): BuyerInternalNotificationModelManager
    {
        return $this->getContainer()->get(BuyerInternalNotificationModelManager::class);
    }

    /**
     * @return ViewingRepository
     */
    protected function getViewingRepository() : ViewingRepository
    {
        return $this->getEntityManager()->getRepository(Viewing::class);
    }

    /**
     * @return BookingRepository
     */
    protected function getBookingRepository() : BookingRepository
    {
        return $this->getEntityManager()->getRepository(Booking::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->getEntityManager()->getRepository(User::class)->findBy(['enabled' => true]);

        if (empty($users)) {
            $output->writeln("No users \n");
            return 0;
        }

        $date = new \DateTime('-1 month');


        /** @var User $user */
        foreach ($this->yieldCollection($users) as $user) {
            $output->writeln(sprintf("Find viewing by date %s", $date->format('Y-m-d')));

            $userViewings = $this->getViewingRepository()->getAllViewingsByDateForCronTask($date, $user);

            if (!empty($userViewings)) {
                continue;
            }

            $userBookings = $this->getBookingRepository()->getAllBookingsByDateForCronTask($date, $user);

            if (!empty($userBookings)) {
                continue;
            }

            $this->getBuyerInternalNotificationModelManager()->create(
                $user,
                [],
                PushNotificationType::DO_NOT_USE_APP_MORE_THAN_MONTH,
                null,
                BuyerInternalNotification::NOT_USING_APP
            );
        }

        $output->writeln("Mission Complete");
        return 0;
    }

}