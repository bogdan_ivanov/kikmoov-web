<?php

namespace App\Command;

use App\Entity\BuyerInternalNotification;
use App\Entity\User;
use App\Entity\Viewing;
use App\InternalNotification\PushNotificationType;
use App\Model\BuyerInternalNotificationModelManager;
use App\Repository\ViewingRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ReminderOfViewingToday
 * @package App\Command
 */
class ReminderOfViewingToday extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('reminder:viewing-today');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return ViewingRepository
     */
    protected function getRepository() : ViewingRepository
    {
        return $this->getEntityManager()->getRepository(Viewing::class);
    }

    /**
     * @return BuyerInternalNotificationModelManager
     */
    protected function getBuyerInternalNotificationModelManager(): BuyerInternalNotificationModelManager
    {
        return $this->getContainer()->get(BuyerInternalNotificationModelManager::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $output->writeln(sprintf("Find viewing by date %s", $date->format('Y-m-d')));

        $viewings = $this->getRepository()->getViewingsByDateForCronTask($date);

        if (empty($viewings)) {
            $output->writeln("No viewing records \n");

            return 0;
        }

        /** @var Viewing $viewing */
        foreach ($this->yieldCollection($viewings) as $viewing) {
            $user = $viewing->getUser();

            if (is_null($user)) {
                $output->writeln(sprintf("No User was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $workspace = $viewing->getWorkSpace();

            if (is_null($workspace)) {
                $output->writeln(sprintf("No Workspace was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $location = $workspace->getLocation();

            if (is_null($location)) {
                $output->writeln(sprintf("No Location was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $seller = $location->getUser();

            if (is_null($seller)) {
                $output->writeln(sprintf("No Seller was found for viewing with id %d \n", $viewing->getId()));
                continue;
            }

            $this->getBuyerInternalNotificationModelManager()->create(
                $user,
                [],
                PushNotificationType::VIEWING_TODAY,
                $viewing->getId(),
                BuyerInternalNotification::VIEWING
            );
        }

        $output->writeln("Mission Complete");
        return 0;
    }

}