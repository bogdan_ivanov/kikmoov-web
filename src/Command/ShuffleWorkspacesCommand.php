<?php

namespace App\Command;

use App\Entity\WorkSpace;
use App\Repository\WorkSpaceRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShuffleWorkspacesCommand extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('shuffle-workspaces');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return WorkSpaceRepository
     */
    protected function getRepository() : WorkSpaceRepository
    {
        return $this->getEntityManager()->getRepository(WorkSpace::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Shuffle workspaces');

        $workspaces = $this->getRepository()->findAll();

        if (empty($workspaces)) {
            $output->writeln("No workspaces records \n");

            return 0;
        }

        $idWorkspaces = array_column($this->getRepository()->getIdWorkspaces(), "id");
        shuffle($idWorkspaces);

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $key => $workspace) {
            $workspace->setSort($idWorkspaces[$key]);
            $this->getEntityManager()->flush($workspace);
        }

        $output->writeln("Mission Complete");

        return 0;
    }
}
