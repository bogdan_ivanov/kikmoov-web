<?php

namespace App\Command\Temp;

use App\Entity\Media;
use App\Entity\WorkSpace;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TempAddCoverImageIdCommand extends ContainerAwareCommand
{

    use YieldTrait;

    protected function configure()
    {
        $this->setName('add-cover-image-id');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return MediaUrlGenerator
     */
    public function getMediaUrlGenerator()
    {
        return $this->getContainer()->get(MediaUrlGenerator::class);
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $workspaces = $this->getEntityManager()->getRepository(WorkSpace::class)->findAll();

        if (empty($workspaces)) {
            $output->writeln('No workspaces was found');
            return 0;
        }

        /** @var WorkSpace $workspace */
        foreach ($workspaces as $workspace) {

            $media = $workspace->getMedias();

            /** @var Media $item */
            foreach ($this->yieldCollection($media) as $item) {
                $link = $this->getMediaUrlGenerator()->generateUrl($item, false);
                if ($link === $workspace->getCoverImageUrl()) {
                    $workspace->setCoverImageId($item->getId());
                    $this->getEntityManager()->flush($workspace);
                }
            }
        }

        return 0;
    }
}