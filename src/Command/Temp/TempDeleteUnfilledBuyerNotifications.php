<?php

namespace App\Command\Temp;

use App\Entity\BuyerInternalNotification;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TempDeleteUnfilledBuyerNotifications extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('delete-unfilled-buyer-notifications');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $notifications = $this->getEntityManager()->getRepository(BuyerInternalNotification::class)->findAll();

        if (empty($notifications)) {
            $output->writeln('No notifications was found');
            return 0;
        }

        /** @var BuyerInternalNotification $notification */
        foreach ($notifications as $notification) {
            if (empty($notification->getActionGroup())) {
                $this->getEntityManager()->remove($notification);
                $this->getEntityManager()->flush($notification);
            }
        }

        return 0;
    }
}