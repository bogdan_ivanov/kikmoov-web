<?php

namespace App\Command\Temp;

use App\Entity\Media;
use App\Entity\WorkSpace;
use App\Services\MediaUrlGenerator;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp;


/**
 * Class TempMigrateMediaAmazonBucketCommand
 * @package App\Command\Temp
 */
class TempMigrateCoverImagesAmazonBucketCommand extends ContainerAwareCommand
{
    /** @var S3Client */
    private $s3;

    protected function configure()
    {
        $this->setName('upload-cover-images-into-bucket');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return mixed
     */
    public function getS3Version()
    {
        return $this->getContainer()->getParameter('s3_version');
    }

    /**
     * @return mixed
     */
    public function getS3Region()
    {
        return $this->getContainer()->getParameter('s3_region');
    }

    /**
     * @return mixed
     */
    public function getS3Bucket()
    {
        return $this->getContainer()->getParameter('s3_bucket_name');
    }

    /**
     * @return MediaUrlGenerator
     */
    public function getMediaUrlGenerator()
    {
        return $this->getContainer()->get(MediaUrlGenerator::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->s3 = $this->getS3Client();

        $this->processWorkspaceImages($input, $output);

        return 0;
    }

    /**
     * @return S3Client
     */
    private function getS3Client()
    {
        return new S3Client([
            'version' => $this->getS3Version(),
            'region' => $this->getS3Region(),
            'credentials' => [
                'key' => $this->getContainer()->getParameter('s3_access_key'),
                'secret'  => $this->getContainer()->getParameter('s3_secret_key')
            ]
        ]);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function processWorkspaceImages(InputInterface $input, OutputInterface $output)
    {
        $workspaces = $this->getEntityManager()->getRepository(WorkSpace::class)->findAll();

        if (empty($workspaces)) {
            $output->writeln('No medias was found');
            return 0;
        }

        /** @var WorkSpace $workspace */
        foreach ($workspaces as $workspace) {

            if (!empty($workspace->getCoverImageUrl())) {

                try {
                    $client = new GuzzleHttp\Client();
                    $positionMedia = strpos($workspace->getCoverImageUrl(),'media/');
                    $key = substr($workspace->getCoverImageUrl(), $positionMedia + 6);
                    $response = $client->get($workspace->getCoverImageUrl());
                    $body = $response->getBody();

                    if (empty($body->getSize())) {
                        continue;
                    }

                    $result = $this->s3->putObject([
                        'Bucket' => $this->getS3Bucket(),
                        'Key' => $key,
                        'Body' => $body,
                        'ACL' => 'public-read',
                        'ContentType' => $response->getHeader('Content-Type')[0],
                    ]);

                    if (!empty($result['ObjectURL'])) {
                        $workspace->setCoverImageUrl($result['ObjectURL']);
                        $this->getEntityManager()->flush($workspace);
                    }
                } catch (S3Exception $e) {
                    $output->writeln('There was an error uploading the file.');
                }
            }
        }
    }
}