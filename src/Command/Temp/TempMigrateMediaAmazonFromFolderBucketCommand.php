<?php

namespace App\Command\Temp;

use App\Entity\Media;
use App\Entity\WorkSpace;
use App\Services\MediaUrlGenerator;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp;

/**
 * Class TempMigrateMediaAmazonFromFolderBucketCommand
 * @package App\Command\Temp
 */
class TempMigrateMediaAmazonFromFolderBucketCommand extends ContainerAwareCommand
{
    /** @var S3Client */
    private $s3;

    protected function configure()
    {
        $this->setName('upload-media-from-folder-into-bucket');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return mixed
     */
    public function getS3Version()
    {
        return $this->getContainer()->getParameter('s3_version');
    }

    /**
     * @return mixed
     */
    public function getS3Region()
    {
        return $this->getContainer()->getParameter('s3_region');
    }

    /**
     * @return mixed
     */
    public function getS3Bucket()
    {
        return $this->getContainer()->getParameter('s3_bucket_name');
    }

    /**
     * @return MediaUrlGenerator
     */
    public function getMediaUrlGenerator()
    {
        return $this->getContainer()->get(MediaUrlGenerator::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->s3 = $this->getS3Client();

        $this->processMediaFiles($input, $output);

        return 0;
    }

    /**
     * @return S3Client
     */
    private function getS3Client()
    {
        return new S3Client([
            'version' => $this->getS3Version(),
            'region' => $this->getS3Region(),
            'credentials' => [
                'key' => $this->getContainer()->getParameter('s3_access_key'),
                'secret'  => $this->getContainer()->getParameter('s3_secret_key')
            ]
        ]);
    }

    /**
     * @param $dir
     * @param array $results
     * @return array
     */
    function getDirContents($dir, &$results = array()){
        $files = scandir($dir);

        foreach($files as $key => $value){
            if ($value[0] !== '.' && $value[1] !== '..') {
                $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
                if (!is_dir($path)) {
                    $positionMedia = strpos($path,'media/');
                    $key = substr($path, $positionMedia + 6);
                    $results[] = $key;
                } else if ($value != "." && $value != "..") {
                    $this->getDirContents($path, $results);
                }
            }
        }

        return $results;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    private function processMediaFiles(InputInterface $input, OutputInterface $output)
    {
        $publicDir = $this->getContainer()->getParameter('kernel.project_dir') . '/public';
        $mediaPart = '/upload/media';

        $medias = $this->getDirContents($publicDir . $mediaPart);

        if (empty($medias)) {
            $output->writeln('No medias was found');
            return 0;
        }

        /** @var Media $media */
        foreach ($medias as $media) {

            $link = $this->getContainer()->getParameter('project_url_admin') . $mediaPart . '/' . $media;

            if (!empty($link)) {
                try {
                    $client = new GuzzleHttp\Client();

                    $response = $client->get($link);

                    $body = $response->getBody();

                    if (empty($body->getSize())) {
                        continue;
                    }

                    $this->s3->putObject([
                        'Bucket' => $this->getS3Bucket(),
                        'Key' => $media,
                        'Body' => $body,
                        'ACL' => 'public-read',
                        'ContentType' => $response->getHeader('Content-Type')[0]
                    ]);

                } catch (S3Exception $e) {
                    $output->writeln('There was an error uploading the file.');
                }
            }
        }
    }
}