<?php

namespace App\Command\Temp;

use App\Entity\WorkSpace;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TempUpdateDeskTypeCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('update-desk-type');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $meetingWorkspaces = $this->getEntityManager()->getRepository(WorkSpace::class)->findBy(['type' => WorkSpace::MEETING_ROOM]);

        if (empty($meetingWorkspaces)) {
            $output->writeln('No meeting-rooms was found');
            return 0;
        }

        /** @var WorkSpace $meetingWorkspace */
        foreach ($meetingWorkspaces as $meetingWorkspace) {
            if (empty($meetingWorkspace->getDeskType())) {
                $meetingWorkspace->setDeskType(WorkSpace::HOURLY_MEETING_ROOM);
                $this->getEntityManager()->flush($meetingWorkspace);
            }
        }

        return 0;
    }
}