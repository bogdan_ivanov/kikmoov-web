<?php

namespace App\Command\Temp;

use App\Entity\Article;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TempUpdateImageLinksBlogCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this->setName('update-blog-links');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return mixed
     */
    public function getProjectUrlAdmin()
    {
        return $this->getContainer()->getParameter('project_url_admin');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $articles = $this->getEntityManager()->getRepository(Article::class)->findAll();

        if (empty($articles)) {
            $output->writeln('No articles was found');
            return 0;
        }

        /** @var Article $article */
        foreach ($articles as $article) {
            if (strpos($article->getCoverImageUrl(), 'http://kikmoov.ddi-dev.com') !== false) {
                $article->setCoverImageUrl(str_replace('http://kikmoov.ddi-dev.com', $this->getProjectUrlAdmin(), $article->getCoverImageUrl()));
                $this->getEntityManager()->flush($article);
            }
        }

        return 0;
    }
}