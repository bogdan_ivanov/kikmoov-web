<?php

namespace App\Command\Temp;

use App\Entity\WorkSpace;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TempUpdateImageLinksCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this->setName('update-links');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return mixed
     */
    public function getProjectUrlAdmin()
    {
        return $this->getContainer()->getParameter('project_url_admin');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $workspaces = $this->getEntityManager()->getRepository(WorkSpace::class)->findAll();

        if (empty($workspaces)) {
            $output->writeln('No workspaces was found');
            return 0;
        }

        /** @var WorkSpace $workspace */
        foreach ($workspaces as $workspace) {
            if (strpos($workspace->getCoverImageUrl(), 'http://kikmoov.ddi-dev.com') !== false) {
                $workspace->setCoverImageUrl(str_replace('http://kikmoov.ddi-dev.com', $this->getProjectUrlAdmin(), $workspace->getCoverImageUrl()));
                $this->getEntityManager()->flush($workspace);
            }
        }

        return 0;
    }
}