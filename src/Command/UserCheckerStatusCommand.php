<?php

namespace App\Command;

use App\Entity\Booking;
use App\Entity\User;
use App\Entity\Viewing;
use App\Repository\BookingRepository;
use App\Repository\UserRepository;
use App\Repository\ViewingRepository;
use App\Services\UserStatusChecker;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserCheckerStatusCommand
 * @package App\Command
 */
class UserCheckerStatusCommand extends ContainerAwareCommand
{
    use YieldTrait;

    protected function configure()
    {
        $this->setName('user:checker-status');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return BookingRepository
     */
    protected function getBookingRepository() : BookingRepository
    {
        return $this->getEntityManager()->getRepository(Booking::class);
    }

    /**
     * @return ViewingRepository
     */
    protected function getViewingRepository() : ViewingRepository
    {
        return $this->getEntityManager()->getRepository(Viewing::class);
    }

    /**
     * @return UserStatusChecker
     */
    protected function getUserStatusChecker() : UserStatusChecker
    {
        return $this->getContainer()->get(UserStatusChecker::class);
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository() : UserRepository
    {
        return $this->getEntityManager()->getRepository(User::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();

        $viewings = $this->getViewingRepository()->userStatusChecker($date);

        $userIds = [];

        if (!empty($viewings)) {
            /** @var Viewing $viewing */
            foreach ($this->yieldCollection($viewings) as $viewing) {
                $user = $viewing->getUser();

                if (is_null($user)) {
                    continue;
                }

                if ($user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                if ($this->getUserStatusChecker()->forceChangeStatus($user, User::VIEWING_ON_GOING) === false) {
                    continue;
                }

                $output->writeln(sprintf("Changed status to VIEWING_ON_GOING for the viewing with id %d for user - %s \n", $viewing->getId(), $user->getEmail()));

                $userIds[] = $user->getId();
            }
        } else {
            $output->writeln("No viewing for today \n");
        }

        $bookings = $this->getBookingRepository()->userStatusChecker($date);

        if (!empty($bookings)) {
            /** @var Booking $booking */
            foreach ($this->yieldCollection($bookings) as $booking) {
                $user = $booking->getUser();

                if (is_null($user)) {
                    continue;
                }

                if ($user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                if ($this->getUserStatusChecker()->forceChangeStatus($user, User::BOOKING_ON_GOING) === false) {
                    continue;
                }

                $output->writeln(sprintf("Changed status to BOOKING_ON_GOING for the booking with id %d for user - %s \n", $booking->getId(), $user->getEmail()));

                $userIds[] = $user->getId();
            }
        } else {
            $output->writeln("No booking for today \n");
        }

        $users = $this->getUserRepository()->findExcept($userIds);

        if (!empty($users)) {
            /** @var User $user */
            foreach ($this->yieldCollection($users) as $user) {
                if ($user->hasRole(User::ROLE_SELLER)) {
                    continue;
                }

                if ($this->getUserStatusChecker()->forceChangeStatus($user, User::SEARCH_ON_GOING) === false) {
                    continue;
                }

                $output->writeln(sprintf("Changed status to SEARCH_ON_GOING for the user with id %d for user - %s \n", $user->getId(), $user->getEmail()));
            }
        } else {
            $output->writeln("No users for changing status to SEARCH_ON_GOING \n");
        }

        return 0;
    }
}
