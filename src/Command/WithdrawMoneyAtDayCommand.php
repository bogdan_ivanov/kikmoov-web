<?php

namespace App\Command;

use App\Entity\Booking;
use App\Payment\StripeClient;
use App\Repository\BookingRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WithdrawMoneyAtDayCommand extends ContainerAwareCommand
{
    const NUM_OF_ATTEMPTS = 3;

    use YieldTrait;

    protected function configure()
    {
        $this->setName('workspace:withdraw-money-at-day');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return BookingRepository
     */
    protected function getBookingRepository() : BookingRepository
    {
        return $this->getEntityManager()->getRepository(Booking::class);
    }

    /**
     * @return StripeClient
     */
    protected function getStripeClient() : StripeClient
    {
        return $this->getContainer()->get(StripeClient::class);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $output->writeln(sprintf("Find bookings by date %s", $date->format('Y-m-d H:i')));

        $bookings = $this->getBookingRepository()->getBookingsForWithdrawMoney($date);

        if (empty($bookings)) {
            $output->writeln("No bookings records \n");

            return 0;
        }

        /** @var Booking $booking */
        foreach ($this->yieldCollection($bookings) as $booking) {
            $user = $booking->getUser();

            if (is_null($booking->getWorkSpace())) {
                $output->writeln(sprintf("No workspace was found for booking with id %d \n", $booking->getId()));

                continue;
            }

            $workspace = $booking->getWorkSpace();

            if (is_null($workspace->getLocation())) {
                $output->writeln(sprintf("No location was found for booking with id %d \n", $booking->getId()));

                continue;
            }

            $location = $workspace->getLocation();

            if (is_null($user)) {
                $output->writeln(sprintf("No user was found for booking with id %d \n", $booking->getId()));

                continue;
            }

            if (is_null($booking->getCustomerId())) {
                $output->writeln(sprintf("CustomerId was missed for booking with id %d \n", $booking->getId()));

                continue;
            }

            $attempts = $booking->getAttempts() + 1;
            $booking->setAttempts($attempts);
            $chargeAmount = ($booking->getAmount() + $booking->getAmountVAT()) * 100;

            try {
                $res = $this->getStripeClient()->chargeCustomer(
                    $chargeAmount,
                    'GBP',
                    $booking->getCustomerId(),
                    null,
                    0,
                    sprintf(
                        "Payment for booking a workspace (id - %d), %s (id - %d)",
                        $workspace->getId(),
                        $location->getName(),
                        $location->getId()
                    )
                );

                $booking->setPaymentLog(json_encode($res->jsonSerialize()));

                if ($res->paid) {
                    $booking->setStatus(Booking::PAYMENT_RECEIVED);
                }

                $this->getEntityManager()->flush($booking);
            } catch (\Exception $exception) {
                $output->writeln($exception->getMessage());

                $booking->setPaymentLog(is_string($exception->getMessage())
                    ? $exception->getMessage()
                    : json_encode($exception->getMessage())
                );

                if ($attempts === self::NUM_OF_ATTEMPTS) {
                    $booking->setStatus(Booking::FAILED_WITHDRAW);
                }

                $this->getEntityManager()->flush($booking);

                continue;
            }
        }

        $output->writeln("Done \n");

        return 0;
    }
}