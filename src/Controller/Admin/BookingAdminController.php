<?php

namespace App\Controller\Admin;

use App\Services\BookingReceiptGenerator;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Error\Error;

/**
 * Class BookingAdminController
 * @package App\Controller\Admin
 */
class BookingAdminController extends CRUDController
{
    /**
     * @param int $id
     * @param BookingReceiptGenerator $generator
     *
     * @return Response
     *
     * @throws Error
     */
    public function downloadAction(int $id, BookingReceiptGenerator $generator): Response
    {
        if (!$booking = $this->admin->getSubject()) {
            throw new NotFoundHttpException(sprintf('Unable to find the booking record with id: %s', $id));
        }

        return new Response(
            $generator->generateBinaryPdf($booking),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="Receipt_Kikmoov '.$booking->getId().'.pdf"'
            )
        );
    }
}