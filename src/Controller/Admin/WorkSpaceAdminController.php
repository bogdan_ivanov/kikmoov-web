<?php

namespace App\Controller\Admin;

use App\Entity\WorkSpace;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class WorkSpaceAdminController
 * @package App\Controller\Admin
 */
class WorkSpaceAdminController extends CRUDController
{

    public function batchActionApproveWorkspaces(ProxyQueryInterface $selectedModelQuery)
    {
        $selectedWorkspaces = $selectedModelQuery->execute();

        try {
            /** @var WorkSpace $workspace */
            foreach ($selectedWorkspaces as $workspace) {
                $workspace->setStatus(WorkSpace::ACTIVE);
                $this->admin->update($workspace);
            }
        } catch (\Exception $e) {
            $this->addFlash(
                'sonata_flash_error',
                'Could not approve workspaces'
            );

            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }

        $this->addFlash(
            'sonata_flash_success',
            'Workspaces were approved'
        );

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }
}