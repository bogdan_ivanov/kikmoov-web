<?php

namespace App\Controller\Api;

use App\Managers\AreaApiManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class AreaController
 * @package App\Controller\Api
 */
class AreaController extends Controller implements ApiExceptionInterface
{

    /**
     *
     * Get areas
     *
     * @Route("/api/areas", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns list of areas",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Location")
     *
     *
     * @param AreaApiManager $areaApiManager
     * @return JsonResponse
     */
    public function getArea(AreaApiManager $areaApiManager) : JsonResponse
    {
        $res = $areaApiManager->getActiveAreas();

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}