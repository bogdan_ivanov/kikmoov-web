<?php

namespace App\Controller\Api;

use App\Managers\BlogApiManager;
use App\Model\ArticleModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class BlogController
 * @package App\Controller\Api
 */
class BlogController extends Controller
{
    /**
     * Get last article
     *
     * @Route("/api/blog/last-article/", methods={"GET"}, name="api_get_last_article")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Return article",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              type="object",
     *              property="article",
     *              @SWG\Property(property="id", type ="integer"),
     *              @SWG\Property(property="coverImageUrl", type ="string"),
     *              @SWG\Property(property="slug", type ="string"),
     *              @SWG\Property(property="description", type ="string"),
     *              @SWG\Property(property="title", type ="string"),
     *              @SWG\Property(property="name", type ="string")
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Blog")
     *
     * @SWG\Response(response="400", description="Returns when something went wrong")
     *
     * @param ArticleModelManager $articleModelManager
     * @return JsonResponse
     */
    public function getLastArticle(ArticleModelManager $articleModelManager) : JsonResponse
    {
        $res = $articleModelManager->getLastArticle();

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Get article by slug
     *
     * @Route("/api/blog/article/{slug}", methods={"GET"}, name="api_get_article")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Return article",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              type="object",
     *              property="article",
     *              @SWG\Property(property="id", type ="integer"),
     *              @SWG\Property(property="coverImageUrl", type ="string"),
     *              @SWG\Property(property="slug", type ="string"),
     *              @SWG\Property(property="description", type ="string"),
     *              @SWG\Property(property="title", type ="string"),
     *              @SWG\Property(property="body", type ="string"),
     *              @SWG\Property(property="createdAt", type ="timestamp"),
     *          ),
     *          @SWG\Property(
     *              type="object",
     *              property="category",
     *              @SWG\Property(property="name", type ="string"),
     *              @SWG\Property(property="slug", type ="string"),
     *              @SWG\Property(property="icon", type ="string")
     *         ),
     *         @SWG\Property(
     *              type="array",
     *              property="related",
     *              @SWG\Items (
     *                  @SWG\Property(property="id", type ="integer"),
     *                  @SWG\Property(property="coverImageUrl", type ="string"),
     *                  @SWG\Property(property="slug", type ="string"),
     *                  @SWG\Property(property="title", type ="string"),
     *                  @SWG\Property(property="description", type ="string")
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Blog")
     *
     * @SWG\Response(response="400", description="Returns when something went wrong")
     *
     * @param BlogApiManager $blogApiManager
     * @param string $slug
     * @return JsonResponse
     * @throws \Exception
     */
    public function getArticleBySlug(BlogApiManager $blogApiManager, string $slug) : JsonResponse
    {
        $res = $blogApiManager->findArticle($slug);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get list of active categories
     *
     * @Route("/api/blog/categories", methods={"GET"}, name="api_get_categories")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Return list of active categories",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              type="array",
     *              property="categories",
     *              @SWG\Items(
     *                  @SWG\Property(property="name", type ="string"),
     *                  @SWG\Property(property="slug", type ="string"),
     *                  @SWG\Property(property="icon", type ="string")
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Blog")
     *
     * @SWG\Response(response="400", description="Returns when something went wrong")
     *
     * @param BlogApiManager $blogApiManager
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function getCategories(BlogApiManager $blogApiManager)
    {
        $res = $blogApiManager->getListOfActiveCategories();

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get list of articles by category slug
     *
     * @Route("/api/blog/category/{slug}", methods={"GET"}, name="api_get_articles_categories")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Return  article",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              type="array",
     *              property="articles",
     *              @SWG\Items (
     *                  @SWG\Property(property="id", type ="integer"),
     *                  @SWG\Property(property="coverImageUrl", type ="string"),
     *                  @SWG\Property(property="slug", type ="string"),
     *                  @SWG\Property(property="title", type ="string"),
     *                  @SWG\Property(property="description", type ="string")
     *             )
     *         ),
     *         @SWG\Property(
     *              property="count",
     *              type="number"
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Blog")
     *
     * @SWG\Response(response="400", description="Returns when something went wrong")
     *
     * @param Request $request
     * @param BlogApiManager $blogApiManager
     * @param string $slug
     * @throws
     * @return JsonResponse
     */
    public function getArticlesByCategory(Request $request, BlogApiManager $blogApiManager, string $slug) : JsonResponse
    {
        $start = intval($request->query->get('start', 0));

        $res = $blogApiManager->findArticlesByCategorySlug($slug, $start);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
