<?php

namespace App\Controller\Api;

use App\Managers\WorkSpaceApiManager;
use App\Model\BookingModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class BookingController
 * @package App\Controller\Api
 */
class BookingController extends Controller implements ApiExceptionInterface
{
    /**
     * Add booking to schedule
     *
     * @Route("/api/v1/workspace/{id}/booking", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="startTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="endTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="paymentToken",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="address",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="city",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="postCode",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when booking time was added to schedule",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="message",
     *             type="string"
     *          ),
     *          @SWG\Property(
     *             property="bookingId",
     *             type="integer"
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Booking")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param $id
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function addBookingToSchedule(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->addBooking($this->getUser(), $id, $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Check available dates
     *
     * @Route("/api/workspace/{id}/booking-available", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="startTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="endTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns status of availability",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="available",
     *             type="boolean"
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Booking")
     *
     * @param Request $request
     * @param BookingModelManager $bookingModelManager
     * @param int $id
     * @return JsonResponse
     */
    public function checkAvailableBookingDate(Request $request, BookingModelManager $bookingModelManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['startTime'])) {
            return new JsonResponse([
                'message' => 'Required data is missing'
            ], Response::HTTP_BAD_REQUEST);
        }

        $startTime = (new \DateTime())->setTimestamp($data['startTime']);
        $now = new \DateTime();

        if ($now > $startTime) {
            return new JsonResponse(['available' => false]);
        }

        $endTime = (isset($data['endTime']))
            ? (new \DateTime())->setTimestamp($data['endTime'])
            : $startTime;

        return new JsonResponse(['available' => $bookingModelManager->isTimeAvailable($id, $startTime, $endTime)]);
    }

    /**
     *
     * @Route("/api/seller/workspace/{id}/booking-status", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true,
     *     description="accepted | declined",
     * )
     *
     * @SWG\Parameter(
     *     name="bookingId",
     *     in="body",
     *     schema={"integer"},
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when status was successfully updated"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function updateStatus(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id)
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['status']) || !isset($data['bookingId'])) {
            return new JsonResponse([
                'message' => 'Required data is missing'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->updateBookingStatus($this->getUser(), $id, intval($data['bookingId']), $data['status']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Add booking request to schedule
     *
     * @Route("/api/v1/workspace/{id}/booking-request", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="startTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="information",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="duration",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true,
     *     description="not-sure | one-month-or-more | six-month-or-more | one-year-or-more"
     * )
     *
     * @SWG\Parameter(
     *     name="tenantType",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true,
     *     description="freelancer | company"
     * )
     *
     * @SWG\Parameter(
     *     name="units",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when booking time was added to schedule",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="message",
     *             type="string"
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Booking")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param $id
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function addBookingRequestToSchedule(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->addBookingRequest($this->getUser(), $id, $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
