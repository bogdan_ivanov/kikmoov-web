<?php

namespace App\Controller\Api;

use App\Entity\ProudToWork;
use App\Entity\Testimonials;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class CmsController
 * @package App\Controller\Api
 */
class CmsController extends Controller implements ApiExceptionInterface
{
    use YieldTrait;

    /**
     *
     * Get list of testimonials
     *
     * @Route("/api/testimonials", methods={"GET"}, name="api_get_testimonials")
     *
     * @SWG\Response(
     *     response="200",
     *     description="get list of testimonials",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              type="array",
     *              property="testimonials",
     *              @SWG\Items(
     *                  @SWG\Property(property="fullName", type ="string"),
     *                  @SWG\Property(property="companyName", type ="string"),
     *                  @SWG\Property(property="positioncon", type ="string"),
     *                  @SWG\Property(property="feedback", type ="string"),
     *                  @SWG\Property(property="photo", type ="string")
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Content Management")
     *
     * @param EntityManagerInterface $entityManager
     * @param MediaUrlGenerator $mediaUrlGenerator
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function getTestimonials(EntityManagerInterface $entityManager, MediaUrlGenerator $mediaUrlGenerator)
    {
        $testimonials = $entityManager->getRepository(Testimonials::class)->findBy(['published' => true]);

        if (empty($testimonials)) {
            return new JsonResponse([
                'testimonials' => $testimonials
            ]);
        }

        $filtered = [];

        /** @var Testimonials $testimonial */
        foreach ($this->yieldCollection($testimonials) as $testimonial) {
            $filtered[] = [
                'fullName' => $testimonial->getFullName(),
                'companyName' => $testimonial->getCompanyName(),
                'position' => $testimonial->getPosition(),
                'feedback' => $testimonial->getFeedback(),
                'photo' => $mediaUrlGenerator->generateUrl($testimonial->getPhoto())
            ];
        }

        return new JsonResponse(['testimonials' => $filtered]);
    }

    /**
     *
     * Get list of proud to work
     *
     * @Route("/api/proud-to-work", methods={"GET"}, name="api_get_proud_to_work")
     *
     * @SWG\Response(
     *     response="200",
     *     description="get list of testimonials",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              type="array",
     *              property="proudToWork",
     *              @SWG\Items(
     *                  @SWG\Property(property="companyName", type ="string"),
     *                  @SWG\Property(property="image", type ="string")
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="Content Management")
     *
     * @param EntityManagerInterface $entityManager
     * @param MediaUrlGenerator $mediaUrlGenerator
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function getProudToWorkList(EntityManagerInterface $entityManager, MediaUrlGenerator $mediaUrlGenerator)
    {
        $proudToWorkList = $entityManager->getRepository(ProudToWork::class)->findBy(['published' => true]);

        if (empty($proudToWorkList)) {
            return new JsonResponse([
                'proudToWork' => $proudToWorkList
            ]);
        }

        $filtered = [];

        /** @var ProudToWork $proudToWork */
        foreach ($this->yieldCollection($proudToWorkList) as $proudToWork) {
            $filtered[] = [
                'companyName' => $proudToWork->getCompanyName(),
                'image' => $mediaUrlGenerator->generateUrl($proudToWork->getImage())
            ];
        }

        return new JsonResponse(['proudToWork' => $filtered]);
    }
}
