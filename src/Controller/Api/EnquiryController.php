<?php

namespace App\Controller\Api;

use App\Managers\EnquiryApiManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class EnquiryController
 * @package App\Controller\Api
 */
class EnquiryController extends Controller implements ApiExceptionInterface
{
    /**
     * Save contact us message from the web
     *
     * @Route("/api/contact-us", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     schema={"string"},
     *     description="Contact name",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     description="Contact email",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="message",
     *     in="body",
     *     schema={"text"},
     *     description="Contact message",
     *     type="string",
     *     required=true
     * )
     *
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when everything is ok"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Enquiry")
     *
     * @param Request $request
     * @param EnquiryApiManager $enquiryApiManager
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function contactUs(Request $request, EnquiryApiManager $enquiryApiManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse(['message' => 'Requested data is empty'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $res = $enquiryApiManager->saveContactEnquiry($data, null);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Save contact us message from the app
     *
     * @Route("/api/v1/contact-us", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="message",
     *     in="body",
     *     schema={"text"},
     *     description="Contact message",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when everything is ok"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Enquiry")
     *
     * @param Request $request
     * @param EnquiryApiManager $enquiryApiManager
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function contactUsFromApp(Request $request, EnquiryApiManager $enquiryApiManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse(['message' => 'Requested data is empty'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $res = $enquiryApiManager->saveContactEnquiry($data, $this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Save leasehold enquiry
     *
     * @Route("/api/leasehold-enquiry", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="area",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     description="north | east | south | west",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="fullName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="numberOfPeople",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="companyName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="message",
     *     in="body",
     *     schema={"text"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when everything is ok"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Enquiry")
     *
     * @param Request $request
     * @throws
     * @param EnquiryApiManager $enquiryApiManager
     * @return JsonResponse
     */
    public function leaseholdEnquiry(Request $request, EnquiryApiManager $enquiryApiManager) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse(['message' => 'Requested data is empty'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $res = $enquiryApiManager->saveLeaseholdEnquiry($data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
