<?php

namespace App\Controller\Api;

use App\Model\FacilityModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class FacilityApiController
 * @package App\Controller\Api
 */
class FacilityApiController extends Controller implements ApiExceptionInterface
{
    /**
     * Get list of active facilities
     *
     * @Route("/api/facilities", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when everything is ok",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *               type="object",
     *               @SWG\Property(property="url", type="string"),
     *               @SWG\Property(property="name", type="string"),
     *               @SWG\Property(property="slug", type="string")
     *          )
     *     )
     * )
     *
     * @SWG\Tag(name="Search")
     *
     * @param FacilityModelManager $facilityModelManager
     * @return JsonResponse
     * @throws \Exception
     */
    public function getListOfFacilities(FacilityModelManager $facilityModelManager) : JsonResponse
    {
        return new JsonResponse($facilityModelManager->grabFacilities());
    }
}
