<?php

namespace App\Controller\Api;

use App\Model\InternalNotificationModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class InternalNotificationController
 * @package App\Controller\Api
 */
class InternalNotificationController extends Controller implements ApiExceptionInterface
{
    /**
     * Get list of internal notifications
     *
     * @Route("/api/seller/internal-notifications", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when everything is ok",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="notifications",
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="id", type="number"),
     *                  @SWG\Property(property="date", type="number"),
     *                  @SWG\Property(property="message", type="string"),
     *                  @SWG\Property(property="status", type="string", description="VIEWING_REQUESTED | VIEWING_ACCEPTED | VIEWING_DECLINED | BOOKING_REQUESTED | BOOKING_ACCEPTED | BOOKING_DECLINED"),
     *                  @SWG\Property(property="from", type="string"),
     *                  @SWG\Property(property="viewingId", type="number"),
     *                  @SWG\Property(property="bookingId", type="number"),
     *                  @SWG\Property(property="workspaceId", type="number")
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Internal Notifications")
     *
     * @param InternalNotificationModelManager $internalNotificationModelManager
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function getInternalNotificationsList(InternalNotificationModelManager $internalNotificationModelManager) : JsonResponse
    {
        $res = $internalNotificationModelManager->getListOfNotifications($this->getUser());
        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * @Route("/api/seller/internal-notifications", methods={"DELETE"})
     *
     * @SWG\Parameter(
     *     name="ids",
     *     in="query",
     *     schema={"array"},
     *     type="array",
     *     items=@SWG\Items(type="number")
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when everything is ok"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Internal Notifications")
     *
     * @param Request $request
     * @param InternalNotificationModelManager $internalNotificationModelManager
     * @return JsonResponse
     */
    public function deleteNotifications(Request $request, InternalNotificationModelManager $internalNotificationModelManager)
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data) || !isset($data['ids']) || is_array($data['ids']) === false) {
            return new JsonResponse(['message' => 'Requested data is empty'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $internalNotificationModelManager->delete($this->getUser(), $data['ids']);

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}
