<?php

namespace App\Controller\Api;

use App\Managers\BuyerNotificationApiManager;
use App\Model\BuyerInternalNotificationModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class PushNotificationController
 * @package App\Controller\Api
 */
class PushNotificationController extends Controller implements ApiExceptionInterface
{

    /**
     *
     * Get list of push notifications
     *
     * @Route("/api/v1/internal-notifications", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="Bearer %ACCESS_TOKEN%",
     *     in="header",
     *     description="Access token that generated for client",
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when everything is ok",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="notifications",
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(property="createdAt", type="date"),
     *                  @SWG\Property(property="message", type="string"),
     *                  @SWG\Property(
     *                      property="action",
     *                      @SWG\Property(property="actionType", type="string", description="VIEWING_TODAY | VIEWING_ACCEPTED | BOOKING_ACCEPTED | ONE_DAY_BEFORE_BOOKING_HOURLY | ONE_DAY_BEFORE_BOOKING_MONTHLY | EXISTING_SAVED_WORKSPACES | DO_NOT_USE_APP_MORE_THAN_MONTH"),
     *                      @SWG\Property(property="id", type="number"),
     *                  )
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Internal Notifications")
     *
     * @param BuyerNotificationApiManager $buyerNotificationApiManager
     * @return JsonResponse
     * @throws \Exception
     */
    public function getInternalNotificationsList(BuyerNotificationApiManager $buyerNotificationApiManager) : JsonResponse
    {
        $res = $buyerNotificationApiManager->getListOfNotifications($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}