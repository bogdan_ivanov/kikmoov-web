<?php

namespace App\Controller\Api;

use App\Managers\SearchManager;
use App\Model\WorkSpaceModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class SearchController
 * @package App\Controller\Api
 */
class SearchController extends Controller
{
    /**
     * Search
     *
     * @Route("/api/search", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="duration",
     *     in="query",
     *     schema={"string"},
     *     description="hourly | monthly",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="type",
     *     in="query",
     *     schema={"string"},
     *     description="desk | private-office | meeting-room",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="address",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Address",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="area",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Area",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="borough",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Borough",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="station",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Station",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="minPrice",
     *     in="query",
     *     schema={"integer"},
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="maxPrice",
     *     in="query",
     *     schema={"integer"},
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="capacity",
     *     in="query",
     *     schema={"integer"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="availableFrom",
     *     in="query",
     *     schema={"integer"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="price",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="highest|lowest",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="date",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="newest|oldest",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="start",
     *     in="query",
     *     schema={"number"},
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="facilities",
     *     in="query",
     *     schema={"array"},
     *     type="array",
     *     items=@SWG\Items(type="string"),
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns results",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="count",
     *              type="number"
     *          ),
     *          @SWG\Property(
     *             property="results",
     *             type="array",
     *             @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(
     *                      property="location",
     *                      type="object",
     *                      @SWG\Property(property="id", type ="integer"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="address", type ="string"),
     *                      @SWG\Property(property="latitude", type ="string"),
     *                      @SWG\Property(property="longitude", type ="string")
     *                  ),
     *                  @SWG\Property(
     *                      property="workspace",
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="quantity", type ="integer"),
     *                      @SWG\Property(property="price", type ="number"),
     *                      @SWG\Property(property="description", type ="string"),
     *                      @SWG\Property(property="size", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="capacity", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="deskType", type ="string", description="desk"),
     *                      @SWG\Property(property="minContractLength", type ="integer", description="private-office"),
     *                      @SWG\Property(property="maxContactLength", type ="integer", description="private-office"),
     *                      @SWG\Property(property="isLiked", type="boolean"),
     *                      @SWG\Property(property="visited", type="boolean"),
     *                      @SWG\Property(
     *                          property="images",
     *                          type ="array",
     *                          @SWG\Items(
     *                              type="object",
     *                              @SWG\Property(property="id", type="integer"),
     *                              @SWG\Property(property="url", type="string"),
     *                          )
     *                      )
     *                  ),
     *                  @SWG\Property(
     *                      property="related",
     *                      type ="array",
     *                      @SWG\Items(
     *                          type="object",
     *                          @SWG\Property(property="id", type ="integer"),
     *                          @SWG\Property(property="quantity", type ="integer"),
     *                          @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                          @SWG\Property(property="price", type ="number"),
     *                          @SWG\Property(property="status", type ="string"),
     *                          @SWG\Property(property="description", type ="string"),
     *                          @SWG\Property(property="type", type ="string"),
     *                          @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="maxContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="isLiked", type="boolean"),
     *                          @SWG\Property(property="visited", type="boolean")
     *                      )
     *                  ),
     *                  @SWG\Property(
     *                      property="user",
     *                      type="object",
     *                      @SWG\Property(property="isLiked", type="boolean")
     *                  )
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Search")
     *
     * @param Request $request
     * @param SearchManager $searchManager
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function search(Request $request, SearchManager $searchManager) : JsonResponse
    {
        $res = $searchManager->search($request->query->all());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Mobile Search
     *
     * @Route("/api/search-mobile", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="duration",
     *     in="query",
     *     schema={"string"},
     *     description="hourly | monthly",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="type",
     *     in="query",
     *     schema={"string"},
     *     description="desk | private-office | meeting-room",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="address",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Address",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="area",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Area",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="borough",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Borough",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="station",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Station",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="minPrice",
     *     in="query",
     *     schema={"integer"},
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="maxPrice",
     *     in="query",
     *     schema={"integer"},
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="capacity",
     *     in="query",
     *     schema={"integer"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="availableFrom",
     *     in="query",
     *     schema={"integer"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="price",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="highest|lowest",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="date",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="newest|oldest",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="start",
     *     in="query",
     *     schema={"number"},
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     schema={"number"},
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns results",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="count",
     *              type="number"
     *          ),
     *          @SWG\Property(
     *             property="results",
     *             type="array",
     *             @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(
     *                      property="location",
     *                      type="object",
     *                      @SWG\Property(property="id", type ="integer"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="address", type ="string"),
     *                      @SWG\Property(property="latitude", type ="string"),
     *                      @SWG\Property(property="longitude", type ="string")
     *                  ),
     *                  @SWG\Property(
     *                      property="workspace",
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="quantity", type ="integer"),
     *                      @SWG\Property(property="price", type ="number"),
     *                      @SWG\Property(property="description", type ="string"),
     *                      @SWG\Property(property="size", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="capacity", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="deskType", type ="string", description="desk"),
     *                      @SWG\Property(property="minContractLength", type ="integer", description="private-office"),
     *                      @SWG\Property(property="maxContactLength", type ="integer", description="private-office"),
     *                      @SWG\Property(property="isLiked", type="boolean"),
     *                      @SWG\Property(property="visited", type="boolean"),
     *                  ),
     *                  @SWG\Property(
     *                      property="related",
     *                      type ="array",
     *                      @SWG\Items(
     *                          type="object",
     *                          @SWG\Property(property="id", type ="integer"),
     *                          @SWG\Property(property="quantity", type ="integer"),
     *                          @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                          @SWG\Property(property="price", type ="number"),
     *                          @SWG\Property(property="status", type ="string"),
     *                          @SWG\Property(property="description", type ="string"),
     *                          @SWG\Property(property="type", type ="string"),
     *                          @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="maxContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="isLiked", type="boolean"),
     *                          @SWG\Property(property="visited", type="boolean")
     *                      )
     *                  ),
     *                  @SWG\Property(
     *                      property="user",
     *                      type="object",
     *                      @SWG\Property(property="isLiked", type="boolean")
     *                  )
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Search Mobile")
     *
     * @param Request $request
     * @param SearchManager $searchManager
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function searchMobile(Request $request, SearchManager $searchManager) : JsonResponse
    {
        $res = $searchManager->searchMobile($request->query->all());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Get suggestion list by provided part of address
     *
     * @Route("/api/suggestion", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="address",
     *     in="query",
     *     schema={"string"},
     *     type="string",
     *     description="Input value",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="duration",
     *     in="query",
     *     schema={"string"},
     *     description="hourly | monthly",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="sessiontoken",
     *     in="query",
     *     schema={"string"},
     *     description="A random string which identifies an autocomplete session",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns suggestion list of results",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(
     *                  property="value",
     *                  type ="string"
     *              ),
     *              @SWG\Property(
     *                  property="type",
     *                  type ="string",
     *                  description="address | borough | area | station"
     *              )
     *          )
     *     )
     * )
     *
     * @SWG\Tag(name="Search")
     *
     * @param Request $request
     * @throws
     * @param SearchManager $searchManager
     * @return JsonResponse
     */
    public function suggestionList(Request $request, SearchManager $searchManager) : JsonResponse
    {
        $input = $request->query->get('address', null);
        $sessionToken = $request->query->get('sessiontoken', null);

        if (is_null($input)) {
            return new JsonResponse([]);
        }

        return new JsonResponse($searchManager->getSuggestionList($input, $sessionToken), Response::HTTP_OK);
    }

    /**
     * Returns min and max borders for the price range pickers
     *
     * @Route("/api/price-range", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns min and max borders",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="min", type="number"),
     *          @SWG\Property(property="max", type="number"),
     *     )
     * )
     *
     * @SWG\Tag(name="Search")
     *
     * @param WorkSpaceModelManager $workSpaceModelManager
     * @return JsonResponse
     */
    public function getPriceRange(WorkSpaceModelManager $workSpaceModelManager) : JsonResponse
    {
        return new JsonResponse($workSpaceModelManager->getPriceRange());
    }
}
