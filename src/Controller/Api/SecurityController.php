<?php

namespace App\Controller\Api;

use App\Security\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class SecurityController
 * @package App\Controller\Api
 */
class SecurityController extends Controller implements ApiExceptionInterface
{
    /**
     *
     * Registration endpoint for buyer
     *
     * @Route("/api/registration/buyer", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="firstname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="lastname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Buyer created successfully"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService

     * @return JsonResponse
     */
    public function registerBuyerAction(Request $request, SecurityService $securityService)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->signUpBuyer($data, isset($data['createdFromApp']));

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Registration endpoint for seller
     *
     * @Route("/api/registration/seller", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="firstname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="lastname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="companyName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="address",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="optionalAddress",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="town",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="postcode",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="invoiceEmail",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Seller created successfully"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService

     * @return JsonResponse
     */
    public function registerSellerAction(Request $request, SecurityService $securityService)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->signUpSeller($data, isset($data['createdFromApp']));

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Check the possibility of using requested email address
     *
     * @Route("/api/check-email", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     description="Email",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when email is not registered"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when email already in database or something went wrong"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService
     *
     * @return JsonResponse
     */
    public function checkEmail(Request $request, SecurityService $securityService) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || !isset($data['email'])) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        [$ok, $data] = $securityService->userExist($data['email']);

        return new JsonResponse([], !$ok ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }

    /**
     * Authentication
     *
     * @Route("/api/login", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="mobileApp",
     *     in="header",
     *     type="boolean",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Authentication passed successfully",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="client_id",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *               property="client_secret",
     *               type="string"
     *         ),
     *         @SWG\Property(
     *              property="accountType",
     *              type="string",
     *              description="Can be seller or buyer"
     *         )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400, description="Returns when something went wrong"
     * )
     *
     * @SWG\Response(
     *     response=401, description="Returns when user is unauthorized"
     * )
     *
     * @SWG\Response(
     *     response=403, description="Returns when user is blocked or has unconfirmed account"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService
     *
     * @return JsonResponse
     */
    public function loginAction(Request $request, SecurityService $securityService)
    {
        $mobileApp = $request->headers->get('mobileApp', null);

        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return new JsonResponse([
                'message' => 'Data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['email']) || !isset($data['password'])) {
            return new JsonResponse([
                'message' => 'Email or password field was missed'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->login($data['email'], $data['password'], $mobileApp);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Confirm registration
     *
     * @Route("/api/confirm/{token}", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     description="User confirmation token",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns if user was confirmed successfully",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="access_token",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *               property="expires_in",
     *               type="number"
     *         ),
     *         @SWG\Property(
     *              property="refresh_token",
     *              type="string"
     *         ),
     *         @SWG\Property(
     *              property="message",
     *              type="string"
     *         ),
     *         @SWG\Property(
     *              property="token_type",
     *              type="string"
     *         ),
     *         @SWG\Property(
     *              property="accountType",
     *              type="string",
     *              description="Can be seller or buyer"
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param $token
     * @param SecurityService $securityService
     *
     * @return JsonResponse
     */
    public function confirmRegistration(string $token, SecurityService $securityService)
    {
        $res = $securityService->confirmAccount($token);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Forgot password
     *
     * @Route("/api/forgot-password", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     description="User Email",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns if the user was found and link was successfully sent"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService
     *
     * @return JsonResponse
     */
    public function forgotPassword(Request $request, SecurityService $securityService) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || !isset($data['email'])) {
            return new JsonResponse([
                'message' => 'One of required params was missed. Please, try again'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->forgotPassword($data['email']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Check forgot password token
     *
     * @Route("/api/forgot-password/{token}", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     description="User confirmation token",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns if the user was found and link was successfully sent"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param SecurityService $securityService
     * @param string $token
     *
     * @return JsonResponse
     */
    public function checkForgotPasswordToken(SecurityService $securityService, string $token) : JsonResponse
    {
        $res = $securityService->checkForgotPasswordToken($token);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Update password
     *
     * @Route("/api/password/{token}", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     description="User confirmation token",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     schema={"string"},
     *     description="new password",
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns if the user was found and link was successfully sent"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Security")
     *
     * @param Request $request
     * @param SecurityService $securityService
     * @param string $token
     *
     * @return JsonResponse
     */
    public function updatePassword(Request $request, SecurityService $securityService, string $token) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || !isset($data['password'])) {
            return new JsonResponse([
                'message' => 'One of required params was missed. Please, try again'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->updatePassword($token, $data['password']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
