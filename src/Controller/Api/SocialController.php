<?php

namespace App\Controller\Api;

use App\Provider\SocialProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class SocialController
 * @package App\Controller\Api
 */
class SocialController extends Controller implements ApiExceptionInterface
{
    /**
     * @Route("/api/fb/check", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="mobileApp",
     *     in="header",
     *     type="boolean",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="authToken",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="firstName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="lastName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Authentication passed successfully",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="client_id",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *               property="client_secret",
     *               type="string"
     *         ),
     *         @SWG\Property(
     *              property="socialToken",
     *              type="string",
     *              description="Social token for getting access and refresh tokens instead of plain password"
     *         ),
     *         @SWG\Property(
     *              property="accountType",
     *              type="string",
     *              description="Can be only buyer"
     *         ),
     *         @SWG\Property(
     *              property="email",
     *              type="string"
     *         )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400, description="Returns when something went wrong"
     * )
     *
     * @SWG\Response(
     *     response=401, description="Returns when user is unauthorized"
     * )
     *
     * @SWG\Tag(name="Social")
     *
     * @param Request $request
     * @param SocialProvider $socialProvider
     *
     * @return JsonResponse
     */
    public function facebookAuth(Request $request, SocialProvider $socialProvider)
    {
        $mobileApp = $request->headers->get('mobileApp', null);

        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['authToken']) || !isset($data['email'])) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        $res = $socialProvider->validate(SocialProvider::FACEBOOK, $data['authToken'], $data['email'], $data, $mobileApp);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * @Route("/api/google/check", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="mobileApp",
     *     in="header",
     *     type="boolean",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="idToken",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="firstName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="lastName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Authentication passed successfully",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="client_id",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *               property="client_secret",
     *               type="string"
     *         ),
     *         @SWG\Property(
     *              property="socialToken",
     *              type="string",
     *              description="Social token for getting access and refresh tokens instead of plain password"
     *         ),
     *         @SWG\Property(
     *              property="accountType",
     *              type="string",
     *              description="Can be only buyer"
     *         ),
     *         @SWG\Property(
     *              property="email",
     *              type="string"
     *         )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400, description="Returns when something went wrong"
     * )
     *
     * @SWG\Response(
     *     response=401, description="Returns when user is unauthorized"
     * )
     *
     * @SWG\Tag(name="Social")
     *
     * @param Request $request
     * @param SocialProvider $socialProvider
     * @return JsonResponse
     */
    public function googleAuth(Request $request, SocialProvider $socialProvider)
    {
        $mobileApp = $request->headers->get('mobileApp', null);

        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['idToken']) || !isset($data['email'])) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        $res = $socialProvider->validate(SocialProvider::GOOGLE, $data['idToken'], $data['email'], $data, $mobileApp);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * @Route("/api/linkedin/check", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="mobileApp",
     *     in="header",
     *     type="boolean",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="code",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     *
     * @SWG\Response(
     *     response=200,
     *     description="Authentication passed successfully",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="client_id",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *               property="client_secret",
     *               type="string"
     *         ),
     *         @SWG\Property(
     *              property="socialToken",
     *              type="string",
     *              description="Social token for getting access and refresh tokens instead of plain password"
     *         ),
     *         @SWG\Property(
     *              property="accountType",
     *              type="string",
     *              description="Can be only buyer"
     *         ),
     *         @SWG\Property(
     *              property="email",
     *              type="string"
     *         )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400, description="Returns when something went wrong"
     * )
     *
     * @SWG\Response(
     *     response=401, description="Returns when user is unauthorized"
     * )
     *
     * @SWG\Tag(name="Social")
     *
     * @param Request $request
     * @param SocialProvider $socialProvider
     * @return JsonResponse
     */
    public function linkedInAuth(Request $request, SocialProvider $socialProvider) : JsonResponse
    {
        $mobileApp = $request->headers->get('mobileApp', null);

        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['code'])) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        $res = $socialProvider->validate(SocialProvider::LINKEDIN, $data['code'], null, $data, $mobileApp);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}