<?php

namespace App\Controller\Api;

use App\Managers\UserApiManager;
use App\Managers\WorkSpaceApiManager;
use App\Security\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class UserController
 * @package App\Controller\Api
 */
class UserController extends Controller implements ApiExceptionInterface
{
    /**
     * Get user details
     *
     * @Route("/api/v1/user", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns user details",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="firstname", type ="string"),
     *          @SWG\Property(property="lastname", type ="string"),
     *          @SWG\Property(property="email", type ="string"),
     *          @SWG\Property(property="phone", type ="string"),
     *          @SWG\Property(property="companyName", type ="string"),
     *          @SWG\Property(property="pushStatus", type="boolean")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function getDetails(UserApiManager $userApiManager)  : JsonResponse
    {
        $res = $userApiManager->getDetails($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Update push notification status
     *
     * @Route("/api/v1/user/push", methods={"PATCH"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when push status updated successfully"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function updatePushNotificationsStatus(UserApiManager $userApiManager)  : JsonResponse
    {
        $res = $userApiManager->triggerPush($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Update user details
     *
     * @Route("/api/v1/user", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="firstname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="lastname",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="company",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when viewing time was added to schedule"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function update(Request $request, UserApiManager $userApiManager) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Nothing to update'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $userApiManager->updateUserData($this->getUser(), $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Like workspace
     *
     * @Route("/api/v1/user/like/{id}", methods={"POST"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when viewing time was added to schedule"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param UserApiManager $userApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function likeWorkspace(UserApiManager $userApiManager, int $id) : JsonResponse
    {
        $res = $userApiManager->likeWorkspace($this->getUser(), $id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Get list of liked workspaces
     *
     * @Route("/api/v1/user/liked", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns results",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="workspaces",
     *             type="array",
     *             @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(
     *                      property="location",
     *                      type="object",
     *                      @SWG\Property(property="id", type ="integer"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="address", type ="string"),
     *                      @SWG\Property(property="latitude", type ="string"),
     *                      @SWG\Property(property="longitude", type ="string")
     *                  ),
     *                  @SWG\Property(
     *                      property="workspace",
     *                      type="object",
     *                      @SWG\Property(property="id", type ="integer"),
     *                      @SWG\Property(property="quantity", type ="integer"),
     *                      @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                      @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                      @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                      @SWG\Property(property="price", type ="integer"),
     *                      @SWG\Property(property="status", type ="string"),
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                      @SWG\Property(property="maxContractLength", required={"false"}, type ="string"),
     *                      @SWG\Property(property="createdAt", type ="number")
     *                  ),
     *                  @SWG\Property(
     *                      property="user",
     *                      type="object",
     *                      @SWG\Property(property="isLiked", type="boolean")
     *                  )
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @return JsonResponse
     * @throws \Exception
     */
    public function getLikedWorkspaces(WorkSpaceApiManager $workSpaceApiManager) : JsonResponse
    {
        $res = $workSpaceApiManager->getLikedWorkspaces($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Delete account
     *
     * @Route("/api/v1/user", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the user was deleted successfully"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function delete(UserApiManager $userApiManager) : JsonResponse
    {
        $res = $userApiManager->delete($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * @Route("/api/v1/user/password", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="oldPassword",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="newPassword",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when the password changed successfully"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Response(
     *     response=401,
     *     description="Return when old password is wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @param SecurityService $securityService
     * @return JsonResponse
     */
    public function updatePassword(Request $request, SecurityService $securityService) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || !isset($data['oldPassword']) || !isset($data['newPassword'])) {
            return new JsonResponse([
                'message' => 'One of required params was missed. Please, try again'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $securityService->changePassword($this->getUser(), $data['oldPassword'], $data['newPassword']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Get seller details
     *
     * @Route("/api/seller/profile", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns user details",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="firstname", type ="string"),
     *          @SWG\Property(property="lastname", type ="string"),
     *          @SWG\Property(property="email", type ="string"),
     *          @SWG\Property(property="phone", type ="string"),
     *          @SWG\Property(property="companyName", type ="string"),
     *          @SWG\Property(property="address", type ="string"),
     *          @SWG\Property(property="optionalAddress", type ="string"),
     *          @SWG\Property(property="town", type ="string"),
     *          @SWG\Property(property="postcode", type ="string"),
     *          @SWG\Property(property="invoiceEmail", type ="string"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function getSellerDetails(UserApiManager $userApiManager)  : JsonResponse
    {
        $res = $userApiManager->getSellerDetails($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Update seller details
     *
     * @Route("/api/seller/profile", methods={"PUT"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns user details",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="firstname", type ="string"),
     *          @SWG\Property(property="lastname", type ="string"),
     *          @SWG\Property(property="email", type ="string"),
     *          @SWG\Property(property="phone", type ="string"),
     *          @SWG\Property(property="companyName", type ="string"),
     *          @SWG\Property(property="address", type ="string"),
     *          @SWG\Property(property="optionalAddress", type ="string"),
     *          @SWG\Property(property="town", type ="string"),
     *          @SWG\Property(property="postcode", type ="string"),
     *          @SWG\Property(property="invoiceEmail", type ="string"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param UserApiManager $userApiManager
     * @return JsonResponse
     */
    public function updateSellerDetails(Request $request, UserApiManager $userApiManager)  : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Nothing to update'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $userApiManager->updateSeller($this->getUser(), $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Add or update user's device information
     *
     * @Route("/api/v1/user/device", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="Bearer %ACCESS_TOKEN%",
     *     in="header",
     *     description="Access token that generated for client",
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="uniqueID",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="model",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="os",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="version",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="token",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="deviceName",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when information was updated successfully",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returned when something went wrong or when device was not found"
     * )
     *
     * @SWG\Response(
     *     response=401,
     *     description="Returns when user unauthorized"
     * )
     *
     * @SWG\Tag(name="Users")
     *
     * @param Request $request
     * @param UserApiManager $userApiManager
     *
     * @throws
     * @return JsonResponse
     */
    public function userDeviceDetails(Request $request,UserApiManager $userApiManager) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }


        if (!isset($data['uniqueID']) || !isset($data['token'])) {
            return new JsonResponse([
                'message' => 'Required data is missing'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $userApiManager->updateUserDevice($this->getUser(), $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
