<?php

namespace App\Controller\Api;

use App\Managers\WorkSpaceApiManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class ViewingController
 * @package App\Controller\Api
 */
class ViewingController extends Controller implements ApiExceptionInterface
{
    /**
     * Add viewing to schedule
     *
     * @Route("/api/v1/workspace/{id}/viewing", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="startTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="endTime",
     *     in="body",
     *     schema={"number"},
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="phone",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns when viewing time was added to schedule",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="message",
     *             type="string"
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Workspace")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function addViewingToSchedule(Request $request, WorkSpaceApiManager $workSpaceApiManager, $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->addViewing($this->getUser(), $id, $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * @Route("/api/seller/workspace/{id}/viewing-status", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true,
     *     description="accepted | declined",
     * )
     *
     * @SWG\Parameter(
     *     name="viewingId",
     *     in="body",
     *     schema={"integer"},
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when status was successfully updated"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function updateStatus(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id)
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['status']) || !isset($data['viewingId'])) {
            return new JsonResponse([
                'message' => 'Required data is missing'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->updateViewingStatus($this->getUser(), $id, intval($data['viewingId']), $data['status']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * @Route("/api/seller/workspace/{id}/viewing-list", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when status was successfully updated",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              property="viewingList",
     *              type ="array",
     *              @SWG\Items (
     *                  @SWG\Property(property="id", type ="number"),
     *                  @SWG\Property(property="startTime", type ="number"),
     *                  @SWG\Property(property="endTime", type ="number"),
     *                  @SWG\Property(property="phone", type ="string"),
     *                  @SWG\Property(property="status", type ="number"),
     *                  @SWG\Property(property="name", type ="string"),
     *                  @SWG\Property(property="email", type ="string"),
     *              )
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @throws
     * @return JsonResponse
     *
     */
    public function getViewingList(WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $res = $workSpaceApiManager->getViewingList($this->getUser(), $id);

        return new JsonResponse(['viewingList' => $res->getData()], $res->getStatus());
    }

    /**
     * @Route("/api/seller/workspace/{id}/booking-list", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when status was successfully updated",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *              property="bookingList",
     *              type ="array",
     *              @SWG\Items (
     *                  @SWG\Property(property="id", type ="number"),
     *                  @SWG\Property(property="startTime", type ="number"),
     *                  @SWG\Property(property="endTime", type ="number"),
     *                  @SWG\Property(property="phone", type ="string"),
     *                  @SWG\Property(property="status", type ="number"),
     *                  @SWG\Property(property="name", type ="string"),
     *                  @SWG\Property(property="email", type ="string"),
     *              )
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @throws
     * @return JsonResponse
     *
     */
    public function getBookingList(WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $res = $workSpaceApiManager->getBookingList($this->getUser(), $id);

        return new JsonResponse(['bookingList' => $res->getData()], $res->getStatus());
    }
}
