<?php

namespace App\Controller\Api;

use App\Managers\BookingApiManager;
use App\Managers\ViewingApiManager;
use App\Managers\WorkSpaceApiManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class WorkSpaceController
 * @package App\Controller\Api
 */
class WorkSpaceController extends Controller implements ApiExceptionInterface
{
    /**
     * Upload images for the workspace
     *
     * @Route("/api/seller/workspace/{id}/media", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     *
     * @SWG\Parameter(
     *     name="image[i]",
     *     in="formData",
     *     type="file"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return when the images were successfully uploaded",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="images",
     *             type ="array",
     *             @SWG\Items(
     *                type="string"
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function addMediaForWorkspace(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $images = $request->files->get('image', false);

        if ($images === false) {
            return new JsonResponse(
                ['message' => 'No media files'],
                Response::HTTP_BAD_REQUEST
            );
        }

        $res = $workSpaceApiManager->addMedias($this->getUser(), $id, $images);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Update workspace
     *
     * @Route("/api/seller/workspace/{id}", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="type",
     *     in="body",
     *     schema={"string"},
     *     description="Type of workspace. Possible options: desc, private, meeting. See the description of the fields for the corresponding types",
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="quantity",
     *     in="body",
     *     schema={"integer"},
     *     description="required for all types",
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="price",
     *     in="body",
     *     schema={"number"},
     *     description="required for all types",
     *     type="number",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="size",
     *     in="body",
     *     schema={"integer"},
     *     description="private-office, meeting-room",
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="capacity",
     *     in="body",
     *     schema={"integer"},
     *     description="private-office, meeting-room",
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="deskType",
     *     in="body",
     *     schema={"string"},
     *     description="One of the options: hourly_hot_desk, monthly_hot_desk, monthly_fixed_desk",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="minContractLength",
     *     in="body",
     *     schema={"integer"},
     *     description="private | desk",
     *     type="integer",
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="facilities",
     *     in="body",
     *     schema={"array"},
     *     type="array",
     *     items=@SWG\Items(type="string"),
     *     required=false
     * )
     *
     * @SWG\Parameter(
     *     name="description",
     *     in="body",
     *     schema={"string"},
     *     description="required for all types",
     *     type="string",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when location was updated"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     *
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateWorkspace(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null || empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->update($this->getUser(), $id, $data);

        return new JsonResponse($res->getData(), $res->getStatus());
    }


    /**
     *
     * Toggle available/unavailable status of workspace
     *
     * @Route("/api/seller/workspace/{id}/status", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     schema={"string"},
     *     type="string",
     *     required=true,
     *     description="active | unavailable",
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when status was successfully updated"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong or user was not found"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateStatus(Request $request, WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['status'])) {
            return new JsonResponse([
                'message' => 'Required data is missing'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $workSpaceApiManager->updateWorkspaceStatus($this->getUser(), $id, $data['status']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Delete workspace
     *
     * @Route("/api/seller/workspace/{id}", methods={"DELETE"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when workspace was removed"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return JsonResponse
     */
    public function deleteWorkspace(WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $res = $workSpaceApiManager->delete($this->getUser(), $id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get workspace date for seller
     *
     * @Route("/api/seller/workspace/{id}", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="id", type="integer"),
     *          @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *          @SWG\Property(property="opensFrom", required={"false"}, type ="string"),
     *          @SWG\Property(property="closesAt", required={"false"}, type ="string"),
     *          @SWG\Property(property="type", type ="string"),
     *          @SWG\Property(property="quantity", type ="integer"),
     *          @SWG\Property(property="price", type ="number"),
     *          @SWG\Property(property="description", type ="string"),
     *          @SWG\Property(property="size", type ="integer", description="private-office, meeting-room"),
     *          @SWG\Property(property="capacity", type ="integer", description="private-office, meeting-room"),
     *          @SWG\Property(property="deskType", type ="string", description="desk"),
     *          @SWG\Property(property="minContractLength", type ="integer", description="private-office"),
     *          @SWG\Property(property="maxContactLength", type ="integer", description="private-office"),
     *          @SWG\Property(
     *              property="images",
     *              type ="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="url", type="string"),
     *              )
     *          )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function getWorkspace(WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $res = $workSpaceApiManager->sellerView($this->getUser(), $id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get workspace data
     *
     * @Route("/api/workspace/{id}", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="location",
     *             type="object",
     *             @SWG\Property(property="id", type ="integer"),
     *             @SWG\Property(property="name", type ="string"),
     *             @SWG\Property(property="status", type ="string"),
     *             @SWG\Property(property="address", type ="string"),
     *             @SWG\Property(property="addressOptional", type ="string"),
     *             @SWG\Property(property="description", type ="string"),
     *             @SWG\Property(property="town", type ="string"),
     *             @SWG\Property(property="postcode", type ="string"),
     *             @SWG\Property(property="latitude", type ="string"),
     *             @SWG\Property(property="longitude", type ="string"),
     *             @SWG\Property(property="videoLink", type ="string"),
     *             @SWG\Property(
     *                  property="nearby",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="distance", type ="string"),
     *                      @SWG\Property(property="duration", type ="string")
     *                  )
     *             )
     *          ),
     *          @SWG\Property(
     *             property="workspace",
     *             type="object",
     *             @SWG\Property(property="id", type ="integer"),
     *             @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *             @SWG\Property(property="quantity", type ="integer"),
     *             @SWG\Property(property="size", required={"false"}, type ="integer"),
     *             @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *             @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *             @SWG\Property(property="price", type ="number"),
     *             @SWG\Property(property="createdAt", type ="number"),
     *             @SWG\Property(property="status", type ="string"),
     *             @SWG\Property(property="description", type ="string"),
     *             @SWG\Property(property="type", type ="string"),
     *             @SWG\Property(property="opensFrom", required={"false"}, type ="string"),
     *             @SWG\Property(property="closesAt", required={"false"}, type ="string"),
     *             @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *             @SWG\Property(property="isLiked", type="boolean"),
     *             @SWG\Property(property="visited", type="boolean"),
     *             @SWG\Property(
     *                  property="images",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="url", type="string"),
     *                  )
     *             ),
     *             @SWG\Property(
     *                  property="facilities",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="slug", type="string"),
     *                      @SWG\Property(property="webIcon", type="string"),
     *                      @SWG\Property(property="appIcon", type="string"),
     *                      @SWG\Property(property="name", type="string"),
     *                  )
     *             )
     *          ),
     *          @SWG\Property(
     *               property="related",
     *               type ="array",
     *               @SWG\Items(
     *                   type="object",
     *                   @SWG\Property(property="id", type ="integer"),
     *                   @SWG\Property(property="quantity", type ="integer"),
     *                   @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                   @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                   @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                   @SWG\Property(property="price", type ="number"),
     *                   @SWG\Property(property="status", type ="string"),
     *                   @SWG\Property(property="description", type ="string"),
     *                   @SWG\Property(property="type", type ="string"),
     *                   @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                   @SWG\Property(property="isLiked", type="boolean"),
     *                   @SWG\Property(property="visited", type="boolean"),
     *                   @SWG\Property(property="availableFrom", type="number")
     *               )
     *           ),
     *           @SWG\Property(
     *                property="bookedDates",
     *                type="array",
     *                @SWG\Items(
     *                    type="object",
     *                    @SWG\Property(property="startTime", type="number"),
     *                    @SWG\Property(property="endTime", type="number")
     *                )
     *           ),
     *           @SWG\Property(
     *                property="user",
     *                type="object",
     *                @SWG\Property(property="id", type ="number"),
     *                @SWG\Property(property="email", type ="string"),
     *                @SWG\Property(property="isLiked", type ="boolean")
     *           )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Workspace")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function viewWorkspace(WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $res = $workSpaceApiManager->view($id, $this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get workspace data for mobile
     *
     * @Route("/api/workspace/{id}/mobile", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="location",
     *             type="object",
     *             @SWG\Property(property="id", type ="integer"),
     *             @SWG\Property(property="name", type ="string"),
     *             @SWG\Property(property="status", type ="string"),
     *             @SWG\Property(property="address", type ="string"),
     *             @SWG\Property(property="addressOptional", type ="string"),
     *             @SWG\Property(property="description", type ="string"),
     *             @SWG\Property(property="town", type ="string"),
     *             @SWG\Property(property="postcode", type ="string"),
     *             @SWG\Property(property="latitude", type ="string"),
     *             @SWG\Property(property="longitude", type ="string"),
     *             @SWG\Property(property="videoLink", type ="string"),
     *             @SWG\Property(
     *                  property="nearby",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="distance", type ="string"),
     *                      @SWG\Property(property="duration", type ="string")
     *                  )
     *             )
     *          ),
     *          @SWG\Property(
     *             property="workspace",
     *             type="object",
     *             @SWG\Property(property="id", type ="integer"),
     *             @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *             @SWG\Property(property="quantity", type ="integer"),
     *             @SWG\Property(property="size", required={"false"}, type ="integer"),
     *             @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *             @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *             @SWG\Property(property="price", type ="number"),
     *             @SWG\Property(property="createdAt", type ="number"),
     *             @SWG\Property(property="status", type ="string"),
     *             @SWG\Property(property="description", type ="string"),
     *             @SWG\Property(property="type", type ="string"),
     *             @SWG\Property(property="opensFrom", required={"false"}, type ="string"),
     *             @SWG\Property(property="closesAt", required={"false"}, type ="string"),
     *             @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *             @SWG\Property(property="isLiked", type="boolean"),
     *             @SWG\Property(property="visited", type="boolean"),
     *             @SWG\Property(
     *                  property="images",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="url", type="string"),
     *                  )
     *             ),
     *             @SWG\Property(
     *                  property="facilities",
     *                  type ="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="slug", type="string"),
     *                      @SWG\Property(property="webIcon", type="string"),
     *                      @SWG\Property(property="appIcon", type="string"),
     *                      @SWG\Property(property="name", type="string"),
     *                  )
     *             )
     *          ),
     *          @SWG\Property(
     *               property="related",
     *               type ="array",
     *               @SWG\Items(
     *                   type="object",
     *                   @SWG\Property(property="id", type ="integer"),
     *                   @SWG\Property(property="quantity", type ="integer"),
     *                   @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                   @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                   @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                   @SWG\Property(property="price", type ="number"),
     *                   @SWG\Property(property="status", type ="string"),
     *                   @SWG\Property(property="description", type ="string"),
     *                   @SWG\Property(property="type", type ="string"),
     *                   @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                   @SWG\Property(property="isLiked", type="boolean"),
     *                   @SWG\Property(property="visited", type="boolean"),
     *                   @SWG\Property(property="availableFrom", type="number")
     *               )
     *           ),
     *           @SWG\Property(
     *                property="bookedDates",
     *                type="array",
     *                @SWG\Items(
     *                    type="object",
     *                    @SWG\Property(property="startTime", type="number"),
     *                    @SWG\Property(property="endTime", type="number")
     *                )
     *           ),
     *           @SWG\Property(
     *                property="user",
     *                type="object",
     *                @SWG\Property(property="id", type ="number"),
     *                @SWG\Property(property="email", type ="string"),
     *                @SWG\Property(property="isLiked", type ="boolean")
     *           )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Workspace")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function viewMobileWorkspace(WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $res = $workSpaceApiManager->mobileView($id, $this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get workspace images
     *
     * @Route("/api/workspace/{id}/images", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="images",
     *              type ="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="url", type="string"),
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Workspace")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function viewWorkspaceImages(WorkSpaceApiManager $workSpaceApiManager, int $id): JsonResponse
    {
        $res = $workSpaceApiManager->workspaceImagesView($id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Get workspace booked dates
     *
     * @Route("/api/workspace/{id}/bookedDates", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="object",
     *           @SWG\Property(
     *                property="bookedDates",
     *                type="array",
     *                @SWG\Items(
     *                    type="object",
     *                    @SWG\Property(property="startTime", type="number"),
     *                    @SWG\Property(property="endTime", type="number")
     *                )
     *           ),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Workspace")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function getBookedDates(WorkSpaceApiManager $workSpaceApiManager, int $id) : JsonResponse
    {
        $res = $workSpaceApiManager->getBookedDates($id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Delete media from workspace
     *
     * @Route("/api/seller/workspace/{id}/media/{mediaId}", methods={"DELETE"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="mediaId",
     *     in="path",
     *     description="Media id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when media was successfully removed"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @param int $mediaId
     *
     * @throws
     *
     * @return JsonResponse
     */
    public function deleteImage(WorkSpaceApiManager $workSpaceApiManager, int $id, int $mediaId): JsonResponse
    {
        $res = $workSpaceApiManager->deleteImage($this->getUser(), $id, $mediaId);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     *
     * Set cover image fot the workspace
     *
     * @Route("/api/seller/workspace/{id}/cover/{mediaId}", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Workspace id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="mediaId",
     *     in="path",
     *     description="Media id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when media was successfully set as cover image",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              type="string",
     *              property="coverImageUrl"
     *          )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns when something went wrong"
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @param int $id
     * @param int $mediaId
     * @return JsonResponse
     * @throws \Exception
     */
    public function setCoverImage(WorkSpaceApiManager $workSpaceApiManager, int $id, int $mediaId): JsonResponse
    {
        $res = $workSpaceApiManager->setCoverImage($this->getUser(), $id, $mediaId);

        return new JsonResponse($res->getData(), $res->getStatus());
    }


    /**
     *
     * list of booking and viewing requests
     *
     * @Route("/api/v1/user/requests", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when workspace was found",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(
     *                  property="booking",
     *                  type ="object",
     *                  required={"false"},
     *                  @SWG\Property(property="id", type ="number"),
     *                  @SWG\Property(property="startTime", type ="number"),
     *                  @SWG\Property(property="createdAt", type ="number"),
     *                  @SWG\Property(property="endTime", type ="number", required={"false"}),
     *                  @SWG\Property(property="amount", type ="number"),
     *                  @SWG\Property(property="status", type="string"),
     *                  @SWG\Property(property="name", type="string"),
     *                  @SWG\Property(property="future", type="boolean"),
     *                  @SWG\Property(property="units", type ="number")
     *              ),
     *              @SWG\Property(
     *                  property="viewing",
     *                  type ="object",
     *                  required={"false"},
     *                  @SWG\Property(property="id", type ="number"),
     *                  @SWG\Property(property="startTime", type ="number"),
     *                  @SWG\Property(property="createdAt", type ="number"),
     *                  @SWG\Property(property="endTime", type ="number", required={"false"}),
     *                  @SWG\Property(property="status", type="string"),
     *                  @SWG\Property(property="future", type="boolean")
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @return JsonResponse
     * @throws \Exception
     */
    public function userRequests(WorkSpaceApiManager $workSpaceApiManager) : JsonResponse
    {
        $res = $workSpaceApiManager->getUserRequests($this->getUser());

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Cancel booking
     *
     * @Route("/api/v1/user/booking/{id}/cancel", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Booking id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns on successful cancellation"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param BookingApiManager $bookingApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function cancelBooking(BookingApiManager $bookingApiManager, int $id) : JsonResponse
    {
        $res = $bookingApiManager->cancelBooking($this->getUser(), $id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Invite attendee to the booking
     *
     * @Route("/api/v1/user/booking/{id}/invite-attendee", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Booking id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="emails",
     *     in="body",
     *     schema={"array"},
     *     type="array",
     *     items=@SWG\Items(type="string"),
     *     required=true,
     *     description="array of emails"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when attendees was successfully invited",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="attendees",
     *             type="array",
     *             @SWG\Items (
     *                 type="string"
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @param BookingApiManager $bookingApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function inviteAttendeeBooking(Request $request, BookingApiManager $bookingApiManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data) || !isset($data['emails'])) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $bookingApiManager->inviteAttendee($this->getUser(), $id, $data['emails']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Invite attendee to the viewing
     *
     * @Route("/api/v1/user/viewing/{id}/invite-attendee", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Viewing id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="emails",
     *     in="body",
     *     schema={"array"},
     *     type="array",
     *     items=@SWG\Items(type="string"),
     *     required=true,
     *     description="array of emails"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns when attendees was successfully invited",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="attendees",
     *             type="array",
     *             @SWG\Items (
     *                 type="string"
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return on failed validation",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="errors",
     *             type ="array",
     *             @SWG\Items (
     *                 @SWG\Property(property="attribute", type ="string"),
     *                 @SWG\Property(property="details", type ="string")
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @param ViewingApiManager $viewingApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function inviteAttendeeViewing(Request $request, ViewingApiManager $viewingApiManager, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data) || !isset($data['emails'])) {
            return new JsonResponse([
                'message' => 'Requested data is empty'
            ], Response::HTTP_BAD_REQUEST);
        }

        $res = $viewingApiManager->inviteAttendee($this->getUser(), $id, $data['emails']);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Cancel viewing
     *
     * @Route("/api/v1/user/viewing/{id}/cancel", methods={"PATCH"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Viewing id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns on successful cancellation"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Return when something went wrong"
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param ViewingApiManager $viewingApiManager
     * @param int $id
     * @return JsonResponse
     */
    public function cancelViewing(ViewingApiManager $viewingApiManager, int $id) : JsonResponse
    {
        $res = $viewingApiManager->cancelViewing($this->getUser(), $id);

        return new JsonResponse($res->getData(), $res->getStatus());
    }

    /**
     * Top workspaces
     *
     * @Route("/api/top-workspaces", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns results",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *             property="results",
     *             type="array",
     *             @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(
     *                      property="location",
     *                      type="object",
     *                      @SWG\Property(property="id", type ="integer"),
     *                      @SWG\Property(property="name", type ="string"),
     *                      @SWG\Property(property="address", type ="string"),
     *                      @SWG\Property(property="latitude", type ="string"),
     *                      @SWG\Property(property="longitude", type ="string")
     *                  ),
     *                  @SWG\Property(
     *                      property="workspace",
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="coverImageUrl", required={"false"}, type ="string"),
     *                      @SWG\Property(property="type", type ="string"),
     *                      @SWG\Property(property="quantity", type ="integer"),
     *                      @SWG\Property(property="price", type ="number"),
     *                      @SWG\Property(property="description", type ="string"),
     *                      @SWG\Property(property="size", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="capacity", type ="integer", description="private-office, meeting-room"),
     *                      @SWG\Property(property="deskType", type ="string", description="desk"),
     *                      @SWG\Property(property="minContractLength", type ="integer", description="private-office"),
     *                      @SWG\Property(property="maxContactLength", type ="integer", description="private-office"),
     *                      @SWG\Property(
     *                          property="images",
     *                          type ="array",
     *                          @SWG\Items(
     *                              type="object",
     *                              @SWG\Property(property="id", type="integer"),
     *                              @SWG\Property(property="url", type="string"),
     *                          )
     *                      )
     *                  ),
     *                  @SWG\Property(
     *                      property="related",
     *                      type ="array",
     *                      @SWG\Items(
     *                          type="object",
     *                          @SWG\Property(property="id", type ="integer"),
     *                          @SWG\Property(property="quantity", type ="integer"),
     *                          @SWG\Property(property="size", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="capacity", required={"false"}, type ="integer"),
     *                          @SWG\Property(property="deskType", required={"false"}, type ="string"),
     *                          @SWG\Property(property="price", type ="number"),
     *                          @SWG\Property(property="status", type ="string"),
     *                          @SWG\Property(property="description", type ="string"),
     *                          @SWG\Property(property="type", type ="string"),
     *                          @SWG\Property(property="minContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="maxContractLength", required={"false"}, type ="string"),
     *                          @SWG\Property(property="isLiked", type="boolean")
     *                      )
     *                  ),
     *                  @SWG\Property(
     *                      property="user",
     *                      type="object",
     *                      @SWG\Property(property="isLiked", type="boolean")
     *                  )
     *             )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="Home page")
     *
     * @param WorkSpaceApiManager $workSpaceApiManager
     * @throws
     * @return JsonResponse
     */
    public function topWorkspaces(WorkSpaceApiManager $workSpaceApiManager) : JsonResponse
    {
        $res = $workSpaceApiManager->getTopBookedWorkspaces();

        return new JsonResponse($res->getData(), $res->getStatus());
    }
}
