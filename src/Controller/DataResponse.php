<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * class for a unified response standard
 *
 * Class DataResponse
 * @package App\Controller
 */
final class DataResponse
{
    /**
     * @var int
     */
    private $status = Response::HTTP_OK;

    /**
     * @var array
     */
    private $data = [];

    /**
     * DataResponse constructor.
     * @param int $status
     * @param array $data
     */
    public function __construct(int $status = 200, $data = [])
    {
        $this->status = $status;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }
}
