<?php

namespace App\Controller;

use App\Managers\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class RequestFileController
 * @package App\Controller
 */
class RequestFileController extends Controller
{

    /**
     *
     * @Route("/export/{id}/booking", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Booking id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Tag(name="Download files")
     *
     * @param int $id
     * @param FileManager $fileManager
     * @return DataResponse|Response
     */
    public function getIcalendarFileBooking(int $id, FileManager $fileManager)
    {
        return $fileManager->generateBookingFile($id);
    }

    /**
     *
     * @Route("/export/{id}/viewing", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Viewing id",
     *     type="integer",
     *     required=true
     * )
     *
     * @SWG\Tag(name="Download files")
     *
     * @param int $id
     * @param FileManager $fileManager
     * @return DataResponse|Response
     */
    public function getIcalendarFileViewing(int $id, FileManager $fileManager)
    {
        return $fileManager->generateViewingFile($id);
    }
}