<?php

namespace App\DataFixtures;

use App\Entity\Area;
use App\Entity\Borough;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BoroughFixture extends Fixture
{

    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $data) {
            $borough = new Borough();
            $borough->setName($data['name']);
            $borough->setStatus(true);
            if (!empty($data['areas'])) {
                foreach ($data['areas'] as $k => $area_data) {
                    $area = $manager->getRepository(Area::class)->findOneBy(['name' => $area_data]);

                    if (empty($area)) {
                        $area = new Area();
                    }

                    $area->setName($area_data);
                    $area->setStatus(true);
                    $area->addBorough($borough);
                    $borough->addArea($area);
                    $manager->persist($area);
                }
            }

            $manager->persist($borough);
            $manager->flush();
        }
    }

    private function getData()
    {
        return [
            [
                'boroughId' => 1,
                'name' => 'Greenwich',
                'areas' => [
                    'Abbey Wood',
                    'Blackheath Royal Standard',
                    'Charlton',
                    'Eltham',
                    'Greenwich',
                    'Horn Park',
                    'Kidbrooke',
                    'Maze Hill',
                    'Middle Park',
                    'New Eltham',
                    'Plumstead',
                    'Shooter\'s Hill',
                    'Thamesmead',
                    'Well Hall',
                    'Westcombe Park',
                    'Woolwich',
                ]
            ],
            [
                'boroughId' => 2,
                'name' => 'Ealing',
                'areas' => [
                    'Bedford Park',
                    'Chiswick',
                    'Ealing',
                    'Greenford',
                    'Hanwell',
                    'Northolt',
                    'Norwood Green',
                    'Park Royal',
                    'Perivale',
                    'Southall',
                    'Sudbury',
                    'West Ealing',
                ]
            ],
            [
                'boroughId' => 3,
                'name' => 'Croydon',
                'areas' => [
                    'Addington',
                    'Addiscombe',
                    'Coombe',
                    'Croydon',
                    'Forestdale',
                    'Kenley',
                    'New Addington',
                    'Norbury',
                    'Old Coulsdon',
                    'Purley',
                    'Riddlesdown',
                    'Sanderstead',
                    'Selhurst',
                    'Selsdon',
                    'Shirley',
                    'South Croydon',
                    'South Norwood',
                    'Thornton Heath',
                    'Upper Norwood',
                    'Waddon',
                    'Woodside'
                ]
            ],
            [
                'boroughId' => 4,
                'name' => 'Bexley',
                'areas' => [
                    'Albany Park',
                    'Barnehurst',
                    'Barnes Cray',
                    'Belvedere',
                    'Bexley',
                    'Bexleyheath',
                    'Blackfen',
                    'Blendon',
                    'Colyers',
                    'Crayford',
                    'Crook Log',
                    'Crossness',
                    'East Wickham',
                    'Erith',
                    'Falconwood',
                    'Foots Cray',
                    'Lamorbey',
                    'Lessness Heath',
                    'Longlands',
                    'North Cray',
                    'North End',
                    'Northumberland Heath',
                    'Ruxley',
                    'Sidcup',
                    'Slade Green',
                    'Thamesmead',
                    'Upper Ruxley',
                    'Upton',
                    'Welling',
                    'West Heath',

                ]
            ],
            [
                'boroughId' => 5,
                'name' => 'Redbridge',
                'areas' => [
                    'Aldborough Hatch',
                    'Barkingside',
                    'Chadwell Heath',
                    'Gants Hill',
                    'Goodmayes',
                    'Hainault',
                    'Ilford',
                    'Loxford',
                    'Newbury Park',
                    'Redbridge',
                    'Seven Kings',
                    'Snaresbrook',
                    'South Woodford',
                    'Wanstead',
                    'Woodford',
                    'Woodford Green'
                ]
            ],
            [
                'boroughId' => 6,
                'name' => 'City',
                'areas' => [
                    'Aldgate',
                    'Barbican',
                    'Blackfriars',
                    'Farringdon',
                    'Temple',

                ]
            ],
            [
                'boroughId' => 7,
                'name' => 'Westminster',
                'areas' => [
                    'Aldwych',
                    'Bayswater',
                    'Belgravia',
                    'Charing Cross',
                    'Chinatown',
                    'Covent Garden',
                    'Knightsbridge',
                    'Lisson Grove',
                    'Little Venice',
                    'Maida Vale',
                    'Marylebone',
                    'Mayfair',
                    'Millbank',
                    'Paddington',
                    'Pimlico',
                    'Soho',
                    'St James\'s',
                    'St John\'s Wood',
                    'Temple',
                    'Westminster'
                ]
            ],
            [
                'boroughId' => 8,
                'name' => 'Brent',
                'areas' => [
                    'Alperton',
                    'Brent Park',
                    'Brondesbury',
                    'Church End',
                    'Cricklewood',
                    'Dollis Hill',
                    'Harlesden',
                    'Kensal Green',
                    'Kenton',
                    'Kilburn',
                    'Kingsbury',
                    'Neasden',
                    'Park Royal',
                    'Preston',
                    'Queen\'s Park',
                    'Queensbury',
                    'Stonebridge',
                    'Sudbury',
                    'Tokyngton',
                    'Wembley',
                    'Wembley Park',
                    'Willesden'
                ]
            ],
            [
                'boroughId' => 9,
                'name' => 'Bromley',
                'areas' => [
                    'Anerley',
                    'Aperfield',
                    'Beckenham',
                    'Bickley',
                    'Biggin Hill',
                    'Bromley',
                    'Bromley Common',
                    'Chelsfield',
                    'Chislehurst',
                    'Coney Hall',
                    'Crystal Palace',
                    'Cudham',
                    'Derry Downs',
                    'Downe',
                    'Eden Park',
                    'Elmers End',
                    'Elmstead',
                    'Goddington',
                    'Hayes',
                    'Hazelwood',
                    'Keston',
                    'Leaves Green',
                    'Locksbottom',
                    'Mottingham',
                    'Orpington',
                    'Penge',
                    'Petts Wood',
                    'Plaistow',
                    'Pratt\'s Bottom',
                    'Ruxley',
                    'Southborough',
                    'St Mary Cray',
                    'St Paul\'s Cray',
                    'Sundridge',
                    'Sydenham',
                    'Upper Ruxley',
                    'West Wickham',
                    'Widmore'
                ]
            ],
            [
                'boroughId' => 10,
                'name' => 'Islington',
                'areas' => [
                    'Angel',
                    'Archway',
                    'Barnsbury',
                    'Canonbury',
                    'Clerkenwell',
                    'De Beauvoir Town',
                    'Farringdon',
                    'Finsbury',
                    'Finsbury Park',
                    'Highbury',
                    'Holloway',
                    'Islington',
                    'King\'s Cross',
                    'Nag\'s Head',
                    'Pentonville',
                    'St Luke\'s',
                    'Tufnell Park',
                    'Upper Holloway',

                ]
            ],
            [
                'boroughId' => 11,
                'name' => 'Havering',
                'areas' => [
                    'Ardleigh Green',
                    'Chase Cross',
                    'Collier Row',
                    'Cranham',
                    'Elm Park',
                    'Emerson Park',
                    'Gallows Corner',
                    'Gidea Park',
                    'Harold Hill',
                    'Harold Park',
                    'Harold Wood',
                    'Havering-atte-Bower',
                    'Hornchurch',
                    'Noak Hill',
                    'North Ockendon',
                    'Rainham',
                    'Romford',
                    'South Hornchurch',
                    'Upminster',
                    'Upminster Bridge',
                    'Wennington',

                ]
            ],
            [
                'boroughId' => 12,
                'name' => 'Barnet',
                'areas' => [
                    'Arkley',
                    'Barnet Gate',
                    'Barnet',
                    'Brent Cross',
                    'Brunswick Park',
                    'Burnt Oak',
                    'Burroughs',
                    'Childs Hill',
                    'Church End',
                    'Cockfosters',
                    'Colindale',
                    'Colney Hatch',
                    'Cricklewood',
                    'East Barnet',
                    'East Finchley',
                    'Edgware',
                    'Finchley',
                    'Friern Barnet',
                    'Golders Green',
                    'Grahame Park',
                    'The Hale',
                    'Hampstead Garden Suburb',
                    'Hendon',
                    'The Hyde',
                    'Mill Hill',
                    'Monken Hadley',
                    'Muswell Hill',
                    'New Barnet',
                    'New Southgate',
                    'North Finchley',
                    'Oakleigh Park',
                    'Osidge',
                    'Temple Fortune',
                    'Totteridge',
                    'West Hendon',
                    'Whetstone',
                    'Woodside Park'
                ]
            ],
            [
                'boroughId' => 13,
                'name' => 'Enfield',
                'areas' => [
                    'Arnos Grove',
                    'Botany Bay',
                    'Brimsdown',
                    'Bulls Cross',
                    'Cockfosters',
                    'Crews Hill',
                    'Edmonton',
                    'Enfield Highway',
                    'Enfield Lock',
                    'Enfield Town',
                    'Enfield Wash',
                    'Freezywater',
                    'Grange Park',
                    'Hadley Wood',
                    'Palmers Green',
                    'Ponders End',
                    'Southgate',
                    'Winchmore Hill'
                ]
            ],
            [
                'boroughId' => 14,
                'name' => 'Wandsworth',
                'areas' => [
                    'Balham',
                    'Battersea',
                    'Clapham',
                    'Earlsfield',
                    'Nine Elms',
                    'Putney',
                    'Roehampton',
                    'Southfields',
                    'Tooting',
                    'Tooting Bec',
                    'Wandsworth'
                ]
            ],
            [
                'boroughId' => 15,
                'name' => 'Southwark',
                'areas' => [
                    'Bankside',
                    'Bermondsey',
                    'Camberwell',
                    'Denmark Hill',
                    'Dulwich',
                    'East Dulwich',
                    'Elephant and Castle',
                    'Kennington',
                    'Newington',
                    'Nunhead',
                    'Peckham',
                    'Rotherhithe',
                    'Surrey Quays',
                    'Sydenham Hill',
                    'Walworth',

                ]
            ],
            [
                'boroughId' => 16,
                'name' => 'Kingston upon Thames',
                'areas' => [
                    'Berrylands',
                    'Chessington',
                    'Coombe',
                    'Hook',
                    'Kingston Vale',
                    'Kingston upon Thames',
                    'Malden Rushett',
                    'Motspur Park',
                    'New Malden',
                    'Norbiton',
                    'Old Malden',
                    'Surbiton',
                    'Tolworth',
                    'Worcester Park'
                ]
            ],
            [
                'boroughId' => 17,
                'name' => 'Tower Hamlets',
                'areas' => [
                    'Bethnal Green',
                    'Blackwall',
                    'Bow',
                    'Bromley',
                    'Cambridge Heath',
                    'Canary Wharf',
                    'Cubitt Town',
                    'Isle of Dogs',
                    'Leamouth',
                    'Limehouse',
                    'Mile End',
                    'Millwall',
                    'Old Ford',
                    'Poplar',
                    'Ratcliff',
                    'Shadwell',
                    'Spitalfields',
                    'Stepney',
                    'Tower Hill',
                    'Wapping',
                    'Whitechapel'
                ]
            ],
            [
                'boroughId' => 18,
                'name' => 'Barking and Dagenham',
                'areas' => [
                    'Barking',
                    'Becontree',
                    'Becontree Heath',
                    'Castle Green',
                    'Chadwell Heath',
                    'Creekmouth',
                    'Dagenham',
                    'Marks Gate',
                    'Rush Green',

                ]
            ],
            [
                'boroughId' => 19,
                'name' => 'Richmond upon Thames',
                'areas' => [
                    'Barnes',
                    'Castelnau',
                    'East Sheen',
                    'Eel Pie Island',
                    'Fulwell',
                    'Ham',
                    'Hampton',
                    'Hampton Hill',
                    'Hampton Wick',
                    'Kew',
                    'Mortlake',
                    'North Sheen',
                    'Petersham',
                    'Richmond',
                    'St Margarets',
                    'Strawberry Hill',
                    'Teddington',
                    'Twickenham',
                    'Whitton'
                ]
            ],
            [
                'boroughId' => 20,
                'name' => 'Newham',
                'areas' => [
                    'Beckton',
                    'Canning Town',
                    'Custom House',
                    'East Ham',
                    'Forest Gate',
                    'Little Ilford',
                    'Manor Park',
                    'Maryland',
                    'North Woolwich',
                    'Plaistow',
                    'Silvertown',
                    'Stratford',
                    'Upton Park',
                    'West Ham'
                ]
            ],
            [
                'boroughId' => 21,
                'name' => 'Sutton',
                'areas' => [
                    'Beddington',
                    'Belmont',
                    'Carshalton',
                    'Cheam',
                    'Sutton',
                    'Wallington',
                    'Worcester Park'
                ]
            ],
            [
                'boroughId' => 22,
                'name' => 'Lewisham',
                'areas' => [
                    'Bellingham',
                    'Blackheath',
                    'Brockley',
                    'Catford',
                    'Chinbrook',
                    'Crofton Park',
                    'Deptford',
                    'Downham',
                    'Forest Hill',
                    'Grove Park',
                    'Hither Green',
                    'Honor Oak',
                    'Ladywell',
                    'Lee',
                    'Lewisham',
                    'New Cross',
                    'Southend',
                    'St Johns',
                    'Sydenham',
                    'Sydenham Hill'
                ]
            ],
            [
                'boroughId' => 23,
                'name' => 'Harrow',
                'areas' => [
                    'Belmont',
                    'Harrow',
                    'Harrow on the Hill',
                    'Harrow Weald',
                    'Hatch End',
                    'Kenton',
                    'North Harrow',
                    'Pinner',
                    'Queensbury',
                    'Rayners Lane',
                    'South Harrow',
                    'Stanmore',
                    'Sudbury',
                    'Wealdstone',
                    'West Harrow',

                ]
            ],
            [
                'boroughId' => 24,
                'name' => 'Camden',
                'areas' => [
                    'Belsize Park',
                    'Bloomsbury',
                    'Camden Town',
                    'Chalk Farm',
                    'Cricklewood',
                    'Fitzrovia',
                    'Frognal',
                    'Gospel Oak',
                    'Hampstead',
                    'Highgate',
                    'Holborn',
                    'Kentish Town',
                    'Kilburn',
                    'King\'s Cross',
                    'Primrose Hill',
                    'Somerstown',
                    'St Giles',
                    'St Pancras',
                    'Swiss Cottage',
                    'West Hampstead',

                ]
            ],
            [
                'boroughId' => 25,
                'name' => 'Haringey',
                'areas' => [
                    'Bounds Green',
                    'Bowes Park',
                    'Crouch End',
                    'Finsbury Park',
                    'Fortis Green',
                    'Harringay',
                    'Hornsey',
                    'Muswell Hill',
                    'Seven Sisters',
                    'South Tottenham',
                    'Stroud Green',
                    'Tottenham',
                    'Tottenham Green',
                    'Tottenham Hale',
                    'Turnpike Lane',
                    'West Green',
                    'Wood Green'
                ]
            ],
            [
                'boroughId' => 26,
                'name' => 'Hounslow',
                'areas' => [
                    'Brentford',
                    'Chiswick',
                    'Cranford',
                    'East Bedfont',
                    'Feltham',
                    'Grove Park',
                    'Gunnersbury',
                    'Hanworth',
                    'Hatton',
                    'Heston',
                    'Hounslow',
                    'Isleworth',
                    'Lampton',
                    'Osterley',
                    'Woodlands'
                ]
            ],
            [
                'boroughId' => 27,
                'name' => 'Lambeth',
                'areas' => [
                    'Brixton',
                    'Clapham',
                    'Gipsy Hill',
                    'Herne Hill',
                    'Kennington',
                    'Lambeth',
                    'Oval',
                    'Stockwell',
                    'Streatham',
                    'Tulse Hill',
                    'Vauxhall',
                    'West Norwood',

                ]
            ],
            [
                'boroughId' => 28,
                'name' => 'Kensington and Chelsea',
                'areas' => [
                    'Brompton',
                    'Chelsea',
                    'Earls Court',
                    'Holland Park',
                    'Kensington',
                    'North Kensington',
                    'Notting Hill',
                    'South Kensington',
                    'West Brompton'
                ]
            ],
            [
                'boroughId' => 29,
                'name' => 'Hammersmith and Fulham',
                'areas' => [
                    'Acton',
                    'Brompton',
                    'Chiswick',
                    'Fulham',
                    'Hammersmith',
                    'Old Oak Common',
                    'Parsons Green',
                    'Sands End',
                    'Shepherd\'s Bush',
                    'West Kensington',
                    'White City',
                    'Wormwood Scrubs'
                ]
            ],
            [
                'boroughId' => 30,
                'name' => 'Waltham Forest',
                'areas' => [
                    'Cann Hall',
                    'Chingford',
                    'Highams Park',
                    'Leyton',
                    'Leytonstone',
                    'Snaresbrook',
                    'Upper Walthamstow',
                    'Walthamstow',
                    'Walthamstow Village'
                ]
            ],
            [
                'boroughId' => 31,
                'name' => 'Merton',
                'areas' => [
                    'Colliers Wood',
                    'Lower Morden',
                    'Merton Park',
                    'Mitcham',
                    'Morden',
                    'Morden Park',
                    'Raynes Park',
                    'South Wimbledon',
                    'St Helier',
                    'Wimbledon'
                ]
            ],
            [
                'boroughId' => 32,
                'name' => 'Hillingdon',
                'areas' => [
                    'Cowley',
                    'Eastcote',
                    'Harefield',
                    'Harlington',
                    'Harmondsworth',
                    'Hayes',
                    'Hillingdon',
                    'Ickenham',
                    'Longford',
                    'Northwood',
                    'Ruislip',
                    'Sipson',
                    'South Ruislip',
                    'Uxbridge',
                    'West Drayton',
                    'Yeading',
                    'Yiewsley'
                ]
            ],
            [
                'boroughId' => 33,
                'name' => 'Hackney',
                'areas' => [
                    'Dalston',
                    'Hackney',
                    'Hackney Central',
                    'Hackney Marshes',
                    'Hackney Wick',
                    'Haggerston',
                    'Homerton',
                    'Hoxton',
                    'Lea Bridge',
                    'Lower Clapton',
                    'Manor House',
                    'Shacklewell',
                    'Shoreditch',
                    'South Hackney',
                    'Stamford Hill',
                    'Stoke Newington',
                    'Upper Clapton',
                    'West Hackney',

                ]
            ],
            [
                'boroughId' => 34,
                'name' => 'Dartford',
                'areas' => [
                    'Dartford'
                ]
            ],
        ];
    }
}