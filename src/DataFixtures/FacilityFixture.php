<?php

namespace App\DataFixtures;

use App\Entity\Facility;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class FacilityFixture
 * @package App\DataFixtures
 */
class FacilityFixture extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $data) {
            $facility = $this->getFacility($data);
            $this->setReference('id_facility_'.$data['iconId'], $facility);

            $manager->persist($facility);
        }

        $manager->flush();
    }

    /**
     * @param $data
     * @return Facility
     */
    private function getFacility($data)
    {
        return (new Facility())
            ->setName($data['name'])
            ->setWebIcon($this->getReference('id_facility_image_' . $data['iconId']))
            ->setAppIcon($this->getReference('id_facility_image_' . $data['iconId']));

    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            FacilityImageFixture::class,
        );
    }

    /**
     * @return array
     */
    private function getData()
    {
        return
            [
                [
                    'name' => 'Pet friendly',
                    'iconId' => 1
                ],
                [
                    'name' => 'Networking events & talks',
                    'iconId' => 2
                ],
                [
                    'name' => 'Disabled Access',
                    'iconId' => 3
                ],
                [
                    'name' => 'Tea & coffee',
                    'iconId' => 4
                ],
                [
                    'name' => '24/7 access',
                    'iconId' => 5
                ],
                [
                    'name' => 'Meeting rooms',
                    'iconId' => 6
                ],
                [
                    'name' => 'Video conferencing',
                    'iconId' => 7
                ],
                [
                    'name' => 'Common area',
                    'iconId' => 8
                ],
                [
                    'name' => 'Bike storage',
                    'iconId' => 9
                ],
                [
                    'name' => 'Cleaning services',
                    'iconId' => 10
                ],
                [
                    'name' => 'Gym',
                    'iconId' => 11
                ],
                [
                    'name' => 'Events areas',
                    'iconId' => 12
                ],
                [
                    'name' => 'Kitchen',
                    'iconId' => 13
                ],
                [
                    'name' => 'Parking',
                    'iconId' => 14
                ],
                [
                    'name' => 'Storage',
                    'iconId' => 15
                ],
                [
                    'name' => 'Childcare',
                    'iconId' => 16
                ],
                [
                    'name' => 'Tele conferencing',
                    'iconId' => 17
                ],
                [
                    'name' => 'TV / monitor / projector',
                    'iconId' => 18
                ],
                [
                    'name' => 'Beer, wine & soft drinks',
                    'iconId' => 19
                ],
                [
                    'name' => 'Showers',
                    'iconId' => 20
                ],
                [
                    'name' => 'Reception',
                    'iconId' => 21
                ],
                [
                    'name' => 'Printing facilities',
                    'iconId' => 22
                ],
                [
                    'name' => 'Whiteboard',
                    'iconId' => 23
                ],
                [
                    'name' => 'Phone booths',
                    'iconId' => 24
                ],
                [
                    'name' => 'Lockable doors',
                    'iconId' => 25
                ],
                [
                    'name' => 'Outdoor space',
                    'iconId' => 26
                ],
                [
                    'name' => 'WiFi',
                    'iconId' => 27
                ],
            ];
    }
}
