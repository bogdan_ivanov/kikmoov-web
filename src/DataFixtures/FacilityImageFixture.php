<?php

namespace App\DataFixtures;

use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class FacilityImageFixture
 * @package App\DataFixtures
 */
class FacilityImageFixture extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager){

        foreach ($this->getImgData() as $img) {
            $media = new Media();
            $media->setProviderName('sonata.media.provider.image');
            $media->setContext('facility');
            $media->setBinaryContent(__DIR__.'/facilityImages/'. $img['icon'] .'.png');
            $media->setEnabled(true);
            $manager->persist($media);
            $manager->flush();
            $this->setReference('id_facility_image_'.$img['iconId'], $media);
        }
    }

    /**
     * @return array
     */
    private function getImgData()
    {
        return
            [
                [
                    'iconId' => 1,
                    'icon' => 'pet-friendly'
                ],
                [
                    'iconId' => 2,
                    'icon' => 'networking-events',
                ],
                [
                    'iconId' => 3,
                    'icon' => 'disabled-access'
                ],
                [
                    'iconId' => 4,
                    'icon' => 'tea-coffee'
                ],
                [
                    'iconId' => 5,
                    'icon' => '7'
                ],
                [
                    'iconId' => 6,
                    'icon' => 'meeting-rooms'
                ],
                [
                    'iconId' => 7,
                    'icon' => 'video-conferancing'
                ],
                [
                    'iconId' => 8,
                    'icon' => 'common-area'
                ],
                [
                    'iconId' => 9,
                    'icon' => 'bike-storage'
                ],
                [
                    'iconId' => 10,
                    'icon' => 'cleaning-services'
                ],
                [
                    'iconId' => 11,
                    'icon' => 'gym'
                ],
                [
                    'iconId' => 12,
                    'icon' => 'events-areas'
                ],
                [
                    'iconId' => 13,
                    'icon' => 'kitchen'
                ],
                [
                    'iconId' => 14,
                    'icon' => 'parking'
                ],
                [
                    'iconId' => 15,
                    'icon' => 'storage'
                ],
                [
                    'iconId' => 16,
                    'icon' => 'child-care'
                ],
                [
                    'iconId' => 17,
                    'icon' => 'tele-conferencing'
                ],
                [
                    'iconId' => 18,
                    'icon' => 'tv'
                ],
                [
                    'iconId' => 19,
                    'icon' => 'beer-wine'
                ],
                [
                    'iconId' => 20,
                    'icon' => 'showers'
                ],
                [
                    'iconId' => 21,
                    'icon' => 'reception'
                ],
                [
                    'iconId' => 22,
                    'icon' => 'printer'
                ],
                [
                    'iconId' => 23,
                    'icon' => 'whiteboard'
                ],
                [
                    'iconId' => 24,
                    'icon' => 'phone-booth'
                ],
                [
                    'iconId' => 25,
                    'icon' => 'lockable-doors'
                ],
                [
                    'iconId' => 26,
                    'icon' => 'outdoor-space'
                ],
                [
                    'iconId' => 27,
                    'icon' => 'wifi'
                ],
            ];
    }

}