<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AreaRepository")
 */
class Area
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $status = false;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"all"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="Location", mappedBy="area")
     */
    private $locations;

    /**
     * @ORM\ManyToMany(targetEntity="Borough", mappedBy="areas")
     */
    private $boroughs;

    public function __toString()
    {
        return is_null($this->name) ? '' : $this->name;
    }

    public function __construct()
    {
        $this->locations = new ArrayCollection();
        $this->boroughs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Area
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus() : ?bool
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return Area
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media|null $image
     * @return Area
     */
    public function setImage(?Media $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param $slug
     * @return Area
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Viewing[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->setArea($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            // set the owning side to null (unless already changed)
            if ($location->getArea() === $this) {
                $location->setArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Borough[]
     */
    public function getBoroughs(): Collection
    {
        return $this->boroughs;
    }

    /**
     * @param Borough $boroughs
     * @return Area
     */
    public function addBorough(Borough $boroughs): self
    {
        if (!$this->boroughs->contains($boroughs)) {
            $this->boroughs[] = $boroughs;
            $boroughs->addArea($this);
        }

        return $this;
    }

    /**
     * @param Borough $boroughs
     * @return $this
     */
    public function removeBorough(Borough $boroughs)
    {
        if ($this->boroughs->contains($boroughs)) {
            $boroughs->removeArea($this);
            $this->boroughs->removeElement($boroughs);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearBoroughs()
    {
        $this->getBoroughs()->clear();

        return $this;
    }
}