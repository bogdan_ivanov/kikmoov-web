<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Booking implements ScheduleStatusOptions, RequestInterface
{
    use TimestampableEntity;

    const FREELANCER = 'freelancer';
    const COMPANY    = 'company';

    const BOOKING = 'booking';
    const BOOKING_REQUEST = 'booking-request';

    const DURATION_NOT_SURE = 'not-sure';
    const DURATION_ONE_MONTH_OR_MORE = 'one-month-or-more';
    const DURATION_SIX_MONTH_OR_MORE = 'six-month-or-more';
    const DURATION_ONE_YEAR_OR_MORE = 'one-year-or-more';

    const NA = 'na';
    const CASH = 'cash';
    const CARD = 'card';

    const PAYMENT_RECEIVED = 'payment-received';
    const FAILED_WITHDRAW  = 'failed-withdraw';

    const VATPercentage = 0.2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\Column(type="string", length=64, options={"default"="booking"})
     */
    private $bookingType = self::BOOKING;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $information;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=64, options={"default"="freelancer"})
     */
    private $tenantType = self::FREELANCER;

    /**
     * @ORM\Column(type="string", length=25, options={"default"="pending"})
     */
    private $status = self::PENDING;

    /**
     * @ORM\Column(type="string", length=10, options={"default"="na"})
     */
    private $paymentType = self::NA;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $paymentLog;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount = 0.0;


    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amountVAT = 0.0;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerId;

    /**
     * @ORM\Column(type="smallint", options={"default"=0})
     */
    private $attempts = 0;

    /**
     * @ORM\Column(type="smallint", options={"default"=1})
     */
    private $units = 1;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $attendees;

    /**
     * @ORM\Column(type="string", length=255, nullable=true )
     */
    private $billingAddress;

    /**
     * @ORM\Column(type="string", length=100, nullable=true )
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true )
     */
    private $postCode;

    /**
     * @var WorkSpace
     * @ORM\ManyToOne(targetEntity="WorkSpace", inversedBy="bookings")
     * @ORM\JoinColumn(name="work_space_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $workSpace;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bookings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var InternalNotification
     * @ORM\OneToOne(targetEntity="InternalNotification")
     * @ORM\JoinColumn(name="internal_notification_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $internalNotification;

    /**
     * @return array
     */
    public static function listOfStatuses() : array
    {
        return [
            self::PENDING,
            self::ACCEPTED,
            self::DECLINED
        ];
    }

    /**
     * @return array
     */
    public static function listUserStatuses() : array
    {
        return [
            self::ACCEPTED,
            self::PAYMENT_RECEIVED
        ];
    }

    /**
     * @return array
     */
    public static function adminStatuses() : array
    {
        return [
            'Pending' => Booking::PENDING,
            'Accepted' => Booking::ACCEPTED,
            'Declined' => Booking::DECLINED,
            'Canceled' => Booking::CANCELED,
            'Payment Received' => Booking::PAYMENT_RECEIVED,
            'Failed Withdraw' => Booking::FAILED_WITHDRAW
        ];
    }

    public static  function mapStatusesInternalNotification() : array
    {
        return [
            InternalNotification::BOOKING_ACCEPTED => self::ACCEPTED,
            InternalNotification::BOOKING_DECLINED => self::DECLINED,
            InternalNotification::BOOKING_REQUESTED => self::PENDING
        ];
    }

    /**
     * @return array
     */
    public static function adminPaymentTypes() : array
    {
        return [
            'NA' => Booking::NA,
            'Cash' => Booking::CASH,
            'Card' => Booking::CARD
        ];
    }

    /**
     * @return array
     */
    public static function adminTenantTypes() : array
    {
        return [
            'Freelancer' => Booking::FREELANCER,
            'Company' => Booking::COMPANY
        ];
    }

    /**
     * @return array
     */
    public static function adminBookingTypes() : array
    {
        return [
            'Booking' => Booking::BOOKING,
            'Booking Request' => Booking::BOOKING_REQUEST
        ];
    }

    /**
     * @return array|null
     */
    public function getCheckedInternalNotification()
    {
        return is_null($this->internalNotification)
            ? null
            :
            [
                'id' => $this->internalNotification->getId(),
                'message' => $this->internalNotification->getMessage(),
                'status' => $this->internalNotification->getStatus()
            ]
        ;
    }

    /**
     * @return string
     */
    public function getTimeForInternalNotification() : string
    {
        $time = "";

        if (is_null($this->getWorkSpace())) {
            return $time;
        }

        if ($this->getWorkSpace()->getType() === WorkSpace::PRIVATE_OFFICE) {
            $time = $this->getStartTime()->format('h:ia');
        } else {
            if (is_null($this->getEndTime())) {
                $time = $this->getStartTime()->format('h:ia');
            } else {
                $time = sprintf('%s to %s', $this->getStartTime()->format('h:ia'), $this->getEndTime()->format('h:ia'));
            }
        }

        return $time;
    }

    public function isFuture(): bool
    {
        return $this->getStartTime()->getTimestamp() >= (new \DateTime())->setTime(0,0,0)->getTimestamp();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBookingType(): ?string
    {
        return $this->bookingType;
    }

    public function setBookingType(string $bookingType): self
    {
        $this->bookingType = $bookingType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getTenantType(): ?string
    {
        return $this->tenantType;
    }

    public function setTenantType(string $tenantType): self
    {
        $this->tenantType = $tenantType;

        return $this;
    }

    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getPaymentLog(): ?string
    {
        return $this->paymentLog;
    }

    public function setPaymentLog(?string $paymentLog): self
    {
        $this->paymentLog = $paymentLog;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmountVAT()
    {
        return $this->amountVAT;
    }

    public function setAmountVAT($amountVAT): self
    {
        $this->amountVAT = $amountVAT;

        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAttempts(): ?int
    {
        return $this->attempts;
    }

    public function setAttempts(int $attempts): self
    {
        $this->attempts = $attempts;

        return $this;
    }

    public function getAttendees(): ?array
    {
        return $this->attendees;
    }

    public function setAttendees(?array $attendees): self
    {
        $this->attendees = $attendees;

        return $this;
    }

    public function getInternalNotification(): ?InternalNotification
    {
        return $this->internalNotification;
    }

    public function setInternalNotification(?InternalNotification $internalNotification): self
    {
        $this->internalNotification = $internalNotification;

        return $this;
    }

    public function getUnits(): ?int
    {
        return $this->units;
    }

    public function setUnits(int $units): self
    {
        $this->units = $units;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddress(): ?string
    {
        return $this->billingAddress;
    }

    /**
     * @param string $billingAddress
     * @return Booking
     */
    public function setBillingAddress(string $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Booking
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return Booking
     */
    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }
}
