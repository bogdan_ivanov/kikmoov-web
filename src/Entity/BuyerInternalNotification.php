<?php

namespace App\Entity;

use App\InternalNotification\PushNotificationType;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class PushNotification
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\BuyerInternalNotificationRepository")
 */
class BuyerInternalNotification
{
    const VIEWING = 'viewing';
    const BOOKING = 'booking';
    const LIKED = 'liked';
    const NOT_USING_APP = 'not-using-app';

    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text")
     */
    private $subject;

    /**
     * @ORM\Column(type="string", length=255, options={"default"="viewing"})
     */
    private $actionType = PushNotificationType::VIEWING_TODAY;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actionGroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isViewed", type="boolean", options={"default":false})
     */
    private $isViewed = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity_id", type="integer", options={"default":null}, nullable=true)
     */
    private $entityId = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\User", inversedBy="pushNotification")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $user;

    /**
     * @return array
     */
    public static function getActionTypes() : array
    {
        return [
            PushNotificationType::VIEWING_ACCEPTED,
            PushNotificationType::BOOKING_ACCEPTED,
            PushNotificationType::VIEWING_TODAY,
            PushNotificationType::DO_NOT_USE_APP_MORE_THAN_MONTH,
            PushNotificationType::EXISTING_SAVED_WORKSPACES,
            PushNotificationType::ONE_DAY_BEFORE_BOOKING_MONTHLY,
            PushNotificationType::ONE_DAY_BEFORE_BOOKING_HOURLY,
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param $message
     * @return BuyerInternalNotification|null
     */
    public function setMessage($message) : ?self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage() : ?string
    {
        return $this->message;
    }

    /**
     * @param $subject
     * @return BuyerInternalNotification
     */
    public function setSubject($subject) : self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject() : ?string
    {
        return $this->subject;
    }

    /**
     * @param $isViewed
     * @return BuyerInternalNotification
     */
    public function setIsViewed($isViewed) : self
    {
        $this->isViewed = $isViewed;

        return $this;
    }

    /**
     * Get isViewed
     *
     * @return boolean
     */
    public function getIsViewed() : bool
    {
        return $this->isViewed;
    }


    /**
     * @param User|null $user
     * @return BuyerInternalNotification
     */
    public function setUser(User $user = null) : self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return null|string
     */
    public function getActionType(): ?string
    {
        return $this->actionType;
    }

    /**
     * @param string $actionType
     * @return BuyerInternalNotification
     */
    public function setActionType(string $actionType): self
    {
        $this->actionType = $actionType;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    /**
     * @param int|null $entityId
     * @return BuyerInternalNotification
     */
    public function setEntityId(?int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getActionGroup(): ?string
    {
        return $this->actionGroup;
    }

    /**
     * @param null|string $actionGroup
     * @return BuyerInternalNotification
     */
    public function setActionGroup(?string $actionGroup): self
    {
        $this->actionGroup = $actionGroup;

        return $this;
    }

}