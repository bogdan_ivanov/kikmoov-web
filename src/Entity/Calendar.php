<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarRepository")
 */
class Calendar
{

    const GOOGLE_CALENDAR = 'google-calendar';
    const OUTLOOK_CALENDAR = 'outlook-calendar';
    const CUSTOM_CALENDAR = 'custom-calendar';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $status = false;

    /**
     * @ORM\Column(type="string", length=25, options={"default"="google-calendar"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="WorkSpace", inversedBy="calendars")
     * @ORM\JoinColumn(name="workspace_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $workspace;

    /**
     * @ORM\OneToMany(targetEntity="CalendarEvent", mappedBy="calendar",  cascade={"all"})
     */
    private $events;

    /**
     * Calendar constructor.
     */
    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return $this->id === null ? 'Company' : $this->getName();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Calendar
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Calendar
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Calendar
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkspace(): ?Workspace
    {
        return $this->workspace;
    }

    /**
     * @param WorkSpace|null $workspace
     * @return Calendar
     */
    public function setWorkspace(?WorkSpace $workspace): self
    {
        $this->workspace = $workspace;

        return $this;
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            'Google Calendar' => self::GOOGLE_CALENDAR,
            'Outlook Calendar' => self::OUTLOOK_CALENDAR,
            'Custom Calendar' => self::CUSTOM_CALENDAR
        ];
    }

    /**
     * @return string
     */
    public static function getCustomTypeLabel(): string
    {
        return self::CUSTOM_CALENDAR;
    }

    /**
     * @return Collection
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(CalendarEvent $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setCalendar($this);
        }

        return $this;
    }

    public function removeEvent(CalendarEvent $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getCalendar() === $this) {
                $event->setCalendar(null);
            }
        }

        return $this;
    }
}