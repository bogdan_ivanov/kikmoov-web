<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarEventRepository")
 */
class CalendarEvent
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $eventId;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $summary;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $htmlLink;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @ORM\ManyToOne(targetEntity="Calendar", inversedBy="events", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $calendar;

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getSummary() : ?string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return CalendarEvent
     */
    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getEventId() : ?string
    {
        return $this->eventId;
    }

    /**
     * @param null|string $eventId
     * @return CalendarEvent
     */
    public function setEventId(?string  $eventId): self
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getHtmlLink() : ?string
    {
        return $this->htmlLink;
    }

    /**
     * @param string $htmlLink
     * @return CalendarEvent
     */
    public function setHtmlLink(?string $htmlLink): self
    {
        $this->htmlLink = $htmlLink;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStart() : ?\DateTimeInterface
    {
        return $this->start;
    }

    /**
     * @param \DateTimeInterface|null $start
     * @return CalendarEvent
     */
    public function setStart(?\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEnd() : ?\DateTimeInterface
    {
        return $this->end;
    }

    /**
     * @param \DateTimeInterface|null $end
     * @return CalendarEvent
     */
    public function setEnd(?\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    /**
     * @param Calendar|null $calendar
     * @return CalendarEvent
     */
    public function setCalendar(?Calendar $calendar): self
    {
        $this->calendar = $calendar;

        return $this;
    }
}