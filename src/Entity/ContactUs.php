<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactUsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContactUs
{
    use TimestampableEntity;

    const PENDING   = 'pending';
    const PROCESSED = 'processed';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, options={"default":"pending"})
     */
    private $status = self::PENDING;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id === null
            ? ''
            : sprintf('%s - %s at (%s)', $this->name, $this->email, $this->getCreatedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @return array
     */
    public static function statusesList() : array
    {
        return [
            'Pending'   => self::PENDING,
            'Processed' => self::PROCESSED
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
