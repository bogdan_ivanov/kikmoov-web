<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FacilityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Facility
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
    * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"all"})
    * @ORM\JoinColumn(name="web_icon_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
    */
    private $webIcon;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"all"})
     * @ORM\JoinColumn(name="app_icon_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $appIcon;

    /**
     * @ORM\Column(type="boolean", options={"default":1})
     */
    private $isActive = true;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name === null ? '' : $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getWebIcon(): ?Media
    {
        return $this->webIcon;
    }

    public function setWebIcon(?Media $webIcon): self
    {
        $this->webIcon = $webIcon;

        return $this;
    }

    public function getAppIcon(): ?Media
    {
        return $this->appIcon;
    }

    public function setAppIcon(?Media $appIcon): self
    {
        $this->appIcon = $appIcon;

        return $this;
    }
}
