<?php

namespace App\Entity;

use Sonata\MediaBundle\Entity\BaseGallery as BaseGallery;
use Sonata\MediaBundle\Model\GalleryHasMediaInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Gallery
 * @ORM\Entity()
 * @ORM\Table(name="gallery")
 */
class Gallery extends BaseGallery
{
    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function addGalleryHasMedia($galleryHasMedia)
    {
        // TODO: Implement addGalleryHasMedia() method.
    }

    public function removeGalleryHasMedia($galleryHasMedia)
    {
        // TODO: Implement removeGalleryHasMedia() method.
    }
}
