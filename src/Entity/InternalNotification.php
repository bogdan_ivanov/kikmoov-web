<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InternalNotificationRepository")
 */
class InternalNotification
{
    use TimestampableEntity;

    const VIEWING_REQUESTED = 'VIEWING_REQUESTED';
    const VIEWING_ACCEPTED  = 'VIEWING_ACCEPTED';
    const VIEWING_DECLINED  = 'VIEWING_DECLINED';
    const BOOKING_REQUESTED = 'BOOKING_REQUESTED';
    const BOOKING_ACCEPTED  = 'BOOKING_ACCEPTED';
    const BOOKING_DECLINED  = 'BOOKING_DECLINED';

    const VIEWING = 'viewing';
    const BOOKING = 'booking';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, options={"default"="viewing"})
     */
    private $type = self::VIEWING;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="internalNotifications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public static function getStatuses() : array
    {
        return [
            self::VIEWING_REQUESTED,
            self::VIEWING_ACCEPTED,
            self::VIEWING_DECLINED,
            self::BOOKING_REQUESTED,
            self::BOOKING_ACCEPTED,
            self::BOOKING_DECLINED
        ];
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
