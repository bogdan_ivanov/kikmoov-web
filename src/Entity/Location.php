<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use App\Validators\Constraints as ValidationAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Location implements WorkSpaceType
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $optionalAddress;

    /**
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="locations")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    /**
     * @Assert\NotBlank()
     * @ValidationAssert\Latitude
     * @ORM\Column(type="string", length=15)
     */
    private $latitude;

    /**
     * @Assert\NotBlank()
     * @ValidationAssert\Longitude
     * @ORM\Column(type="string", length=15)
     */
    private $longitude;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $nearby;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=64)
     */
    private $town;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=10)
     */
    private $postcode;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $videoLink;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $workSpaceTypes = [];

    /**
     * @ORM\OneToMany(targetEntity="WorkSpace", mappedBy="location")
     */
    private $workspaces;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="locations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $isVisited = false;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company;

    public function __construct()
    {
        $this->workspaces = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return is_null($this->name) ? '' : sprintf('%s (id: %d)', $this->name, $this->id);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getOptionalAddress(): ?string
    {
        return $this->optionalAddress;
    }

    public function setOptionalAddress(?string $optionalAddress): self
    {
        $this->optionalAddress = $optionalAddress;

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWorkSpaceTypes(): ?array
    {
        return $this->workSpaceTypes;
    }

    public function setWorkSpaceTypes(?array $workSpaceTypes): self
    {
        $this->workSpaceTypes = $workSpaceTypes;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|WorkSpace[]
     */
    public function getWorkspaces(): Collection
    {
        return $this->workspaces;
    }

    public function addWorkspace(WorkSpace $workspace): self
    {
        if (!$this->workspaces->contains($workspace)) {
            $this->workspaces[] = $workspace;
            $workspace->setLocation($this);
        }

        return $this;
    }

    public function removeWorkspace(WorkSpace $workspace): self
    {
        if ($this->workspaces->contains($workspace)) {
            $this->workspaces->removeElement($workspace);
            // set the owning side to null (unless already changed)
            if ($workspace->getLocation() === $this) {
                $workspace->setLocation(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVideoLink(): ?string
    {
        return $this->videoLink;
    }

    public function setVideoLink(?string $videoLink): self
    {
        $this->videoLink = $videoLink;

        return $this;
    }

    public function getNearby(): ?array
    {
        return $this->nearby;
    }

    public function setNearby(?array $nearby): self
    {
        $this->nearby = $nearby;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getisVisited()
    {
        return $this->isVisited;
    }

    /**
     * @param $isVisited
     * @return Location
     */
    public function setIsVisited($isVisited): self
    {
        $this->isVisited = $isVisited;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCompany(): ?string
    {
        return $this->company;
    }

    /**
     * @param null|string $company
     * @return Location
     */
    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressLabel() : string
    {
        return implode(', ', [
                $this->getAddress(),
                $this->getTown(),
                $this->getPostcode()
            ]
        );
    }
}
