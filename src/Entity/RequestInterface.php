<?php

namespace App\Entity;

/**
 * Interface RequestInterface
 * @package App\Entity
 */
interface RequestInterface
{
    /**
     * @return InternalNotification|null
     */
    public function getInternalNotification(): ?InternalNotification;

    /**
     * @param InternalNotification|null $internalNotification
     * @return RequestInterface
     */
    public function setInternalNotification(?InternalNotification $internalNotification);
}
