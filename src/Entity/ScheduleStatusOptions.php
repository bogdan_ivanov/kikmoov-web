<?php

namespace App\Entity;

/**
 * Interface ScheduleStatusOptions
 * @package App\Entity
 */
interface ScheduleStatusOptions
{
    const PENDING  = 'pending';
    const ACCEPTED = 'accepted';
    const DECLINED = 'declined';
    const CANCELED = 'canceled';
}
