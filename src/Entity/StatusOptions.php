<?php

namespace App\Entity;

/**
 * Interface StatusOptions
 * @package App\Entity
 */
interface StatusOptions
{
    const ACTIVE = 'active';
    const PENDING = 'pending';
    const UNAVAILABLE = 'unavailable';
    const INCOMPLETE = 'incomplete';
}
