<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\GroupInterface;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Sonata\UserBundle\Entity\BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    const ROLE_SELLER = 'ROLE_SELLER';

    const SELLER = 'seller';
    const BUYER  = 'buyer';

    //priority statuses
    const ACCOUNT_CREATED     = 1;
    const SEARCH_ON_GOING     = 2;
    const BOOKED_VIEWING      = 3; //made a viewing request
    const VIEWING_ON_GOING    = 4; //at date of viewing date
    const VIEWING_COMPLETED   = 5; //at time of viewing
    const NEGOTIATION_STARTED = 6;
    const BOOKING_REQUESTED   = 7; //booked a workspace (not booking request)
    const BOOKING_ON_GOING    = 8; //at date of booking
    const BOOKING_COMPLETED   = 9; //at time of booking
    const HAS_SUCCESSFULLY_UPLOADED = 10; //for seller (at least one workspace)
    const DRAFT_STARTED       = 11; // for seller when he start to create workspace

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="created_from_app", type="boolean", options={"default":0})
     */
    protected $createdFromApp = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $optionalAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $invoiceEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedInUid;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $linkedInData;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialPassword;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $pushStatus = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="smallint", options={"default":1})
     */
    private $status = self::ACCOUNT_CREATED;

    /**
     * related to status
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdated;

    /**
     * @ORM\ManyToMany(targetEntity="Group", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="user_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $groups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Viewing", mappedBy="user")
     */
    private $viewings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="user")
     */
    private $bookings;

    /**
     * @ORM\ManyToMany(targetEntity="WorkSpace", cascade={"persist"})
     * @ORM\JoinTable(name="liked_workspaces",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="work_space_id", referencedColumnName="id")}
     * )
     */
    protected $likedWorkspaces;

    /**
     * @ORM\OneToMany(targetEntity="Location", mappedBy="user")
     */
    private $locations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InternalNotification", mappedBy="user")
     */
    private $internalNotifications;

    /**
     *
     * @ORM\ManyToMany(targetEntity="\App\Entity\UserDevice", inversedBy="users", indexBy="id")
     * @ORM\JoinTable(
     *      name="user_to_device",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private $devices;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->groups = new ArrayCollection();
        $this->viewings = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->likedWorkspaces = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->internalNotifications = new ArrayCollection();
        $this->devices = new ArrayCollection();
    }

    /**
     * @return array
     */
    public static function getStatusesList() : array
    {
        return [
            'Account created' => self::ACCOUNT_CREATED,
            'Search on-going' => self::SEARCH_ON_GOING,
            'Booked viewing' => self::BOOKED_VIEWING,
            'Viewing on-going' => self::VIEWING_ON_GOING,
            'Viewing completed' => self::VIEWING_COMPLETED,
            'Negotiation started' => self::NEGOTIATION_STARTED,
            'Booking requested' => self::BOOKING_REQUESTED,
            'Booking on-going' => self::BOOKING_ON_GOING,
            'Booking completed' => self::BOOKING_COMPLETED,
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesListSeller() : array
    {
        return [
            'Account created' => self::ACCOUNT_CREATED,
            'Has successfully uploaded' => self::HAS_SUCCESSFULLY_UPLOADED,
            'Draft started' => self::DRAFT_STARTED,
        ];
    }

    /**
     * @return string
     */
    public function getStatusAsString() : string
    {
        $statusText = "";

        switch ($this->status) {
            case self::ACCOUNT_CREATED:
                $statusText = 'Account created';
                break;
            case self::SEARCH_ON_GOING:
                $statusText = 'Search on-going';
                break;
            case self::BOOKED_VIEWING:
                $statusText = 'Booked viewing';
                break;
            case self::VIEWING_ON_GOING:
                $statusText = 'Viewing on-going';
                break;
            case self::VIEWING_COMPLETED:
                $statusText = 'Viewing completed';
                break;
            case self::NEGOTIATION_STARTED:
                $statusText = 'Negotiation started';
                break;
            case self::BOOKING_REQUESTED:
                $statusText = 'Booking requested';
                break;
            case self::BOOKING_ON_GOING:
                $statusText = 'Booking on-going';
                break;
            case self::BOOKING_COMPLETED:
                $statusText = 'Booking completed';
                break;
            case self::HAS_SUCCESSFULLY_UPLOADED:
                $statusText = 'Has successfully uploaded';
                break;
            case self::DRAFT_STARTED:
                $statusText = 'Draft started';
                break;
        }

        return $statusText;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param $email
     * @return BaseUser
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedFromApp(): ?bool
    {
        return $this->createdFromApp;
    }

    public function setCreatedFromApp(bool $createdFromApp): self
    {
        $this->createdFromApp = $createdFromApp;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(GroupInterface $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(GroupInterface $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }

    /**
     * @return Collection|WorkSpace[]
     */
    public function getLikedWorkspaces(): Collection
    {
        return $this->likedWorkspaces;
    }

    public function addLikedWorkspace(WorkSpace $likedWorkspace): self
    {
        if (!$this->likedWorkspaces->contains($likedWorkspace)) {
            $this->likedWorkspaces[] = $likedWorkspace;
        }

        return $this;
    }

    public function removeLikedWorkspace(WorkSpace $likedWorkspace): self
    {
        if ($this->likedWorkspaces->contains($likedWorkspace)) {
            $this->likedWorkspaces->removeElement($likedWorkspace);
        }

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->setUser($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            // set the owning side to null (unless already changed)
            if ($location->getUser() === $this) {
                $location->setUser(null);
            }
        }

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getOptionalAddress(): ?string
    {
        return $this->optionalAddress;
    }

    public function setOptionalAddress(?string $optionalAddress): self
    {
        $this->optionalAddress = $optionalAddress;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getInvoiceEmail(): ?string
    {
        return $this->invoiceEmail;
    }

    public function setInvoiceEmail(?string $invoiceEmail): self
    {
        $this->invoiceEmail = $invoiceEmail;

        return $this;
    }

    public function getLinkedInUid(): ?string
    {
        return $this->linkedInUid;
    }

    public function setLinkedInUid(?string $linkedInUid): self
    {
        $this->linkedInUid = $linkedInUid;

        return $this;
    }

    public function getSocialPassword(): ?string
    {
        return $this->socialPassword;
    }

    public function setSocialPassword(?string $socialPassword): self
    {
        $this->socialPassword = $socialPassword;

        return $this;
    }

    public function getLinkedInData(): ?array
    {
        return $this->linkedInData;
    }

    public function setLinkedInData(?array $linkedInData): self
    {
        $this->linkedInData = $linkedInData;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getPushStatus(): ?bool
    {
        return $this->pushStatus;
    }

    public function setPushStatus(bool $pushStatus): self
    {
        $this->pushStatus = $pushStatus;

        return $this;
    }

    /**
     * @return Collection|Viewing[]
     */
    public function getViewings(): Collection
    {
        return $this->viewings;
    }

    public function addViewing(Viewing $viewing): self
    {
        if (!$this->viewings->contains($viewing)) {
            $this->viewings[] = $viewing;
            $viewing->setUser($this);
        }

        return $this;
    }

    public function removeViewing(Viewing $viewing): self
    {
        if ($this->viewings->contains($viewing)) {
            $this->viewings->removeElement($viewing);
            // set the owning side to null (unless already changed)
            if ($viewing->getUser() === $this) {
                $viewing->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setUser($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getUser() === $this) {
                $booking->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InternalNotification[]
     */
    public function getInternalNotifications(): Collection
    {
        return $this->internalNotifications;
    }

    public function addInternalNotification(InternalNotification $internalNotification): self
    {
        if (!$this->internalNotifications->contains($internalNotification)) {
            $this->internalNotifications[] = $internalNotification;
            $internalNotification->setUser($this);
        }

        return $this;
    }

    public function removeInternalNotification(InternalNotification $internalNotification): self
    {
        if ($this->internalNotifications->contains($internalNotification)) {
            $this->internalNotifications->removeElement($internalNotification);
            // set the owning side to null (unless already changed)
            if ($internalNotification->getUser() === $this) {
                $internalNotification->setUser(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        $this->setLastUpdated(new \DateTime());

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeInterface
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(?\DateTimeInterface $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return Collection|UserDevice[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(UserDevice $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
        }

        return $this;
    }

    public function removeDevice(UserDevice $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
        }

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() : void
    {
        $this->lastUpdated = new \DateTime();
    }
}
