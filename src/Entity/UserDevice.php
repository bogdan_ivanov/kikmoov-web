<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserDevice
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserDeviceRepository")
 * @ORM\Table(name="device")
 */
class UserDevice
{
    use TimestampableEntity;

    const STATE_PRODUCTION = 1;
    const STATE_DEVELOPMENT = 0;

    const DEVICE_STATUS_ACTIVE  = 1;
    const DEVICE_STATUS_DISABLED  = 2;
    const DEVICE_STATUS_REMOVED = 0;

    const IOS = 'ios';
    const ANDROID = 'android';

    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="device_name", type="string", length=255, nullable=true)
     */
    private $deviceName;

    /**
     * @Required()
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=255)
     */
    private $deviceToken;

    /**
     * @Required()
     * @var string
     *
     * @ORM\Column(name="device_os", type="string", length=16)
     */
    private $deviceOS;

    /**
     * @var string
     *
     * @ORM\Column(name="device_model", type="string", length=255, nullable=true)
     */
    private $deviceModel;

    /**
     * @var string
     *
     * @ORM\Column(name="device_identifier", type="string", length=255, nullable=true)
     */
    private $deviceIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_authorization", type="datetime")
     */
    private $lastAuthorization;

    /**
     * @ORM\Column(name="count_of_authorizations", type="smallint")
     */
    private $countOfAuthorizations = 0;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User", mappedBy="devices")
     */
    private $users;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->deviceName . ' (' . $this->deviceModel . ')';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @param User $users
     * @return $this
     */
    public function addUser(User $users)
    {
        $users->addDevice($this);
        $this->users[] = $users;

        return $this;
    }

    /**
     * @param User $users
     */
    public function removeUser(User $users)
    {
        $users->removeDevice($this);
        $this->users->removeElement($users);
    }

    /**
     * @return array
     */
    public static function deviceOS()
    {
        return [
            self::ANDROID => 'Android',
            self::IOS     => 'iOS'
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     *
     * @return UserDevice
     */
    public function setDeviceName($deviceName) : ?self
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName() : ?string
    {
        return $this->deviceName;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return UserDevice
     */
    public function setDeviceToken($deviceToken) : self
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken() : ?string
    {
        return $this->deviceToken;
    }

    /**
     * Set deviceModel
     *
     * @param string $deviceModel
     *
     * @return UserDevice
     */
    public function setDeviceModel($deviceModel) : self
    {
        $this->deviceModel = $deviceModel;

        return $this;
    }

    /**
     * Get deviceModel
     *
     * @return string
     */
    public function getDeviceModel() : ?string
    {
        return $this->deviceModel;
    }

    /**
     * Set deviceIdentifier
     *
     * @param string $deviceIdentifier
     *
     * @return UserDevice
     */
    public function setDeviceIdentifier($deviceIdentifier) : self
    {
        $this->deviceIdentifier = $deviceIdentifier;

        return $this;
    }

    /**
     * Get deviceIdentifier
     *
     * @return string
     */
    public function getDeviceIdentifier() : ?string
    {
        return $this->deviceIdentifier;
    }

    /**
     * Set lastAuthorization
     *
     * @param \DateTime $lastAuthorization
     *
     * @return UserDevice
     */
    public function setLastAuthorization($lastAuthorization) : self
    {
        $this->lastAuthorization = $lastAuthorization;

        return $this;
    }

    /**
     * Get lastAuthorization
     *
     * @return \DateTime
     */
    public function getLastAuthorization() : ?\DateTimeInterface
    {
        return $this->lastAuthorization;
    }

    /**
     * Set countOfAuthorizations
     *
     * @param integer $countOfAuthorizations
     *
     * @return UserDevice
     */
    public function setCountOfAuthorizations($countOfAuthorizations) : self
    {
        $this->countOfAuthorizations = $countOfAuthorizations;

        return $this;
    }

    /**
     * Get countOfAuthorizations
     *
     * @return integer
     */
    public function getCountOfAuthorizations() : ?int
    {
        return $this->countOfAuthorizations;
    }

    /**
     * Set deviceOS
     *
     * @param string $deviceOS
     *
     * @return UserDevice
     */
    public function setDeviceOS($deviceOS) : self
    {
        $this->deviceOS = $deviceOS;

        return $this;
    }

    /**
     * Get deviceOS
     *
     * @return string
     */
    public function getDeviceOS() : ?string
    {
        return $this->deviceOS;
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return $this
     */
    public function clearUsers()
    {
        $this->getUsers()->clear();

        return $this;
    }


    /**
     * @return string
     */
    public function getVersion() : ?string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return $this
     */
    public function setVersion(string $version) : self
    {
        $this->version = $version;

        return $this;
    }
}