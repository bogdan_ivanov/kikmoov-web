<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ViewingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Viewing implements ScheduleStatusOptions, RequestInterface
{
    use TimestampableEntity;

    const BOOKED = 'booked';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25, options={"default"="pending"})
     */
    private $status = self::PENDING;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $attendees;

    /**
     * @var WorkSpace
     * @ORM\ManyToOne(targetEntity="WorkSpace", inversedBy="viewings")
     * @ORM\JoinColumn(name="work_space_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $workSpace;

    /**
     * @var WorkSpace
     * @ORM\ManyToOne(targetEntity="User", inversedBy="viewings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var InternalNotification
     * @ORM\OneToOne(targetEntity="InternalNotification")
     * @ORM\JoinColumn(name="internal_notification_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $internalNotification;

    /**
     * @return array
     */
    public static function listOfStatuses() : array
    {
        return [
            self::PENDING,
            self::ACCEPTED,
            self::DECLINED
        ];
    }

    /**
     * @return array
     */
    public static function adminStatuses() : array
    {
        return [
            'Pending' => self::PENDING,
            'Accepted' => self::ACCEPTED,
            'Declined' => self::DECLINED,
            'Booked' => self::BOOKED
        ];
    }

    /**
     * @return array
     */
    public static  function mapStatusesInternalNotification() : array
    {
        return [
            InternalNotification::VIEWING_ACCEPTED => self::ACCEPTED,
            InternalNotification::VIEWING_DECLINED => self::DECLINED,
            InternalNotification::VIEWING_REQUESTED => self::PENDING
        ];
    }

    /**
     * @return array|null
     */
    public function getCheckedInternalNotification()
    {
        return is_null($this->internalNotification)
            ? null
            :
            [
                'id' => $this->internalNotification->getId(),
                'message' => $this->internalNotification->getMessage(),
                'status' => $this->internalNotification->getStatus()
            ]
        ;
    }

    /**
     * @return string
     */
    public function getTimeForInternalNotification() : string
    {
        $time = "";

        if (is_null($this->getWorkSpace())) {
            return $time;
        }

        if ($this->getWorkSpace()->getType() === WorkSpace::PRIVATE_OFFICE) {
            $time = $this->getStartTime()->format('h:ia');
        } else {
            if (is_null($this->getEndTime())) {
                $time = $this->getStartTime()->format('h:ia');
            } else {
                $time = sprintf('%s to %s', $this->getStartTime()->format('h:ia'), $this->getEndTime()->format('h:ia'));
            }
        }

        return $time;
    }

    public function isFuture(): bool
    {
        return $this->getStartTime()->getTimestamp() >= (new \DateTime())->setTime(0,0,0)->getTimestamp();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWorkSpace(): ?WorkSpace
    {
        return $this->workSpace;
    }

    public function setWorkSpace(?WorkSpace $workSpace): self
    {
        $this->workSpace = $workSpace;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getInternalNotification(): ?InternalNotification
    {
        return $this->internalNotification;
    }

    public function setInternalNotification(?InternalNotification $internalNotification): self
    {
        $this->internalNotification = $internalNotification;

        return $this;
    }

    public function getAttendees(): ?array
    {
        return $this->attendees;
    }

    public function setAttendees(?array $attendees): self
    {
        $this->attendees = $attendees;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail(?string $email) : self
    {
        $this->email = $email;

        return $this;
    }
}
