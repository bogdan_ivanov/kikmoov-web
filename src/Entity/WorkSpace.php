<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use App\Validators\Constraints\Time as TimeValidator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkSpaceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WorkSpace implements WorkSpaceType, StatusOptions
{
    use TimestampableEntity;

    const HOURLY_HOT_DESK = 'hourly_hot_desk';
    const MONTHLY_HOT_DESK = 'monthly_hot_desk';
    const MONTHLY_FIXED_DESK = 'monthly_fixed_desk';
    const DAILY_HOT_DESK = 'daily_hot_desk';

    const HOURLY_MEETING_ROOM = 'hourly_meeting_room';
    const DAILY_MEETING_ROOM = 'daily_meeting_room';

    const MONTHLY = 'monthly';
    const HOURLY = 'hourly';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, options={"default"="desk"})
     */
    private $type = self::DESK;

    /**
     * @ORM\Column(type="string", length=25, options={"default"="pending"})
     */
    private $status = self::PENDING;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $deskType;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $capacity;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minContractLength;

    /**
     * @Assert\Url
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $coverImageUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $coverImageId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $availableFrom;

    /**
     * @TimeValidator
     * @ORM\Column(type="string", nullable=true)
     */
    private $opensFrom;

    /**
     * @TimeValidator
     * @ORM\Column(type="string", nullable=true)
     */
    private $closesAt;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $visited = false;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $topPick = false;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Facility", cascade={"persist"})
     * @ORM\JoinTable(name="work_space_facilities",
     *     joinColumns={ @ORM\JoinColumn(name="work_space_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={ @ORM\JoinColumn(name="facility_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     *
     */
    private $facilities;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="work_space_medias",
     *      joinColumns={ @ORM\JoinColumn(name="work_space_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={ @ORM\JoinColumn(name="media_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *)
     */
    private $medias;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Viewing", mappedBy="workSpace")
     */
    private $viewings;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="workSpace")
     */
    private $bookings;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="workspaces")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $location;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity="Calendar", mappedBy="workspace",  cascade={"persist"})
     */
    private $calendars;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleCalendarId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outlookCalendarId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outlookCalendarName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sort;

    /**
     * WorkSpace constructor.
     */
    public function __construct()
    {
        $this->facilities = new ArrayCollection();
        $this->medias = new ArrayCollection();
        $this->viewings = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->calendars = new ArrayCollection();
    }

    /**
     * @ORM\PreRemove()
     */
    public function setCreatedAtValue()
    {
        if (!$this->facilities->isEmpty()) {
            $this->facilities->clear();
        }
    }

    /**
     * @return array
     */
    public static function getListOfStatuses() : array
    {
        return [
            'Active' => self::ACTIVE,
            'Pending' => self::PENDING,
            'Unavailable' => self::UNAVAILABLE,
            'Incomplete' => self::INCOMPLETE
        ];
    }

    /**
     * @return array
     */
    public static function getPermissibleStatusesList() : array
    {
        return [
            'Active' => self::ACTIVE,
            'Unavailable' => self::UNAVAILABLE
        ];
    }

    /**
     * @return array
     */
    public static function getWorkspaceTypes()
    {
        return [
            self::PRIVATE_OFFICE,
            self::DESK,
            self::MEETING_ROOM,
        ];
    }

    public static function monthlyWorkspaceTypes()
    {
        return [
            WorkSpace::DESK,
            WorkSpace::PRIVATE_OFFICE
        ];
    }

    public static function hourlyWorkspaceTypes()
    {
        return [
            WorkSpace::DESK,
            WorkSpace::MEETING_ROOM
        ];
    }

    public static function monthlyWorkspaceDesks()
    {
        return [
            WorkSpace::MONTHLY_HOT_DESK,
            WorkSpace::MONTHLY_FIXED_DESK
        ];
    }

    /**
     * @return string
     */
    public function getTypeLabel() : string
    {
        if ($this->type === self::MEETING_ROOM) {
            return 'Meeting room';
        } elseif ($this->type === self::DESK) {
            return 'Desk';
        } elseif ($this->type === self::PRIVATE_OFFICE) {
            return 'Private office';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeskTypeLabel() : string
    {
        if ($this->type === self::DESK) {
            if ($this->getDeskType() === self::HOURLY_HOT_DESK) {
                return 'Hourly hot desk';
            } elseif ($this->getDeskType() === self::MONTHLY_FIXED_DESK) {
                return 'Monthly Fixed Desk';
            } elseif ($this->getDeskType() === self::MONTHLY_HOT_DESK) {
                return 'Monthly Hot Desk';
            } elseif ($this->getDeskType() === self::DAILY_HOT_DESK) {
                return 'Daily Hot Desk';
            }
        }

        if ($this->type === self::MEETING_ROOM) {
            if ($this->getDeskType() === self::HOURLY_MEETING_ROOM) {
                return 'Hourly meeting room';
            } elseif ($this->getDeskType() === self::DAILY_MEETING_ROOM) {
                return 'Daily meeting room';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getHourlyDeskTypeLabel() : string
    {
        if ($this->type === self::DESK) {
            if ($this->getDeskType() === self::HOURLY_HOT_DESK) {
                return 'Hourly desk';
            } elseif ($this->getDeskType() === self::DAILY_HOT_DESK) {
                return 'Daily Desk';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getTypeQuantityDesks() : string
    {
        if ($this->type === self::MEETING_ROOM) {
            return '';
        } elseif ($this->type === self::DESK) {
            return '| Desks ' . $this->getQuantity();
        } elseif ($this->type === self::PRIVATE_OFFICE) {
            return '| Desks ' . $this->getQuantity();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getWorkspaceInfo() : string
    {
        return sprintf(
            '%s | %s | %s',
            $this->getLocation()->getName(),
            $this->getTypeLabel(),
            $this->getType() === self::MEETING_ROOM ? $this->formattedPrice() . ' per month' : $this->formattedPrice() . ' per hour'
        );
    }

    /**
     * @return string
     */
    public function formattedPrice() : string
    {
        return '£' . number_format((float) $this->price, 2, '.', ',');
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        if ($this->getType() === self::PRIVATE_OFFICE) {
            return self::MONTHLY;
        }

        if ($this->getType() === self::DESK && !in_array($this->getDeskType(), $this->getHourlyDeskTypes())) {
            return self::MONTHLY;
        }

        return self::HOURLY;
    }

    /**
     * Only for desk
     * @return float|int
     */
    public function pricePerMinute()
    {
        return $this->price / 60;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMinContractLength(): ?int
    {
        return $this->minContractLength;
    }

    public function setMinContractLength(?int $minContractLength): self
    {
        $this->minContractLength = $minContractLength;

        return $this;
    }

    public function getDeskType(): ?string
    {
        return $this->deskType;
    }

    public function setDeskType(?string $deskType): self
    {
        $this->deskType = $deskType;

        return $this;
    }

    /**
     * @return Collection|Facility[]
     */
    public function getFacilities(): Collection
    {
        return $this->facilities;
    }

    public function addFacility(Facility $facility): self
    {
        if (!$this->facilities->contains($facility)) {
            $this->facilities[] = $facility;
        }

        return $this;
    }

    public function removeFacility(Facility $facility): self
    {
        if ($this->facilities->contains($facility)) {
            $this->facilities->removeElement($facility);
        }

        return $this;
    }

    /**
     * @return Collection|Media[]
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): self
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
        }

        return $this;
    }

    public function removeMedia(Media $media): self
    {
        if ($this->medias->contains($media)) {
            $this->medias->removeElement($media);
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Viewing[]
     */
    public function getViewings(): Collection
    {
        return $this->viewings;
    }

    public function addViewing(Viewing $viewing): self
    {
        if (!$this->viewings->contains($viewing)) {
            $this->viewings[] = $viewing;
            $viewing->setWorkspace($this);
        }

        return $this;
    }

    public function removeViewing(Viewing $viewing): self
    {
        if ($this->viewings->contains($viewing)) {
            $this->viewings->removeElement($viewing);
            // set the owning side to null (unless already changed)
            if ($viewing->getWorkspace() === $this) {
                $viewing->setWorkspace(null);
            }
        }

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setWorkspace($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getWorkspace() === $this) {
                $booking->setWorkspace(null);
            }
        }

        return $this;
    }

    public function getAvailableFrom(): ?\DateTimeInterface
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(?\DateTimeInterface $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getCoverImageUrl(): ?string
    {
        return $this->coverImageUrl;
    }

    public function setCoverImageUrl(?string $coverImageUrl): self
    {
        $this->coverImageUrl = $coverImageUrl;

        return $this;
    }

    public function getCoverImageId(): ?string
    {
        return $this->coverImageId;
    }

    public function setCoverImageId(?int $coverImageId): self
    {
        $this->coverImageId = $coverImageId;

        return $this;
    }

    public function getOpensFrom(): ?string
    {
        return $this->opensFrom;
    }

    public function setOpensFrom(?string $opensFrom): self
    {
        $this->opensFrom = $opensFrom;

        return $this;
    }

    public function getClosesAt(): ?string
    {
        return $this->closesAt;
    }

    public function setClosesAt(?string $closesAt): self
    {
        $this->closesAt = $closesAt;

        return $this;
    }

    public function getVisited(): ?bool
    {
        return $this->visited;
    }

    public function setVisited(bool $visited): self
    {
        $this->visited = $visited;

        return $this;
    }

    public function getTopPick(): ?bool
    {
        return $this->topPick;
    }

    public function setTopPick(bool $topPick): self
    {
        $this->topPick = $topPick;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCalendars(): Collection
    {
        return $this->calendars;
    }

    public function addCalendar(Calendar $calendar): self
    {
        if (!$this->calendars->contains($calendar)) {
            $this->calendars[] = $calendar;
            $calendar->setWorkspace($this);
        }

        return $this;
    }

    public function removeCalendar(Calendar $calendar): self
    {
        if ($this->calendars->contains($calendar)) {
            $this->calendars->removeElement($calendar);
            // set the owning side to null (unless already changed)
            if ($calendar->getWorkspace() === $this) {
                $calendar->setWorkspace(null);
            }
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getGoogleCalendarId(): ?string
    {
        return $this->googleCalendarId;
    }

    /**
     * @param null|string $googleCalendarId
     * @return WorkSpace
     */
    public function setGoogleCalendarId(?string $googleCalendarId): self
    {
        $this->googleCalendarId = $googleCalendarId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOutlookCalendarId(): ?string
    {
        return $this->outlookCalendarId;
    }

    /**
     * @param null|string $outlookCalendarId
     * @return WorkSpace
     */
    public function setOutlookCalendarId(?string $outlookCalendarId): self
    {
        $this->outlookCalendarId = $outlookCalendarId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutlookCalendarName() : ?string
    {
        return $this->outlookCalendarName;
    }

    /**
     * @param null|string $outlookCalendarName
     * @return WorkSpace
     */
    public function setOutlookCalendarName(?string $outlookCalendarName): self
    {
        $this->outlookCalendarName = $outlookCalendarName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @param WorkSpace $workspace
     * @return bool
     */
    public function checkAvailableFromFieldRequired(WorkSpace $workspace) : bool
    {
        return $workspace->getDeskType() === WorkSpace::HOURLY_HOT_DESK || $workspace->getDeskType() === WorkSpace::DAILY_MEETING_ROOM;
    }

    /**
     * @return array
     */
    public static function getHourlyDeskTypes() : array
    {
        return [
            self::HOURLY_HOT_DESK,
            self::DAILY_HOT_DESK
        ];
    }

    /**
     * @return array
     */
    public static function getDailyTypes() : array
    {
        return [
            self::DAILY_MEETING_ROOM,
            self::DAILY_HOT_DESK
        ];
    }

    /**
     * @return array
     */
    public static function getMeetingRoomTypes() : array
    {
        return [
            self::HOURLY_MEETING_ROOM,
            self::DAILY_MEETING_ROOM
        ];
    }

    /**
     * @return array
     */
    public static function getDeskTypes() : array
    {
        return [
            self::HOURLY_HOT_DESK,
            self::DAILY_HOT_DESK,
            self::MONTHLY_HOT_DESK,
            self::MONTHLY_FIXED_DESK
        ];
    }
}
