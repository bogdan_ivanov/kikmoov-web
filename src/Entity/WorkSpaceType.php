<?php

namespace App\Entity;

/**
 * Interface WorkSpaceType
 * @package App\Entity
 */
interface WorkSpaceType
{
    const DESK = 'desk';
    const PRIVATE_OFFICE = 'private-office';
    const MEETING_ROOM = 'meeting-room';
}
