<?php

namespace App\EventListener;

use App\Entity\Location;
use App\Entity\User;
use App\Entity\WorkSpace;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class LocationEventListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        /** @var Location $entity */
        $entity = $args->getObject();

        if (!$entity instanceof Location) {
            return;
        }

        if ($entity->getWorkspaces()->isEmpty()) {
            return;
        }

        $entityManager = $args->getObjectManager();

        $users = $entityManager->getRepository(User::class)->findAll();

        /** @var User $user */
        foreach ($users as $user) {
            $likedWorkspaces = $user->getLikedWorkspaces();

            if ($likedWorkspaces->isEmpty()) {
                continue;
            }

            foreach ($entity->getWorkspaces() as $workspace) {
                if ($likedWorkspaces->contains($workspace)) {
                    $user->removeLikedWorkspace($workspace);
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
            }
        }
    }
}