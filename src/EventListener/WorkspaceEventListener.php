<?php

namespace App\EventListener;

use App\Entity\Facility;
use App\Entity\User;
use App\Entity\WorkSpace;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * Class WorkspaceEventListener
 * @package App\EventListener
 */
class WorkspaceEventListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        /** @var WorkSpace $entity */
        $entity = $args->getObject();

        if (!$entity instanceof WorkSpace) {
            return;
        }

        $entityManager = $args->getObjectManager();

        if (!$entity->getFacilities()->isEmpty()) {
            /** @var Facility $facility */
            foreach ($entity->getFacilities() as $facility) {
                $entity->removeFacility($facility);
                $entityManager->persist($entity);
                $entityManager->flush();
            }
        }

        $users = $entityManager->getRepository(User::class)->findAll();

        /** @var User $user */
        foreach ($users as $user) {
            $likedWorkspaces = $user->getLikedWorkspaces();

            if ($likedWorkspaces->isEmpty()) {
                continue;
            }

            if ($likedWorkspaces->contains($entity)) {
                $user->removeLikedWorkspace($entity);
                $entityManager->persist($user);
                $entityManager->flush();
            }
        }
    }
}
