<?php

namespace App\InternalNotification;

use App\Entity\InternalNotification;

/**
 * Class InternalNotificationType
 * @package App\InternalNotification
 */
final class InternalNotificationType
{
    /**
     * @param string $event
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function findEvent(string $event, array $params)
    {
        if (in_array($event, array_flip(InternalNotificationType::getEvents())) === false) {
            throw new \Exception('Internal notification does not exist');
        }

        ['message' => $message] = $this->{self::getEvents()[$event]}();

        $message = $this->renderMessage($message, $params);

        return [
            'event' => $event,
            'message' => $message
        ];
    }

    /**
     * @return array
     */
    protected static function getEvents()
    {
        return [
            InternalNotification::VIEWING_REQUESTED => 'viewingRequested',
            InternalNotification::VIEWING_ACCEPTED  => 'viewingAccepted',
            InternalNotification::VIEWING_DECLINED  => 'viewingDeclined',
            InternalNotification::BOOKING_REQUESTED => 'bookingRequested',
            InternalNotification::BOOKING_ACCEPTED  => 'bookingAccepted',
            InternalNotification::BOOKING_DECLINED  => 'bookingDeclined',
        ];
    }

    /**
     * @param string $template
     * @param array $variables
     * @return mixed|string
     */
    private function renderMessage(string $template, array $variables) : string
    {
        foreach($variables as $key => $value){
            $template = str_replace('{' . strtoupper($key) . '}', $value, $template);
        }

        return $template;
    }

    /**
     * @return array
     */
    protected function viewingRequested()
    {
        return [
            'event' => InternalNotification::VIEWING_REQUESTED,
            'message' => 'A viewing has been requested for <b>{TIME}</b> on <b>{DATE}</b> at <b>{LOCATION_NAME}</b>'
        ];
    }

    /**
     * @return array
     */
    protected function viewingAccepted() : array
    {
        return [
            'event' => InternalNotification::VIEWING_ACCEPTED,
            'message' => 'You have accepted a viewing request for <b>{TIME}</b> on <b>{DATE}</b> at <b>{LOCATION_NAME}</b>'
        ];
    }

    /**
     * @return array
     */
    protected function viewingDeclined() : array
    {
        return [
            'event' => InternalNotification::VIEWING_DECLINED,
            'message' => 'You have accepted a viewing request for <b>{TIME}</b> on <b>{DATE}</b> at <b>{LOCATION_NAME}</b>'
        ];
    }

    /**
     * @return array
     */
    protected function bookingRequested() : array
    {
        return [
            'event' => InternalNotification::BOOKING_REQUESTED,
            'message' => 'A booking has been requested for the <b>{TIME}</b> on <b>{DATE}</b> at <b>{WORKSPACE_INFO}</b>'
        ];
    }

    /**
     * @return array
     */
    protected function bookingAccepted() : array
    {
        return [
            'event' => InternalNotification::BOOKING_ACCEPTED,
            'message' => 'You have accepted a booking for the <b>{TIME}</b> on <b>{DATE}</b> at <b>{WORKSPACE_INFO}</b>'
        ];
    }

    /**
     * @return array
     */
    protected function bookingDeclined() : array
    {
        return [
            'event' => InternalNotification::BOOKING_DECLINED,
            'message' => 'You have declined a booking for the <b>{TIME}</b> on <b>{DATE}</b> at <b>{WORKSPACE_INFO}</b>'
        ];
    }
}
