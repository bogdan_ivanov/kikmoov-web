<?php

namespace App\InternalNotification;

/**
 * Class PushNotificationType
 * @package App\InternalNotification
 */
final class PushNotificationType
{
    const VIEWING_TODAY = 'VIEWING_TODAY';
    const VIEWING_ACCEPTED  = 'VIEWING_ACCEPTED';
    const BOOKING_ACCEPTED  = 'BOOKING_ACCEPTED';
    const ONE_DAY_BEFORE_BOOKING_HOURLY  = 'ONE_DAY_BEFORE_BOOKING_HOURLY';
    const ONE_DAY_BEFORE_BOOKING_MONTHLY = 'ONE_DAY_BEFORE_BOOKING_MONTHLY';
    const EXISTING_SAVED_WORKSPACES  = 'EXISTING_SAVED_WORKSPACES';
    const DO_NOT_USE_APP_MORE_THAN_MONTH  = 'DO_NOT_APP_USE_MORE_THAN_MONTH';

    /**
     * @param string $event
     * @return mixed
     * @throws \Exception
     */
    public function findEvent(string $event)
    {
        if (in_array($event, array_flip(self::getEvents())) === false) {
            throw new \Exception('Internal notification does not exist');
        }

        $messageData= $this->{self::getEvents()[$event]}();

        return $messageData;
    }

    /**
     * @return array
     */
    protected static function getEvents()
    {
        return [
            self::VIEWING_TODAY => 'viewingToday',
            self::VIEWING_ACCEPTED  => 'viewingAccepted',
            self::BOOKING_ACCEPTED  => 'bookingAccepted',
            self::ONE_DAY_BEFORE_BOOKING_HOURLY => 'oneDayBeforeBookingHourly',
            self::ONE_DAY_BEFORE_BOOKING_MONTHLY  => 'oneDayBeforeBookingMonthly',
            self::EXISTING_SAVED_WORKSPACES  => 'existingSavedWorkspaces',
            self::DO_NOT_USE_APP_MORE_THAN_MONTH  => 'doNotUseAppMoreThanMonth',
        ];
    }

    /**
     * @return array
     */
    protected function viewingToday()
    {
        return [
            'event' => self::VIEWING_TODAY,
            'title' => 'Your viewing',
            'body' => 'A gentle reminder, you have a viewing today! Tap to see booking details',
        ];
    }

    /**
     * @return array
     */
    protected function viewingAccepted()
    {
        return [
            'event' => self::VIEWING_ACCEPTED,
            'title' => 'Viewing Request accepted',
            'body' => 'Hi [user], your viewing request for [address], on the [startDate] at [startTime] has been accepted! Tap to see details',
        ];
    }

    /**
     * @return array
     */
    protected function bookingAccepted()
    {
        return [
            'event' => self::BOOKING_ACCEPTED,
            'title' => 'Booking approved',
            'body' => 'Hi [user], your booking request for [address] has been approved! We will be in touch with you ASAP. Tap to see details',
        ];
    }

    /**
     * @return array
     */
    protected function oneDayBeforeBookingHourly()
    {
        return [
            'event' => self::ONE_DAY_BEFORE_BOOKING_HOURLY,
            'title' => 'Reminder – you have  a workspace booked',
            'body' => 'A gentle reminder, you have a workspace booked on the [startDate] at [startTime] ! Tap to see details',
        ];
    }

    /**
     * @return array
     */
    protected function oneDayBeforeBookingMonthly()
    {
        return [
            'event' => self::ONE_DAY_BEFORE_BOOKING_MONTHLY,
            'title' => 'Reminder – you have a meeting room booked',
            'body' => 'A gentle reminder, you have a meeting room booked [startDate] at [startTime]! Tap to see details',
        ];
    }

    /**
     * @return array
     */
    protected function existingSavedWorkspaces()
    {
        return [
            'event' => self::EXISTING_SAVED_WORKSPACES,
            'title' => 'Saved Workspaces',
            'body' => 'Still looking for a workspace? Don’t forget, You still have amazing workspaces as part of your shortlist! Tap here to see all saved workspaces',
        ];
    }

    /**
     * @return array
     */
    protected function doNotUseAppMoreThanMonth()
    {
        return [
            'event' => self::DO_NOT_USE_APP_MORE_THAN_MONTH,
            'title' => 'Still in need of a workspace?',
            'body' => 'Still looking for a workspace? We have amazing, new workspaces for you! Tap here to check them out',
        ];
    }
}