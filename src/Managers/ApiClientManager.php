<?php

namespace App\Managers;

use App\Entity\Client;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use FOS\OAuthServerBundle\Entity\ClientManager;
use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\UserBundle\Model\User;

/**
 * Class ApiClientManager
 * @package App\Managers
 */
class ApiClientManager
{
    /**
     * @var ClientManager
     */
    private $clientManager;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ClientManager constructor.
     * @param ClientManager $clientManager
     * @param EntityManager $entityManager
     */
    public function __construct(ClientManager $clientManager, EntityManager $entityManager)
    {
        $this->clientManager = $clientManager;
        $this->em = $entityManager;
    }

    /**
     * @return EntityRepository
     */
    protected function getRepository() : EntityRepository
    {
        return $this->em->getRepository(Client::class);
    }

    /**
     * @param string $email
     * @return Client|null
     */
    public function findClientByEmail(string $email) : ?Client
    {
        return $this->getRepository()->findOneBy(['name' => $email]);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getApiAccess(User $user) : array
    {
        $client = $this->findClientByEmail($user->getEmail());

        if ($client === null) {
            $client = $this->createClientManually($user);
        }

        return [
            'client_id' => $client->getPublicId(),
            'client_secret' => $client->getSecret()
        ];
    }

    /**
     * @param User $user
     * @return Client
     */
    public function createClientManually(User $user) : Client
    {
        $client = $this->clientManager->createClient();
        $client->setName($user->getEmail());
        $client->setAllowedGrantTypes(['password', 'refresh_token']);
        $this->clientManager->updateClient($client);

        return $client;
    }
}
