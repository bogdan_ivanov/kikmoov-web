<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Model\AreaModelManager;
use App\Model\ArticleModelManager;
use App\Model\CategoryModelManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AreaApiManager
 * @package App\Managers
 */
class AreaApiManager
{

    /**
     * @var AreaModelManager
     */
    private $areaModelManager;

    /**
     * AreaManager constructor.
     * @param AreaModelManager $areaModelManager
     */
    public function __construct(
        AreaModelManager $areaModelManager
    )
    {
        $this->areaModelManager = $areaModelManager;
    }

    /**
     * @return DataResponse
     */
    public function getActiveAreas() : DataResponse
    {
        return $this->areaModelManager->getActiveAreas();
    }
}