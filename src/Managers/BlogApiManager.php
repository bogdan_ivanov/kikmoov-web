<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Model\ArticleModelManager;
use App\Model\CategoryModelManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogApiManager
 * @package App\Managers
 */
class BlogApiManager
{
    /**
     * @var CategoryModelManager
     */
    private $categoryModelManager;

    /**
     * @var ArticleModelManager
     */
    private $articleModelManager;

    /**
     * BlogApiManager constructor.
     * @param CategoryModelManager $categoryModelManager
     * @param ArticleModelManager $articleModelManager
     */
    public function __construct(
        CategoryModelManager $categoryModelManager,
        ArticleModelManager $articleModelManager
    )
    {
        $this->categoryModelManager = $categoryModelManager;
        $this->articleModelManager = $articleModelManager;
    }

    /**
     * @param string $slug
     * @return DataResponse
     * @throws \Exception
     */
    public function findArticle(string $slug) : DataResponse
    {
        return $this->articleModelManager->getArticle($slug);
    }

    /**
     * @return DataResponse
     * @throws \Exception
     */
    public function getListOfActiveCategories() : DataResponse
    {
        return $this->categoryModelManager->getActiveCategories();
    }

    /**
     * @param string $slug
     * @param int $start
     * @return DataResponse
     * @throws \Exception
     */
    public function findArticlesByCategorySlug(string $slug, int $start) : DataResponse
    {
        $category = $this->categoryModelManager->findBySlug($slug);

        if (is_null($category)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                [
                    'message' => 'Category does not exist'
                ]
            );
        }

        $res = $this->articleModelManager->getListOfArticlesByCategory($category, $start);

        return new DataResponse(Response::HTTP_OK, $res);
    }

    /**
     * @return DataResponse
     */
    public function getLastArticle() : DataResponse
    {
        return $this->articleModelManager->getLastArticle();
    }
}
