<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\Booking;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use App\Entity\BuyerInternalNotification;
use App\Entity\InternalNotification;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\InternalNotification\PushNotificationType;
use App\Model\BookingModelManager;
use App\Model\CalendarEventModelManager;
use App\Model\CalendarModelManager;
use App\Model\InternalNotificationModelManager;
use App\Model\BuyerInternalNotificationModelManager;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Payment\StripeClient;
use App\Services\BookingReceiptGenerator;
use App\Services\RouterWrapper;
use App\Services\UserStatusChecker;
use App\Traits\YieldTrait;
use App\Validators\BookingValidator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookingApiManager
 * @package App\Managers
 */
class BookingApiManager implements Schedule
{
    use YieldTrait;

    /**
     * @var BookingModelManager
     */
    private $bookingModelManager;

    /**
     * @var BookingValidator
     */
    private $bookingValidator;

    /**
     * @var StripeClient
     */
    private $stripeClient;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var RouterWrapper
     */
    private $router;

    /**
     * @var InternalNotificationModelManager
     */
    private $internalNotificationModelManager;

    /**
     * @var UserStatusChecker
     */
    private $userStatusChecker;

    /**
     * @var BuyerInternalNotificationModelManager
     */
    private $buyerInternalNotificationModelManager;

    /**
     * @var CalendarModelManager
     */
    private $calendarModelManager;

    /**
     * @var CalendarEventModelManager
     */
    private $calendarEventModelManager;

    /**
     * @var BookingReceiptGenerator
     */
    private $bookingReceiptGenerator;

    /**
     * BookingApiManager constructor.
     * @param BookingModelManager $bookingModelManager
     * @param BookingValidator $bookingValidator
     * @param StripeClient $stripeClient
     * @param NotificationService $notificationService
     * @param RouterWrapper $routerWrapper
     * @param InternalNotificationModelManager $internalNotificationModelManager
     * @param UserStatusChecker $userStatusChecker
     * @param BuyerInternalNotificationModelManager $buyerInternalNotificationModelManager
     * @param CalendarModelManager $calendarModelManager
     * @param CalendarEventModelManager $calendarEventModelManager
     * @param BookingReceiptGenerator $bookingReceiptGenerator
     */
    public function __construct(
        BookingModelManager $bookingModelManager,
        BookingValidator $bookingValidator,
        StripeClient $stripeClient,
        NotificationService $notificationService,
        RouterWrapper $routerWrapper,
        InternalNotificationModelManager $internalNotificationModelManager,
        UserStatusChecker $userStatusChecker,
        BuyerInternalNotificationModelManager $buyerInternalNotificationModelManager,
        CalendarModelManager $calendarModelManager,
        CalendarEventModelManager $calendarEventModelManager,
        BookingReceiptGenerator $bookingReceiptGenerator
    ) {
        $this->bookingModelManager = $bookingModelManager;
        $this->bookingValidator = $bookingValidator;
        $this->stripeClient = $stripeClient;
        $this->notificationService = $notificationService;
        $this->router = $routerWrapper;
        $this->internalNotificationModelManager = $internalNotificationModelManager;
        $this->userStatusChecker = $userStatusChecker;
        $this->buyerInternalNotificationModelManager = $buyerInternalNotificationModelManager;
        $this->calendarModelManager = $calendarModelManager;
        $this->calendarEventModelManager = $calendarEventModelManager;
        $this->bookingReceiptGenerator = $bookingReceiptGenerator;
    }

    /**
     * @return BookingModelManager
     */
    public function getBookingModelManager(): BookingModelManager
    {
        return $this->bookingModelManager;
    }

    /**
     * @param WorkSpace $workSpace
     * @return DataResponse
     * @throws \Exception
     */
    public function getList(WorkSpace $workSpace) : DataResponse
    {
        $list = [];

        if ($workSpace->getBookings()->isEmpty()) {
            return new DataResponse(Response::HTTP_OK);
        }

        /** @var Booking $booking */
        foreach ($this->yieldCollection($workSpace->getBookings()) as $booking) {
            $user = $booking->getUser();

            $list[] = [
                'id' => $booking->getId(),
                'startTime' => $booking->getStartTime()->getTimestamp(),
                'endTime' => (is_null($booking->getEndTime())) ? null : $booking->getEndTime()->getTimestamp(),
                'phone' => $booking->getPhone(),
                'status' => $booking->getStatus(),
                'meetingName' => ($booking->getBookingType() === 'booking-request') ? $booking->getName() : null,
                'name' => (!is_null($user)) ? $user->getFullname() : null,
                'email' => (!is_null($user)) ? $user->getEmail() : null
            ];
        }

        return new DataResponse(Response::HTTP_OK, $list);
    }

    /**
     * @param WorkSpace $workSpace
     * @return array
     * @throws \Exception
     */
    public function getBookedDates(WorkSpace $workSpace)
    {
        $bookedDates = [];

        if ($workSpace->getBookings()->isEmpty()) {
            return $bookedDates;
        }

        $now = new \DateTime();

        /** @var Booking $booking */
        foreach ($this->yieldCollection($workSpace->getBookings()) as $booking) {
            $startTimeTimestamp = $booking->getStartTime()->getTimestamp();
            //old ones
            if ($now->getTimestamp() > $startTimeTimestamp) {
                continue;
            }

            $bookedDates[] = [
                'startTime' => $startTimeTimestamp,
                'endTime' => ($booking->getEndTime()) ? $booking->getEndTime()->getTimestamp() : $startTimeTimestamp
            ];
        }

        return $bookedDates;
    }

    /**
     * @param User $user
     * @param WorkSpace $workSpace
     * @param $data
     * @return DataResponse
     * @throws \Exception
     */
    public function addBooking(User $user, WorkSpace $workSpace, $data): DataResponse
    {
        if (empty($data['startTime'])) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Please, choose a start date']
            );
        }

        if (!is_null($workSpace->getAvailableFrom())) {
            $startTime = (new \DateTime())->setTimestamp($data['startTime']);

            if ($workSpace->getAvailableFrom()->getTimestamp() > $startTime->getTimestamp()) {
                return new DataResponse(
                    Response::HTTP_BAD_REQUEST,
                    ['message' => 'Booking is temporarily unavailable. Please, try again later']
                );
            }
        }

        if ($workSpace->getType() === WorkSpace::PRIVATE_OFFICE) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Booking is not available for workspace with the private office type']
            );
        }

        if ($workSpace->getType() === WorkSpace::DESK && !in_array($workSpace->getDeskType(), WorkSpace::getHourlyDeskTypes())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'This workspace available only for booking by the hour. Please, use booking request instead']
            );
        }

        $booking = $this->bookingModelManager->createBooking();

        if (!empty($errors = $this->bookingValidator->bookingValidator($booking, $data))) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }


        $startTime = (new \DateTime())->setTimestamp($data['startTime']);
        $endTime = (new \DateTime())->setTimestamp($data['endTime']);

        //min booking time is 1 hour
        $minTime = 1 * 60 * 60;

        if ($minTime > $endTime->getTimestamp() - $startTime->getTimestamp()) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'The minimum time for booking is 1 hour']
            );
        }

//        if (!$this->bookingModelManager->isTimeAvailable($workSpace->getId(), $startTime, $endTime)) {
//            return new DataResponse(
//                Response::HTTP_BAD_REQUEST,
//                ['message' => 'Please pick up another time']
//            );
//        }

        $diffDate = $endTime->diff($startTime);
        $minutes = (($diffDate->d * 24 * 60) + ($diffDate->h * 60) + $diffDate->i);

        if (in_array($workSpace->getDeskType(), WorkSpace::getDailyTypes())) {
            $amount = $workSpace->getPrice();
        } else {
            $amount = $workSpace->pricePerMinute() * $minutes;
        }

        $booking->setStartTime($startTime);
        $booking->setEndTime($endTime);
        $booking->setUser($user);
        $booking->setPaymentType(Booking::CARD);
        $booking->setAmount($amount);
        $booking->setAmountVAT($amount * Booking::VATPercentage);
        $booking->setStatus(Booking::ACCEPTED);
        $booking->setWorkSpace($workSpace);

        $addingEvents = true;
        try {
            $addingEvents = $this->addEventsIntoCalendar($workSpace, $startTime, $endTime, $booking);
        } catch (\Exception $exception) {
            //TODO: make notification about problem with calendars
        }

        if (!$addingEvents) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'This time is not available']
            );
        }

        try {
            $client = $this->stripeClient->createCustomer($data['paymentToken'], $booking->getEmail());
            $booking->setCustomerId($client->id);
        } catch (\Exception $exception) {
            return new DataResponse(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'message' => 'Unable to create payment invoice. Please check the card details and try again',
                    'error' => $exception->getMessage()
                ]
            );
        }

        try {
            $this->bookingModelManager->update($booking);
        } catch (\Exception $exception) {
            return new DataResponse(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'message' => 'Unable to create booking record. Please check the data and try again'
                ]
            );
        }

        $location = $workSpace->getLocation();
        $receiptFilePath = $this->bookingReceiptGenerator->generatePdf($booking, "Receipt_Kikmoov {$booking->getId()}");

        $this->notificationService->sendEmail(
            $booking->getUser(),
            $workSpace->getType() === WorkSpace::DESK ? NotificationsEvent::BOOKING_HOURLY_DESK : NotificationsEvent::BOOKING_HOURLY_MEETING_ROOM,
            [
                'seller' => $location->getUser(),
                'buyer' => $booking->getUser(),
                'location' => $location,
                'workspace' => $workSpace,
                'booking' => $booking,
                'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workSpace->getId())),
                'downloadFileLink' => $this->router->generateAdminLink('download_booking_icalendar_file', ['id' => $booking->getId(),]),
                'attached' => $receiptFilePath
            ],
            $location->getUser(),
            ($booking->getUser()->getEmail() !== $booking->getEmail()) ? $booking->getEmail() : ''
        );

        $this->internalNotificationModelManager->create(
            $location->getUser(),
            $booking,
            [
                'time' => $booking->getTimeForInternalNotification(),
                'date' => $booking->getStartTime()->format('l, jS F'),
                'workspace_info' => $workSpace->getWorkspaceInfo()
            ],
            InternalNotification::BOOKING_ACCEPTED,
            InternalNotification::BOOKING
        );

        $this->userStatusChecker->changeStatus($booking->getUser(), User::BOOKING_REQUESTED);

        return new DataResponse(
            Response::HTTP_CREATED,
            [
                'message' => 'You have been successfully added to the booking schedule',
                'bookingId' => $booking->getId()
            ]
        );
    }

    /**
     * @param WorkSpace $workSpace
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @param Booking $booking
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function addEventsIntoCalendar
    (
        WorkSpace $workSpace,
        \DateTimeInterface $startTime,
        \DateTimeInterface $endTime,
        Booking $booking
    ) : bool
    {
        $events = $this->calendarEventModelManager->getCalendarEvents($workSpace, $startTime, $endTime);

        if (!empty($events)) {
            return false;
        }

        $calendars = $this->calendarModelManager->getOutsideCalendars($workSpace);

        if ($calendars) {
            /** @var Calendar $calendar */
            foreach ($calendars as $calendar) {
                if ($workSpace->getType() === WorkSpace::MEETING_ROOM) {
                    $title = $booking->getName() . ' - ' . $calendar->getName();
                } else {
                    $title = $workSpace->getLocation()->getName() . ' - ' . $workSpace->getHourlyDeskTypeLabel();
                }
                $summary = 'You have a meeting at ' . $workSpace->getLocation()->getAddress() . ' on ' . $booking->getStartTime()->format('l, d M Y H:i') . '.
For any additional information or problems please visit kikmoov.com or get in touch info@kikmoov.com';
                $this->calendarEventModelManager->addCalendarEvents(
                    $calendar,
                    $workSpace,
                    $startTime,
                    $endTime,
                    $title,
                    $summary);
            }
        }

        $customCalendar = $this->calendarModelManager->getCustomCalendar($workSpace);

        if (empty($customCalendar)) {
            $customCalendar = $this->calendarModelManager->addCustomCalendar($workSpace);
        }

        $this->calendarEventModelManager->addCustomEvent($customCalendar, $startTime, $endTime, $workSpace);

        return true;
    }

    /**
     * @param User $user
     * @param WorkSpace $workSpace
     * @param $data
     * @return DataResponse
     * @throws \Exception
     */
    public function addBookingRequest(User $user, WorkSpace $workSpace, $data): DataResponse
    {
        if (empty($data['startTime'])) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Please, choose a start date']
            );
        }

        if ($workSpace->getType() === WorkSpace::MEETING_ROOM ||
            ($workSpace->getType() === WorkSpace::DESK && $workSpace->getDeskType() === WorkSpace::HOURLY_HOT_DESK)
        ) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Booking request is not available for this workspace. Please, use booking instead']
            );
        }

        $booking = $this->bookingModelManager->createBooking();

        if (!empty($errors = $this->bookingValidator->bookingRequestValidator($booking, $data))) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        $startTime = (new \DateTime())->setTimestamp($data['startTime']);

        $booking->setStartTime($startTime);
        $booking->setBookingType(Booking::BOOKING_REQUEST);
        $booking->setUser($user);
        $booking->setPaymentType(Booking::NA);
        $booking->setAmount($workSpace->getPrice());
        $booking->setStatus(Booking::PENDING);
        $booking->setWorkSpace($workSpace);

        $this->bookingModelManager->update($booking);

        $location = $workSpace->getLocation();

        $this->notificationService->sendEmail(
            $booking->getUser(),
            NotificationsEvent::BOOKING_REQUEST,
            [
                'seller' => $location->getUser(),
                'buyer' => $booking->getUser(),
                'location' => $location,
                'workspace' => $workSpace,
                'booking' => $booking,
                'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workSpace->getId()))
            ],
            $location->getUser(),
            ($booking->getUser()->getEmail() !== $booking->getEmail()) ? $booking->getEmail() : ''
        );

        $this->internalNotificationModelManager->create(
            $location->getUser(),
            $booking,
            [
                'time' => $booking->getStartTime()->format('h:ia'),
                'date' => $booking->getStartTime()->format('l, jS F'),
                'workspace_info' => $workSpace->getWorkspaceInfo()
            ],
            InternalNotification::BOOKING_REQUESTED,
            InternalNotification::BOOKING
        );

        return new DataResponse(
            Response::HTTP_CREATED,
            [
                'message' => 'You have been successfully added to the booking schedule',
                'internalNotification' => $booking->getCheckedInternalNotification()
            ]
        );
    }

    /**
     * @param WorkSpace $workSpace
     * @param int $bookingId
     * @param string $status
     * @return DataResponse
     * @throws \Exception
     */
    public function updateBookingStatus(WorkSpace $workSpace, int $bookingId, string $status) : DataResponse
    {
        if (!in_array($status, Booking::listOfStatuses())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => "Wrong status"]
            );
        }

        $booking = $this->bookingModelManager->findOneBy(['workSpace' => $workSpace, 'id' => $bookingId]);

        if (is_null($booking)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => "Booking record doesn't exist"]
            );
        }

        if ($booking->getStatus() === $status) {
            return new DataResponse(Response::HTTP_OK, ['message' => 'Nothing to change']);
        }

        $booking->setStatus($status);
        $this->bookingModelManager->update($booking);

        $location = $workSpace->getLocation();

        $internalParams = [
            'time' => $booking->getTimeForInternalNotification(),
            'date' => $booking->getStartTime()->format('l, jS F'),
            'workspace_info' => $workSpace->getWorkspaceInfo()
        ];

        if ($status === Booking::ACCEPTED) {
            $this->notificationService->sendEmail(
                $booking->getUser(),
                NotificationsEvent::BOOKING_APPROVED,
                [
                    'seller' => $location->getUser(),
                    'buyer' => $booking->getUser(),
                    'location' => $location,
                    'workspace' => $workSpace,
                    'booking' => $booking,
                    'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workSpace->getId()))
                ]
            );

            $pushParams = [
                'startTime' => $booking->getStartTime()->format('H:i'),
                'startDate' => $booking->getStartTime()->format('d.m.Y'),
                'address' => $workSpace->getLocation()->getAddress(),
                'user' => $booking->getUser()->getFullname()
            ];

            $this->buyerInternalNotificationModelManager->create(
                $booking->getUser(),
                $pushParams,
                PushNotificationType::BOOKING_ACCEPTED,
                $booking->getId(),
                BuyerInternalNotification::BOOKING
            );

            $this->internalNotificationModelManager->changeStatus(
                $booking->getInternalNotification(),
                InternalNotification::BOOKING_ACCEPTED,
                $internalParams
            );
        } elseif ($status === Booking::DECLINED) {
            $this->internalNotificationModelManager->changeStatus(
                $booking->getInternalNotification(),
                InternalNotification::BOOKING_DECLINED,
                $internalParams
            );
        }

        return new DataResponse(Response::HTTP_OK, [
            'internalNotification' => $booking->getCheckedInternalNotification()
        ]);
    }

    /**
     * @param Booking $booking
     * @return array
     */
    protected function checkBooking(Booking $booking) : array
    {
        if ($booking->getStatus() === Booking::CANCELED) {
            return [
                false,
                ['message' => "Booking already canceled"]
            ];
        }

        $workspace = $booking->getWorkSpace();

        if (is_null($workspace)) {
            return [
                false,
                ['message' => "Workspace was not found"]
            ];
        }

        if ($workspace->getStatus() !== WorkSpace::ACTIVE) {
            return [
                false,
                ['message' => "Workspace is not available. Please, try again later"]
            ];
        }

        /** @var Location $location */
        $location = $workspace->getLocation();

        if ($location === null) {
            return [
                false,
                ['message' => 'No location was found']
            ];
        }

        return [
            true,
            [
                'booking' => $booking,
                'workspace' => $workspace,
                'location' => $location
            ]
        ];
    }

    /**
     * @param int $bookingID
     * @return array
     */
    public function checkBookingById(int $bookingID) : array
    {
        $booking = $this->bookingModelManager->findOneBy(['id' => $bookingID]);

        if (is_null($booking)) {
            return [
                false,
                ['message' => "Booking record was not found"]
            ];
        }

        return $this->checkBooking($booking);
    }

    /**
     * @param User $user
     * @param int $bookingID
     * @return array
     */
    public function checkBookingByUser(User $user, int $bookingID) : array
    {
        $booking = $this->bookingModelManager->findOneBy(['id' => $bookingID, 'user' => $user]);

        if (is_null($booking)) {
            return [
                false,
                ['message' => "Booking record was not found"]
            ];
        }

        return $this->checkBooking($booking);
    }

    /**
     * @param User $user
     * @param int $bookingID
     * @return DataResponse
     * @throws \Exception
     */
    public function cancelBooking(User $user, int $bookingID) : DataResponse
    {
        [$ok, $data] = $this->checkBookingByUser($user, $bookingID);

        if (!$ok) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                $data
            );
        }

        /**
         * @var Booking $booking
         * @var Location $location
         * @var WorkSpace $workspace
         */
        ['booking' => $booking, 'workspace' => $workspace, 'location' => $location] = $data;

        $now = (new \DateTime())->setTime(0,0,0);

        if ($booking->getStartTime()->getTimestamp() < $now->getTimestamp()) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => 'You cannot cancel the expired booking'
            ]);
        }

        if ($booking->getStatus() === Booking::PAYMENT_RECEIVED) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => 'You cannot cancel already paid booking'
            ]);
        }

        $booking->setStatus(Booking::CANCELED);
        $this->bookingModelManager->update($booking);

        $interval = $booking->getStartTime()->diff($now);

        $this->buyerInternalNotificationModelManager->deleteNotification(
            $booking->getId(),
            PushNotificationType::BOOKING_ACCEPTED
        );

        if ($booking->getWorkSpace()->getDuration() === WorkSpace::HOURLY) {
            $this->notificationService->sendEmail(
                $booking->getUser(),
                ($interval->d > 1) ? NotificationsEvent::BOOKING_CANCELED_MORE_THAN_24 : NotificationsEvent::BOOKING_CANCELED_LESS_THAN_24,
                [
                    'seller' => $location->getUser(),
                    'buyer' => $booking->getUser(),
                    'location' => $location,
                    'workspace' => $workspace,
                    'booking' => $booking,
                    'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
                ],
                $location->getUser()
            );
        } else {
            $this->notificationService->sendEmail(
                $booking->getUser(),
                NotificationsEvent::BOOKING_CANCELED,
                [
                    'seller' => $location->getUser(),
                    'buyer' => $booking->getUser(),
                    'location' => $location,
                    'workspace' => $workspace,
                    'booking' => $booking,
                    'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
                ],
                $location->getUser()
            );
        }

        $this->buyerInternalNotificationModelManager->delete(
            $booking->getUser(),
            $booking->getId(),
            BuyerInternalNotification::BOOKING
        );

        return new DataResponse(Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @param int $bookingID
     * @param array $emails
     * @return DataResponse
     */
    public function inviteAttendee(User $user, int $bookingID, array $emails) : DataResponse
    {
        [$ok, $data] = $this->checkBookingByUser($user, $bookingID);

        if (!$ok) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                $data
            );
        }

        /**
         * @var Booking $booking
         * @var Location $location
         */
        ['booking' => $booking, 'workspace' => $workspace, 'location' => $location] = $data;

        if (!empty($errors = $this->bookingValidator->validateAttendees($emails, $booking->getAttendees()))) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                $errors
            );
        }

        $booking->setAttendees((!empty($booking->getAttendees())) ? array_merge($booking->getAttendees(), $emails) : $emails);
        $this->bookingModelManager->update($booking);

        $this->sendEmailsToAttendees($emails, [
            'buyer' => $user,
            'seller' => $location->getUser(),
            'workspace' => $workspace,
            'location' => $location,
            'booking' => $booking,
            'link' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId())),
        ]);

        return new DataResponse(
            Response::HTTP_OK,
            [
                'attendees' => $booking->getAttendees()
            ]
        );
    }

    /**
     * @param array $emails
     * @param array $data
     */
    private function sendEmailsToAttendees(array $emails, array $data) : void
    {
        foreach ($emails as $email) {
            $this->notificationService->sendEmail(
                $email,
                NotificationsEvent::BOOKING_INVITE_ATTENDEE,
                $data
            );
        }
    }
}