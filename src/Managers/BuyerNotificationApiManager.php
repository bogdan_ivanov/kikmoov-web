<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\BuyerInternalNotification;
use App\Entity\User;
use App\Repository\BuyerInternalNotificationRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BuyerNotificationApiManager
 * @package App\Managers
 */
class BuyerNotificationApiManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BuyerNotificationApiManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct
    (
        EntityManager $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return BuyerInternalNotificationRepository
     */
    protected function getRepository() : BuyerInternalNotificationRepository
    {
        return $this->entityManager->getRepository(BuyerInternalNotification::class);
    }

    /**
     * @param User $user
     * @return DataResponse
     * @throws \Exception
     */
    public function getListOfNotifications(User $user) : DataResponse
    {
        $notifications = $this->getRepository()->findBy(['user' => $user], ['id' => 'DESC']);

        $filtered = [];

        if (empty($notifications)) {
            return new DataResponse(Response::HTTP_OK, ['notifications' => $filtered]);
        }

        /** @var BuyerInternalNotification $notification */
        foreach ($this->yieldCollection($notifications) as $notification) {
            $internalNotification = [
                'createdAt' => $notification->getCreatedAt()->getTimestamp(),
                'message' => $notification->getMessage(),
                'action' => [
                    'actionType' => $notification->getActionType(),
                    'id' => $notification->getEntityId()
                ]
            ];

            $filtered[] = $internalNotification;
        }

        return new DataResponse(Response::HTTP_OK, ['notifications' => $filtered]);
    }
}