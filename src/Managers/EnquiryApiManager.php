<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\User;
use App\Model\ContactUsModelManager;
use App\Model\EnquiryModelManager;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Validators\ContactUsValidator;
use App\Validators\EnquiryValidator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EnquiryApiManager
 * @package App\Managers
 */
class EnquiryApiManager
{
    /**
     * @var EnquiryModelManager
     */
    private $enquiryModelManager;

    /**
     * @var EnquiryValidator
     */
    private $validator;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * EnquiryApiManager constructor.
     * @param EnquiryModelManager $enquiryModelManager
     * @param EnquiryValidator $enquiryValidator
     * @param NotificationService $notificationService
     */
    public function __construct(
        EnquiryModelManager $enquiryModelManager,
        EnquiryValidator $enquiryValidator,
        NotificationService $notificationService
    ) {
        $this->enquiryModelManager = $enquiryModelManager;
        $this->validator = $enquiryValidator;
        $this->notificationService = $notificationService;
    }

    /**
     * @param array $data
     * @param User|null $user
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveContactEnquiry(array $data, ?User $user) : DataResponse
    {
        $contactUs = $this->enquiryModelManager->createContactUs();

        if (!is_null($user)) {
            $data['name'] = $user->getFullname();
            $data['email'] = $user->getEmail();
        }

        $errors = $this->validator->validateContactUs($contactUs, $data);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        $this->enquiryModelManager->updateContactUs($contactUs);

        $this->notificationService->sendEmail(
            $data['email'],
            NotificationsEvent::CONTACT_ENQUIRY,
            [
                'contactEnquiry' => $contactUs
            ]
        );

        return new DataResponse(Response::HTTP_CREATED);
    }

    /**
     * @param array $data
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveLeaseholdEnquiry(array $data) : DataResponse
    {
        $leaseholdEnquiry = $this->enquiryModelManager->createLeaseholdEnquiry();

        $errors = $this->validator->validateLeaseholdEnquiry($leaseholdEnquiry, $data);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        $this->enquiryModelManager->updateLeaseholdEnquiry($leaseholdEnquiry);

        $this->notificationService->sendEmail(
            $data['email'],
            NotificationsEvent::LEASEHOLD_ENQUIRY,
            [
                'leaseholdEnquiry' => $leaseholdEnquiry
            ]
        );

        return new DataResponse(Response::HTTP_CREATED);
    }
}
