<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\Booking;
use App\Entity\Location;
use App\Entity\Viewing;
use App\Entity\WorkSpace;
use App\Services\CalendarFileGenerator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FileManager
 * @package App\Managers
 */
class FileManager
{

    /**
     * @var BookingApiManager
     */
    private $bookingApiManager;

    /**
     * @var CalendarFileGenerator
     */
    private $calendarFileGenerator;

    /**
     * @var ViewingApiManager
     */
    private $viewingApiManager;

    /**
     * FileManager constructor.
     * @param BookingApiManager $bookingApiManager
     * @param ViewingApiManager $viewingApiManager
     * @param CalendarFileGenerator $calendarFileGenerator
     */
    public function __construct
    (
        BookingApiManager $bookingApiManager,
        ViewingApiManager $viewingApiManager,
        CalendarFileGenerator $calendarFileGenerator
    )
    {
        $this->bookingApiManager = $bookingApiManager;
        $this->viewingApiManager = $viewingApiManager;
        $this->calendarFileGenerator = $calendarFileGenerator;
    }

    /**
     * @param int $bookingId
     * @return DataResponse|Response
     */
    public function generateBookingFile(int $bookingId)
    {
        [$ok, $data] = $this->bookingApiManager->checkBookingById($bookingId);

        if (!$ok) {
            return $this->calendarFileGenerator->fileGeneratorError($data);
        }

        /**
         * @var Booking $booking
         * @var Location $location
         * @var WorkSpace $workspace
         */
        ['booking' => $booking, 'workspace' => $workspace, 'location' => $location] = $data;

        return $this->calendarFileGenerator->fileGenerator($workspace, $booking);
    }

    /**
     * @param int $viewingID
     * @return bool|Response
     */
    public function generateViewingFile(int $viewingID)
    {
        [$ok, $data] = $this->viewingApiManager->checkViewingById($viewingID);

        if (!$ok) {
            return $this->calendarFileGenerator->fileGeneratorError($data);
        }

        /**
         * @var Viewing $viewing
         * @var Location $location
         * @var WorkSpace $workspace
         */
        ['viewing' => $viewing, 'workspace' => $workspace, 'location' => $location] = $data;

        return $this->calendarFileGenerator->fileGenerator($workspace, $viewing);
    }
}