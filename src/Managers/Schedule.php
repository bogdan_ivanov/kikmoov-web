<?php

namespace App\Managers;

/**
 * Interface Schedule
 * @package App\Managers
 */
interface Schedule
{
    const MAX_FUTURE_TIME = '+3 months';
}
