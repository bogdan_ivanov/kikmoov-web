<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\Facility;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\WorkSpace;
use App\Model\AreaModelManager;
use App\Model\BoroughModelManager;
use App\Model\LocationModelManager;
use App\Model\WorkSpaceModelManager;
use App\Services\GoogleMapService;
use App\Services\UserStatusChecker;
use App\Traits\YieldTrait;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;
use GuzzleHttp;

/**
 * Class SearchManager
 * @package App\Managers
 */
class SearchManager
{
    use YieldTrait;

    const HOURLY  = 'hourly';
    const MONTHLY = 'monthly';

    /**
     * @var WorkSpaceModelManager
     */
    protected $workSpaceModelManager;

    /**
     * @var LocationModelManager
     */
    protected $locationModelManager;

    /**
     * @var AreaModelManager
     */
    protected $areaModelManager;

    /**
     * @var BoroughModelManager
     */
    protected $boroughModelManager;

    /**
     * @var string
     */
    protected $googleApiKey;

    /**
     * @var GoogleMapService
     */
    protected $googleMapService;

    protected $searchRadius;

    const GOOGLE_MAPS_PLACE = 'https://maps.googleapis.com/maps/api/place/autocomplete';

    const SEARCH_QUERIES = [
        'kings' => 'king\'s'
    ];

    /**
     * SearchManager constructor.
     * @param WorkSpaceModelManager $workSpaceModelManager
     * @param LocationModelManager $locationModelManager
     * @param AreaModelManager $areaModelManager
     * @param BoroughModelManager $boroughModelManager
     * @param GoogleMapService $googleMapService
     * @param string $googleApiKey
     * @param int $searchRadius
     */
    public function __construct(
        WorkSpaceModelManager $workSpaceModelManager,
        LocationModelManager $locationModelManager,
        AreaModelManager $areaModelManager,
        BoroughModelManager $boroughModelManager,
        GoogleMapService $googleMapService,
        string $googleApiKey,
        int $searchRadius
    ) {
        $this->workSpaceModelManager = $workSpaceModelManager;
        $this->locationModelManager = $locationModelManager;
        $this->areaModelManager = $areaModelManager;
        $this->boroughModelManager = $boroughModelManager;
        $this->googleMapService = $googleMapService;
        $this->googleApiKey = $googleApiKey;
        $this->searchRadius = $searchRadius;
    }

    /**
     * @param array $params
     * @param bool|null $additionalInfoNeeded
     * @return DataResponse
     * @throws \Exception
     */
    public function search(array $params, ?bool $additionalInfoNeeded = true) : DataResponse
    {
        [$ok, $data] = $this->preparingInputSearchParams($params);

        if (!$ok) {
            return new DataResponse(
                $data['status'],
                [
                    'message' => $data['message']
                ]
            );
        }

        $params = $data;

        $workspaces = $this->workSpaceModelManager->search($params);

        if (empty($workspaces)) {
            return new DataResponse(Response::HTTP_OK,
                ['results' => $workspaces, 'count' => 0]
            );
        }

        $filtered = $this->searchResultProcessing(
            $workspaces,
            $params,
            $additionalInfoNeeded
        );

        $countOfWorkspaces = count($filtered);

        $start = isset($params['start']) ? intval($params['start']) : 0;

        $results = array_slice($filtered, $start, 30);

        //default sorting should be by distance of the station when searching by station
        if (!empty($results) && !empty($params['station']) && empty($params['price']) && empty($params['date'])) {
            //get full name of searched subway station
            $lastPart = strstr(implode(',', array_column($results[0]['location']['nearby'], 'name')), $params['station']);
            $station = substr($lastPart, 0, strpos($lastPart, ','));

            usort($results, function ($locationA, $locationB) use ($results, $station) {
                //get positions of searched station at the results
                $positionA = array_search($station, array_column($locationA['location']['nearby'], 'name'));
                $positionB = array_search($station, array_column($locationB['location']['nearby'], 'name'));

                return $locationA['location']['nearby'][$positionA]['distance'] > $locationB['location']['nearby'][$positionB]['distance'];

            });
        }

        if (empty($params['price']) && empty($params['date']) && empty($params['capacity']) && empty($params['station'])) {
            shuffle($results);
        }

        return new DataResponse(Response::HTTP_OK, ['results' => $results, 'count' => $countOfWorkspaces]);
    }

    /**
     * @param array $params
     * @return DataResponse
     * @throws \Exception
     */
    public function searchMobile(array $params) : DataResponse
    {
        [$ok, $data] = $this->preparingInputSearchParams($params);

        if (!$ok) {
            return new DataResponse(
                $data['status'],
                [
                    'message' => $data['message']
                ]
            );
        }

        $params = $data;

        $workspaces = $this->workSpaceModelManager->search($params);

        if (empty($workspaces)) {
            return new DataResponse(Response::HTTP_OK,
                ['results' => $workspaces, 'count' => 0]
            );
        }

        $filtered = $this->searchMobileResultProcessing(
            $workspaces,
            $params
        );

        $countOfWorkspaces = count($filtered);

        $start = isset($params['start']) ? intval($params['start']) : 0;

        $limit = isset($params['limit']) ? intval($params['limit']) : 30;

        $results = array_slice($filtered, $start, $limit);

        //default sorting should be by distance of the station
        // when searching by station
        if (!empty($results) && !empty($params['station']) && empty($params['price']) && empty($params['date'])) {
            //get full name of searched subway station
            $lastPart = strstr(implode(',', array_column($results[0]['location']['nearby'], 'name')), $params['station']);
            $station = substr($lastPart, 0, strpos($lastPart, ','));

            usort($results, function ($locationA, $locationB) use ($results, $station) {
                //get positions of searched station at the results
                $positionA = array_search($station, array_column($locationA['location']['nearby'], 'name'));
                $positionB = array_search($station, array_column($locationB['location']['nearby'], 'name'));

                return $locationA['location']['nearby'][$positionA]['distance'] > $locationB['location']['nearby'][$positionB]['distance'];

            });
        }

        if (empty($params['price']) && empty($params['date']) && empty($params['capacity']) && empty($params['station'])) {
            shuffle($results);
        }

        return new DataResponse(Response::HTTP_OK, ['results' => $results, 'count' => $countOfWorkspaces]);
    }

    /**
     * @param array $params
     * @return array
     */
    private function preparingInputSearchParams(array $params) : array
    {
        if (!isset($params['duration'])) {
            return [
                false,
                [
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message' => 'Duration parameter is required'
                ]
            ];
        }

        if ($params['duration'] !== self::HOURLY && $params['duration'] !== self::MONTHLY) {
            return [
                false,
                [
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message' => 'Wrong duration parameter'
                ]
            ];
        }

        if (!empty($params['type'])) {
            if ($params['duration'] === self::HOURLY && $params['type'] === WorkSpace::PRIVATE_OFFICE) {
                return [
                    false,
                    [
                        'status' => Response::HTTP_BAD_REQUEST,
                        'message' => 'Invalid type for requested duration'
                    ]
                ];
            } else if ($params['duration'] === self::MONTHLY && $params['type'] === WorkSpace::MEETING_ROOM) {
                return [
                    false,
                    [
                        'status' => Response::HTTP_BAD_REQUEST,
                        'message' => 'Invalid type for requested duration'
                    ]
                ];
            }
        } else {
            if ($params['duration'] === self::HOURLY) {
                $params['type'] = WorkSpace::hourlyWorkspaceTypes();
            } else {
                $params['type'] = WorkSpace::monthlyWorkspaceTypes();
            }
        }

        if (!empty($params['station'])) {
            $params['station'] = trim($params['station']);
        }

        if (!empty($params['address'])) {
            $params['address'] = $this->handleInputs($params['address']);
            $params['coordinates'] = $this->googleMapService->getCoordinatesByPlace($params['address']);
            $params['searchRadius'] = $this->searchRadius;
        }
        if (!empty($params['area'])) {
            $params['coordinates'] = $this->googleMapService->getCoordinatesByPlace($params['area']);
            $params['searchRadius'] = $this->searchRadius;
        }
        if (!empty($params['station'])) {
            $params['coordinates'] = $this->googleMapService->getCoordinatesByPlace($params['station']);
            $params['searchRadius'] = $this->searchRadius;
        }

        return [
            true,
            $params
        ];
    }

    /**
     * @param array $workspaces
     * @param array $params
     * @param bool|null $additionalInfoNeeded
     * @return array
     * @throws \Exception
     */
    protected function searchResultProcessing
    (
        array $workspaces,
        array $params,
        ?bool $additionalInfoNeeded = true
    ) : array
    {
        $availableFrom = $params['availableFrom'] ?? null;

        $results = [];

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $workspace) {

            if ($this->searchProccesCheckings($workspace, $params)) {
                continue;
            }

            if ($workspace->getType() === WorkSpace::DESK && in_array($workspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks())) {
                $addedWorkspaces = array_filter($results, function ($currentWorkspace) use ($workspace) {
                    if ($currentWorkspace['location']['id'] == $workspace->getLocation()->getId()) {
                        return $currentWorkspace;
                    }
                });

                $i = array_search($workspace->getType(), array_column(array_column($addedWorkspaces, 'workspace'), 'type'));

                if ($i === false) {
                    $results[] = $this->workSpaceModelManager->locationCardView($workspace, $availableFrom, $additionalInfoNeeded);
                 }
            } else {
                $results[] = $this->workSpaceModelManager->locationCardView($workspace, $availableFrom, $additionalInfoNeeded);
            }
        }

        return $results;
    }

    /**
     * @param array $workspaces
     * @param array $params
     * @return array
     * @throws \Exception
     */
    protected function searchMobileResultProcessing
    (
        array $workspaces,
        array $params
    ) : array
    {
        $availableFrom = $params['availableFrom'] ?? null;

        $results = [];

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($workspaces) as $workspace) {

            if ($this->searchProccesCheckings($workspace, $params)) {
                continue;
            }

            if ($workspace->getType() === WorkSpace::DESK && in_array($workspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks())) {
                $addedWorkspaces = array_filter($results, function ($currentWorkspace) use ($workspace) {
                    if ($currentWorkspace['location']['id'] == $workspace->getLocation()->getId()) {
                        return $currentWorkspace;
                    }
                });

                $i = array_search($workspace->getType(), array_column(array_column($addedWorkspaces, 'workspace'), 'type'));

                if ($i === false) {
                    $results[] = $this->workSpaceModelManager->locationCardMobileView($workspace, $availableFrom);
                 }
            } else {
                $results[] = $this->workSpaceModelManager->locationCardMobileView($workspace, $availableFrom);
            }
        }

        return $results;
    }


    /**
     * @param WorkSpace $workspace
     * @param array $params
     * @return bool
     */
    private function searchProccesCheckings(WorkSpace $workspace, array $params) : bool
    {
        if ($workspace->getLocation() === null) {
            return true;
        }

        if ($params['duration'] === self::HOURLY) {
            if ($workspace->getType() === WorkSpace::PRIVATE_OFFICE) {
                return true;
            }

            if ($workspace->getType() === WorkSpace::DESK && !in_array($workspace->getDeskType(), WorkSpace::getHourlyDeskTypes())) {
                return true;
            }
        } else {
            if ($workspace->getType() === WorkSpace::MEETING_ROOM) {
                return true;
            }

            if ($workspace->getType() === WorkSpace::DESK && in_array($workspace->getDeskType(), WorkSpace::getHourlyDeskTypes())) {
                return true;
            }
        }

        if (!empty($params['facilities'])) {
            if ($workspace->getFacilities()->isEmpty()) {
                return true;
            }

            if ($this->checkForFacilities($workspace->getFacilities(), $params['facilities']) === false) {
                return true;
            }
        }

        if (!empty($params['station'])) {
            if (stripos(implode(',', array_column($workspace->getLocation()->getNearby(), 'name')), $params['station']) === false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Collection|Facility[] $wsFacilities
     * @param array $facilities
     * @return bool
     */
    private function checkForFacilities(Collection $wsFacilities, array $facilities) : bool
    {
        $ok = true;

        foreach ($facilities as $facilitySlug) {
            $satisfy = false;
            /** @var Facility $facility */
            foreach ($wsFacilities as $index => $facility) {
                if ($facility->getIsActive() && $facility->getSlug() === $facilitySlug) {
                    $satisfy = true;
                    unset($wsFacilities[$index]);
                }
            }

            if (!$satisfy) {
                $ok = false;
                break;
            }
        }

        return $ok;
    }

    /**
     * @param string $input
     * @return string
     */
    private function handleInputs(string $input) : string
    {
        foreach (self::SEARCH_QUERIES as $key => $value) {
            if (strpos($input, $key) !== false) {
                $input = str_replace($key, $value, $input);
            }
        }

        return $input;
    }

    /**
     * @param string $input
     * @param null|string $sessionToken
     * @return array
     */
    public function getSuggestionList(string $input, ?string $sessionToken) : array
    {
        $needed = [
            'transit_station', 'subway_station', 'street_address', 'route', 'bus_station', 'train_station'
        ];

        $input = trim(strtolower($input));

        $input = $this->handleInputs($input);

        $result = [];
        $client = new GuzzleHttp\Client();

        try {
            $places = $client->get(
                sprintf(self::GOOGLE_MAPS_PLACE . '/json?input=%s&key=%s&location=51.506014, -0.108947&components=country:gb&language=en-GB&radius=52000&strictbounds&sessiontoken=%s',
                    $input,
                    $this->googleApiKey,
                    $sessionToken
                )
            );

            $response = json_decode($places->getBody(), true);

            if (!isset($response['predictions'])) {
                return $result;
            }
        } catch (\Exception $exception) {

        }

        if (!empty($response)) {
            foreach ($response['predictions'] as $place) {
            $intersected = array_intersect($place['types'], $needed);
            foreach ($intersected as $item) {
                if ($item == $needed[0] || $item == $needed[1] || $item == $needed[4] || $item == $needed[5]) {
                    $result[] = ['value' => $place['structured_formatting']['main_text'], 'type' => 'station'];
                } else {
                    $result[] = ['value' => $place['structured_formatting']['main_text'], 'type' => 'address'];
                }
            }

            }
        }

        $areas = $this->areaModelManager->getActiveAreasSuggestion($input);

        if (!empty($areas)) {
            $result = array_merge($result, $areas);
        }

        $boroughs = $this->boroughModelManager->getActiveBoroughsSuggestion($input);

        if (!empty($boroughs)) {
            $result = array_merge($result, $boroughs);
        }

        return $result;
    }
}
