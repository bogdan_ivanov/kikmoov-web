<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\User;
use App\Entity\UserDevice;
use App\Entity\WorkSpace;
use App\Model\DeviceModelManager;
use App\Model\WorkSpaceModelManager;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Validators\UserValidator;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserApiManager
 * @package App\Managers
 */
class UserApiManager
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var UserValidator
     */
    private $userValidator;

    /**
     * @var WorkSpaceModelManager
     */
    private $workSpaceModelManager;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var DeviceModelManager
     */
    private $userDeviceModelManager;

    /**
     * UserApiManager constructor.
     * @param UserManager $userManager
     * @param UserValidator $userValidator
     * @param WorkSpaceModelManager $workSpaceModelManager
     * @param NotificationService $notificationService
     * @param DeviceModelManager $deviceModelManager
     */
    public function __construct(
        UserManager $userManager,
        UserValidator $userValidator,
        WorkSpaceModelManager $workSpaceModelManager,
        NotificationService $notificationService,
        DeviceModelManager $deviceModelManager
    ) {
        $this->userManager = $userManager;
        $this->userValidator = $userValidator;
        $this->workSpaceModelManager = $workSpaceModelManager;
        $this->notificationService = $notificationService;
        $this->userDeviceModelManager = $deviceModelManager;
    }

    /**
     * @param User $user
     * @param int $id
     * @return DataResponse
     */
    public function likeWorkspace(User $user, int $id) : DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        $workspace = $res['workspace'];

        if ($user->getLikedWorkspaces()->contains($workspace)) {
            $user->removeLikedWorkspace($workspace);
        } else {
            $user->addLikedWorkspace($workspace);
        }

        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @param array $data
     * @return DataResponse
     */
    public function updateUserData(User $user, array $data) : DataResponse
    {
        $errors = $this->userValidator->updateUserDetails($user, $data);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Profile has been updated successfully',
            'details' => $this->getUserDetails($user)
        ]);
    }

    /**
     * @param User $user
     * @return array
     */
    protected function getUserDetails(User $user) : array
    {
        return [
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'companyName' => $user->getCompanyName(),
            'pushStatus' => $user->getPushStatus()
        ];
    }

    /**
     * @param User $user
     * @return DataResponse
     */
    public function getDetails(User $user) : DataResponse
    {
        return new DataResponse(Response::HTTP_OK, [
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'companyName' => $user->getCompanyName(),
            'pushStatus' => $user->getPushStatus()
        ]);
    }

    /**
     * @param User $user
     * @return DataResponse
     */
    public function triggerPush(User $user) : DataResponse
    {
        $user->setPushStatus(!$user->getPushStatus());
        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return DataResponse
     */
    public function delete(User $user) : DataResponse
    {
        $this->notificationService->sendEmail(
            $user,
            NotificationsEvent::ACCOUNT_DELETED,
            [
                'user' => $user
            ]
        );

        $this->userManager->deleteUser($user);

        return new DataResponse(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param User $user
     * @return DataResponse
     */
    public function getSellerDetails(User $user) : DataResponse
    {
        return new DataResponse(Response::HTTP_OK, [
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'companyName' => $user->getCompanyName(),
            'address' => $user->getAddress(),
            'optionalAddress' => $user->getOptionalAddress(),
            'town' => $user->getTown(),
            'postcode' => $user->getPostcode(),
            'invoiceEmail' => $user->getInvoiceEmail()
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     * @return DataResponse
     */
    public function updateSeller(User $user, array $data) : DataResponse
    {
        $errors = $this->userValidator->updateSellerDetails($user, $data);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Profile has been updated successfully'
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     * @return DataResponse
     */
    public function updateUserDevice(User $user, array $data) : DataResponse
    {
        $device = $this->userDeviceModelManager->findOneBy(['deviceIdentifier' => $data['uniqueID']]);

        if (is_null($device)) {
            return $this->deviceChecker($user, $data);
        }

        $needFlush = false;

        if ($device->getDeviceToken() !== $data['token']) {
            $device->setDeviceToken($data['token']);
            $needFlush = true;
        }

        if ($device->getUsers()->contains($user) === false) {
            $device->addUser($user);
            $needFlush = true;
        }

        if ($needFlush) {
            try {
                $this->userDeviceModelManager->update($device);
            } catch (\Exception $exception) {
                return new DataResponse(
                    Response::HTTP_BAD_REQUEST,
                    []
                );
            }
        }

        return new DataResponse(
            Response::HTTP_OK,
            [
                'device' => $device
            ]
        );
    }

    /**
     * @param User $user
     * @param array $deviceInfo
     * @return DataResponse
     */
    public function deviceChecker(User $user, array $deviceInfo) : DataResponse
    {
        $device = $this->userDeviceModelManager->findOneBy(['deviceIdentifier' => $deviceInfo['uniqueID']]);

        if ($device instanceof UserDevice) {
            $device->setLastAuthorization(new \DateTime());

            if ($device->getDeviceToken() !== $deviceInfo['token']) {
                $device->setDeviceToken($deviceInfo['token']);
            }
        } else {
            if (stripos($deviceInfo['os'], UserDevice::IOS) !== false) {
                $deviceInfo['os'] = UserDevice::IOS;
            }
            $device = new UserDevice();
            $device->addUser($user);
            $device->setDeviceToken($deviceInfo['token']);
            $device->setDeviceIdentifier($deviceInfo['uniqueID']);
            $device->setLastAuthorization(new \DateTime());
            $device->setDeviceOS(strtolower($deviceInfo['os']));
            $device->setDeviceModel($deviceInfo['model']);
            $device->setDeviceName($deviceInfo['deviceName']);
            $device->setVersion($deviceInfo['version']);
        }

        $this->userDeviceModelManager->update($device);

        return new DataResponse(
            Response::HTTP_OK,
            [
                'device' => $device
            ]
        );
    }
}
