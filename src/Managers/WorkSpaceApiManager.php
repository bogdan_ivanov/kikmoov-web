<?php

namespace App\Managers;

use App\Controller\DataResponse;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\Viewing;
use App\Entity\WorkSpace;
use App\Model\WorkSpaceModelManager;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationService;
use App\Notifications\NotificationsEvent;
use App\Provider\WorkspaceMediaProvider;
use App\Services\RouterWrapper;
use App\Traits\YieldTrait;
use App\Validators\WorkSpaceValidator;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WorkSpaceApiManager
 * @package App\Managers
 */
class WorkSpaceApiManager
{
    use YieldTrait;

    /**
     * @var WorkSpaceModelManager
     */
    private $workSpaceModelManager;

    /**
     * @var WorkSpaceValidator
     */
    private $validator;

    /**
     * @var WorkspaceMediaProvider
     */
    protected $workspaceMediaProvider;

    /**
     * @var BookingApiManager
     */
    private $bookingApiManager;

    /**
     * @var ViewingApiManager
     */
    private $viewingApiManager;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var RouterWrapper
     */
    private $router;

    /**
     * WorkSpaceApiManager constructor.
     * @param WorkSpaceModelManager $workSpaceModelManager
     * @param WorkSpaceValidator $validator
     * @param WorkspaceMediaProvider $workspaceMediaProvider
     * @param BookingApiManager $bookingApiManager
     * @param ViewingApiManager $viewingApiManager
     * @param NotificationService $notificationService
     * @param RouterWrapper $routerWrapper
     */
    public function __construct(
        WorkSpaceModelManager $workSpaceModelManager,
        WorkSpaceValidator $validator,
        WorkspaceMediaProvider $workspaceMediaProvider,
        BookingApiManager $bookingApiManager,
        ViewingApiManager $viewingApiManager,
        NotificationService $notificationService,
        RouterWrapper $routerWrapper
    )
    {
        $this->workSpaceModelManager = $workSpaceModelManager;
        $this->validator = $validator;
        $this->workspaceMediaProvider = $workspaceMediaProvider;
        $this->bookingApiManager = $bookingApiManager;
        $this->viewingApiManager = $viewingApiManager;
        $this->notificationService = $notificationService;
        $this->router = $routerWrapper;
    }

    /**
     * @param Location $location
     * @param array $data
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addWorkspace(Location $location, array $data)
    {
        if (!isset($data['type']) || !in_array($data['type'], WorkSpace::getWorkspaceTypes())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                [
                    'message' => sprintf(
                        'Missed or unknown workspace type. Available options: %s',
                        implode(", ", WorkSpace::getWorkspaceTypes())
                    )
                ]
            );
        }

        $workspace = $this->workSpaceModelManager->createWorkspace();
        $workspace->setType($data['type']);
        $workspace->setLocation($location);
        $location->getUser()->setStatus(User::DRAFT_STARTED);

        $errors = $this->validator->validateRequest($workspace, $data, WorkSpaceValidator::CREATE_ACTION);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        if ($data['type'] === WorkSpace::PRIVATE_OFFICE || $workspace->checkAvailableFromFieldRequired($workspace)) {
            if ($data['availableFrom']) {
                $availableFrom = (new \DateTime())->setTimestamp($data['availableFrom']);
            } else {
                $availableFrom = new \DateTime();
            }

            $workspace->setAvailableFrom($availableFrom);
        }

        if (isset($data['facilities']) && is_array($data['facilities']) && !empty($data['facilities'])) {
            $this->workSpaceModelManager->addFacilitiesToWorkSpace($workspace, $data['facilities']);
        }

        $this->workSpaceModelManager->updateWorkspace($workspace);

        $this->notificationService->sendEmail(
            $location->getUser(),
            NotificationsEvent::WORKSPACE_WAITING_APPROVE,
            [
                'workspace' => $workspace,
                'seller' => $location->getUser(),
                'location' => $location,
                'adminLink' => $this->router->generateAdminLink('admin_app_workspace_edit', ['id' => $workspace->getId()]),
                'linkPortal' => $this->router->generateFrontLink(FrontUrls::MAINPORTAL)
            ]
        );

        return new DataResponse(Response::HTTP_CREATED, ['id' => $workspace->getId()]);
    }

    /**
     * @param User $user
     * @param int $workspaceID
     * @return array
     */
    private function checkOwnership(User $user, int $workspaceID): array
    {
        [$ok, $data] = $this->workSpaceModelManager->checkWorkspaceExistence($workspaceID);

        if (!$ok) {
            return [
                $ok,
                $data
            ];
        }

        /** @var Location $location */
        ['location' => $location, 'workspace' => $workspace] = $data;

        $locationUser = $location->getUser();

        if ($locationUser === null) {
            return [
                false,
                ['message' => 'No location owner was found']
            ];
        }

        if ($locationUser->getId() !== $user->getId()) {
            return [
                false,
                ['message' => 'You are not the owner of this workspace']
            ];
        }

        return [
            true,
            ['location' => $location, 'workspace' => $workspace]
        ];
    }

    /**
     * @param User $user
     * @param int $id
     * @param []UploadedFile $images
     * @return DataResponse
     * @throws \Exception
     */
    public function addMedias(User $user, int $id, array $images)
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        ['workspace' => $workspace] = $res;

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            if (!$image instanceof UploadedFile) {
                continue;
            }

            $this->workspaceMediaProvider->uploadMedia($workspace, $image);
        }

        $urls = $this->workspaceMediaProvider->getPublicLinks($workspace);

        return new DataResponse(Response::HTTP_OK, ['images' => $urls]);
    }

    /**
     * @param User $user
     * @param int $id
     * @param array $data
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(User $user, int $id, array $data)
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /** @var WorkSpace $workspace */
        ['workspace' => $workspace] = $res;

        if (isset($data['type']) && !in_array($data['type'], WorkSpace::getWorkspaceTypes())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                [
                    'message' => sprintf(
                        'Missed or unknown workspace type. Available options: %s',
                        implode(", ", WorkSpace::getWorkspaceTypes())
                    )
                ]
            );
        }

        $workspace->setType($data['type']);

        $errors = $this->validator->validateRequest($workspace, $data, WorkSpaceValidator::UPDATE_ACTION);

        if (!empty($errors)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['errors' => $errors]);
        }

        if (isset($data['availableFrom']) && $data['availableFrom']) {
            $availableFrom = (new \DateTime())->setTimestamp($data['availableFrom']);
            $workspace->setAvailableFrom($availableFrom);
        }

        if (isset($data['facilities']) && is_array($data['facilities']) && !empty($data['facilities'])) {
            //because many to many table
            $workspace->getFacilities()->clear();
            $this->workSpaceModelManager->addFacilitiesToWorkSpace($workspace, $data['facilities']);
        }

        $this->workSpaceModelManager->updateWorkspace($workspace);

        return new DataResponse(Response::HTTP_OK, ['id' => $workspace->getId()]);
    }

    /**
     * @param User $user
     * @param int $id
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(User $user, int $id): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['workspace' => $workspace, 'location' => $location] = $res;

        $this->notificationService->sendEmail(
            $location->getUser(),
            NotificationsEvent::WORKSPACE_DELETED,
            [
                'workspace' => $workspace,
                'seller' => $location->getUser(),
                'location' => $location
            ]
        );

        $this->workSpaceModelManager->deleteWorkspace($workspace);

        return new DataResponse(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param User $user
     * @param int $id
     * @return DataResponse
     * @throws \Exception
     */
    public function sellerView(User $user, int $id): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /** @var WorkSpace $workspace */
        ['workspace' => $workspace] = $res;

        //some fields are duplicated, it is done on purpose for better reading and understanding the code
        if ($workspace->getType() === WorkSpace::MEETING_ROOM) {
            $workspaceData = [
                'quantity' => $workspace->getQuantity(),
                'price' => $workspace->getPrice(),
                'size' => $workspace->getSize(),
                'capacity' => $workspace->getCapacity(),
                'description' => $workspace->getDescription(),
                'deskType' => $workspace->getDeskType(),
                'availableFrom' => !is_null($workspace->getAvailableFrom()) ? $workspace->getAvailableFrom()->getTimestamp() : null,
            ];
        } elseif ($workspace->getType() === WorkSpace::PRIVATE_OFFICE) {
            $workspaceData = [
                'quantity' => $workspace->getQuantity(),
                'price' => $workspace->getPrice(),
                'size' => $workspace->getSize(),
                'capacity' => $workspace->getCapacity(),
                'minContractLength' => $workspace->getMinContractLength(),
                'availableFrom' => !is_null($workspace->getAvailableFrom()) ? $workspace->getAvailableFrom()->getTimestamp() : null,
                'description' => $workspace->getDescription()
            ];
        } else {
            $workspaceData = [
                'quantity' => $workspace->getQuantity(),
                'price' => $workspace->getPrice(),
                'deskType' => $workspace->getDeskType(),
                'minContractLength' => $workspace->getMinContractLength(),
                'availableFrom' => !is_null($workspace->getAvailableFrom()) ? $workspace->getAvailableFrom()->getTimestamp() : null,
                'description' => $workspace->getDescription()
            ];
        }

        $workspaceData['id'] = $id;
        $workspaceData['coverImageUrl'] = $workspace->getCoverImageUrl();
        $workspaceData['coverImageId'] = $workspace->getCoverImageId();
        $workspaceData['type'] = $workspace->getType();
        $workspaceData['images'] = $this->workspaceMediaProvider->getPublicLinks($workspace);
        $workspaceData['opensFrom'] = $workspace->getOpensFrom();
        $workspaceData['closesAt'] = $workspace->getClosesAt();
        $workspaceData['facilities'] = $this->workSpaceModelManager->getListOfFacilities($workspace);

        return new DataResponse(Response::HTTP_OK, $workspaceData);
    }

    /**
     * @param int $id
     * @param User|null $user
     * @return DataResponse
     * @throws \Exception
     */
    public function view(int $id, ?User $user): DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['location' => $location, 'workspace' => $workspace] = $res;

        $bookedDates = (is_null($user)) ? [] : $this->bookingApiManager->getBookedDates($workspace);
        $userData = $this->workSpaceModelManager->userCardView($workspace);

        $workspaceEvents = $this->workSpaceModelManager->workspaceEvents($workspace);
        $workspaceInfo = $this->workSpaceModelManager->workspaceInfo($workspace);
        $workspaceFacilities = $this->workSpaceModelManager->workspaceFacilities($workspace);
        $workspaceImages = $this->workSpaceModelManager->workspaceImagesView($workspace, false, MediaProviderInterface::FORMAT_REFERENCE);

        $workspaceData = array_merge($workspaceInfo, $workspaceFacilities, $workspaceImages, $workspaceEvents);

        return new DataResponse(Response::HTTP_OK, [
            'location' => [
                'id' => $location->getId(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'addressOptional' => $location->getOptionalAddress(),
                'description' => $location->getDescription(),
                'town' => $location->getTown(),
                'postcode' => $location->getPostcode(),
                'latitude' => $location->getLatitude(),
                'longitude' => $location->getLongitude(),
                'videoLink' => $location->getVideoLink(),
                'nearby' => $location->getNearby()
            ],
            'workspace' => $workspaceData,
            'related' => $this->workSpaceModelManager->getRelatedSeparateWorkspaces($workspace),
            'bookedDates' => $bookedDates,
            'user' => $userData
        ]);
    }

    /**
     * @param int $id
     * @param User|null $user
     * @return DataResponse
     * @throws \Exception
     */
    public function mobileView(int $id, ?User $user): DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['location' => $location, 'workspace' => $workspace] = $res;

        $bookedDates = (is_null($user)) ? [] : $this->bookingApiManager->getBookedDates($workspace);
        $userData = $this->workSpaceModelManager->userCardView($workspace);

        $workspaceEvents = $this->workSpaceModelManager->workspaceEvents($workspace);
        $workspaceInfo = $this->workSpaceModelManager->workspaceInfo($workspace);
        $workspaceFacilities = $this->workSpaceModelManager->workspaceFacilities($workspace);
        $workspaceImages = $this->workSpaceModelManager->workspaceImagesView($workspace);

        $workspaceData = array_merge($workspaceInfo, $workspaceFacilities, $workspaceImages, $workspaceEvents);


        return new DataResponse(Response::HTTP_OK, [
            'location' => [
                'id' => $location->getId(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'addressOptional' => $location->getOptionalAddress(),
                'description' => $location->getDescription(),
                'town' => $location->getTown(),
                'postcode' => $location->getPostcode(),
                'latitude' => $location->getLatitude(),
                'longitude' => $location->getLongitude(),
                'videoLink' => $location->getVideoLink(),
                'nearby' => $location->getNearby()
            ],
            'workspace' => $workspaceData,
            'related' => $this->workSpaceModelManager->getMobileRelatedSeparateWorkspaces($workspace),
            'bookedDates' => $bookedDates,
            'user' => $userData
        ]);
    }


    /**
     * @param int $id
     * @return DataResponse
     * @throws \Exception
     */
    public function workspaceImagesView(int $id) : DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['location' => $location, 'workspace' => $workspace] = $res;


        return new DataResponse(Response::HTTP_OK, [
            $this->workSpaceModelManager->workspaceImagesView($workspace)
        ]);
    }

    /**
     * @param int $id
     * @return DataResponse
     * @throws \Exception
     */
    public function getBookedDates(int $id) : DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['location' => $location, 'workspace' => $workspace] = $res;

        $bookedDates = $this->bookingApiManager->getBookedDates($workspace);

        return new DataResponse(Response::HTTP_OK,
            [
                'bookedDates' => $bookedDates
            ]
        );
    }

    /**
     * @param User $user
     * @param int $id
     * @param array $data
     * @return DataResponse
     * @throws \Exception
     */
    public function addViewing(User $user, int $id, array $data) : DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        return $this->viewingApiManager->addViewing($user, $workspace, $data);
    }

    /**
     * @param User $user
     * @param int $id
     * @param int $mediaId
     * @return DataResponse
     */
    public function deleteImage(User $user, int $id, int $mediaId): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /** @var WorkSpace $workspace */
        ['workspace' => $workspace] = $res;

        $ok = $this->workspaceMediaProvider->removeMedia($workspace, $mediaId);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => "Image not exist"
            ]);
        }

        return new DataResponse(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param User $user
     * @param int $id
     * @param int $mediaId
     * @return DataResponse
     * @throws \Exception
     */
    public function setCoverImage(User $user, int $id, int $mediaId): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($user, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /** @var WorkSpace $workspace */
        ['workspace' => $workspace] = $res;

        $ok = $this->workspaceMediaProvider->setCoverImage($workspace, $mediaId);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => "Cover image does not exist or cannot be saved"
            ]);
        }

        return new DataResponse(Response::HTTP_OK, ['coverImageUrl' => $workspace->getCoverImageUrl()]);
    }

    /**
     * @param User $user
     * @param int $id
     * @param array $data
     * @return DataResponse
     * @throws \Exception
     */
    public function addBooking(User $user, int $id, array $data): DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        return $this->bookingApiManager->addBooking($user, $workspace, $data);
    }

    /**
     * @param User $user
     * @param int $id
     * @param array $data
     * @return DataResponse
     * @throws \Exception
     */
    public function addBookingRequest(User $user, int $id, array $data): DataResponse
    {
        [$ok, $res] = $this->workSpaceModelManager->checkWorkspaceExistence($id, false);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        return $this->bookingApiManager->addBookingRequest($user, $workspace, $data);
    }

    /**
     * @param User $user
     * @return DataResponse
     * @throws \Exception
     */
    public function getLikedWorkspaces(User $user): DataResponse
    {
        if (empty($user->getLikedWorkspaces())) {
            return new DataResponse(Response::HTTP_OK, ['workspaces' => []]);
        }

        $workspaces = [];

        /** @var WorkSpace $workspace */
        foreach ($this->yieldCollection($user->getLikedWorkspaces()) as $workspace) {
            if ($workspace->getStatus() === WorkSpace::ACTIVE) {
                $workspaces[] = $this->workSpaceModelManager->locationCardViewSaved($workspace);
            }
        }

        return new DataResponse(Response::HTTP_OK, ['workspaces' => $workspaces]);
    }

    /**
     * @param User $seller
     * @param int $id
     * @param int $bookingId
     * @param string $status
     * @return DataResponse
     */
    public function updateBookingStatus(User $seller, int $id, int $bookingId, string $status): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($seller, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         * @var Location $location
         */
        ['workspace' => $workspace] = $res;

        return $this->bookingApiManager->updateBookingStatus($workspace, $bookingId, $status);
    }

    /**
     * @param User $seller
     * @param int $id
     * @param int $viewingId
     * @param string $status
     * @return DataResponse
     */
    public function updateViewingStatus(User $seller, int $id, int $viewingId, string $status): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($seller, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        $dataResponse = $this->viewingApiManager->updateStatus($workspace, $viewingId, $status);

        return $dataResponse;
    }

    /**
     * @param User $seller
     * @param int $id
     * @param string $status
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateWorkspaceStatus(User $seller, int $id, string $status) : DataResponse
    {
        [$ok, $res] = $this->checkOwnership($seller, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        if (!in_array($status, WorkSpace::getPermissibleStatusesList())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => "Wrong status"]
            );
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        if ($workspace->getStatus() === $status) {
            return new DataResponse(
                Response::HTTP_OK,
                [
                    'message' => 'Nothing to change'
                ]
            );
        }

        if (!in_array($workspace->getStatus(), WorkSpace::getPermissibleStatusesList())) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                [
                    'message' => 'Updating status is not allowed for this workspace'
                ]
            );
        }

        $workspace->setStatus($status);
        $this->workSpaceModelManager->updateWorkspace($workspace);

        return new DataResponse(Response::HTTP_OK,
            [
                'message' => 'Workspace was successfully updated',
                'status' => $workspace->getStatus()
            ]
        );
    }

    /**
     * @param User $seller
     * @param int $id
     * @return DataResponse
     * @throws \Exception
     */
    public function getViewingList(User $seller, int $id): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($seller, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        return $this->viewingApiManager->getList($workspace);
    }

    /**
     * @param User $seller
     * @param int $id
     * @return DataResponse
     * @throws \Exception
     */
    public function getBookingList(User $seller, int $id): DataResponse
    {
        [$ok, $res] = $this->checkOwnership($seller, $id);

        if (!$ok) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, $res);
        }

        /**
         * @var WorkSpace $workspace
         */
        ['workspace' => $workspace] = $res;

        return $this->bookingApiManager->getList($workspace);
    }

    /**
     * @param User $user
     * @return DataResponse
     * @throws \Exception
     */
    public function getUserRequests(User $user): DataResponse
    {
        $bookings = $this->bookingApiManager->getBookingModelManager()->getBookingWorkspaces($user);
        $viewings = $this->viewingApiManager->getViewingModelManager()->getViewingWorkspaces($user);

        $res = array_merge($bookings, $viewings);

        if (empty($res)) {
            return new DataResponse(Response::HTTP_OK);
        }

        $filtered = $this->workSpaceModelManager->sortRequests($res, 'endTime', 'desc');

        return new DataResponse(Response::HTTP_OK, $filtered);
    }

    /**
     * @return DataResponse
     * @throws \Exception
     */
    public function getTopBookedWorkspaces() : DataResponse
    {
        $workspaces = $this->workSpaceModelManager->getTopBookedWorkspaces();
        $topPicks = $this->workSpaceModelManager->getTopPicksWorkspaces();

        if (!empty($workspaces) && !empty($topPicks)) {
            array_unshift($workspaces, ...$topPicks);
        } elseif (empty($workspaces) && !empty($topPicks)) {
            $workspaces = $topPicks;
        }

        return new DataResponse(Response::HTTP_OK, ['results' => $workspaces]);
    }
}
