<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181102095525 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE work_space_facilities (work_space_id INT NOT NULL, facility_id INT NOT NULL, INDEX IDX_8E8AB05EF6E2D91C (work_space_id), INDEX IDX_8E8AB05EA7014910 (facility_id), PRIMARY KEY(work_space_id, facility_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE work_space_facilities ADD CONSTRAINT FK_8E8AB05EF6E2D91C FOREIGN KEY (work_space_id) REFERENCES work_space (id)');
        $this->addSql('ALTER TABLE work_space_facilities ADD CONSTRAINT FK_8E8AB05EA7014910 FOREIGN KEY (facility_id) REFERENCES facility (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE work_space_facilities');
    }
}
