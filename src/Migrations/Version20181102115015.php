<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181102115015 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE viewing ADD work_space_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE viewing ADD CONSTRAINT FK_F5BB4698F6E2D91C FOREIGN KEY (work_space_id) REFERENCES work_space (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_F5BB4698F6E2D91C ON viewing (work_space_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE viewing DROP FOREIGN KEY FK_F5BB4698F6E2D91C');
        $this->addSql('DROP INDEX IDX_F5BB4698F6E2D91C ON viewing');
        $this->addSql('ALTER TABLE viewing DROP work_space_id');
    }
}
