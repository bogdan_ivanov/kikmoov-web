<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181203162954 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facility ADD web_icon_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE facility ADD CONSTRAINT FK_105994B2BC1679AC FOREIGN KEY (web_icon_id) REFERENCES media (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_105994B2BC1679AC ON facility (web_icon_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facility DROP FOREIGN KEY FK_105994B2BC1679AC');
        $this->addSql('DROP INDEX UNIQ_105994B2BC1679AC ON facility');
        $this->addSql('ALTER TABLE facility DROP web_icon_id');
    }
}
