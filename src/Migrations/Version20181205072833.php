<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181205072833 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_request DROP FOREIGN KEY FK_6129CABFA76ED395');
        $this->addSql('DROP INDEX IDX_6129CABFA76ED395 ON booking_request');
        $this->addSql('ALTER TABLE booking_request ADD status VARCHAR(25) DEFAULT \'pending\' NOT NULL, DROP user_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_request ADD user_id INT DEFAULT NULL, DROP status');
        $this->addSql('ALTER TABLE booking_request ADD CONSTRAINT FK_6129CABFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_6129CABFA76ED395 ON booking_request (user_id)');
    }
}
