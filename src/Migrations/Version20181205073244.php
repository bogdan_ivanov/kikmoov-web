<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181205073244 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking ADD booking_type VARCHAR(64) NOT NULL, ADD information LONGTEXT NOT NULL, ADD duration VARCHAR(64) NOT NULL, ADD tenant_type VARCHAR(64) NOT NULL, ADD payment_log LONGTEXT DEFAULT NULL, ADD amount NUMERIC(10, 2) NOT NULL, DROP card_holder_name, DROP card_number, DROP ccv, DROP billing_address, DROP city, DROP postcode, DROP expiry_month, DROP expiry_year, CHANGE meeting_name name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking ADD card_holder_name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD card_number VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD ccv VARCHAR(3) NOT NULL COLLATE utf8mb4_unicode_ci, ADD billing_address VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD city VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD postcode VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD expiry_month VARCHAR(2) NOT NULL COLLATE utf8mb4_unicode_ci, ADD expiry_year VARCHAR(2) NOT NULL COLLATE utf8mb4_unicode_ci, DROP booking_type, DROP information, DROP duration, DROP tenant_type, DROP payment_log, DROP amount, CHANGE name meeting_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
