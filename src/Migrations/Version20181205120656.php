<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181205120656 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking ADD payment_token VARCHAR(255) DEFAULT NULL, CHANGE booking_type booking_type VARCHAR(64) DEFAULT \'booking\' NOT NULL, CHANGE tenant_type tenant_type VARCHAR(64) DEFAULT \'freelancer\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP payment_token, CHANGE booking_type booking_type VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE tenant_type tenant_type VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
