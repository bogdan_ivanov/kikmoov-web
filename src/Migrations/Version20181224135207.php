<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181224135207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE internal_notification ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE internal_notification ADD CONSTRAINT FK_22B36E25A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_22B36E25A76ED395 ON internal_notification (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE internal_notification DROP FOREIGN KEY FK_22B36E25A76ED395');
        $this->addSql('DROP INDEX IDX_22B36E25A76ED395 ON internal_notification');
        $this->addSql('ALTER TABLE internal_notification DROP user_id');
    }
}
