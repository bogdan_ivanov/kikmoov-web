<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181224144135 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE viewing DROP FOREIGN KEY FK_F5BB4698F1D1E506');
        $this->addSql('ALTER TABLE viewing ADD CONSTRAINT FK_F5BB4698F1D1E506 FOREIGN KEY (internal_notification_id) REFERENCES internal_notification (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE viewing DROP FOREIGN KEY FK_F5BB4698F1D1E506');
        $this->addSql('ALTER TABLE viewing ADD CONSTRAINT FK_F5BB4698F1D1E506 FOREIGN KEY (internal_notification_id) REFERENCES internal_notification (id) ON DELETE CASCADE');
    }
}
