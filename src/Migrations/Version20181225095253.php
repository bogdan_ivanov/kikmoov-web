<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181225095253 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE liked_workspaces DROP FOREIGN KEY FK_CADD7F37F6E2D91C');
        $this->addSql('ALTER TABLE liked_workspaces ADD CONSTRAINT FK_CADD7F37F6E2D91C FOREIGN KEY (work_space_id) REFERENCES work_space (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE liked_workspaces DROP FOREIGN KEY FK_CADD7F37F6E2D91C');
        $this->addSql('ALTER TABLE liked_workspaces ADD CONSTRAINT FK_CADD7F37F6E2D91C FOREIGN KEY (work_space_id) REFERENCES work_space (id)');
    }
}
