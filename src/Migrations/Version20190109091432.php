<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190109091432 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_space_facilities DROP FOREIGN KEY FK_8E8AB05EA7014910');
        $this->addSql('ALTER TABLE work_space_facilities ADD CONSTRAINT FK_8E8AB05EA7014910 FOREIGN KEY (facility_id) REFERENCES facility (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_space_facilities DROP FOREIGN KEY FK_8E8AB05EA7014910');
        $this->addSql('ALTER TABLE work_space_facilities ADD CONSTRAINT FK_8E8AB05EA7014910 FOREIGN KEY (facility_id) REFERENCES facility (id)');
    }
}
