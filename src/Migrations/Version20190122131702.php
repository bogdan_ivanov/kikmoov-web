<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190122131702 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, status TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_D7943D68989D9B62 (slug), UNIQUE INDEX UNIQ_D7943D683DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE area ADD CONSTRAINT FK_D7943D683DA5256D FOREIGN KEY (image_id) REFERENCES media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE location ADD area_id INT DEFAULT NULL, DROP area');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBBD0F409C FOREIGN KEY (area_id) REFERENCES area (id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CBBD0F409C ON location (area_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBBD0F409C');
        $this->addSql('DROP TABLE area');
        $this->addSql('DROP INDEX IDX_5E9E89CBBD0F409C ON location');
        $this->addSql('ALTER TABLE location ADD area VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP area_id');
    }
}
