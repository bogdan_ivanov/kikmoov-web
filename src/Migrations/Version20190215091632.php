<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190215091632 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_to_device (user_id INT NOT NULL, device_id INT NOT NULL, INDEX IDX_2F1D37F7A76ED395 (user_id), INDEX IDX_2F1D37F794A4C7D4 (device_id), PRIMARY KEY(user_id, device_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, device_name VARCHAR(255) DEFAULT NULL, device_token VARCHAR(255) NOT NULL, device_os VARCHAR(16) NOT NULL, device_model VARCHAR(255) DEFAULT NULL, device_identifier VARCHAR(255) DEFAULT NULL, version VARCHAR(255) DEFAULT NULL, last_authorization DATETIME NOT NULL, count_of_authorizations SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_to_device ADD CONSTRAINT FK_2F1D37F7A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_to_device ADD CONSTRAINT FK_2F1D37F794A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_to_device DROP FOREIGN KEY FK_2F1D37F794A4C7D4');
        $this->addSql('DROP TABLE user_to_device');
        $this->addSql('DROP TABLE device');
    }
}
