<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190408073934 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A14682D40A1F FOREIGN KEY (workspace_id) REFERENCES work_space (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE calendar_event ADD CONSTRAINT FK_57FA09C9A40A2C8 FOREIGN KEY (calendar_id) REFERENCES calendar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_space ADD google_calendar_id VARCHAR(255) DEFAULT NULL, ADD outlook_calendar_id VARCHAR(255) DEFAULT NULL, ADD outlook_calendar_name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE calendar_event DROP FOREIGN KEY FK_57FA09C9A40A2C8');
        $this->addSql('ALTER TABLE work_space DROP google_calendar_id, DROP outlook_calendar_id, DROP outlook_calendar_name');

    }
}
