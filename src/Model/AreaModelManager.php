<?php

namespace App\Model;

use App\Controller\DataResponse;
use App\Entity\Area;
use App\Entity\Borough;
use App\Repository\AreaRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AreaModelManager
 * @package App\Model
 */
class AreaModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * CategoryModelManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return AreaRepository
     */
    protected function getRepository(): AreaRepository
    {
        return $this->entityManager->getRepository(Area::class);
    }

    /**
     * @return DataResponse
     * @throws \Exception
     */
    public function getActiveAreas() : DataResponse
    {
        $areas = $this->getRepository()->getActiveAreas();

        if (empty($areas)) {
            return new DataResponse(Response::HTTP_OK, $areas);
        }

        $result = [];

        /** @var Area $area */
        foreach ($this->yieldCollection($areas) as $area) {
            $result[] = $this->getAreaCard($area);
        }

        return new DataResponse(Response::HTTP_OK, $result);
    }

    /**
     * @param Area $area
     * @return array
     */
    private function getAreaCard(Area $area) : array
    {
        $boroughs = [];
        /** @var Borough $borough */
        foreach ($area->getBoroughs()->getValues() as $borough) {
            $boroughs[] = $borough->getName();
        }

        return [
            'name' => $area->getName() . ' (' . implode(', ', $boroughs) . ')',
            'slug' => $area->getSlug()
        ];
    }

    /**
     * @param $slug
     * @return null|object
     */
    public function getAreaBySlug($slug)
    {
        $area = $this->getRepository()->findOneBy(['slug' => $slug]);

        return $area;
    }

    /**
     * @param $area
     * @return mixed
     */
    public function getActiveAreasSuggestion($area)
    {
        return $this->getRepository()->getActiveAreasSuggestion($area);
    }
}