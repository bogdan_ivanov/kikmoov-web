<?php

namespace App\Model;

use App\Controller\DataResponse;
use App\Entity\Article;
use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ArticleModelManager
 * @package App\Model
 */
class ArticleModelManager
{
    use YieldTrait;
    
    /**
     * @var EntityManager
     */
    private $entityManager;

    private $mediaUrlGenerator;

    /**
     * ArticleModelManager constructor.
     * @param EntityManager $entityManager
     * @param MediaUrlGenerator $mediaUrlGenerator
     */
    public function __construct(EntityManager $entityManager, MediaUrlGenerator $mediaUrlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->mediaUrlGenerator = $mediaUrlGenerator;
    }

    /**
     * @return ArticleRepository
     */
    protected function getRepository(): ArticleRepository
    {
        return $this->entityManager->getRepository(Article::class);
    }

    /**
     * @param string $slug
     * @return DataResponse
     * @throws \Exception
     */
    public function getArticle(string $slug): DataResponse
    {
        $article = $this->getRepository()->findOneBy(['slug' => $slug]);

        if (is_null($article)) {
            return new DataResponse(Response::HTTP_NOT_FOUND, [
                'message' => 'Article does not exist'
            ]);
        }

        $category = $article->getCategory();

        if (is_null($category)) {
            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Article has not category. Please, contact the admin'
            ]);
        }

        $related = $this->getRepository()->getSimilarArticlesByCategory($category, $article->getId());

        $filtered = [];

        if (!empty($related)) {
            /** @var Article $currentArticle */
            foreach ($related as $currentArticle) {
                $filtered[] = $this->articleCardView($currentArticle);
            }
        }

        return new DataResponse(Response::HTTP_OK, [
            'article' => [
                'id' => $article->getId(),
                'coverImageUrl' => $article->getCoverImageUrl(),
                'slug' => $article->getSlug(),
                'title' => $article->getTitle(),
                'body' => $article->getBody(),
                'createdAt' => $article->getCreatedAt()->getTimestamp()
            ],
            'category' => [
                'name' => $category->getName(),
                'slug' => $category->getSlug(),
                'icon' => $this->mediaUrlGenerator->generateUrl($category->getIcon())
            ],
            'related' => $filtered
        ]);
    }

    /**
     * @param Article $article
     * @return array
     */
    protected function articleCardView(Article $article) : array
    {
        return [
            'id' => $article->getId(),
            'coverImageUrl' => $article->getCoverImageUrl(),
            'slug' => $article->getSlug(),
            'title' => $article->getTitle(),
            'description' => $article->getDescription()
        ];
    }

    /**
     * @param Category $category
     * @param int $start
     * @return array
     * @throws \Exception
     */
    public function getListOfArticlesByCategory(Category $category, int $start) : array
    {
        $articles = [];
        $count = 0;

        if (empty($category->getArticles())) {
            return [
                'articles' => $articles,
                'count' => $count
            ];
        }

        $sliceOfArticles = $category->getArticles()->slice($start, 30);

        /** @var Article $article */
        foreach ($this->yieldCollection($sliceOfArticles) as $article) {
            if ($article->getPublished() === false) {
                continue;
            }

            $count++;
            $articles[] = $this->articleCardView($article);
        }

        return [
            'articles' => $articles,
            'count' => $count
        ];
    }

    /**
     * @return DataResponse
     */
    public function getLastArticle(): DataResponse
    {
        if (!$lastArticle = $this->getRepository()->getLastArticle('news-room')) {
            $lastArticle = $this->getRepository()->getLastArticle();
        }

        if (is_null($lastArticle)) {
            return new DataResponse(Response::HTTP_NOT_FOUND, [
                'message' => 'Article does not exist'
            ]);
        }

        return new DataResponse(Response::HTTP_OK, $lastArticle);
    }
}
