<?php

namespace App\Model;

use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;
use App\Traits\YieldTrait;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class BookingModelManager
 * @package App\Model
 */
class BookingModelManager
{
    use YieldTrait;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * ViewingModelManager constructor.
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return BookingRepository
     */
    protected function getRepository() : BookingRepository
    {
        return $this->entityManager->getRepository(Booking::class);
    }

    /**
     * @return Booking
     */
    public function createBooking() : Booking
    {
        return new Booking();
    }

    /**
     * @param Booking $booking
     * @param bool $andFlush
     */
    public function update(Booking $booking, $andFlush = true) : void
    {
        $this->entityManager->persist($booking);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Booking|null
     */
    public function findOneBy(array $criteria, array $orderBy = null) : ?Booking
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * @param int $workspaceId
     * @param \DateTime $startTime
     * @param \DateTime $endTime
     * @return bool
     */
    public function isTimeAvailable(int $workspaceId, \DateTime $startTime, \DateTime $endTime) : bool
    {
        return $this->getRepository()->isTimeAvailable($workspaceId, $startTime, $endTime);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getBookingWorkspaces(User $user) : array
    {
        return $this->getRepository()->getBookingWorkspaces($user);
    }
}
