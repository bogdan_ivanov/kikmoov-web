<?php

namespace App\Model;

use App\Entity\Borough;
use App\Repository\BoroughRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class BoroughModelManager
 * @package App\Model
 */
class BoroughModelManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * CategoryModelManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return BoroughRepository
     */
    protected function getRepository(): BoroughRepository
    {
        return $this->entityManager->getRepository(Borough::class);
    }

    /**
     * @return mixed
     */
    public function getActiveBoroughsSuggestion($borough)
    {
        return $this->getRepository()->getActiveBoroughsSuggestion($borough);
    }
}