<?php

namespace App\Model;

use App\Controller\DataResponse;
use App\Entity\BuyerInternalNotification;
use App\Entity\User;
use App\Entity\UserDevice;
use App\InternalNotification\PushNotificationType;
use App\Repository\BuyerInternalNotificationRepository;
use App\Services\PushService;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BuyerInternalNotificationModelManager
 * @package App\Model
 */
class BuyerInternalNotificationModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PushNotificationType
     */
    protected $pushNotificationType;

    /**
     * @var PushService
     */
    private $pushService;

    /**
     * @var EngineInterface
     */
    private $twig;

    /**
     * BuyerInternalNotificationModelManager constructor.
     * @param EntityManager $entityManager
     * @param PushService $pushService
     * @param EngineInterface $twigEngine
     * @param PushNotificationType $pushNotificationType
     */
    public function __construct
    (
        EntityManager $entityManager,
        PushService $pushService,
        EngineInterface $twigEngine,
        PushNotificationType $pushNotificationType
    )
    {
        $this->entityManager = $entityManager;
        $this->pushNotificationType = $pushNotificationType;
        $this->pushService = $pushService;
        $this->twig = $twigEngine;
    }

    /**
     * @return BuyerInternalNotificationRepository
     */
    protected function getRepository() : BuyerInternalNotificationRepository
    {
        return $this->entityManager->getRepository(BuyerInternalNotification::class);
    }

    /**
     * @param User $user
     * @param array $params
     * @param string $eventName
     * @param int|null $entityId
     * @param string $eventGroup
     * @return DataResponse
     * @throws \Exception
     */
    public function create(
        User $user,
        array $params,
        string $eventName = PushNotificationType::VIEWING_ACCEPTED,
        ?int $entityId = null,
        ?string $eventGroup = BuyerInternalNotification::VIEWING
    ) : DataResponse
    {
        try {
            $messageData =$this->pushNotificationType->findEvent($eventName);

            $parsedMessage = $this->renderMessage($messageData['body'], $params);

        } catch (\Exception $exception) {
            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        $devices = $user->getDevices();

        $collectionDevices = [];

        /** @var UserDevice $device */
        foreach ($this->yieldCollection($devices) as $device) {
            $collectionDevices[] = $device->getDeviceToken();
        }

        try {
            $pushNotification = new BuyerInternalNotification();
            $pushNotification->setMessage($parsedMessage);
            $pushNotification->setSubject($messageData['title']);
            $pushNotification->setActionType($eventName);
            $pushNotification->setEntityId($entityId);
            $pushNotification->setUser($user);
            $pushNotification->setActionGroup($eventGroup);
            $this->entityManager->persist($pushNotification);
            $this->entityManager->flush($pushNotification);

            if ($collectionDevices && $user->getPushStatus()) {
                $parsedMessage = str_replace('[', '', $parsedMessage);
                $parsedMessage = str_replace(']', '', $parsedMessage);
                $this->pushService->sendMessage($messageData['title'], $parsedMessage, $collectionDevices);
            }
        } catch (\Exception $exception) {
            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Status successfully changed'
        ]);
    }

    /**
     * @param User $user
     * @param int $entityId
     * @param string $eventGroup
     * @return DataResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(User $user, int $entityId, string $eventGroup) : DataResponse
    {
        $pushNotifications = $this->getRepository()->findBy(['entityId' => $entityId, 'user' => $user, 'actionGroup' => $eventGroup]);

        if (is_null($pushNotifications)) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => 'Notifications not found'
            ]);
        }

        foreach ($this->yieldCollection($pushNotifications) as $pushNotification) {
            $this->entityManager->remove($pushNotification);
            $this->entityManager->flush();
        }

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Notifications successfully removed'
        ]);
    }

    /**
     * @param string $message
     * @param array $params
     * @return mixed|string
     */
    private function renderMessage(string $message, array $params)
    {
        foreach ($params as $key => $param) {
            $message = str_replace('[' . $key . ']', '[' . $param . ']', $message);
        }

        return $message;
    }

    /**
     * @param int $entityId
     * @param string $actionType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteNotification(int $entityId, string $actionType)
    {
        $buyerNotifications = $this->getRepository()->findBy(['entityId' => $entityId, 'actionType' => $actionType]);

        if (!is_null($buyerNotifications)) {
            foreach ($buyerNotifications as $buyerNotification) {
                $this->entityManager->remove($buyerNotification);
            }
            $this->entityManager->flush();
        }
    }
}