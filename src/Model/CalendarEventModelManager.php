<?php

namespace App\Model;


use App\Calendar\CalendarInterface;
use App\Calendar\GoogleCalendar;
use App\Calendar\OutlookCalendar;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use App\Entity\WorkSpace;
use App\Repository\CalendarEventRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;

/**
 * Class CalendarEventModelManager
 * @package App\Model
 */
class CalendarEventModelManager
{

    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var OutlookCalendar
     */
    private $outlookCalendar;


    /**
     * CalendarEventModelManager constructor.
     * @param EntityManager $entityManager
     * @param GoogleCalendar $googleCalendar
     * @param OutlookCalendar $outlookCalendar
     */
    public function __construct
    (
        EntityManager $entityManager,
        GoogleCalendar $googleCalendar,
        OutlookCalendar $outlookCalendar
    )
    {
        $this->entityManager = $entityManager;
        $this->googleCalendar = $googleCalendar;
        $this->outlookCalendar = $outlookCalendar;
    }

    /**
     * @return CalendarEventRepository
     */
    protected function getRepository() : CalendarEventRepository
    {
        return $this->entityManager->getRepository(CalendarEvent::class);
    }

    /**
     * @param CalendarEvent $calendarEvent
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(CalendarEvent $calendarEvent, $andFlush = true) : void
    {
        $this->entityManager->persist($calendarEvent);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param WorkSpace $workSpace
     * @return array
     */
    public function getListEventsByWorkspaces(WorkSpace $workSpace)
    {
        $events = $this->getRepository()->getTimeEventsByWorkspace($workSpace);

        return $events;
    }

    /**
     * @param WorkSpace $workSpace
     * @param $startTime
     * @param $endTime
     * @return array
     */
    public function getCalendarEvents(WorkSpace $workSpace, $startTime, $endTime)
    {
        return $this->getRepository()->getCalendarEvents($workSpace, $startTime, $endTime);
    }

    /**
     * @param Calendar $calendar
     * @param WorkSpace $workSpace
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @param string $title
     * @param string $summary
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function addCalendarEvents
    (
        Calendar $calendar,
        WorkSpace $workSpace,
        \DateTimeInterface $startTime,
        \DateTimeInterface $endTime,
        string $title,
        string $summary
    )
    {
        if ($calendar->getType() === Calendar::GOOGLE_CALENDAR) {
            $this->addGoogleEvent($calendar, $workSpace, $startTime, $endTime, $title, $summary);
        } elseif ($calendar->getType() === Calendar::OUTLOOK_CALENDAR) {
            $this->addOutlookEvent($calendar, $workSpace, $startTime, $endTime, $title, $summary);
        }
    }

    /**
     * @param Calendar $calendar
     * @param WorkSpace $workSpace
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @param string $title
     * @param string $summary
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addGoogleEvent
    (
        Calendar $calendar,
        WorkSpace $workSpace,
        \DateTimeInterface $startTime,
        \DateTimeInterface $endTime,
        string $title,
        string $summary
    )
    {
        $response = $this->googleCalendar->addEvent(
            $workSpace->getGoogleCalendarId(),
            $startTime->getTimestamp(),
            $endTime->getTimestamp(),
            $title,
            $summary
        );
        $calendarEvent = new CalendarEvent();
        $calendarEvent->setStart(new \DateTime($response['start']['dateTime']));
        $calendarEvent->setEnd(new \DateTime($response['end']['dateTime']));
        $calendarEvent->setCalendar($calendar);
        $calendarEvent->setSummary($response['summary']);
        $calendarEvent->setHtmlLink($response['htmlLink']);
        $calendarEvent->setEventId($response['id']);
        $this->update($calendarEvent);
    }

    /**
     * @param Calendar $calendar
     * @param WorkSpace $workSpace
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @param string $title
     * @param string $summary
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function addOutlookEvent
    (
        Calendar $calendar,
        WorkSpace $workSpace,
        \DateTimeInterface $startTime,
        \DateTimeInterface $endTime,
        string $title,
        string $summary
    )
    {
        $response = $this->outlookCalendar->addEvent(
            $workSpace->getOutlookCalendarId(),
            $startTime->getTimestamp(),
            $endTime->getTimestamp(),
            $title,
            $summary
        );

        $calendarEvent = new CalendarEvent();
        $calendarEvent->setSummary($response['subject']);
        $calendarEvent->setHtmlLink($response['webLink']);
        $calendarEvent->setEventId($response['id']);
        $calendarEvent->setStart((new \DateTime($response['start']['dateTime'], new \DateTimeZone($response['start']['timeZone']))));
        $calendarEvent->setEnd((new \DateTime($response['end']['dateTime'], new \DateTimeZone($response['end']['timeZone']))));
        $calendarEvent->setCalendar($calendar);
        $this->update($calendarEvent);
    }

    /**
     * @param Calendar $customCalendar
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @param WorkSpace $workSpace
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCustomEvent
    (
        Calendar $customCalendar,
        \DateTimeInterface $startTime,
        \DateTimeInterface $endTime,
        WorkSpace $workSpace
    )
    {
        $customCalendarEvent = new CalendarEvent();
        $customCalendarEvent->setStart($startTime);
        $customCalendarEvent->setEnd($endTime);
        $customCalendarEvent->setCalendar($customCalendar);
        $customCalendarEvent->setSummary('Book workspace with ID ' . $workSpace->getId());

        $this->update($customCalendarEvent, true);
    }
}