<?php

namespace App\Model;

use App\Entity\Calendar;
use App\Entity\WorkSpace;
use App\Repository\CalendarRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;

/**
 * Class CalendarModelManager
 * @package App\Model
 */
class CalendarModelManager
{

    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * FacilityModelManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return CalendarRepository
     */
    protected function getRepository() : CalendarRepository
    {
        return $this->entityManager->getRepository(Calendar::class);
    }

    /**
     * @param Calendar $calendar
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Calendar $calendar, $andFlush = true) : void
    {
        $this->entityManager->persist($calendar);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param WorkSpace $workSpace
     * @return Calendar|null
     */
    public function getCustomCalendar(WorkSpace $workSpace)
    {
        $calendar = $this->getRepository()->findOneBy([
            'type' => Calendar::CUSTOM_CALENDAR,
            'workspace' => $workSpace
        ]);

        return $calendar;
    }

    /**
     * @param WorkSpace $workSpace
     * @return Calendar[]
     */
    public function getOutsideCalendars(WorkSpace $workSpace)
    {
        return $this->getRepository()->getOutsideCalendars($workSpace);
    }

    /**
     * @param WorkSpace $workSpace
     * @return Calendar
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCustomCalendar(WorkSpace $workSpace) : Calendar
    {
        $customCalendar = new Calendar();
        $customCalendar->setStatus(false);
        $customCalendar->setWorkspace($workSpace);
        $customCalendar->setName('Custom calendar');
        $customCalendar->setType(Calendar::CUSTOM_CALENDAR);
        $this->update($customCalendar, true);
        return $customCalendar;
    }

}