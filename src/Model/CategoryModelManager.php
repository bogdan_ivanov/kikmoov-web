<?php

namespace App\Model;

use App\Controller\DataResponse;
use App\Entity\Article;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryModelManager
 * @package App\Model
 */
class CategoryModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var MediaUrlGenerator
     */
    private $mediaUrlGenerator;

    /**
     * CategoryModelManager constructor.
     * @param EntityManager $entityManager
     * @param MediaUrlGenerator $mediaUrlGenerator
     */
    public function __construct(EntityManager $entityManager, MediaUrlGenerator $mediaUrlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->mediaUrlGenerator = $mediaUrlGenerator;
    }

    /**
     * @return CategoryRepository
     */
    protected function getRepository(): CategoryRepository
    {
        return $this->entityManager->getRepository(Category::class);
    }

    /**
     * @return DataResponse
     * @throws \Exception
     */
    public function getActiveCategories() : DataResponse
    {
        $categories = $this->getRepository()->findBy(['active' => true]);

        if (empty($categories)) {
            return new DataResponse(Response::HTTP_OK, ['categories' => $categories]);
        }

        $filtered = [];

        /** @var Category $category */
        foreach ($categories as $category) {
            if (is_null($category->getIcon())) {
                continue;
            }

            $articles = $category->getArticles();

            if ($articles->isEmpty()) {
                continue;
            }

            $atLeastOnePublishedArticle = false;

            /** @var Article $article */
            foreach($this->yieldCollection($articles) as $article) {
                if ($article->getPublished()) {
                    $atLeastOnePublishedArticle = true;
                    break;
                }
            }

            if ($atLeastOnePublishedArticle === false) {
                continue;
            }

            $filtered[] = [
                'slug' => $category->getSlug(),
                'name' => $category->getName(),
                'icon' => $this->mediaUrlGenerator->generateUrl($category->getIcon())
            ];
        }

        return new DataResponse(Response::HTTP_OK, ['categories' => $filtered]);
    }

    /**
     * @param string $categorySlug
     * @return Category|null
     */
    public function findBySlug(string $categorySlug): ?Category
    {
        return $this->getRepository()->findOneBy(['slug' => $categorySlug, 'active' => true]);
    }
}
