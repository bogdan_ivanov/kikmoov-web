<?php

namespace App\Model;

use App\Entity\UserDevice;
use App\Repository\UserDeviceRepository;
use App\Traits\YieldTrait;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DeviceModelManager
 * @package App\Model
 */
class DeviceModelManager
{
    use YieldTrait;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * DeviceModelManager constructor.
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return UserDeviceRepository
     */
    protected function getRepository() : UserDeviceRepository
    {
        return $this->entityManager->getRepository(UserDevice::class);
    }

    /**
     * @return UserDevice
     */
    public function createUserDevice() : UserDevice
    {
        return new UserDevice();
    }

    /**
     * @param UserDevice $userDevice
     * @param bool $andFlush
     */
    public function update(UserDevice $userDevice, $andFlush = true) : void
    {
        $this->entityManager->persist($userDevice);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return UserDevice|null
     */
    public function findOneBy(array $criteria, array $orderBy = null) : ?UserDevice
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

}