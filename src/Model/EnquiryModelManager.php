<?php

namespace App\Model;

use App\Entity\ContactUs;
use App\Entity\LeaseholdEnquiry;
use Doctrine\ORM\EntityManager;

/**
 * Class EnquiryModelManager
 * @package App\Model
 */
class EnquiryModelManager
{
    /** @var EntityManager */
    private $entityManager;

    /**
     * ContactUsModelManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return ContactUs
     */
    public function createContactUs(): ContactUs
    {
        return new ContactUs();
    }

    /**
     * @param ContactUs $contactUs
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @return void
     */
    public function updateContactUs(ContactUs $contactUs, bool $andFlush = true): void
    {
        $this->entityManager->persist($contactUs);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @return LeaseholdEnquiry
     */
    public function createLeaseholdEnquiry(): LeaseholdEnquiry
    {
        return new LeaseholdEnquiry();
    }

    /**
     * @param LeaseholdEnquiry $leaseholdEnquiry
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateLeaseholdEnquiry(LeaseholdEnquiry $leaseholdEnquiry, bool $andFlush = true): void
    {
        $this->entityManager->persist($leaseholdEnquiry);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }
}
