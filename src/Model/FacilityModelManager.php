<?php

namespace App\Model;

use App\Entity\Facility;
use App\Entity\WorkSpace;
use App\Provider\FacilityProvider;
use App\Repository\FacilityRepository;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

/**
 * Class FacilityModelManager
 * @package App\Model
 */
class FacilityModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var FacilityProvider
     */
    private $facilityProvider;

    /**
     * @var MediaUrlGenerator
     */
    private $mediaUrlGenerator;

    /**
     * FacilityModelManager constructor.
     * @param EntityManager $entityManager
     * @param FacilityProvider $facilityProvider
     * @param MediaUrlGenerator $mediaUrlGenerator
     */
    public function __construct(EntityManager $entityManager, FacilityProvider $facilityProvider, MediaUrlGenerator $mediaUrlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->facilityProvider = $facilityProvider;
        $this->mediaUrlGenerator = $mediaUrlGenerator;
    }

    /**
     * @return FacilityRepository
     */
    protected function getRepository() : FacilityRepository
    {
        return $this->entityManager->getRepository(Facility::class);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function grabFacilities() : array
    {
        $facilities = $this->getRepository()->findBy(['isActive' => true]);

        if (empty($facilities)) {
            return $facilities;
        }

        $result = [];

        /** @var Facility $facility */
        foreach ($this->yieldCollection($facilities) as $facility) {
            $webIcon = null;
            if (!is_null($facility->getWebIcon())) {
                $webIcon = $this->mediaUrlGenerator->generateUrl($facility->getWebIcon());
            }

            $appIcon = null;
            if (!is_null($facility->getAppIcon())) {
                $appIcon = $this->mediaUrlGenerator->generateUrl($facility->getAppIcon());
            }

            $result[] = [
                'slug' => $facility->getSlug(),
                'name' => $facility->getName(),
                'webIcon'  => $webIcon,
                'appIcon'  => $appIcon
            ];
        }

        return $result;
    }

    /**
     * @param array $slugs
     * @return mixed
     */
    public function findByArrayOfSlugs(array $slugs)
    {
        return $this->getRepository()->findByArrayOfSlugs($slugs);
    }

    /**
     * @param WorkSpace $workSpace
     * @return array
     * @throws \Exception
     */
    public function getListOfFacilities(WorkSpace $workSpace) : array
    {
        $facilities = [];

        if ($workSpace->getFacilities()->isEmpty()) {
            return $facilities;
        }

        /** @var Facility $facility */
        foreach ($workSpace->getFacilities() as $facility) {
            if (!$facility->getIsActive()) {
                continue;
            }

            $webIcon = null;
            if (!is_null($facility->getWebIcon())) {
                $webIcon = $this->mediaUrlGenerator->generateUrl($facility->getWebIcon());
            }

            $appIcon = null;
            if (!is_null($facility->getAppIcon())) {
                $appIcon = $this->mediaUrlGenerator->generateUrl($facility->getAppIcon());
            }

            $facilities[] = [
                'slug' => $facility->getSlug(),
                'name' => $facility->getName(),
                'webIcon'  => $webIcon,
                'appIcon'  => $appIcon
            ];
        }

        return $facilities;
    }
}
