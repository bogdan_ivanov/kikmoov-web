<?php

namespace App\Model;

use App\Controller\DataResponse;
use App\Entity\Booking;
use App\Entity\InternalNotification;
use App\Entity\RequestInterface;
use App\Entity\User;
use App\Entity\Viewing;
use App\InternalNotification\InternalNotificationType;
use App\Repository\InternalNotificationRepository;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InternalNotificationModelManager
 * @package App\Model
 */
class InternalNotificationModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var InternalNotificationType
     */
    protected $internalNotificationType;

    /**
     * InternalNotificationService constructor.
     * @param EntityManager $entityManager
     * @param InternalNotificationType $internalNotificationType
     */
    public function __construct(EntityManager $entityManager, InternalNotificationType $internalNotificationType)
    {
        $this->entityManager = $entityManager;
        $this->internalNotificationType = $internalNotificationType;
    }

    /**
     * @return InternalNotificationRepository
     */
    protected function getRepository() : InternalNotificationRepository
    {
        return $this->entityManager->getRepository(InternalNotification::class);
    }

    /**
     * @param User $seller
     * @param RequestInterface $entity
     * @param array $params
     * @param string $eventName
     * @param string $type
     * @return DataResponse
     */
    public function create(
        User $seller,
        RequestInterface $entity,
        array $params,
        string $eventName = InternalNotification::VIEWING_REQUESTED,
        string $type = InternalNotification::VIEWING
    ) : DataResponse
    {
        if ($type !== InternalNotification::VIEWING && $type !== InternalNotification::BOOKING) {
            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Wrong type'
            ]);
        }

        try {
            ['message' => $message] = $this->internalNotificationType->findEvent($eventName, $params);
        } catch (\Exception $exception) {
            //@todo catch exception

            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        $internalNotification = new InternalNotification();
        $internalNotification->setStatus($eventName);
        $internalNotification->setMessage($message);
        $internalNotification->setType($type);
        $internalNotification->setUser($seller);

        try {
            $this->entityManager->persist($internalNotification);
            $this->entityManager->flush($internalNotification);

            $entity->setInternalNotification($internalNotification);
            $this->entityManager->flush($entity);
        } catch (\Exception $exception) {
            //@todo catch exception

            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        return new DataResponse(Response::HTTP_CREATED, [
            'message' => 'Successfully created',
            'internalNotification' => $internalNotification
        ]);
    }

    /**
     * @param InternalNotification $internalNotification
     * @param string $status
     * @param array $params
     * @return DataResponse
     */
    public function changeStatus(InternalNotification $internalNotification, string $status, array $params) : DataResponse
    {
        if (!in_array($status, InternalNotification::getStatuses())) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, [
                'message' => 'Wrong status. Please, try again'
            ]);
        }

        if ($internalNotification->getStatus() === $status) {
            return new DataResponse(Response::HTTP_OK, [
                'message' => 'Nothing to update'
            ]);
        }
        try {
            ['message' => $message] = $this->internalNotificationType->findEvent($status, $params);
        } catch (\Exception $exception) {
            //@todo catch exception

            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        try {
            $internalNotification->setStatus($status);
            $internalNotification->setMessage($message);
            $this->entityManager->flush($internalNotification);
        } catch (\Exception $exception) {
            //@todo catch exception
            return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'message' => 'Something went wrong. Please, try again later'
            ]);
        }

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Status successfully changed'
        ]);
    }

    /**
     * @param User $seller
     * @param array $ids
     */
    public function delete(User $seller, array $ids) : void
    {
        foreach ($ids as $id) {
            $internalNotifications = $this->getRepository()->findOneBy(['id' => $id, 'user' => $seller]);

            if (is_null($internalNotifications)) {
                continue;
            }

            try {
                $this->entityManager->remove($internalNotifications);
                $this->entityManager->flush();
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    /**
     * @param User $seller
     * @return DataResponse
     * @throws \Exception
     */
    public function getListOfNotifications(User $seller) : DataResponse
    {
        $notifications = $this->getRepository()->findBy(['user' => $seller], ['id' => 'DESC']);

        $filtered = [];

        if (empty($notifications)) {
            return new DataResponse(Response::HTTP_OK, ['notifications' => $filtered]);
        }

        /** @var InternalNotification $notification */
        foreach ($this->yieldCollection($notifications) as $notification) {
            $internalNotification = [
                'id' => $notification->getId(),
                'date' => $notification->getCreatedAt()->getTimestamp(),
                'message' => $notification->getMessage(),
                'status' => $notification->getStatus(),
            ];

            if ($notification->getType() === InternalNotification::BOOKING) {
                $booking = $this->entityManager->getRepository(Booking::class)->findOneBy(['internalNotification' => $notification]);

                if (is_null($booking)) {
                    continue;
                }

                if (is_null($booking->getUser())) {
                    continue;
                }

                $internalNotification['from'] = $booking->getUser()->getFullname();
                $internalNotification['bookingId'] = $booking->getId();
                $internalNotification['workspaceId'] = $booking->getWorkSpace()->getId();
            } else {
                $viewing = $this->entityManager->getRepository(Viewing::class)->findOneBy(['internalNotification' => $notification]);

                if (is_null($viewing)) {
                    continue;
                }

                if (is_null($viewing->getUser())) {
                    continue;
                }

                $internalNotification['from'] = $viewing->getUser()->getFullname();
                $internalNotification['viewingId'] = $viewing->getId();
                $internalNotification['workspaceId'] = $viewing->getWorkSpace()->getId();
            }

            $filtered[] = $internalNotification;
        }

        return new DataResponse(Response::HTTP_OK, ['notifications' => $filtered]);
    }
}
