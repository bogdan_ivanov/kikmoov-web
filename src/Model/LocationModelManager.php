<?php

namespace App\Model;

use App\Entity\Location;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

/**
 * Class LocationModelManager
 * @package App\Model
 */
class LocationModelManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * LocationModelManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return LocationRepository
     */
    protected function getRepository() : LocationRepository
    {
        return $this->entityManager->getRepository(Location::class);
    }

    /**
     * @return Location
     */
    public function createLocation() : Location
    {
        return new Location();
    }

    /**
     * @param Location $location
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateLocation(Location $location, $andFlush = true) : void
    {
        $this->entityManager->persist($location);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Location|null
     */
    public function findOneBy(array $criteria, array $orderBy = null) : ?Location
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * @param int $id
     * @return Location|null
     */
    public function findOneById(int $id) : ?Location
    {
        return $this->getRepository()->findOneBy(['id' => $id]);
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return Location[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param Location $location
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteLocation(Location $location) : void
    {
        $this->entityManager->remove($location);
        $this->entityManager->flush($location);
    }

    /**
     * @param string $address
     * @return array
     */
    public function findByAddress(string $address) : array
    {
        return $this->getRepository()->findByAddress($address);
    }
}
