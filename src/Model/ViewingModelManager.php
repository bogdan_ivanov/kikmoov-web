<?php

namespace App\Model;

use App\Entity\User;
use App\Entity\Viewing;
use App\Repository\ViewingRepository;
use Doctrine\Common\Persistence\ObjectManager;

class ViewingModelManager
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * ViewingModelManager constructor.
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return ViewingRepository
     */
    protected function getRepository() : ViewingRepository
    {
        return $this->entityManager->getRepository(Viewing::class);
    }

    /**
     * @return Viewing
     */
    public function createViewing() : Viewing
    {
        return new Viewing();
    }

    /**
     * @param Viewing $viewing
     * @param bool $andFlush
     */
    public function update(Viewing $viewing, $andFlush = true) : void
    {
        $this->entityManager->persist($viewing);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Viewing|null
     */
    public function findOneBy(array $criteria, array $orderBy = null) : ?Viewing
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getViewingWorkspaces(User $user) : array
    {
        return $this->getRepository()->getViewingWorkspaces($user);
    }
}
