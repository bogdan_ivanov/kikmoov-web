<?php

namespace App\Model;

use App\Entity\Booking;
use App\Entity\Calendar;
use App\Entity\CalendarEvent;
use App\Entity\Facility;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\Viewing;
use App\Entity\WorkSpace;
use App\Notifications\FrontUrls;
use App\Provider\WorkspaceMediaProvider;
use App\Repository\CalendarEventRepository;
use App\Repository\WorkSpaceRepository;
use App\Services\RouterWrapper;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class WorkSpaceModelManager
 * @package App\Model
 */
class WorkSpaceModelManager
{
    use YieldTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var WorkspaceMediaProvider
     */
    private $workspaceMediaProvider;

    /**
     * @var FacilityModelManager
     */
    private $facilityModelManager;

    /**
     * @var CalendarEventModelManager
     */
    private $calendarEventModelManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var RouterWrapper
     */
    private $router;

    /**
     * WorkSpaceModelManager constructor.
     * @param EntityManager $entityManager
     * @param WorkspaceMediaProvider $workspaceMediaProvider
     * @param FacilityModelManager $facilityModelManager
     * @param TokenStorage $tokenStorage
     * @param CalendarEventModelManager $calendarEventModelManager
     * @param RouterWrapper $routerWrapper
     */
    public function __construct(
        EntityManager $entityManager,
        WorkspaceMediaProvider $workspaceMediaProvider,
        FacilityModelManager $facilityModelManager,
        TokenStorage $tokenStorage,
        CalendarEventModelManager $calendarEventModelManager,
        RouterWrapper $routerWrapper
    )
    {
        $this->entityManager = $entityManager;
        $this->workspaceMediaProvider = $workspaceMediaProvider;
        $this->facilityModelManager = $facilityModelManager;
        $this->tokenStorage = $tokenStorage;
        $this->calendarEventModelManager = $calendarEventModelManager;
        $this->router = $routerWrapper;
    }

    /**
     * @return WorkSpaceRepository
     */
    protected function getRepository(): WorkSpaceRepository
    {
        return $this->entityManager->getRepository(WorkSpace::class);
    }

    /**
     * @return User|null
     */
    protected function getUser() :?User
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (!\is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    /**
     * @return WorkSpace
     */
    public function createWorkspace(): WorkSpace
    {
        return new WorkSpace();
    }

    /**
     * @param WorkSpace $workSpace
     * @param bool $andFlush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateWorkspace(WorkSpace $workSpace, $andFlush = true): void
    {
        $this->entityManager->persist($workSpace);

        if ($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return WorkSpace|null
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?WorkSpace
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * @param WorkSpace $workSpace
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteWorkspace(WorkSpace $workSpace): void
    {
        $this->entityManager->remove($workSpace);
        $this->entityManager->flush($workSpace);
    }

    /***
     * @param WorkSpace $workSpace
     * @return bool
     * @throws \Exception
     */
    public function isLikedByUser(WorkSpace $workSpace)
    {
        $liked = false;

        if (is_null($this->getUser())) {
            return $liked;
        }

        $user = $this->getUser();

        if ($user->getLikedWorkspaces()->isEmpty()) {
            return $liked;
        }

        /** @var WorkSpace $likedWorkspace */
        foreach ($this->yieldCollection($user->getLikedWorkspaces()) as $likedWorkspace) {
            if ($likedWorkspace->getId() === $workSpace->getId()) {
                $liked = true;
                break;
            }
        }

        return $liked;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search(array $params): array
    {
        return $this->getRepository()->search($params);
    }

    /**
     * @param WorkSpace $workSpace
     * @return array
     * @throws \Exception
     */
    public function userCardView(WorkSpace $workSpace)
    {
        if (is_null($this->getUser())) {
            return [
                'id' => null,
                'email' => null,
                'isLiked' => null
            ];
        }

        return [
            'id' => $this->getUser()->getId(),
            'email' => $this->getUser()->getEmail(),
            'isLiked' => $this->isLikedByUser($workSpace)
        ];
    }

    /**
     * @param int $workspaceID
     * @param bool $sellerRequest
     * @return array
     */
    public function checkWorkspaceExistence(int $workspaceID, bool $sellerRequest = true): array
    {
        $workspace = $this->findOneBy(['id' => $workspaceID]);

        if ($workspace === null) {
            return [
                false,
                ['message' => 'No workspace was found']
            ];
        }

        if (!$sellerRequest && $workspace->getStatus() !== WorkSpace::ACTIVE) {
            return [
                false,
                ['message' => 'Workspace is not available. Please contact the admin']
            ];
        }

        /** @var Location $location */
        $location = $workspace->getLocation();

        if ($location === null) {
            return [
                false,
                ['message' => 'No location was found']
            ];
        }

        return [
            true,
            [
                'workspace' => $workspace,
                'location' => $location
            ]
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @param null|string $availableFrom
     * @param bool|null $additionalInfoNeeded
     * @return array
     * @throws \Exception
     */
    public function locationCardView(WorkSpace $workspace, ?string $availableFrom = '', ?bool $additionalInfoNeeded = true): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        $related = [];

        if ($workspace->getType() === WorkSpace::DESK && in_array($workspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks())) {
            $related = !is_null($availableFrom)
                ? $this->filterRelatedWorkspacesAvailableFrom($workspace, $availableFrom, $additionalInfoNeeded)
                : $this->getRelatedSeparateWorkspaces($workspace, $additionalInfoNeeded);
        }

        return [
            'location' => [
                'id' => $location->getId(),
                'nearby' => $location->getNearby(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'latitude' => $location->getLatitude(),
                'longitude' => $location->getLongitude(),
            ],
            'related' => $related,
            'workspace' => $this->workspaceCardView($workspace, $additionalInfoNeeded),
            'user' => $this->userCardView($workspace)
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @param null|string $availableFrom
     * @return array
     * @throws \Exception
     */
    public function locationCardMobileView(WorkSpace $workspace, ?string $availableFrom = ''): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        $related = [];

        if ($workspace->getType() === WorkSpace::DESK && in_array($workspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks())) {
            $related = !is_null($availableFrom)
                ? $this->filterRelatedWorkspacesAvailableFrom($workspace, $availableFrom, false)
                : $this->getRelatedSeparateWorkspaces($workspace, false);
        }

        return [
            'location' => [
                'id' => $location->getId(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'latitude' => $location->getLatitude(),
                'longitude' => $location->getLongitude(),
            ],
            'related' => $related,
            'workspace' => $this->workspaceInfo($workspace),
            'user' => $this->userCardView($workspace)
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function locationCardViewSaved(WorkSpace $workspace): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        return [
            'location' => [
                'id' => $location->getId(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'latitude' => $location->getLatitude(),
                'longitude' => $location->getLongitude(),
            ],
            'related' => [],
            'workspace' => $this->workspaceCardView($workspace),
            'user' => $this->userCardView($workspace)
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @param bool|null $additionalInfoNeeded
     * @return array
     * @throws \Exception
     */
    public function workspaceCardView(WorkSpace $workspace, ?bool $additionalInfoNeeded = true): array
    {
        $events = $this->getWorkspaceListEvents($workspace);

        $result = [
            'id' => $workspace->getId(),
            'coverImageUrl' => $workspace->getCoverImageUrl(),
            'description' => $workspace->getDescription(),
            'quantity' => $workspace->getQuantity(),
            'duration' => $workspace->getDuration(),
            'size' => $workspace->getSize(),
            'capacity' => $workspace->getCapacity(),
            'price' => $workspace->getPrice(),
            'status' => $workspace->getStatus(),
            'deskType' => $workspace->getDeskType(),
            'type' => $workspace->getType(),
            'minContractLength' => $workspace->getMinContractLength(),
            'createdAt' => $workspace->getCreatedAt()->getTimestamp(),
            'facilities' => $this->facilityModelManager->getListOfFacilities($workspace),
            'opensFrom' => $workspace->getOpensFrom(),
            'closesAt' => $workspace->getClosesAt(),
            'isLiked' => $this->isLikedByUser($workspace),
            'visited' => $workspace->getVisited(),
            'availableFrom' => is_null($workspace->getAvailableFrom()) ? null : $workspace->getAvailableFrom()->getTimestamp(),
            'events' => $events,
            'indepthUrl' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
        ];

        if ($additionalInfoNeeded) {
            $result['images'] = $this->workspaceMediaProvider->getPublicLinks($workspace);
        }

        return $result;
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function workspaceInfo(WorkSpace $workspace) : array
    {
        return [
            'id' => $workspace->getId(),
            'coverImageUrl' => $workspace->getCoverImageUrl(),
            'description' => $workspace->getDescription(),
            'quantity' => $workspace->getQuantity(),
            'duration' => $workspace->getDuration(),
            'size' => $workspace->getSize(),
            'capacity' => $workspace->getCapacity(),
            'price' => $workspace->getPrice(),
            'status' => $workspace->getStatus(),
            'deskType' => $workspace->getDeskType(),
            'type' => $workspace->getType(),
            'minContractLength' => $workspace->getMinContractLength(),
            'createdAt' => $workspace->getCreatedAt()->getTimestamp(),
            'opensFrom' => $workspace->getOpensFrom(),
            'closesAt' => $workspace->getClosesAt(),
            'isLiked' => $this->isLikedByUser($workspace),
            'visited' => $workspace->getVisited(),
            'availableFrom' => is_null($workspace->getAvailableFrom()) ? null : $workspace->getAvailableFrom()->getTimestamp(),
            'indepthUrl' => $this->router->generateFrontLink(sprintf(FrontUrls::IN_DEPTH, $workspace->getId()))
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function workspaceEvents(WorkSpace $workspace) : array
    {
        return [
            'events' =>  $this->getWorkspaceListEvents($workspace)
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function workspaceFacilities(WorkSpace $workspace) : array
    {
        return [
            'facilities' => $this->facilityModelManager->getListOfFacilities($workspace),
        ];
    }

    /**
     * @param WorkSpace $workSpace
     * @param bool $absolute
     * @param string $format
     * @return array
     * @throws \Exception
     */
    public function workspaceImagesView(WorkSpace $workSpace, $absolute = false, $format = WorkspaceMediaProvider::PREVIEW) : array
    {
        return [
            'images' => $this->workspaceMediaProvider->getPublicLinks($workSpace, $absolute, $format)
        ];
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    private function getWorkspaceListEvents(WorkSpace $workspace) : array
    {
        $events = $this->calendarEventModelManager->getListEventsByWorkspaces($workspace);

        $bookedDates = [];

        $now = new \DateTime();

        /** @var Booking $booking */
        foreach ($this->yieldCollection($workspace->getBookings()) as $booking) {
            $startTime = $booking->getStartTime();
            //old ones
            if ($now->getTimestamp() > $startTime->getTimestamp()) {
                continue;
            }

            $bookedDates[] = [
                'start' => $booking->getStartTime(),
                'end' => ($booking->getEndTime()) ? $booking->getEndTime() : $startTime
            ];
        }

        $events = array_merge($events, $bookedDates);

        return $events;
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function getListOfFacilities(WorkSpace $workspace) : array
    {
        return $this->facilityModelManager->getListOfFacilities($workspace);
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function getRelatedWorkspaces(WorkSpace $workspace): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        $related = [];

        //do not need to make an additional query, since $location object already contains all the information that we need
        /** @var WorkSpace $relatedWorkspace */
        foreach ($this->yieldCollection($location->getWorkspaces()) as $relatedWorkspace) {
            if ($relatedWorkspace->getStatus() !== WorkSpace::ACTIVE) {
                continue;
            }

            if ($relatedWorkspace->getId() === $workspace->getId()) {
                continue;
            }

            if ($workspace->getType() !== $relatedWorkspace->getType()) {
                continue;
            }

            if ($workspace->getType() === WorkSpace::DESK) {
                if ($workspace->getDeskType() === $relatedWorkspace->getDeskType()) {
                    continue;
                } elseif (array_search($relatedWorkspace->getDeskType(), array_column($related, 'deskType')) !== false) {
                    continue;
                }
            }

            $item = $this->workspaceCardView($relatedWorkspace);

            $related[] = $item;
        }

        return $related;
    }

    /**
     * @param WorkSpace $workspace
     * @param bool $additionalInfoNeeded
     * @return array
     * @throws \Exception
     */
    public function getRelatedSeparateWorkspaces(WorkSpace $workspace, bool $additionalInfoNeeded = true): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        $related = [];

        //do not need to make an additional query, since $location object already contains all the information that we need
        /** @var WorkSpace $relatedWorkspace */
        foreach ($this->yieldCollection($location->getWorkspaces()) as $relatedWorkspace) {

            if (!$this->checkRelatedWorkspace($relatedWorkspace, $workspace)) {
                continue;
            }

            $workspaceEvents = [];

            if ($additionalInfoNeeded) {
                $workspaceEvents = $this->workspaceEvents($relatedWorkspace);
            }

            $workspaceInfo = $this->workspaceInfo($relatedWorkspace);

            $workspaceFacilities = [];

            if ($additionalInfoNeeded) {
                $workspaceFacilities = $this->workspaceFacilities($relatedWorkspace);
            }

            $workspaceImages = [];
            if ($additionalInfoNeeded) {
                $workspaceImages = $this->workspaceImagesView($relatedWorkspace, false, MediaProviderInterface::FORMAT_REFERENCE);
            }

            $workspaceData = array_merge($workspaceInfo, $workspaceFacilities, $workspaceImages, $workspaceEvents);

            $related[] = $workspaceData;
        }

        return $related;
    }

    /**
     * @param WorkSpace $workspace
     * @return array
     * @throws \Exception
     */
    public function getMobileRelatedSeparateWorkspaces(WorkSpace $workspace): array
    {
        /** @var  $location */
        $location = $workspace->getLocation();

        $related = [];

        //do not need to make an additional query, since $location object already contains all the information that we need
        /** @var WorkSpace $relatedWorkspace */
        foreach ($this->yieldCollection($location->getWorkspaces()) as $relatedWorkspace) {

            if (!$this->checkRelatedWorkspace($relatedWorkspace, $workspace)) {
                continue;
            }

            $workspaceEvents = $this->workspaceEvents($relatedWorkspace);
            $workspaceInfo = $this->workspaceInfo($relatedWorkspace);
            $workspaceFacilities = $this->workspaceFacilities($relatedWorkspace);
            $workspaceImages = $this->workspaceImagesView($relatedWorkspace);

            $workspaceData = array_merge($workspaceInfo, $workspaceFacilities, $workspaceImages, $workspaceEvents);

            $related[] = $workspaceData;
        }

        return $related;
    }

    /**
     * @param WorkSpace $relatedWorkspace
     * @param WorkSpace $workspace
     * @return bool
     */
    private function checkRelatedWorkspace(WorkSpace $relatedWorkspace, WorkSpace $workspace) : bool
    {
        if ($relatedWorkspace->getStatus() !== WorkSpace::ACTIVE) {
            return false;
        }

        if ($relatedWorkspace->getId() === $workspace->getId()) {
            return false;
        }

        if ($workspace->getType() !== $relatedWorkspace->getType()) {
            return false;
        }

        if ($workspace->getType() === WorkSpace::DESK) {
            if (in_array($workspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks()) && in_array($relatedWorkspace->getDeskType(), WorkSpace::getHourlyDeskTypes())) {
                return false;
            }

            if (in_array($workspace->getDeskType(), WorkSpace::getHourlyDeskTypes()) && in_array($relatedWorkspace->getDeskType(), WorkSpace::monthlyWorkspaceDesks())) {
                return false;
            }

            if ($workspace->getDeskType() === $relatedWorkspace->getDeskType()) {
                return false;
            }
        }

        return true;
    }


    /**
     * @param WorkSpace $workspace
     * @param null|string $availableFrom
     * @param bool|null $additionalInfoNeeded
     * @return array
     * @throws \Exception
     */
    public function filterRelatedWorkspacesAvailableFrom(WorkSpace $workspace, ?string $availableFrom, ?bool $additionalInfoNeeded = true) : array
    {
        $workspaces = $this->getRelatedSeparateWorkspaces($workspace, $additionalInfoNeeded);

        $related = [];
        //do not need to make an additional query, since $location object already contains all the information that we need
        foreach ($workspaces as $relatedWorkspace) {
            if (!empty($availableFrom) && !is_null($relatedWorkspace['availableFrom'])) {
                if ($relatedWorkspace['availableFrom'] < $availableFrom) {
                    continue;
                }
            }

            $related[] = $relatedWorkspace;
        }

        return $related;
    }

    /**
     * @return array
     */
    public function getPriceRange(): array
    {
        $borders = $this->getRepository()->getPriceRange();
        return (empty($borders)) ? ['min' => 0, 'max' => 0] : $borders[0];
    }

    /**
     * @param $array
     * @param string $sortByProperty
     * @param string $direction
     * @return array
     * @throws \Exception
     */
    public function sortRequests($array, string $sortByProperty, string $direction = 'asc'): array
    {
        $keys = [];

        $getter = 'get' . ucfirst($sortByProperty);

        foreach ($this->yieldCollection($array) as $k => $v) {
            if (!method_exists($v, $getter)) {
                continue;
            }

            $propValue = $v->{$getter}();

            if ($propValue instanceof \DateTime) {
                $modifiedValue = $propValue->getTimestamp();
            } elseif (is_object($propValue)) {
                $modifiedValue = $propValue->getId();
            } else {
                $modifiedValue = $propValue;
            }

            $keys[] = (is_numeric($modifiedValue)) ? $modifiedValue : strtolower($modifiedValue);
        }

        if ($direction === 'asc') {
            asort($keys);
        } else {
            arsort($keys);
        }

        $sorted = [];

        foreach ($keys as $k => $tmp) {
            $this->normalizeRequestData($array[$k], $sorted);
        }

        return $sorted;
    }

    /**
     * @param $item
     * @param array $result
     * @throws \Exception
     */
    protected function normalizeRequestData($item, array &$result): void
    {
        $filteredItem = [];

        if ($item instanceof Booking) {
            /** @var Booking $booking */
            if ($item->getBookingType() === Booking::BOOKING_REQUEST && !in_array($item->getStatus(), Booking::listUserStatuses())) {
                return;
            }

            $booking = $item;
            $filteredItem['booking'] = [
                'id' => $booking->getId(),
                'startTime' => $booking->getStartTime()->getTimestamp(),
                'endTime' => (is_null($booking->getEndTime())) ? null : $booking->getEndTime()->getTimestamp(),
                'amount' => $booking->getAmount(),
                'amountVAT' => $booking->getAmountVAT(),
                'status' => $booking->getStatus(),
                'future' => $booking->isFuture(),
                'createdAt' => $booking->getCreatedAt()->getTimestamp(),
                'name' => $booking->getName(),
                'attendees' => $booking->getAttendees(),
                'duration' => $booking->getWorkSpace()->getDuration(),
                'units' => $booking->getUnits()
            ];
        } else {
            /** @var Viewing $viewing */
            $viewing = $item;
            $filteredItem['viewing'] = [
                'id' => $viewing->getId(),
                'startTime' => $viewing->getStartTime()->getTimestamp(),
                'endTime' => (is_null($viewing->getEndTime())) ? null : $viewing->getEndTime()->getTimestamp(),
                'status' => $viewing->getStatus(),
                'future' => $viewing->isFuture(),
                'createdAt' => $viewing->getCreatedAt()->getTimestamp(),
                'duration' => $viewing->getWorkSpace()->getDuration(),
                'attendees' => $viewing->getAttendees(),
            ];
        }

        /** @var WorkSpace $workspace */
        $workspace = $item->getWorkSpace();

        if (is_null($workspace)) {
            return;
        }

        /** @var Location $location */
        $location = $workspace->getLocation();

        if (is_null($location)) {
            return;
        }

        $images = $this->workspaceMediaProvider->getPublicLinks($workspace);

        $seller = $location->getUser();

        $filteredItem['location'] = [
            'id' => $location->getId(),
            'name' => $location->getName(),
            'address' => $location->getAddress(),
            'addressOptional' => $location->getOptionalAddress(),
            'description' => $location->getDescription(),
            'town' => $location->getTown(),
            'postcode' => $location->getPostcode(),
            'latitude' => $location->getLatitude(),
            'longitude' => $location->getLongitude(),
            'nearby' => $location->getNearby(),
            'phone' => $seller->getPhone(),
            'email' => $seller->getEmail()
        ];

        $filteredItem['workspace'] = [
            'id' => $workspace->getId(),
            'coverImageUrl' => $workspace->getCoverImageUrl(),
            'description' => $workspace->getDescription(),
            'quantity' => $workspace->getQuantity(),
            'size' => $workspace->getSize(),
            'capacity' => $workspace->getCapacity(),
            'price' => $workspace->getPrice(),
            'deskType' => $workspace->getDeskType(),
            'type' => $workspace->getType(),
            'images' => $images,
        ];

        $result[] = $filteredItem;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getTopBookedWorkspaces() : array
    {
        $topWorkspaces = $this->getRepository()->getTopBookedWorkspaces();

        $workspaces = [];
        if (empty($topWorkspaces)) {
            return $workspaces;
        }

        foreach ($topWorkspaces as $topWorkspace) {
            $workspace = $this->getRepository()->findOneBy(['id' => $topWorkspace['id']]);
            $workspaces[] = $this->locationCardView($workspace);
        }

        return $workspaces;
    }

    /**
     * @param WorkSpace $workspace
     * @param array $slugsOfFacilities
     * @throws
     */
    public function addFacilitiesToWorkSpace(WorkSpace &$workspace, array $slugsOfFacilities)
    {
        $facilities = $this->facilityModelManager->findByArrayOfSlugs($slugsOfFacilities);

        if (!empty($facilities)) {
            /** @var Facility $facility */
            foreach ($this->yieldCollection($facilities) as $facility) {
                $workspace->addFacility($facility);
            }
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getTopPicksWorkspaces() : array
    {
        $workspaces = $this->getRepository()->getTopPicksWorkspaces();

        if (empty($workspaces)) {
            return [];
        }

        $topPicks = [];

        /** @var WorkSpace $workSpace */
        foreach ($workspaces as $workSpace) {
            $workspace = $this->getRepository()->findOneBy(['id' => $workSpace['id']]);
            $topPicks[] = $this->locationCardView($workspace);
        }

        return $topPicks;
    }
}
