<?php

namespace App\Notifications;

/**
 * Class FrontUrls
 * @package App
 */
class FrontUrls
{
    const CONFIRM_REGISTRATION = '/confirm/%s';
    const CONFIRM_FORGOT_PASSWORD = '/recover/%s';
    const IN_DEPTH = '/indepth/%d';
    const MAINPORTAL = '/mainportal';
    const NOTIFICATIONS = '/mainportal/notifications';
    const HOW_IT_WORKS = '/how-it-works';
    const CO_COMMUNITY = '/blog';
    const HELP_GUIDES = '/blog/community-market/tech-city';
    const FAQ = '/how-it-works#faq';

    /**
     * @var string
     */
    private $projectUrl;

    /**
     * FrontUrls constructor.
     * @param string $projectUrl
     */
    public function __construct(string $projectUrl)
    {
        $this->projectUrl = $projectUrl;
    }

    /**
     * @param string $urlFromConstant
     * @return string
     */
    public function generateUrl(string $urlFromConstant)
    {
        return $this->projectUrl . $urlFromConstant;
    }
}
