<?php

namespace App\Notifications;

use App\Entity\NotificationLog;
use Doctrine\ORM\EntityManager;

/**
 * Class NotificationLogService
 * @package App\Notifications
 */
class NotificationLogService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * NotificationLogService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $status
     * @param string $event
     * @param string $to
     * @param string $subject
     * @param string|null $body
     * @param string|null $log
     */
    public function save(string $status, string $event, string $to, string $subject, ?string $body = null, ?string $log = null)
    {
        $notificationLog = new NotificationLog();
        $notificationLog->setEvent($event);
        $notificationLog->setStatus($status);
        $notificationLog->setToEmail($to);
        $notificationLog->setSubject($subject);
        $notificationLog->setBody($body);
        $notificationLog->setLog($log);

        try {
            $this->entityManager->persist($notificationLog);
            $this->entityManager->flush($notificationLog);
        } catch (\Exception $exception) {
            return;
        }
    }
}
