<?php

namespace App\Notifications;

use App\Entity\NotificationLog;
use App\Entity\User;
use Swift_Attachment;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Class NotificationService
 * @package App\Notifications
 */
class NotificationService
{
    /**
     * @var NotificationsEvent
     */
    protected $notificationEvent;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var string
     */
    private $adminEmail;

    /**
     * @var NotificationLogService
     */
    private $notificationLogService;

    /**
     * NotificationService constructor.
     * @param NotificationsEvent $notificationsEvent
     * @param \Swift_Mailer $mailer
     * @param TwigEngine $twigEngine
     * @param NotificationLogService $notificationLogService
     * @param string $adminEmail
     */
    public function __construct(
        NotificationsEvent $notificationsEvent,
        \Swift_Mailer $mailer,
        TwigEngine $twigEngine,
        NotificationLogService $notificationLogService,
        string $adminEmail
    ) {
        $this->notificationEvent = $notificationsEvent;
        $this->mailer = $mailer;
        $this->twig = $twigEngine;
        $this->notificationLogService = $notificationLogService;
        $this->adminEmail = $adminEmail;
    }

    /**
     * @param $userOrEmail
     * @param string $event
     * @param array $data
     * @param User|null $seller
     * @param null|string $additionalEmail
     */
    public function sendEmail($userOrEmail, string $event, array $data, ?User $seller = null, ?string $additionalEmail = '') : void
    {
        $notificationType = $this->notificationEvent->getTemplate($event);

        //wrong event type
        if (is_null($notificationType)) {
            return;
        }
        $this->tryToSend(
            $event,
            ($userOrEmail instanceof User) ? $userOrEmail->getEmail() : $userOrEmail,
            $notificationType->getSubject(),
            'emails/' . $notificationType->getTemplateName() . '.html.twig',
            $data
        );

        if (!is_null($seller)) {
            $this->tryToSend(
                $event,
                $seller->getEmail(),
                $notificationType->getSubject(),
                'emails/' . $notificationType->getTemplateName() . '__seller.html.twig',
                $data
            );
        }

        $this->tryToSend(
            $event,
            $this->adminEmail,
            $notificationType->getSubject(),
            'emails/' . $notificationType->getTemplateName() . '__admin.html.twig',
            $data
        );

        if (!empty($additionalEmail)) {
            $this->tryToSend(
                $event,
                $additionalEmail,
                $notificationType->getSubject(),
                'emails/' . $notificationType->getTemplateName() . '.html.twig',
                $data
            );
        }
    }

    /**
     * @param User $user
     * @param string $event
     * @param array $data
     */
    public function sendEmailToUser(User $user, string $event, array $data)
    {
        $notificationType = $this->notificationEvent->getTemplate($event);

        //wrong event type
        if (is_null($notificationType)) {
            return;
        }

        $this->tryToSend(
            $event,
            $user->getEmail(),
            $notificationType->getSubject(),
            'emails/' . $notificationType->getTemplateName() . '.html.twig',
            $data
        );
    }

    /**
     * @param string $event
     * @param array $data
     */
    public function sendEmailToAdmin(string $event, array $data)
    {
        $notificationType = $this->notificationEvent->getTemplate($event);

        //wrong event type
        if (is_null($notificationType)) {
            return;
        }

        $this->tryToSend(
            $event,
            $this->adminEmail,
            $notificationType->getSubject(),
            'emails/' . $notificationType->getTemplateName() . '__admin.html.twig',
            $data
        );
    }

    /**
     * @param string $event
     * @param string $toEmail
     * @param string $subject
     * @param string $templatePath
     * @param array $data
     */
    private function tryToSend(string $event, string $toEmail, string $subject, string $templatePath, array $data) : void
    {
        if (!$this->twig->exists($templatePath)) {
            $this->notificationLogService->save(
                NotificationLog::NOT_FOUND,
                $event,
                $toEmail,
                $subject,
                "no template",
                sprintf("Template %s was not found", $templatePath)
            );

            return;
        }

        try {
            $template = $this->twig->render(
                $templatePath,
                $data
            );
        } catch (\Exception $exception) {
            $this->notificationLogService->save(
                NotificationLog::CANT_RENDER,
                $event,
                $toEmail,
                $subject,
                "cannot render template",
                $exception->getMessage()
            );

            return;
        }

        try {
            $message = (new \Swift_Message())
                ->setFrom(['info@kikmoov.com' => 'kikmoov'])
                ->setTo($toEmail)
                ->setSubject($subject)
                ->setBody($template, 'text/html')
            ;

            if (isset($data['attached']) && file_exists($data['attached'])) {
                $message->attach(Swift_Attachment::fromPath($data['attached']));
            }

            //Can be 0 which indicates failure
            $res = $this->mailer->send($message);

            $this->notificationLogService->save(
                NotificationLog::SENT,
                $event,
                $toEmail,
                $subject,
                $template
            );
        } catch (\Exception $exception) {
            $this->notificationLogService->save(
                NotificationLog::FAILED,
                $event,
                $toEmail,
                $subject,
                $template,
                $exception->getMessage()
            );

            return;
        }
    }
}
