<?php

namespace App\Notifications;

/**
 * Class NotificationType
 * @package App\Notifications
 */
final class NotificationType
{
    /**
     * @var string
     */
    private $eventName;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $templateGroupName;

    /**
     * NotificationType constructor.
     * @param string $eventName
     * @param string $subject
     * @param string $templateGroupName
     */
    public function __construct(
        string $eventName,
        string $subject,
        string $templateGroupName = ''
    ) {
        $this->eventName = $eventName;
        $this->subject = $subject;
        $this->templateGroupName = $templateGroupName;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return $this->eventName;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateGroupName . '/' . strtolower($this->eventName);
    }
}
