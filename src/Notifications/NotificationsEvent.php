<?php

namespace App\Notifications;

/**
 * 1. Add the constant, eg TEST_METHOD_HERE
 * 2. Add a private static method, eg testMethodHere
 * 3. Add the template in template/emails path, eg test_method_here.html.twig
 * 4. If you need to send the email to admin, seller, buyer add a postfix, eg test_method_here__seller.html.twig, test_method_here__admin.html.twig
 * 5. If you need to send the email only to seller, admin, you don't need to add __seller postfix, eg test_method_here.html.twig - will be template for the seller, test_method_here__admin.html.twig
 *
 * Class NotificationsEvent
 * @package App\Notifications
 */
final class NotificationsEvent
{
    const LOCATION_DELETED = 'LOCATION_DELETED'; //23
    const LOCATION_ADDED = 'LOCATION_ADDED'; //19 - not implemented, because of 20

    const WORKSPACE_DELETED = 'WORKSPACE_DELETED'; //23
    const WORKSPACE_UNAVAILABLE = 'WORKSPACE_UNAVAILABLE'; //27
    const WORKSPACE_WAITING_APPROVE = 'WORKSPACE_WAITING_APPROVE'; //20,21
    const WORKSPACE_APPROVED = 'WORKSPACE_APPROVED'; //22
    const WORKSPACE_NO_BOOKING_OR_VIEWING_MORE_THAN_MONTH = 'WORKSPACE_NO_BOOKING_OR_VIEWING_MORE_THAN_MONTH'; //24 @todo not implemented

    const VIEWING_REQUEST = 'VIEWING_REQUEST'; //1
    const VIEWING_ONE_HOUR_BEFORE = 'VIEWING_ONE_HOUR_BEFORE'; //3
    const VIEWING_AT_TIME = 'VIEWING_AT_TIME'; //4
    const VIEWING_ONE_HOUR_AFTER = 'VIEWING_ONE_HOUR_AFTER'; //5
    const VIEWING_ONE_WEEK_AFTER_IF_STATUS_NOT_BOOKED = 'VIEWING_ONE_WEEK_AFTER_IF_STATUS_NOT_BOOKED'; //6
    const VIEWING_APPROVED = 'VIEWING_APPROVED'; //2
    const VIEWING_INVITE_ATTENDEE = 'VIEWING_INVITE_ATTENDEE';

    const BOOKING_APPROVED = 'BOOKING_APPROVED'; //8
    const BOOKING_CANCELED_MORE_THAN_24 = 'BOOKING_CANCELED_MORE_THAN_24'; //15
    const BOOKING_CANCELED_LESS_THAN_24 = 'BOOKING_CANCELED_LESS_THAN_24'; //16
    const BOOKING_CANCELED = 'BOOKING_CANCELED'; // for viewings/monthly bookings
    const BOOKING_REQUEST = 'BOOKING_REQUEST'; //7
    const BOOKING_ONE_HOUR_BEFORE = 'BOOKING_ONE_HOUR_BEFORE'; //12
    const BOOKING_ONE_HOUR_AFTER = 'BOOKING_ONE_HOUR_AFTER'; //12
    const BOOKING_HOURLY_DESK = 'BOOKING_HOURLY_DESK'; //11, 10
    const BOOKING_AT_TIME = 'BOOKING_AT_TIME'; //13
    const BOOKING_HOURLY_MEETING_ROOM = 'BOOKING_HOURLY_MEETING_ROOM'; //9
    const BOOKING_INVITE_ATTENDEE = 'BOOKING_INVITE_ATTENDEE';

    const REMINDER_OF_LIKED_WORKSPACES = 'REMINDER_OF_LIKED_WORKSPACES'; //17

    const ACCOUNT_CREATED_BUYER  = 'ACCOUNT_CREATED_BUYER'; //28
    const ACCOUNT_CREATED_SELLER = 'ACCOUNT_CREATED_SELLER'; //29
    const ACCOUNT_DELETED = 'ACCOUNT_DELETED'; //14
    const FORGOT_PASSWORD = 'FORGOT_PASSWORD';

    const LEASEHOLD_ENQUIRY = 'LEASEHOLD_ENQUIRY'; //30
    const CONTACT_ENQUIRY = 'CONTACT_ENQUIRY'; //31




    //18,25,26 missed

    /**
     * Try to keep spaces between blocks
     * @return array
     */
    protected static function notificationEvents() : array
    {
        $arrayOfEvents = [
            self::LOCATION_DELETED => self::LOCATION_DELETED,
            self::LOCATION_ADDED => self::LOCATION_ADDED,

            self::WORKSPACE_DELETED => self::WORKSPACE_DELETED,
            self::WORKSPACE_UNAVAILABLE => self::WORKSPACE_UNAVAILABLE,
            self::WORKSPACE_WAITING_APPROVE => self::WORKSPACE_WAITING_APPROVE,
            self::WORKSPACE_APPROVED => self::WORKSPACE_APPROVED,
            self::WORKSPACE_NO_BOOKING_OR_VIEWING_MORE_THAN_MONTH => self::WORKSPACE_NO_BOOKING_OR_VIEWING_MORE_THAN_MONTH,

            self::VIEWING_REQUEST => self::VIEWING_REQUEST,
            self::VIEWING_APPROVED => self::VIEWING_APPROVED,
            self::VIEWING_ONE_HOUR_BEFORE => self::VIEWING_ONE_HOUR_BEFORE,
            self::VIEWING_AT_TIME => self::VIEWING_AT_TIME,
            self::VIEWING_ONE_HOUR_AFTER => self::VIEWING_ONE_HOUR_AFTER,
            self::VIEWING_ONE_WEEK_AFTER_IF_STATUS_NOT_BOOKED => self::VIEWING_ONE_WEEK_AFTER_IF_STATUS_NOT_BOOKED,
            self::VIEWING_INVITE_ATTENDEE => self::VIEWING_INVITE_ATTENDEE,

            self::BOOKING_APPROVED => self::BOOKING_APPROVED,
            self::BOOKING_CANCELED_MORE_THAN_24 => self::BOOKING_CANCELED_MORE_THAN_24,
            self::BOOKING_CANCELED_LESS_THAN_24 => self::BOOKING_CANCELED_LESS_THAN_24,
            self::BOOKING_CANCELED => self::BOOKING_CANCELED,
            self::BOOKING_REQUEST => self::BOOKING_REQUEST,
            self::BOOKING_ONE_HOUR_BEFORE => self::BOOKING_ONE_HOUR_BEFORE,
            self::BOOKING_ONE_HOUR_AFTER => self::BOOKING_ONE_HOUR_AFTER,
            self::BOOKING_HOURLY_DESK => self::BOOKING_HOURLY_DESK,
            self::BOOKING_AT_TIME => self::BOOKING_AT_TIME,
            self::BOOKING_HOURLY_MEETING_ROOM => self::BOOKING_HOURLY_MEETING_ROOM,
            self::BOOKING_INVITE_ATTENDEE => self::BOOKING_INVITE_ATTENDEE,

            self::REMINDER_OF_LIKED_WORKSPACES => self::REMINDER_OF_LIKED_WORKSPACES,

            self::ACCOUNT_CREATED_BUYER => self::ACCOUNT_CREATED_BUYER,
            self::ACCOUNT_CREATED_SELLER => self::ACCOUNT_CREATED_SELLER,
            self::ACCOUNT_DELETED => self::ACCOUNT_DELETED,

            self::FORGOT_PASSWORD => self::FORGOT_PASSWORD,

            self::LEASEHOLD_ENQUIRY => self::LEASEHOLD_ENQUIRY,
            self::CONTACT_ENQUIRY => self::CONTACT_ENQUIRY,
        ];

        return array_combine($arrayOfEvents, $arrayOfEvents);
    }

    /**
     * @param string $eventName
     * @return mixed|null
     */
    public function getTemplate(string $eventName) :? NotificationType
    {
        $event = self::notificationEvents()[$eventName] ? self::notificationEvents()[$eventName] : null;

        if (is_null($event)) {
            return null;
        }

        //@magic - make constant as method name
        $methodName = str_replace("_", "", lcfirst(ucwords(strtolower($event), "_")));

        //@magic - run private static method of the current class
        return call_user_func(NotificationsEvent::class . '::' . $methodName, $eventName);
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function locationDeleted($eventName)
    {
        return new NotificationType(
            $eventName,
            "Location was deleted",
            "location"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function locationAdded($eventName)
    {
        return new NotificationType(
            $eventName,
            "New location was added",
            "location"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function workspaceDeleted($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspace was deleted",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function workspaceUnavailable($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspace is unavailable",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function workspaceWaitingApprove($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspace is waiting for approve",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function workspaceApproved($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspace has been approved",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function workspaceNoBookingOrViewingMoreThanMonth($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspace is not popular",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function reminderOfLikedWorkspaces($eventName)
    {
        return new NotificationType(
            $eventName,
            "Workspaces that you liked",
            "workspace"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingRequest($eventName)
    {
        return new NotificationType(
            $eventName,
            "Viewing request",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingOneHourBefore($eventName)
    {
        return new NotificationType(
            $eventName,
            "One hour before viewing",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingAtTime($eventName)
    {
        return new NotificationType(
            $eventName,
            "Time for the viewing",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingOneHourAfter($eventName)
    {
        return new NotificationType(
            $eventName,
            "One hour after viewing",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingOneWeekAfterIfStatusNotBooked($eventName)
    {
        return new NotificationType(
            $eventName,
            "Status not changed",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingApproved($eventName)
    {
        return new NotificationType(
            $eventName,
            "Viewing has been approved",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function viewingInviteAttendee($eventName)
    {
        return new NotificationType(
            $eventName,
            "You have received an invitation",
            "viewing"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingApproved($eventName)
    {
        return new NotificationType(
            $eventName,
            "Booking has been approved",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingCanceledMoreThan24($eventName)
    {
        return new NotificationType(
            $eventName,
            "Booking canceled",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingCanceledLessThan24($eventName)
    {
        return new NotificationType(
            $eventName,
            "Booking canceled",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingCanceled($eventName)
    {
        return new NotificationType(
            $eventName,
            "Booking canceled",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingRequest($eventName)
    {
        return new NotificationType(
            $eventName,
            "Booking request",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingOneHourBefore($eventName)
    {
        return new NotificationType(
            $eventName,
            "One hour before booking",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingOneHourAfter($eventName)
    {
        return new NotificationType(
            $eventName,
            "One hour after booking",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingHourlyDesk($eventName)
    {
        return new NotificationType(
            $eventName,
            "Book a desk",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingAtTime($eventName)
    {
        return new NotificationType(
            $eventName,
            "Time for the booking",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingHourlyMeetingRoom($eventName)
    {
        return new NotificationType(
            $eventName,
            "Book a meeting room",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function bookingInviteAttendee($eventName)
    {
        return new NotificationType(
            $eventName,
            "You have received an invitation",
            "booking"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function accountCreatedBuyer($eventName)
    {
        return new NotificationType(
            $eventName,
            "Your account has been successfully created",
            "account"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function accountCreatedSeller($eventName)
    {
        return new NotificationType(
            $eventName,
            "Your account has been successfully created",
            "account"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function forgotPassword($eventName)
    {
        return new NotificationType(
            $eventName,
            "Forgot password link",
            "security"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function accountDeleted($eventName)
    {
        return new NotificationType(
            $eventName,
            "Account was deleted",
            "account"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function leaseholdEnquiry($eventName)
    {
        return new NotificationType(
            $eventName,
            "Leasehold Enquiry",
            "enquiry"
        );
    }

    /**
     * @param $eventName
     * @return NotificationType
     */
    private static function contactEnquiry($eventName)
    {
        return new NotificationType(
            $eventName,
            "Contact Enquiry",
            "enquiry"
        );
    }
}
