<?php

namespace App\Provider;

use GuzzleHttp;

/**
 * Class FacebookClient
 * @package App\Provider
 */
class FacebookClient
{
    /**
     * @var string
     */
    private $faceBookAppToken;

    /**
     * FacebookClient constructor.
     * @param string $faceBookAppToken
     */
    public function __construct(string $faceBookAppToken)
    {
        $this->faceBookAppToken = $faceBookAppToken;
    }

    /**
     * @param string $authToken
     * @return bool
     */
    public function verifyToken(string $authToken) : bool
    {
        $client = new GuzzleHttp\Client();

        try {
            $payload = $client->get(sprintf(
                'https://graph.facebook.com/debug_token?input_token=%s&access_token=%s',
                $authToken,
                $this->faceBookAppToken
            ));

            $response = json_decode($payload->getBody(), true);

            if (!isset($response['data'])) {
                return false;
            }

            if (!isset($response['data']['is_valid'])) {
                return false;
            }

            return $response['data']['is_valid'];
        } catch (\Exception $exception) {
            return false;
        }
    }
}
