<?php

namespace App\Provider;

use Google_Client;

/**
 * Class GoogleClient
 * @package App\Provider
 */
class GoogleClient
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $iosClientId;

    /**
     * @var string
     */
    private $androidClientId;

    /**
     * GoogleClient constructor.
     * @param string $apiKey
     * @param string $clientId
     * @param string $clientSecret
     * @param string $iosClientId
     * @param string $androidClientId
     */
    public function __construct(
        string $apiKey,
        string $clientId,
        string $clientSecret,
        string $iosClientId,
        string $androidClientId
    ) {
        $this->apiKey = $apiKey;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->iosClientId = $iosClientId;
        $this->androidClientId = $androidClientId;
    }

    /**
     * @param $token
     * @return bool
     */
    public function verifyWebIdToken($token) : bool
    {
        $client = new Google_Client([
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'developer_key' => $this->apiKey
        ]);

        $payload = $client->verifyIdToken($token);

        return (!$payload) ? false : true;
    }

    /**
     * @param $token
     * @return bool
     */
    public function verifyIosIdToken($token) : bool
    {
        $client = new Google_Client([
            'client_id' => $this->iosClientId,
            'client_secret' => $this->clientSecret,
            'developer_key' => $this->apiKey
        ]);

        $payload = $client->verifyIdToken($token);

        return (!$payload) ? false : true;
    }

    /**
     * @param $token
     * @return bool
     */
    public function verifyAndroidIdToken($token) : bool
    {
        $client = new Google_Client([
            'client_id' => $this->androidClientId,
            'client_secret' => $this->clientSecret,
            'developer_key' => $this->apiKey
        ]);

        $payload = $client->verifyIdToken($token);

        return (!$payload) ? false : true;
    }

}
