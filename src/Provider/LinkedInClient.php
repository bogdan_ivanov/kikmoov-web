<?php

namespace App\Provider;

use GuzzleHttp;

/**
 * Class LinkedInClient
 * @package App\Provider
 */
class LinkedInClient
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $redirectUri;

    /**
     * LinkedInClient constructor.
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     */
    public function __construct(string $clientId, string $clientSecret, string $redirectUri)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUri = $redirectUri;
    }

    /**
     * @param string $code
     * @param bool|null $isMobile
     * @return array
     */
    public function verifyToken(string $code, ?bool $isMobile) : array
    {
        [$ok, $data] = $this->getAccessToken($code);

        if ($isMobile) {
            $ok = true;
            $data['token'] = $code;
        }

        if (!$ok) {
            return [
                $ok,
                $data
            ];
        }

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $data['token'],
                'x-li-format' => 'json'
            ]
        ]);

        try {
            //from this we will get id of user, his firstName, lastName and url of profile picture
            $payload = $client->get('https://api.linkedin.com/v2/me');

            $response = json_decode($payload->getBody(), true);

            //from this we will get emailAddress of user
            $payloadEmail = $client->get('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))');

            $responseEmail = json_decode($payloadEmail->getBody(), true);

            $response = array_merge($response, $responseEmail);

            return [
                true,
                $response
            ];
        } catch (\Exception $exception) {
            return [
                false,
                [
                    'message' => $exception->getMessage()
                ]
            ];
        }
    }

    /**
     * @param string $code
     * @return array
     */
    protected function getAccessToken(string $code)
    {
        $client = new GuzzleHttp\Client();
        $params = http_build_query([
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $this->redirectUri,
            'grant_type' => 'authorization_code',
            'client_id' => $this->clientId,
            'code' => $code
        ]);

        try {
            $payload = $client->post('https://www.linkedin.com/oauth/v2/accessToken?' . $params);
            $response = json_decode($payload->getBody(), true);

            if (isset($response['access_token'])) {
                return [
                    true,
                    [
                        'token' => $response['access_token']
                    ]
                ];
            } else {
                return [
                    false,
                    [
                        'message' => 'Cannot authorized via LinkedIn. Please, try again later'
                    ]
                ];
            }
        } catch (\Exception $exception) {
            return [
                false,
                [
                    'message' => $exception->getMessage()
                ]
            ];
        }
    }
}
