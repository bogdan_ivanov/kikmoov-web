<?php

namespace App\Provider;

use App\Controller\DataResponse;
use App\Entity\User;
use App\Managers\ApiClientManager;
use App\Security\SocialPasswordEncoder;
use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SocialProvider
 * @package App\Provider
 */
class SocialProvider
{
    const GOOGLE   = 'google';
    const LINKEDIN = 'linkedin';
    const FACEBOOK = 'facebook';

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ApiClientManager
     */
    protected $apiClientManager;

    /**
     * @var SocialPasswordEncoder
     */
    private $socialPasswordEncoder;

    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;

    /**
     * @var GoogleClient
     */
    private $googleClient;

    /**
     * @var FacebookClient
     */
    private $facebookClient;

    /**
     * @var LinkedInClient
     */
    private $linkedInClient;

    /**
     * SocialProvider constructor.
     * @param UserManager $userManager
     * @param ApiClientManager $apiClientManager
     * @param SocialPasswordEncoder $socialPasswordEncoder
     * @param TokenGenerator $tokenGenerator
     * @param GoogleClient $googleClient
     * @param FacebookClient $facebookClient
     * @param LinkedInClient $linkedInClient
     */
    public function __construct(
        UserManager $userManager,
        ApiClientManager $apiClientManager,
        SocialPasswordEncoder $socialPasswordEncoder,
        TokenGenerator $tokenGenerator,
        GoogleClient $googleClient,
        FacebookClient $facebookClient,
        LinkedInClient $linkedInClient
    ) {
        $this->userManager = $userManager;
        $this->apiClientManager = $apiClientManager;
        $this->socialPasswordEncoder = $socialPasswordEncoder;
        $this->tokenGenerator = $tokenGenerator;
        $this->googleClient = $googleClient;
        $this->facebookClient = $facebookClient;
        $this->linkedInClient = $linkedInClient;
    }

    /**
     * @param string $socialType
     * @param string $token
     * @param null|string $email
     * @param array $data
     * @param bool $mobileApp
     * @return DataResponse
     */
    public function validate(string $socialType, string $token, ?string $email, array $data, ?bool $mobileApp) : DataResponse
    {
        if ($socialType !== self::LINKEDIN && !isset($data['id'])) {
            return new DataResponse(Response::HTTP_BAD_REQUEST, ['message' => 'Required data was missed. Please, try again']);
        }

        $isValid = false;

        if ($socialType === self::GOOGLE) {
            if (isset($data['platform'])) {
                if ($data['platform'] === 'ios') {
                    $isValid = $this->googleClient->verifyIosIdToken($token);
                } else {
                    $isValid = $this->googleClient->verifyAndroidIdToken($token);
                }
            } else {
                $isValid = $this->googleClient->verifyWebIdToken($token);
            }
        } else if ($socialType === self::FACEBOOK) {
            $isValid = $this->facebookClient->verifyToken($token);
        } else if ($socialType === self::LINKEDIN) {
            [$isValid, $data] = $this->linkedInClient->verifyToken($token, $mobileApp);

            if (!isset($data['elements'][0]['handle~']['emailAddress'])) {
                return new DataResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [
                    'message' => 'LinkedIn server is temporarily unavailable. Please, try again'
                ]);
            }

            if ($isValid) {
                $email = $data['elements'][0]['handle~']['emailAddress'];
            }
        }

        if (!$isValid) {
            return new DataResponse(Response::HTTP_UNAUTHORIZED, ['message' => 'Invalid authorization data. Please, try again']);
        }

        $user = $this->userManager->findUserByEmail($email);

        if ($user === null) {
            /** @var User $user */
            $user = $this->userManager->createUser();
            $user->setEnabled(false);
            $user->setEmail($email);
            $user->setEnabled(true);
            $user->setPlainPassword($this->tokenGenerator->generateToken());

            if ($socialType === self::LINKEDIN) {
                if (isset($data['firstName']['preferredLocale'])) {
                    $firstNameLocalised = $data['firstName']['preferredLocale']['language'] . '_' . $data['firstName']['preferredLocale']['country'];
                    $user->setFirstname(isset($data['firstName']['localized']) ? $data['firstName']['localized'][$firstNameLocalised] : "User");
                }
                if (isset($data['lastName']['preferredLocale'])) {
                    $lastNameLocalised = $data['lastName']['preferredLocale']['language'] . '_' . $data['lastName']['preferredLocale']['country'];
                    $user->setLastname(isset($data['lastName']['localized']) ? $data['lastName']['localized'][$lastNameLocalised] : null);
                }
            } else {
                $user->setFirstname(isset($data['firstName']) ? $data['firstName'] : "User");
                $user->setLastname(isset($data['lastName']) ? $data['lastName'] : null);
            }
        }

        if (!is_null($mobileApp)) {
            if ($user->hasRole(User::ROLE_SELLER)) {
                return new DataResponse(
                    Response::HTTP_FORBIDDEN,
                    ['message' => 'This email is not linked to a Find a Space account.If you wish to find a space, please create an account with another email address.']
                );
            }
        }

        if (!$user->isEnabled()) {
            $user->setEnabled(true);
            $user->setConfirmationToken(null);
        }

        if ($socialType === self::GOOGLE) {
            if (is_null($user->getGplusUid())) {
                $user->setGplusUid($data['id']);
            }

            $user->setGplusData(json_encode($data));
        } else if ($socialType === self::FACEBOOK) {
            if (is_null($user->getFacebookUid())) {
                $user->setFacebookUid($data['id']);
            }

            $user->setFacebookData(json_encode($data));
        } else {
            if (is_null($user->getLinkedInUid())) {
                $user->setLinkedInUid($data['id']);
            }

            $user->setLinkedInData($data);
        }

        $user->setSocialPassword($this->tokenGenerator->generateToken());
        $this->userManager->updateUser($user);

        $tokens = $this->apiClientManager->getApiAccess($user);

        $tokens['socialToken'] = $this->socialPasswordEncoder->encode($user->getSocialPassword());
        $tokens['accountType'] = $user->hasRole(User::ROLE_SELLER) ? User::SELLER : User::BUYER;
        $tokens['email'] = $user->getEmail();

        return new DataResponse(Response::HTTP_OK, $tokens);
    }
}
