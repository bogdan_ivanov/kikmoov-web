<?php

namespace App\Provider;

use App\Entity\Media;
use App\Entity\WorkSpace;
use App\Services\MediaUrlGenerator;
use App\Traits\YieldTrait;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Sonata\MediaBundle\Entity\MediaManager;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class WorkspaceMediaProvider
 * @package App\Provider
 */
class WorkspaceMediaProvider
{

    const PREVIEW = 'preview';

    use YieldTrait;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var MediaUrlGenerator
     */
    private $mediaUrlGenerator;

    /**
     * WorkspaceMediaProvider constructor.
     * @param ObjectManager $objectManager
     * @param MediaManager $mediaManager
     * @param MediaUrlGenerator $mediaUrlGenerator
     */
    public function __construct(
        ObjectManager $objectManager,
        MediaManager $mediaManager,
        MediaUrlGenerator $mediaUrlGenerator
    ) {
        $this->objectManager = $objectManager;
        $this->mediaManager = $mediaManager;
        $this->mediaUrlGenerator = $mediaUrlGenerator;
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository()
    {
        return $this->objectManager->getRepository(Media::class);
    }


    /**
     * @param WorkSpace $workSpace
     * @param File $file
     */
    public function uploadMedia(WorkSpace $workSpace, File $file)
    {
        $media = new Media();
        $media->setBinaryContent($file);
        $media->setContext('workspace');
        $media->setProviderName('sonata.media.provider.image');
        $this->mediaManager->save($media);
        $workSpace->addMedia($media);
        $this->objectManager->flush();
    }

    /**
     * @param WorkSpace $workSpace
     * @param $id
     * @return bool
     */
    public function removeMedia(WorkSpace $workSpace, int $id) : bool
    {
        /** @var Media $media */
        $media = $this->getRepository()->findOneBy(['id' => $id]);

        if ($media === null) {
            return false;
        }

        $workSpace->removeMedia($media);
        $this->objectManager->persist($workSpace);
        $this->objectManager->flush();

        return true;
    }

    /**
     * @param WorkSpace $workSpace
     * @param bool $absolute
     * @param null|string $formatType
     * @return array
     * @throws \Exception
     */
    public function getPublicLinks(
        WorkSpace $workSpace,
        $absolute = false,
        ?string $formatType = self::PREVIEW
    ) : array
    {
        $urls = [];

        if ($workSpace->getMedias()->isEmpty()) {
            return $urls;
        }


        /** @var Media $media */
        foreach ($this->yieldCollection($workSpace->getMedias()) as $media) {
            $urls[] = [
                'url' => $this->mediaUrlGenerator->generateUrl($media, $absolute, $formatType),
                'id' => $media->getId()
            ];
        }

        $k = array_search($workSpace->getCoverImageId(), array_column($urls, 'id'));
        if ($k) {
            $coverImageItem = $urls[$k];
            unset($urls[$k]);
            array_unshift($urls, $coverImageItem);
        }

        return $urls;
    }

    /**
     * @param WorkSpace $workSpace
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function setCoverImage(WorkSpace $workSpace, int $id)
    {
        $ok = false;

        /** @var Media $media */
        foreach ($this->yieldCollection($workSpace->getMedias()) as $media) {
            if ($media->getId() !== $id) {
                continue;
            }

            $url = $this->mediaUrlGenerator->generateUrl($media);
            $workSpace->setCoverImageUrl($url);
            $workSpace->setCoverImageId($id);
            try {
                $this->objectManager->flush();
                $ok = true;
            } catch (\Exception $exception) {
                return false;
            }
        }

        return $ok;
    }
}
