<?php

namespace App\Repository;

use App\Entity\Area;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AreaRepository
 * @package App\Repository
 */
class AreaRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Area::class);
    }

    /**
     * @return mixed
     */
    public function getActiveAreas()
    {
        return $this->createQueryBuilder('a')
            ->where('a.status = :status')
            ->setParameter('status', true)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $area
     * @return mixed
     */
    public function getActiveAreasSuggestion($area)
    {
        return $this->createQueryBuilder('a')
            ->select('a.name as value, \'area\' as type')
            ->where('a.status = :status')
            ->andWhere('LOWER(a.name) LIKE :area')
            ->setParameters([
                'status' => true,
                'area' => '%'.$area.'%'
            ])
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return mixed
     */
    public function getActiveAreasSlugs()
    {
        return $this->createQueryBuilder('a')
            ->select('a.slug')
            ->where('a.status = :status')
            ->setParameter('status', true)
            ->getQuery()
            ->getResult()
            ;
    }
}