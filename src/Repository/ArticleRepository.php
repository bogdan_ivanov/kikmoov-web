<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    const LIMIT_SIMILAR_ARTICLES = 3;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @param Category $category
     * @param int $articleId
     * @return mixed
     */
    public function getSimilarArticlesByCategory(Category $category, int $articleId)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.category', 'c')
            ->where('c.id = :categoryId')
            ->andWhere('a.id != :articleId')
            ->setParameters([
                'categoryId' => $category->getId(),
                'articleId'  => $articleId
            ])
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(self::LIMIT_SIMILAR_ARTICLES)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getLastArticle(string $slug = null): ?array
    {
        $builder = $this->createQueryBuilder('a');
        $builder->select("c.name, a.title, a.description, a.id, a.coverImageUrl, a.slug, c.slug as category_slug")
            ->innerJoin('a.category', 'c')
            ->where('c.id = a.category')
            ->andWhere('a.published = 1');

        if ($slug) {
            $builder->andWhere('c.slug = :slug')
                ->setParameter('slug', $slug);
        }

        $builder->orderBy('a.updatedAt', 'DESC')
            ->setMaxResults(1);

        return $builder
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_ARRAY)
        ;
    }
}
