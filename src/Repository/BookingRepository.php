<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param int $workspaceId
     * @param \DateTime $startTime
     * @param \DateTime $endTime
     * @return bool
     */
    public function isTimeAvailable(int $workspaceId, \DateTime $startTime, \DateTime $endTime) : bool
    {
        $query = $this->createQueryBuilder('b')
            ->innerJoin('b.workSpace', 'w')
            ->where('b.startTime BETWEEN :startTime AND :endTime')
            ->orWhere('b.endTime BETWEEN :startTime AND :endTime')
            ->orWhere(':startTime BETWEEN b.startTime AND b.endTime')
            ->andWhere('w.id = :id')
            ->andWhere('b.bookingType = :bookingType')
            ->setParameters([
                'startTime' => $startTime->modify("+1 minute"),
                'endTime' => $endTime,
                'id' => $workspaceId,
                'bookingType' => Booking::BOOKING
            ])
        ;

        $results = $query->getQuery()->getResult();

        return empty($results);
    }

    /**
     * @return mixed
     */
    public function oneDayBeforeStart()
    {
        $now = new \DateTime('+1 day');

        $query = $this->createQueryBuilder('b')
            ->where('b.status = :accepted')
            ->andWhere('b.startTime LIKE :now')
            ->andWhere('b.bookingType = :bookingType')
            ->setParameters([
                'accepted' => Booking::ACCEPTED,
                'now' => $now->format("Y-m-d") . "%",
                'bookingType' => Booking::BOOKING
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getBookingWorkspaces(User $user)
    {
        return $this->createQueryBuilder('b')
            ->where('b.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function getBookingsForCronTask(\DateTime $date)
    {
        $query = $this->createQueryBuilder('b')
            ->where('b.status IN (:statuses)')
            ->andWhere('b.startTime LIKE :start')
            ->andWhere('b.bookingType = :bookingType')
            ->setParameters([
                'statuses' => [Booking::ACCEPTED, Booking::PAYMENT_RECEIVED],
                'start' => $date->format("Y-m-d H:i") . "%",
                'bookingType' => Booking::BOOKING,
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function getBookingsForWithdrawMoney(\DateTime $date)
    {
        $query = $this->createQueryBuilder('b')
            ->where('b.status = :accepted')
            ->andWhere('b.startTime LIKE :start')
            ->andWhere('b.bookingType = :bookingType')
            ->setParameters([
                'accepted' => Booking::ACCEPTED,
                'start' => $date->format("Y-m-d H:i") . "%",
                'bookingType' => Booking::BOOKING,
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function userStatusChecker(\DateTime $date)
    {
        $query = $this->createQueryBuilder('b')
            ->where('b.status IN (:statuses)')
            ->andWhere('b.startTime LIKE :start')
            ->andWhere('b.bookingType = :bookingType')
            ->setParameters([
                'statuses' => [Booking::ACCEPTED, Booking::PAYMENT_RECEIVED],
                'start' => $date->format("Y-m-d") . "%",
                'bookingType' => Booking::BOOKING,
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @param User $user
     * @return mixed
     */
    public function getAllBookingsByDateForCronTask(\DateTime $date, User $user)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.createdAt >= :start')
            ->andWhere('v.user = :user')
            ->setParameters([
                'start' => $date->format("Y-m-d") . "%",
                'user'=> $user
            ])
        ;

        return $query->getQuery()->getResult();
    }
}
