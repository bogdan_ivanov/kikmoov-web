<?php

namespace App\Repository;

use App\Entity\Borough;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class BoroughRepository
 * @package App\Repository
 */
class BoroughRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Borough::class);
    }

    /**
     * @param $borough
     * @return mixed
     */
    public function getActiveBoroughsSuggestion($borough)
    {
        return $this->createQueryBuilder('b')
            ->select('b.name as value, \'borough\' as type')
            ->where('b.status = :status')
            ->andWhere('LOWER(b.name) LIKE :borough')
            ->setParameters([
                'status' => true,
                'borough' => '%'.$borough.'%'
            ])
            ->getQuery()
            ->getResult()
            ;
    }
}