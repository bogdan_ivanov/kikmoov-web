<?php

namespace App\Repository;

use App\Entity\BuyerInternalNotification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class UserDeviceRepository
 * @package App\Repository
 */
class BuyerInternalNotificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BuyerInternalNotification::class);
    }
}