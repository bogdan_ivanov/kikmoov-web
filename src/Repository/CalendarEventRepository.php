<?php

namespace App\Repository;

use App\Entity\CalendarEvent;
use App\Entity\WorkSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CalendarEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method CalendarEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method CalendarEvent[]    findAll()
 * @method CalendarEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarEventRepository extends ServiceEntityRepository
{
    /**
     * CalendarEventRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CalendarEvent::class);
    }

    /**
     * @param WorkSpace $workSpace
     * @return array
     */
    public function getTimeEventsByWorkspace(WorkSpace $workSpace)
    {
        $query = $this->createQueryBuilder('ce')
            ->select('ce.start, ce.end')
            ->innerJoin('ce.calendar', 'c')
            ->where('c.workspace = :workspace')
            ->andWhere('c.status = :status')
            ->setParameters([
                'workspace' =>  $workSpace,
                'status' => true,
            ])
        ;

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param WorkSpace $workSpace
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     * @return array
     */
    public function getCalendarEvents(WorkSpace $workSpace, \DateTimeInterface $startTime, \DateTimeInterface $endTime)
    {
        return $this->createQueryBuilder('ce')
            ->select('ce.id')
            ->innerJoin('ce.calendar', 'c')
            ->where('c.workspace = :workspace')
            ->andWhere('c.status = :status')
            ->andWhere('(ce.start BETWEEN :start AND :end OR ce.end BETWEEN :start AND :end) OR (ce.start BETWEEN :start AND :end OR ce.end BETWEEN :start AND :end) OR (ce.start >= :start AND ce.end <= :end)')
            ->setParameters([
                'workspace' =>  $workSpace,
                'status' => true,
                'start' => $startTime,
                'end' => $endTime
            ])
            ->getQuery()->getArrayResult();

    }

}
