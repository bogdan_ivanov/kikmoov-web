<?php

namespace App\Repository;

use App\Entity\Calendar;
use App\Entity\WorkSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Calendar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calendar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calendar[]    findAll()
 * @method Calendar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Calendar::class);
    }

    public function getGoogleCalendar(WorkSpace $workspace)
    {
        return $this->createQueryBuilder('c')
            ->where('c.workspace = :workspace')
            ->andWhere('c.type = :type')
            ->setParameters([
                'workspace' => $workspace,
                'type' => Calendar::GOOGLE_CALENDAR
            ])
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param WorkSpace $workSpace
     * @return mixed
     */
    public function getOutsideCalendars(WorkSpace $workSpace)
    {
        return $this->createQueryBuilder('c')
            ->where('c.workspace = :workspace')
            ->andWhere('c.type IN (:type)')
            ->andWhere('c.status = :status')
            ->setParameters([
                'workspace' => $workSpace,
                'type' =>
                    [
                        Calendar::GOOGLE_CALENDAR,
                        Calendar::OUTLOOK_CALENDAR
                    ],
                'status' => true
            ])
            ->getQuery()
            ->getResult()
            ;
    }
}
