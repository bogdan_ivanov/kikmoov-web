<?php

namespace App\Repository;

use App\Entity\InternalNotification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InternalNotification|null find($id, $lockMode = null, $lockVersion = null)
 * @method InternalNotification|null findOneBy(array $criteria, array $orderBy = null)
 * @method InternalNotification[]    findAll()
 * @method InternalNotification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InternalNotificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InternalNotification::class);
    }
}
