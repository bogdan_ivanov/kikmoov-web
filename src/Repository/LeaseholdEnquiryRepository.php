<?php

namespace App\Repository;

use App\Entity\LeaseholdEnquiry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LeaseholdEnquiry|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaseholdEnquiry|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaseholdEnquiry[]    findAll()
 * @method LeaseholdEnquiry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaseholdEnquiryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LeaseholdEnquiry::class);
    }

//    /**
//     * @return LeaseholdEnquiry[] Returns an array of LeaseholdEnquiry objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeaseholdEnquiry
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
