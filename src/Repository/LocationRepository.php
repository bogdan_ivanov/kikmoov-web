<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\WorkSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Location::class);
    }

    /**
     * @param string $address
     * @return array
     */
    public function findByAddress(string $address): array
    {
        $query = $this->createQueryBuilder('l')
            ->innerJoin('l.workspaces', 'w')
            ->where('LOWER(l.address) LIKE :address OR LOWER(l.optionalAddress) LIKE :address 
            OR LOWER(l.borough) LIKE :address OR LOWER(l.area) LIKE :address')
            ->andWhere('w.status = :active')
            ->setParameters(['address' => '%' . $address . '%', 'active' => WorkSpace::ACTIVE])
        ;

        return $query->getQuery()->getResult();
    }
}
