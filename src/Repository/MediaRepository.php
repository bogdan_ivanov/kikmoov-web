<?php

namespace App\Repository;

use App\Entity\Media;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class MediaRepository
 * @package App\Repository
 */
class MediaRepository extends ServiceEntityRepository
{
     public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Media::class);
    }

    /**
     * @return mixed
     */
    public function getMediaWithoutWorkspaceContext()
    {
        return $this->createQueryBuilder('m')
            ->where('m.context != :context')
            ->setParameter('context', 'workspace')
            ->getQuery()
            ->getResult()
            ;
    }
}