<?php

namespace App\Repository;

use App\Entity\ProudToWork;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProudToWork|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProudToWork|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProudToWork[]    findAll()
 * @method ProudToWork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProudToWorkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProudToWork::class);
    }

//    /**
//     * @return ProudToWork[] Returns an array of ProudToWork objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProudToWork
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
