<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function findExcept(array $ids)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id NOT IN (:ids)')
            ->andWhere('u.enabled = :enabled')
            ->andWhere('u.status != :status')
            ->setParameters(['ids' => $ids, 'enabled' => true, 'status' => User::ACCOUNT_CREATED])
            ->getQuery()
            ->getResult()
            ;
    }
}
