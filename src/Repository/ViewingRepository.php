<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Viewing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Viewing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viewing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viewing[]    findAll()
 * @method Viewing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViewingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Viewing::class);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getViewingWorkspaces(User $user)
    {
        return $this->createQueryBuilder('v')
            ->where('v.user = :user')
            ->andWhere('v.status IN (:statuses)')
            ->setParameters([
                'user'=> $user,
                'statuses' => [Viewing::ACCEPTED]
            ])
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function getViewingsForCronTask(\DateTime $date)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.status = :accepted')
            ->andWhere('v.startTime LIKE :start')
            ->setParameters([
                'accepted' => Viewing::ACCEPTED,
                'start' => $date->format("Y-m-d H:i") . "%",
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function getViewingsByDateForCronTask(\DateTime $date)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.status = :accepted')
            ->andWhere('v.startTime LIKE :start')
            ->setParameters([
                'accepted' => Viewing::ACCEPTED,
                'start' => $date->format("Y-m-d") . "%",
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @param User $user
     * @return mixed
     */
    public function getAllViewingsByDateForCronTask(\DateTime $date, User $user)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.createdAt >= :start')
            ->andWhere('v.user = :user')
            ->setParameters([
                'start' => $date->format("Y-m-d") . "%",
                'user'=> $user
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function userStatusChecker(\DateTime $date)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.status = :accepted')
            ->andWhere('v.startTime LIKE :start')
            ->setParameters([
                'accepted' => Viewing::ACCEPTED,
                'start' => $date->format("Y-m-d") . "%",
            ])
        ;

        return $query->getQuery()->getResult();
    }
}
