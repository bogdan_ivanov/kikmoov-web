<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\User;
use App\Entity\WorkSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WorkSpace|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkSpace|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkSpace[]    findAll()
 * @method WorkSpace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkSpaceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WorkSpace::class);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search(array $params) : array
    {
        $queryParameters = [];

        $query = $this->createQueryBuilder('w')
            ->innerJoin('w.location', 'l')
            ->where('w.status = :active')
        ;

        $queryParameters['active'] = WorkSpace::ACTIVE;

        if (!empty($params['type'])) {
            if (is_array($params['type'])) {
                $query->andWhere('w.type IN (:type)');
            } else {
                $query->andWhere('w.type = :type');
            }

            $queryParameters['type'] = $params['type'];
        }

        //if search are going by address or area or station get results around by radius too
        if (!empty($params['address'])) {
            $query->andWhere('LOWER(l.address) LIKE :address OR 6371 * acos( cos( radians(:origLat) ) * cos( radians( l.latitude ) ) 
* cos( radians( l.longitude ) - radians(:origLon) ) + sin( radians(:origLat) ) * sin(radians(l.latitude)) )  < :radius');

            //into kilometres
            $queryParameters['radius'] = $params['searchRadius'];

            $queryParameters['origLat'] = $params['coordinates']['latitude'];
            $queryParameters['origLon'] = $params['coordinates']['longitude'];
            $queryParameters['address'] = '%' . trim(strtolower($params['address'])) . '%';
        }

        if (!empty($params['area'])) {
            $query->innerJoin('l.area', 'a');
            $query->andWhere('LOWER(a.name) LIKE :area OR 6371 * acos( cos( radians(:origLat) ) * cos( radians( l.latitude ) ) 
* cos( radians( l.longitude ) - radians(:origLon) ) + sin( radians(:origLat) ) * sin(radians(l.latitude)) )  < :radius');

            $queryParameters['area'] = '%' . trim(strtolower($params['area'])) . '%';
            //into kilometres
            $queryParameters['radius'] = $params['searchRadius'];

            $queryParameters['origLat'] = $params['coordinates']['latitude'];
            $queryParameters['origLon'] = $params['coordinates']['longitude'];
        }

        if (!empty($params['station'])) {
            $query->andWhere('LOWER(l.nearby) LIKE :station OR 6371 * acos( cos( radians(:origLat) ) * cos( radians( l.latitude ) ) 
* cos( radians( l.longitude ) - radians(:origLon) ) + sin( radians(:origLat) ) * sin(radians(l.latitude)) )  < :radius');

            $queryParameters['station'] = '%' . trim(strtolower($params['station'])) . '%';
            //into kilometres
            $queryParameters['radius'] = $params['searchRadius'];

            $queryParameters['origLat'] = $params['coordinates']['latitude'];
            $queryParameters['origLon'] = $params['coordinates']['longitude'];
        }

        if (!empty($params['borough'])) {
            $query->innerJoin('l.area', 'ar');
            $query->innerJoin('ar.boroughs', 'b');
            $query->andWhere('LOWER(b.name) LIKE :borough');

            $queryParameters['borough'] = '%' . trim(strtolower($params['borough'])) . '%';
        }

        if (!empty($params['capacity'])) {
            $query->andWhere('w.capacity >= :capacity');
            $query->orderBy('w.capacity', 'ASC');
            $queryParameters['capacity'] = intval($params['capacity']);
        }

        if (!empty($params['minPrice'])) {
            $query->andWhere('w.price >= :minPrice');
            $queryParameters['minPrice'] = intval($params['minPrice']);
        }

        if (!empty($params['maxPrice'])) {
            $query->andWhere('w.price <= :maxPrice');
            $queryParameters['maxPrice'] = intval($params['maxPrice']);
        }

        if (!empty($params['availableFrom'])) {
            //25.01.2019 01:00:00 = 25.01.2019 00:00:00 -
            $query->andWhere('w.availableFrom LIKE :availableFrom OR w.availableFrom is NULL OR w.availableFrom <= :availableFromDate');
            $queryParameters['availableFrom'] = (new \DateTime())->setTimestamp(intval($params['availableFrom']))->format("Y-m-d") . '%';
            $queryParameters['availableFromDate'] = (new \DateTime())->setTimestamp(intval($params['availableFrom']))->format("Y-m-d");
        }

        if (empty($params['price']) && empty($params['date']) && empty($params['capacity'])) {
            $query->orderBy('w.sort', 'ASC');
        }

        if (!empty($params['price'])) {
            if ($params['price'] === 'highest') {
                $query->addOrderBy("w.price", 'DESC');
            } else if ($params['price'] === 'lowest') {
                $query->addOrderBy("w.price", 'ASC');
            }
        }

        if (!empty($params['date'])) {
            if ($params['date'] === 'newest') {
                $query->addOrderBy("w.createdAt", 'DESC');
            } else if ($params['date'] === 'oldest') {
                $query->addOrderBy("w.createdAt", 'ASC');
            }
        }

//        $start = isset($params['start']) ? intval($params['start']) : 0;
//        $limit = isset($params['limit']) ? intval($params['limit']) : 30;
//
//        $query
//            ->setMaxResults($limit)
//            ->setFirstResult($start)
//        ;

        return $query->setParameters($queryParameters)->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getPriceRange() : array
    {
        return $this->createQueryBuilder('w')
            ->select('MIN(w.price) AS min, MAX(w.price) AS max')
            ->where('w.status = :status')
            ->setParameter('status', WorkSpace::ACTIVE)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @return array
     */
    public function getTopBookedWorkspaces()
    {
        $query = $this->createQueryBuilder('w')
            ->select('w.id, COUNT(b.id) as qty')
            ->innerJoin('w.bookings', 'b')
            ->innerJoin('w.location', 'l')
            ->where('w.status = :active')
            ->andWhere('b.status = :accepted')
            ->orWhere('b.status = :paymentReceived')
            ->andWhere('w.availableFrom <= :availableFrom')
            ->orWhere('w.availableFrom IS NULL')
            ->groupBy('w.id')
            ->orderBy('qty', 'DESC')
            ->setMaxResults(10)
            ->setParameters([
                'active' =>  WorkSpace::ACTIVE,
                'accepted' => Booking::ACCEPTED,
                'paymentReceived' => Booking::PAYMENT_RECEIVED,
                'availableFrom' => (new \DateTime())->format("Y-m-d")
            ])
        ;

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @return array
     */
    public function getTopPicksWorkspaces()
    {
        $query = $this->createQueryBuilder('w')
            ->select('w.id')
            ->where('w.status = :active')
            ->andWhere('w.availableFrom <= :availableFrom')
            ->orWhere('w.availableFrom IS NULL')
            ->andWhere('w.topPick = :topPick')
            ->groupBy('w.id')
            ->setMaxResults(10)
            ->setParameters([
                'active' =>  WorkSpace::ACTIVE,
                'availableFrom' => (new \DateTime())->format("Y-m-d"),
                'topPick' => true
            ])
        ;

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @return array
     */
    public function getIdWorkspaces() : array
    {
        $query = $this->createQueryBuilder('w')
            ->select('w.id');

        return $query->getQuery()->getArrayResult();
    }
}
