<?php

namespace App\Security;

use App\Controller\DataResponse;
use App\Entity\User;
use App\Managers\ApiClientManager;
use App\Notifications\FrontUrls;
use App\Notifications\NotificationsEvent;
use App\Notifications\NotificationService;
use App\Validators\SignUpValidator;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use FOS\UserBundle\Doctrine\UserManager;
use OAuth2\OAuth2;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class SecurityService
 * @package App\Security
 */
class SecurityService
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var ApiClientManager
     */
    protected $apiClientManager;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @var SignUpValidator
     */
    private $signUpValidator;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var FrontUrls
     */
    protected $frontUrls;

    /**
     * @var OAuth2
     */
    private $OAuth2;

    /**
     * SecurityService constructor.
     * @param UserManager $userManager
     * @param ApiClientManager $apiClientManager
     * @param EncoderFactoryInterface $encoderFactory
     * @param SignUpValidator $signUpValidator
     * @param TokenGeneratorInterface $tokenGenerator
     * @param NotificationService $notificationService
     * @param FrontUrls $frontUrls
     * @param OAuth2 $OAuth2
     */
    public function __construct(
        UserManager $userManager,
        ApiClientManager $apiClientManager,
        EncoderFactoryInterface $encoderFactory,
        SignUpValidator $signUpValidator,
        TokenGeneratorInterface $tokenGenerator,
        NotificationService $notificationService,
        FrontUrls $frontUrls,
        OAuth2 $OAuth2
    )
    {
        $this->userManager = $userManager;
        $this->apiClientManager = $apiClientManager;
        $this->encoderFactory = $encoderFactory;
        $this->signUpValidator = $signUpValidator;
        $this->tokenGenerator = $tokenGenerator;
        $this->notificationService = $notificationService;
        $this->frontUrls = $frontUrls;
        $this->OAuth2 = $OAuth2;
    }

    /**
     * @param string $email
     * @param string $password
     * @param bool $mobileApp
     * @return DataResponse
     */
    public function login(string $email, string $password, ?bool $mobileApp): DataResponse
    {
        $user = $this->userManager->findUserByUsernameOrEmail($email);

        if ($user === null) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => sprintf('User with email %s does not exist', $email)]
            );
        }

        if (!is_null($mobileApp)) {
            if ($user->hasRole(User::ROLE_SELLER)) {
                return new DataResponse(
                    Response::HTTP_FORBIDDEN,
                    ['message' => 'This email is not linked to a Find a Space account. If you wish to find a space, please create an account with another email address.']
                );
            }
        }

        if (!$user->isEnabled()) {
            return new DataResponse(
                Response::HTTP_FORBIDDEN,
                ['message' => 'Please, activate your account or contact the admin']
            );
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $salt = $user->getSalt();

        if (!$encoder->isPasswordValid($user->getPassword(), $password, $salt)) {
            return new DataResponse(Response::HTTP_UNAUTHORIZED, ['message' => 'Wrong email or password']);
        }

        $apiKeys = $this->apiClientManager->getApiAccess($user);

        $apiKeys['accountType'] = $user->hasRole(User::ROLE_SELLER) ? User::SELLER : User::BUYER;

        return new DataResponse(Response::HTTP_OK, $apiKeys);
    }

    /**
     * @param $email
     * @return array
     */
    public function userExist($email): array
    {
        $user = $this->userManager->findUserByUsernameOrEmail($email);

        if ($user === null) {
            return [
                false,
                ['message' => sprintf('User with email %s does not exist', $email)]
            ];
        }

        return [
            true,
            ['user' => $user]
        ];
    }

    /**
     * @param array $userData
     * @param bool $fromMobile
     * @return DataResponse
     */
    public function signUpSeller(array $userData, $fromMobile = false) : DataResponse
    {
        /** @var User $user */
        $user = $this->userManager->createUser();

        $errors = $this->signUpValidator->sellerSignUpValidator($user, $userData);

        if (!empty($errors)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['errors' => $errors]
            );
        }

        [$ok, $userFromDb] = $this->userExist($userData['email']);

        if ($ok) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => sprintf('User with email %s already exist', $userData['email'])]
            );
        }

        $user->addRole(User::ROLE_SELLER);
        $user->setPlainPassword($userData['password']);
        $user->setEnabled(false);
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $user->setCreatedFromApp($fromMobile);

        $this->userManager->updateUser($user);

        $this->notificationService->sendEmailToUser(
            $user,
            NotificationsEvent::ACCOUNT_CREATED_SELLER,
            [
                'link' => $this->frontUrls->generateUrl(sprintf(FrontUrls::CONFIRM_REGISTRATION, $user->getConfirmationToken())),
                'seller' => $user
            ]
        );

        return new DataResponse(Response::HTTP_CREATED, ['message' => 'User successfully created']);
    }

    /**
     * @param array $userData
     * @param bool $fromMobile
     * @return DataResponse
     */
    public function signUpBuyer(array $userData, $fromMobile = false): DataResponse
    {
        /** @var User $user */
        $user = $this->userManager->createUser();

        $errors = $this->signUpValidator->buyerSignUpValidator($user, $userData);

        if (!empty($errors)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['errors' => $errors]
            );
        }

        [$ok, $userFromDb] = $this->userExist($userData['email']);

        if ($ok) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => sprintf('User with email %s already exist', $userData['email'])]
            );
        }

        $user->setPlainPassword($userData['password']);
        $user->setEnabled(false);
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $user->setCreatedFromApp($fromMobile);

        $this->userManager->updateUser($user);

        $this->notificationService->sendEmailToUser(
            $user,
            NotificationsEvent::ACCOUNT_CREATED_BUYER,
            [
                'link' => $this->frontUrls->generateUrl(sprintf(FrontUrls::CONFIRM_REGISTRATION, $user->getConfirmationToken())),
                'buyer' => $user
            ]
        );

        return new DataResponse(Response::HTTP_CREATED, ['message' => 'User successfully created']);
    }

    /**
     * @param string $token
     * @return DataResponse
     */
    public function confirmAccount(string $token) : DataResponse
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);

        if (is_null($user)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'User not found']
            );
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $this->userManager->updateUser($user);

        $client = $this->apiClientManager->createClientManually($user);

        $apiKeys = $this->OAuth2->createAccessToken($client, $user);

        $apiKeys['message'] = 'Account has been successfully verified';
        $apiKeys['accountType'] = $user->hasRole(User::ROLE_SELLER) ? User::SELLER : User::BUYER;

        return new DataResponse(
            Response::HTTP_OK,
            $apiKeys
        );
    }

    /**
     * @param string $email
     * @return DataResponse
     */
    public function forgotPassword(string $email) : DataResponse
    {
        $user = $this->userManager->findUserByUsernameOrEmail($email);

        if (is_null($user)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'User not found']
            );
        }

        //each time we need to generate a new one
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $user->setPasswordRequestedAt(new \DateTime());
        $this->userManager->updateUser($user);

        $this->notificationService->sendEmailToUser(
            $user,
            NotificationsEvent::FORGOT_PASSWORD,
            [
                'link' => $this->frontUrls->generateUrl(sprintf(FrontUrls::CONFIRM_FORGOT_PASSWORD, $user->getConfirmationToken())),
                'user' => $user
            ]
        );

        return new DataResponse(
            Response::HTTP_OK,
            ['message' => 'Reset link was send to your email']
        );
    }

    /**
     * @param string $token
     * @return DataResponse
     */
    public function checkForgotPasswordToken(string $token) : DataResponse
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);

        if (is_null($user)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Invalid token. Please, try to reset you password']
            );
        }

        return new DataResponse();
    }

    /**
     * @param string $token
     * @param string $newPassword
     * @return DataResponse
     */
    public function updatePassword(string $token, string $newPassword) : DataResponse
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);

        if (is_null($user)) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['message' => 'Invalid token. Please, try to reset you password']
            );
        }

        if (!empty($errors = $this->signUpValidator->validatePassword($newPassword))) {
            return new DataResponse(
                Response::HTTP_BAD_REQUEST,
                ['errors' => $errors]
            );
        }

        $user->setPlainPassword($newPassword);
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);
        $user->setEnabled(true);
        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Password has been successfully changed'
        ]);
    }

    /**
     * @param User $user
     * @param string $oldPassword
     * @param string $newPassword
     * @return DataResponse
     */
    public function changePassword(User $user, string $oldPassword, string $newPassword) : DataResponse
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $salt = $user->getSalt();

        if (!$encoder->isPasswordValid($user->getPassword(), $oldPassword, $salt)) {
            return new DataResponse(Response::HTTP_UNAUTHORIZED, ['message' => 'Wrong password']);
        }

        $user->setPlainPassword($newPassword);

        $this->userManager->updateUser($user);

        return new DataResponse(Response::HTTP_OK, [
            'message' => 'Password has been successfully changed'
        ]);
    }
}
