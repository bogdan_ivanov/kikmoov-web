<?php

namespace App\Security;

/**
 * Class SocialPasswordEncoder
 * @package App\Provider
 */
class SocialPasswordEncoder
{
    /**
     * @var string
     */
    private $secret;

    /**
     * SocialPasswordEncoder constructor.
     * @param string $secret
     */
    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encode(string $password) : string
    {
        return hash_hmac('ripemd256', $password, $this->secret);
    }

    /**
     * @param string $password
     * @param string $correct
     * @return bool
     */
    public function compare(string $password, string $correct): bool
    {
        return hash_equals($password, $this->encode($correct));
    }
}
