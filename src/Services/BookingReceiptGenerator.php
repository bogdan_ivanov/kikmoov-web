<?php

namespace App\Services;

use App\Entity\Booking;
use App\Entity\Location;
use App\Entity\WorkSpace;
use Knp\Snappy\Pdf;
use Sonata\UserBundle\Model\UserInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Twig\Error\Error;

/**
 * Class ReceiptGenerator
 * @package App\Services
 */
class BookingReceiptGenerator
{
    /**
     * @var string
     */
    private $receiptsFolder;

    /**
     * @var Pdf
     */
    private $pdf;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * BookingReceiptGenerator constructor.
     *
     * @param TwigEngine $twig
     * @param string $projectDir
     * @param string $receiptsFolder
     */
    public function __construct(TwigEngine $twig, string $projectDir, string $receiptsFolder)
    {
        $this->pdf = new Pdf("{$projectDir}/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64");
        $this->twig = $twig;
        $this->receiptsFolder = $receiptsFolder;
    }


    /**
     * Generated and return binary pdf file for Booking receipt
     *
     * @param Booking $booking
     *
     * @return string
     *
     * @throws Error
     */
    public function generateBinaryPdf(Booking $booking): string
    {
        $content = $this->twig->render($this->getTemplate(), $this->buildReceiptData($booking));

        return  $this->pdf->getOutputFromHtml($content);
    }

    /**
     * Generated and save receipt pdf file
     *
     * @param Booking $booking
     * @param string $fileName
     *
     * @return string
     *
     * @throws Error
     */
    public function generatePdf(Booking $booking, string $fileName): string
    {
        $pathToFile = "{$this->receiptsFolder}/{$fileName}.pdf";
        $content = $this->twig->render($this->getTemplate(), $this->buildReceiptData($booking));

        $this->pdf->generateFromHtml($content,  $pathToFile);

        return $pathToFile;
    }

    /**
     * Method collects data to create receipt pdf file
     *
     * @param Booking $booking
     *
     * @return array
     */
    private function buildReceiptData(Booking $booking): array
    {
        /**
         * @var UserInterface
         */
        $buyer = $booking->getUser();

        /**
         * @var WorkSpace
         */
        $workSpace = $booking->getWorkSpace();

        /**
         * @var Location
         */
        $location = $workSpace->getLocation();

        return [
            'bookingId' => $booking->getId(),
            'date' => $booking->getCreatedAt()->format('d/m/Y'),
            'amount' => number_format((float) $booking->getAmount(), 2, '.', ''),
            'total' => number_format((float) $booking->getAmount() + $booking->getAmountVAT(), 2, '.', ''),
            'vat' => number_format((float) $booking->getAmountVAT(), 2, '.', ''),
            'buyerName' => $buyer->getFullname(),
            'buyerAddress' => "{$booking->getBillingAddress()}, {$booking->getCity()}, {$booking->getPostCode()}",
            'location' => $location,
            'workspace' => $workSpace,
        ];
    }

    /**
     * Return path for receipt pdf template
     *
     * @return string
     */
    private function getTemplate(): string
    {
        return 'emails/booking/receipt.html.twig';
    }
}