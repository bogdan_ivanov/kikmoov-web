<?php

namespace App\Services;

use App\Entity\Booking;
use App\Entity\Viewing;
use App\Entity\WorkSpace;
use App\Managers\ViewingApiManager;
use Eluceo\iCal\Component\Event;
use Eluceo\iCal\Property\Event\Geo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class CalendarFileGenerator
 * @package App\Services
 */
class CalendarFileGenerator
{

    /**
     * @var string
     */
    private $projectURL;

    /**
     * @var ViewingApiManager
     */
    private $viewingApiManager;

    /**
     * CalendarFileGenerator constructor.
     * @param ViewingApiManager $viewingApiManager
     * @param string $projectURL
     */
    public function __construct
    (
        string $projectURL,
        ViewingApiManager $viewingApiManager
    )
    {
        $this->viewingApiManager = $viewingApiManager;
        $this->projectURL = $projectURL;
    }

    /**
     * @param WorkSpace $workSpace
     * @param $entity
     * @return bool|Response
     */
    public function fileGenerator(WorkSpace $workSpace, $entity)
    {
        if (!($entity instanceof Booking || $entity instanceof  Viewing)) {
            return $this->fileGeneratorError(
                ['message' => 'Record was not found']
            );
        }

        $vCalendar = new \Eluceo\iCal\Component\Calendar($this->projectURL);

        $tz  = 'Europe/London';
        $dtz = new \DateTimeZone($tz);
        date_default_timezone_set($tz);

        $DtStart = new \DateTime($entity->getStartTime()->format('Y-m-d H:i'), $dtz);
        $DtEnd = new \DateTime($entity->getEndTime()->format('Y-m-d H:i'), $dtz);
        $vTimezoneRuleDst = new \Eluceo\iCal\Component\TimezoneRule(\Eluceo\iCal\Component\TimezoneRule::TYPE_DAYLIGHT);
        $vTimezoneRuleDst->setTzName('BST');
        $vTimezoneRuleDst->setDtStart($DtStart);
        $vTimezoneRuleDst->setDtStart($DtEnd);
        $vTimezoneRuleDst->setTzOffsetFrom('+0100');
        $vTimezoneRuleDst->setTzOffsetTo('+0200');
        $dstRecurrenceRule = new \Eluceo\iCal\Property\Event\RecurrenceRule();
        $dstRecurrenceRule->setFreq(\Eluceo\iCal\Property\Event\RecurrenceRule::FREQ_YEARLY);
        $dstRecurrenceRule->setByMonth(3);
        $dstRecurrenceRule->setByDay('-1SU');
        $vTimezoneRuleDst->setRecurrenceRule($dstRecurrenceRule);

        $vTimezoneRuleStd = new \Eluceo\iCal\Component\TimezoneRule(\Eluceo\iCal\Component\TimezoneRule::TYPE_STANDARD);
        $vTimezoneRuleStd->setTzName('GMT');
        $vTimezoneRuleStd->setDtStart($DtStart);
        $vTimezoneRuleStd->setDtStart($DtEnd);
        $vTimezoneRuleStd->setTzOffsetFrom('+0200');
        $vTimezoneRuleStd->setTzOffsetTo('+0100');
        $stdRecurrenceRule = new \Eluceo\iCal\Property\Event\RecurrenceRule();
        $stdRecurrenceRule->setFreq(\Eluceo\iCal\Property\Event\RecurrenceRule::FREQ_YEARLY);
        $stdRecurrenceRule->setByMonth(10);
        $stdRecurrenceRule->setByDay('-1SU');
        $vTimezoneRuleStd->setRecurrenceRule($stdRecurrenceRule);

        $vTimezone = new \Eluceo\iCal\Component\Timezone($tz);
        $vTimezone->addComponent($vTimezoneRuleStd);
        $vTimezone->addComponent($vTimezoneRuleDst);

        $vCalendar->setTimezone($vTimezone);

        $vEvent = new \Eluceo\iCal\Component\Event();
        $vEvent->setDtStart($DtStart);
        $vEvent->setDtEnd($DtEnd);
        $vEvent->setStatus(Event::STATUS_CONFIRMED);
        $vEvent->setNoTime(false);
        $vEvent->setLocation($workSpace->getLocation()->getAddress());
        $vEvent->setGeoLocation(new Geo($workSpace->getLocation()->getLatitude(), $workSpace->getLocation()->getLongitude()));
        $vEvent->setSummary($this->makeSummary($workSpace, $entity));
        $vEvent->setDescriptionHTML($this->makeDescription($entity));
        $vEvent->setDescription($this->makeDescription($entity));

        $vEvent->setUseTimezone(true);

        $vCalendar->addComponent($vEvent);

        $filename = $this->generateFileName($workSpace);

        $file = new Response($vCalendar->render());

        $disposition = $file->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename . '.ics'
        );

        $file->headers->set('Content-Disposition', $disposition);
        $file->headers->set('Content-Type', 'text/calendar; charset=utf-8');

        return $file;
    }

    /**
     * @param array $data
     * @return Response
     */
    public function fileGeneratorError(array $data)
    {
        $vCalendar = new \Eluceo\iCal\Component\Calendar($this->projectURL);

        $vEvent = new \Eluceo\iCal\Component\Event();
        $vEvent->setSummary('Sorry, some problem is happened');
        $vEvent->setDescription($data['message'] . '. Please contact us');

        $vCalendar->addComponent($vEvent);

        $file = new Response($vCalendar->render());

        $disposition = $file->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'kikmoov.ics'
        );

        $file->headers->set('Content-Disposition', $disposition);
        $file->headers->set('Content-Type', 'text/calendar; charset=utf-8');

        return $file;
    }

    /**
     * @param $entity
     * @return string
     */
    private function makeDescription($entity) : string
    {
        if ($entity instanceof Booking) {
            /**  @var Booking $entity */
            return sprintf('
                <br>Booking details:</br>
                <br>%s</br>
                <br>%s</br>
            ',
                $entity->getWorkSpace()->getLocation()->getName(),
                $entity->getWorkSpace()->getLocation()->getAddressLabel()
            );
        } else {

            /**  @var Viewing $entity */

            return sprintf('
                <br>Viewing details:</br>
                <br>%s</br>
                <br>%s</br>
            ',
                $entity->getWorkSpace()->getLocation()->getName(),
                $entity->getWorkSpace()->getLocation()->getAddressLabel()
            );
        }
    }

    /**
     * @param WorkSpace $workSpace
     * @return string
     */
    private function generateFileName(WorkSpace $workSpace) : string
    {
        return str_replace('/', '', stripslashes($workSpace->getLocation()->getName() . $workSpace->getId()));
    }

    /**
     * @param WorkSpace $workSpace
     * @param $entity
     * @return string
     */
    private function makeSummary(WorkSpace $workSpace, $entity) : string
    {
        if ($entity instanceof Booking) {
            return 'Booking for workspace ' . $workSpace->getLocation()->getName();
        } else {
            return 'Viewing for workspace ' . $workSpace->getLocation()->getName();
        }
    }


}