<?php

namespace App\Services;

use App\Entity\CronJob;
use App\Repository\CronJobRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class CronJobService
 * @package App\Services
 */
class CronJobService
{
    /**
     * @var CronJobRepository
     */
    private $repository;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->repository = $entityManager->getRepository(CronJob::class);
    }

    public function checkEnd(string $jobName): bool
    {
        /** @var CronJob $process */
        $process = $this->getJob($jobName);

        return $process->getRun();
    }

    public function setJobStatus(string $jobName, bool $status)
    {
        $process = $this->getJob($jobName);
        $process->setRun($status);
        $process->setStartTime(new \DateTime());
        $this->repository->save($process);
    }

    private function getJob(string $name): CronJob
    {
        $cronJob = $this->repository->findOneBy([
            'name' => $name,
        ]);

        return $cronJob ? $cronJob : new CronJob($name);
    }

    public function getStartTime(string $name): ?\DateTime
    {
        $cronJob = $this->repository->findOneBy([
            'name' => $name,
        ]);

        if ($cronJob) {
            return $cronJob->getStartTime();
        }

        return null;
    }
}