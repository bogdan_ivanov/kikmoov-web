<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class GoogleMapService
 * @package App\Services
 */
class GoogleMapService
{

    /**
     * @var string
     */
    public $googleApiKey;

    const GOOGLE_MAPS_GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s';

    /**
     * GoogleMapService constructor.
     * @param string $googleApiKey
     */
    public function __construct
    (
        string $googleApiKey
    )
    {
        $this->googleApiKey = $googleApiKey;
    }

    /**
     * @param string $place
     * @return array
     */
    public function getCoordinatesByPlace(string $place) : array
    {
        //adding 'London' to be sure that a place will be searched in the London
        $place = urlencode($place . ', London');

        $coordinates = [];
        $client = new Client();

        try {
            $places = $client->get(
                sprintf(self::GOOGLE_MAPS_GEOCODING,
                    $place,
                    $this->googleApiKey
                )
            );

            $response = json_decode($places->getBody(), true);

            if (!isset($response['results'])) {
                return $coordinates;
            }
        } catch (\Exception $exception) {
            return $coordinates;
        }

        if (!empty($response['results'])) {
            $coordinates['latitude'] = $response['results'][0]['geometry']['location']['lat'];
            $coordinates['longitude'] = $response['results'][0]['geometry']['location']['lng'];
        }

        return $coordinates;
    }

}