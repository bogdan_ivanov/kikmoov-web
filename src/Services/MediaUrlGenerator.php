<?php

namespace App\Services;

use App\Entity\Media;
use App\Provider\WorkspaceMediaProvider;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class MediaUrlGenerator
 * @package App\Services
 */
class MediaUrlGenerator
{
    /**
     * @var Container
     */
    private $container;

    /**
     * MediaUrlGenerator constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return string
     */
    protected function getProjectUrlAdmin() : string
    {
        return $this->container->getParameter('project_url_admin');
    }

    /**
     * To save the time and avoid any addition checking for media, just mark as optional param
     *
     * @param Media|null $media
     * @param bool $absolute
     * @param string $formatType
     * @return null|string
     */
    public function generateUrl(
        ?Media $media,
        $absolute = false,
        ?string $formatType = MediaProviderInterface::FORMAT_REFERENCE
    ) : ?string
    {

        if (is_null($media)) {
            return null;
        }

        try {
            $provider = $this->container->get($media->getProviderName());
        } catch (\Exception $exception) {
            return null;
        }

        $prefix = ($absolute) ? $this->getProjectUrlAdmin() : '';

        if ($formatType === WorkspaceMediaProvider::PREVIEW) {
            $formatType = $provider->getFormatName($media, $formatType);
        }

        return $prefix . $provider->generatePublicUrl($media , $formatType);
    }

    /**
     * @param Media|null $media
     * @return null|string
     */
    public function getGeneratePath(?Media $media) : ?string
    {
        if (is_null($media)) {
            return null;
        }

        try {
            $provider = $this->container->get($media->getProviderName());
        } catch (\Exception $exception) {
            return null;
        }

        return $provider->generatePath($media);
    }
}
