<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class PushService
 * @package App\Services
 */
class PushService
{

    /**
     * @var string
     */
    private $pushUrl;

    /**
     * @var Client
     */
    private $client;

    /**
     * PushService constructor.
     * @param $pushUrl
     */
    public function __construct($pushUrl)
    {
        $this->pushUrl = $pushUrl;

        $this->client = new Client([
            "verify" => false,
            "http_errors" => false
        ]);
    }

    /**
     * @param string $title
     * @param string $body
     * @param array $devices
     * @return int
     */
    public function sendMessage(string $title, string $body, array $devices) : int
    {
        $res = $this->client->post(
            $this->pushUrl,
            [
                'json' => [
                    'message' => [
                        'title' => $title,
                        'body' => $body
                    ],
                    'devices' => $devices
                ]
            ]
        );

        return $res->getStatusCode();
    }
}