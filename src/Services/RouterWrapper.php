<?php

namespace App\Services;

use Symfony\Component\Routing\Router;

/**
 * Class RouterWrapper
 * @package App\Services
 */
class RouterWrapper
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $adminUrl;

    /**
     * @var string
     */
    private $frontUrl;

    /**
     * RouterWrapper constructor.
     * @param Router $router
     * @param string $adminUrl
     * @param string $frontUrl
     */
    public function __construct(Router $router, string $adminUrl, string $frontUrl)
    {
        $this->router = $router;
        $this->adminUrl = $adminUrl;
        $this->frontUrl = $frontUrl;
    }

    /**
     * @return Router
     */
    public function getRouter() : Router
    {
        return $this->router;
    }

    /**
     * @param string $name
     * @param array $parameters
     * @return string
     */
    public function generateAdminLink(string $name, array $parameters)
    {
        return $this->adminUrl . $this->router->generate($name,  $parameters);
    }

    /**
     * @param string $path
     * @return string
     */
    public function generateFrontLink(string $path)
    {
        return $this->frontUrl . $path;
    }
}
