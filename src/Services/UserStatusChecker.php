<?php

namespace App\Services;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserStatusChecker
 * @package App\Services
 */
class UserStatusChecker
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserStatusChecker constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @param string $status
     */
    public function changeStatus(User $user, string $status)
    {
        if (is_null($user)) {
            return;
        }

        if ($status > $user->getStatus()) {
            $user->setStatus($status);
            $this->entityManager->flush();
        }
    }

    /**
     * @param User $user
     * @param string $status
     * @return bool
     */
    public function forceChangeStatus(User $user, string $status) : bool
    {
        if ($user->getStatus() === $status) {
            return false;
        }

        $user->setStatus($status);
        $this->entityManager->flush();

        return true;
    }
}
