<?php

namespace App\Storage;

use App\Security\SocialPasswordEncoder;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use FOS\OAuthServerBundle\Storage\OAuthStorage as BaseOAuthStorage;
use OAuth2\Model\IOAuth2Client;
use FOS\OAuthServerBundle\Model\ClientInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class OAuthStorage
 * @package App\Storage
 */
class OAuthStorage extends BaseOAuthStorage
{
    private $socialPasswordEncoder;

    /**
     * OAuthStorage constructor.
     * @param ClientManagerInterface $clientManager
     * @param AccessTokenManagerInterface $accessTokenManager
     * @param RefreshTokenManagerInterface $refreshTokenManager
     * @param AuthCodeManagerInterface $authCodeManager
     * @param SocialPasswordEncoder $socialPasswordEncoder
     * @param null|UserProviderInterface $userProvider
     * @param null|EncoderFactoryInterface $encoderFactory
     */
    public function __construct(
        ClientManagerInterface $clientManager,
        AccessTokenManagerInterface $accessTokenManager,
        RefreshTokenManagerInterface $refreshTokenManager,
        AuthCodeManagerInterface $authCodeManager,
        SocialPasswordEncoder $socialPasswordEncoder,
        ?UserProviderInterface $userProvider = null,
        ?EncoderFactoryInterface $encoderFactory = null
    ) {
        parent::__construct($clientManager, $accessTokenManager, $refreshTokenManager, $authCodeManager, $userProvider, $encoderFactory);

        $this->socialPasswordEncoder = $socialPasswordEncoder;
    }

    /**
     * @param IOAuth2Client $client
     * @param $username
     * @param $password
     * @return array|bool
     */
    public function checkUserCredentials(IOAuth2Client $client, $username, $password)
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        try {
            $user = $this->userProvider->loadUserByUsername($username);
        } catch (AuthenticationException $e) {
            return false;
        }

        if (!$user->isEnabled()) {
            return false;
        }

        if ($user->getSocialPassword() !== null) {
            if ($this->socialPasswordEncoder->compare($password, $user->getSocialPassword())) {
                return [
                    'data' => $user,
                ];
            }
        }

        $encoder = $this->encoderFactory->getEncoder($user);

        if ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            return [
                'data' => $user,
            ];
        }

        return false;
    }
}