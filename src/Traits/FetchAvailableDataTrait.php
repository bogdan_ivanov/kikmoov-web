<?php

namespace App\Traits;

use App\Services\CronJobService;

/**
 * Trait FetchAvailableDataTrait
 * @package App\Traits
 */
trait FetchAvailableDataTrait
{
    protected function getCronJobService(): CronJobService
    {
        return $this->getContainer()->get(CronJobService::class);
    }

    protected function isProcessRunning(): bool
    {
        return $this->getCronJobService()->checkEnd($this->getName());
    }

    protected function toggleProcessStatus(bool $status)
    {
        $this->getCronJobService()->setJobStatus($this->getName(), $status);
    }

    protected function getProcessStartTime(): int
    {
        if ($res = $this->getCronJobService()->getStartTime($this->getName())->getTimestamp()) {
            return $res;
        }

        return 0;
    }

    protected function checkPreviousProcess(): bool
    {
        $startDate = $this->getProcessStartTime();
        $nowDate = new \DateTime();
        $nowDate = $nowDate->getTimestamp();

        if ($startDate + 180 < $nowDate) {

            $this->toggleProcessStatus(false);

            return true;
        }

        return false;
    }
}