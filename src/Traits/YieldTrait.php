<?php

namespace App\Traits;

/**
 * Trait YieldTrait
 * @package App\Traits
 */
trait YieldTrait
{
    /**
     * @param $data
     * @return \Generator
     * @throws \Exception
     */
    protected function yieldCollection($data)
    {
        if (is_iterable($data)) {
            foreach ($data as $element) {
                yield $element;
            }
        } else {
            throw new \Exception('Argument must be iterable');
        }
    }
}
