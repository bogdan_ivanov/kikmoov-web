<?php

namespace App\Twig;

use App\Entity\WorkSpace;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AppExtension
 * @package App\Twig
 */
class AppExtension extends AbstractExtension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AppExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array|\Twig_Filter[]
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('price', array($this, 'formatPrice')),
        );
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('removeSwaggerExtraData', array($this, 'removeSwaggerExtraData')),
            new TwigFunction('projectUrl', array($this, 'projectUrl')),
            new TwigFunction('frontUrl', array($this, 'frontUrl')),
            new TwigFunction('checkWorkspaceType', array($this, 'checkWorkspaceType')),
        );
    }

    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return string
     */
    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$' . $price;

        return $price;
    }

    /**
     * @param array $data
     * @return array
     */
    public function removeSwaggerExtraData(array $data)
    {
        if (isset($data['spec']) && is_array($data['spec'])) {
            $spec = $data['spec'];

            if (isset($spec['paths']) && is_array($spec['paths']) && !empty($spec['paths'])) {
                $paths = $spec['paths'];

                if (isset($paths['/api/doc.json'])) {
                    unset($data['spec']['paths']['/api/doc.json']);
                }
            }
        }
        return $data;
    }

    /**
     * @param WorkSpace $workspace
     * @param string $person
     * @return string
     */
    public function checkWorkspaceType(WorkSpace $workspace, string $person)
    {
        if($workspace->getType() != 'desk')
        {
            return $workspace->getCapacity().' '.$person;
        }

        return '';
    }

    /**
     * @return string
     */
    public function projectUrl() : string
    {
        return $this->container->getParameter("project_url");
    }

    /**
     * @param string $path
     * @return string
     */
    public function frontUrl(string $path) : string
    {
        return $this->projectUrl() . $path;
    }
}
