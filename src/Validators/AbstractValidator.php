<?php

namespace App\Validators;

use App\Traits\YieldTrait;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractValidator
 * @package App\Validators
 */
abstract class AbstractValidator
{
    use YieldTrait;

    const CREATE_ACTION = 1;
    const UPDATE_ACTION = 2;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * AbstractValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return ValidatorInterface
     */
    protected function getValidator() : ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @param $field
     * @param $value
     * @param array $constraint
     * @param array $errors
     * @return bool
     */
    protected function validateProperty(string $field, $value, array $constraint, &$errors = []) : bool
    {
        $isValid = true;

        $violationList = $this->getValidator()->validate($value, $constraint);

        if ($violationList->count() > 0) {
            $isValid = false;
            /** @var ConstraintViolation $violation */
            foreach ($violationList as $violation) {
                $errors[] = [
                    'attribute' => $field,
                    'details' => $violation->getMessage()
                ];
            }
        }

        return $isValid;
    }

    /**
     * @param array $data
     * @param Collection $constraint
     * @return array
     */
    protected function validateCollection(array $data, Collection $constraint) : array
    {
        $violationList = $this->getValidator()->validate($data, $constraint);

        $errors = [];

        if ($violationList->count() > 0) {
            /** @var ConstraintViolation $violation */
            foreach ($violationList as $violation) {
                $field = preg_replace('/\[|\]/', "", $violation->getPropertyPath());
                $errors[] = [
                    'attribute' => $field,
                    'details' => $violation->getMessage()
                ];
            }
        }

        return $errors;
    }

    /**
     * A little bit of black magic
     *
     * @param object $entityInstance
     * @param array $data
     * @param array $validationRules
     * @param int $actionType
     * @param bool $checkGettersAndSetters
     * @return array
     */
    protected function magicValidator(
        object $entityInstance,
        array $data,
        array $validationRules,
        $actionType = self::CREATE_ACTION,
        bool $checkGettersAndSetters = true
    ) {
        $errors = [];

        //we need a copy of the data to optimize the cycle
        $copyData = $data;

        foreach ($validationRules as $fieldName => $condition) {
            $hasError = false;
            $fieldInList = false;

            $isRequired = (isset($condition['required']) && $condition['required']);
            $requiredFieldExists = false;

            foreach ($copyData as $field => $value) {
                if ($field !== $fieldName) {
                    continue;
                } else {
                    $fieldInList = true;
                }

                if ($value === '' || $value === null || empty($value) && !$isRequired) {
                    continue;
                }

                if (isset($condition['constraints']) && is_array($condition['constraints']) &&
                    !empty($condition['constraints'])
                ) {
                    $hasError = !$this->validateProperty($fieldName, $value, $condition['constraints'], $errors);
                }

                if ($isRequired) {
                    $requiredFieldExists = isset($data[$fieldName]);
                }

                unset($copyData[$fieldName]);
                break;
            }

            //if we need to create an entity, all fields described as required must be provided
            if ($isRequired && !$requiredFieldExists && $actionType === self::CREATE_ACTION) {
                $errors[] = [
                    'attribute' => $fieldName,
                    'details'   => 'This value should not be blank'
                ];
                continue;
            }

            //we don't know nothing about this field
            if (!$fieldInList) {
                continue;
            }

            //validation failed
            if ($hasError) {
                continue;
            }

            //if an array of errors was touched, it's no need to set other values
            if (!empty($errors)) {
                continue;
            }

            //sometimes we need to convert value manually or do something with it before setting
            if ($checkGettersAndSetters && (!isset($condition['checkGettersAndSetters']) ||
                    (isset($condition['checkGettersAndSetters']) && $condition['checkGettersAndSetters']))
            ) {
                $setter = 'set' . ucfirst($fieldName);
                $getter = 'get' . ucfirst($fieldName);

                if (!method_exists($entityInstance, $setter) && !method_exists($entityInstance, $getter)) {
                    continue;
                }

                $entityInstance->{$setter}($data[$fieldName]);
            }
        }

        return $errors;
    }
}
