<?php

namespace App\Validators;

use App\Entity\Booking;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\Luhn;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class BookingValidator
 * @package App\Validators
 */
class BookingValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function bookingRules(): array
    {
        $nowTimestamp = (new \DateTime())->getTimestamp();

        return [
            'startTime' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'endTime' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'phone' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'name' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'paymentToken' => [
                'required' => true,
                'constraints' => [new NotBlank()],
                'checkGettersAndSetters' => false
            ],
            'billingAddress' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'city' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'postCode' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ]
        ];
    }

    /**
     * @param Booking $booking
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function bookingValidator(Booking &$booking, array $data, $actionType = self::CREATE_ACTION)
    {
        return $this->magicValidator($booking, $data, self::bookingRules(), $actionType);
    }

    /**
     * @return array
     */
    protected static function bookingRequestRules(): array
    {
        $nowTimestamp = (new \DateTime())->setTime(0, 0, 0)->getTimestamp();

        return [
            'startTime' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'phone' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'name' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'information' => [
                'required' => true,
                'constraints' => [new NotBlank()],
            ],
            'tenantType' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Choice(['choices' => [Booking::FREELANCER, Booking::COMPANY]])],
            ],
            'duration' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Choice([
                        'choices' => [
                            Booking::DURATION_NOT_SURE,
                            Booking::DURATION_ONE_MONTH_OR_MORE,
                            Booking::DURATION_SIX_MONTH_OR_MORE,
                            Booking::DURATION_ONE_YEAR_OR_MORE,
                        ]
                    ])
                ],
            ],
            'units' => [
                'required' => false,
                new GreaterThanOrEqual([
                    'value' => 1,
                    'message' => 'You can book one or more units'
                ])
            ]
        ];
    }

    /**
     * @param Booking $booking
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function bookingRequestValidator(Booking &$booking, array $data, $actionType = self::CREATE_ACTION)
    {
        return $this->magicValidator($booking, $data, self::bookingRequestRules(), $actionType);
    }

    /**
     * @param array $emails
     * @param array $alreadyInvited
     * @return array
     */
    public function validateAttendees(array $emails, ?array $alreadyInvited) : array
    {
        $errorsList = [];

        foreach ($emails as $email) {
            if (!empty($alreadyInvited)) {
                if (in_array($email, $alreadyInvited)) {
                    $errorsList[] = [
                        'attribute' => "email",
                        'details' => sprintf('Email address already in list - %s', $email)
                    ];

                    continue;
                }
            }

            $errors = $this->getValidator()->validate($email, [new Email()]);

            if (0 !== count($errors)) {
                $errorsList[] = [
                    'attribute' => "email",
                    'details' => sprintf('Invalid email address - %s', $email)
                ];
            }
        }

        return $errorsList;
    }
}
