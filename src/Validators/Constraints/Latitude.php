<?php

namespace App\Validators\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Latitude extends Constraint
{
    public $message = 'Invalid format "{{ string }}". Should be ISO 6709 ex. +40.20361';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}
