<?php

namespace App\Validators\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Longitude extends Constraint
{
    public $message = 'Invalid format "{{ string }}". Should be ISO 6709 ex. -075.00417';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}
