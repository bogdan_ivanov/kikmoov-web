<?php

namespace App\Validators\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class TimeValidator
 * @package App\Validators\Constraints
 */
class TimeValidator extends ConstraintValidator
{
    const PATTERN = '/^(\d{2}):(\d{2})$/';

    /**
     * Checks whether a time is valid.
     *
     * @param int $hour   The hour
     * @param int $minute The minute
     *
     * @return bool Whether the time is valid
     *
     * @internal
     */
    public static function checkTime($hour, $minute)
    {
        return $hour >= 0 && $hour < 24 && $minute >= 0 && $minute < 60;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Time) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Time');
        }

        if (null === $value || '' === $value || $value instanceof \DateTimeInterface) {
            return;
        }

        if (!is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;

        if (!preg_match(static::PATTERN, $value, $matches)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(Time::INVALID_FORMAT_ERROR)
                ->addViolation();

            return;
        }

        if (!self::checkTime($matches[1], $matches[2])) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(Time::INVALID_TIME_ERROR)
                ->addViolation();
        }
    }
}
