<?php

namespace App\Validators;

use App\Entity\ContactUs;
use App\Entity\LeaseholdEnquiry;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class EnquiryValidator
 * @package App\Validators
 */
class EnquiryValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function contactUsValidator(): array
    {
        return [
            'name' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'message' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Length(['min' => 10])]
            ]
        ];
    }

    /**
     * @param ContactUs $contactUs
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function validateContactUs(ContactUs &$contactUs, array $data, $actionType = self::CREATE_ACTION): array
    {
        return $this->magicValidator($contactUs, $data, self::contactUsValidator(), $actionType);
    }

    /**
     * @return array
     */
    protected static function leaseholdEnquiryValidator(): array
    {
        return [
            'area' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Choice([
                        'choices' => [
                            LeaseholdEnquiry::NORTH,
                            LeaseholdEnquiry::EAST,
                            LeaseholdEnquiry::SOUTH,
                            LeaseholdEnquiry::WEST
                        ],
                        'multiple' => true
                    ])
                ]
            ],
            'numberOfPeople' => [
                'required' => true,
                'constraints' => new NotBlank(), new Type("integer"), new GreaterThanOrEqual(1)
            ],
            'fullName' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'companyName' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'phone' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ]
        ];
    }


    /**
     * @param LeaseholdEnquiry $leaseholdEnquiry
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function validateLeaseholdEnquiry(LeaseholdEnquiry &$leaseholdEnquiry, array $data, $actionType = self::CREATE_ACTION): array
    {
        return $this->magicValidator($leaseholdEnquiry, $data, self::leaseholdEnquiryValidator(), $actionType);
    }
}
