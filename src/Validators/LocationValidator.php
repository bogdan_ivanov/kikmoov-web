<?php

namespace App\Validators;

use App\Entity\Area;
use App\Entity\AreaList;
use App\Entity\Location;
use App\Entity\WorkSpace;
use App\Repository\AreaRepository;
use App\Validators\Constraints\Latitude;
use App\Validators\Constraints\Longitude;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class LocationValidator
 * @package App\Validators
 */
class LocationValidator extends AbstractValidator
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * LocationValidator constructor.
     * @param ValidatorInterface $validator
     * @param EntityManager $entityManager
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        parent::__construct($validator);
        $this->entityManager = $entityManager;
    }

    public function getAreas()
    {
        /** @var AreaRepository $repository */
        $repository = $this->entityManager->getRepository(Area::class);

        return $repository->getActiveAreasSlugs();
    }

    /**
     * @param $areas
     * @return array
     */
    protected static function fieldsValidator($areas)
    {
        return [
            'name' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Length(['max' => 40])]
            ],
            'address' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'optionalAddress' => [
                'required' => false,
            ],
            'borough' => [
                'required' => false,
            ],
            'company' => [
                'required' => false,
            ],
            'area' => [
                'required' => true,
                'constraints' => [
                    new Choice([
                        'choices' => array_column($areas, 'slug')
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'latitude' => [
                'required' => true,
                'constraints' => [new Latitude()]
            ],
            'longitude' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Longitude()]
            ],
            'nearby' => [
                'required' => false,
                'constraints' => [
                    new All([
                        new Collection([
                            "type" => new NotBlank(),
                            "name" => new NotBlank(),
                            "distance" => new NotBlank(),
                            "duration" => new NotBlank()
                        ])
                    ])
                ]
            ],
            'town' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'postcode' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Length(['max' => 10])]
            ],
            'description' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'workSpaceTypes' => [
                'required' => false,
                'constraints' => [
                    new Choice([
                        'choices' => [WorkSpace::DESK, WorkSpace::PRIVATE_OFFICE, WorkSpace::MEETING_ROOM],
                        'multiple' => true
                    ])
                ]
            ]
        ];
    }

    /**
     * A little bit of black magic
     * @param Location $location
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function validateRequest(Location &$location, array $data, $actionType = self::CREATE_ACTION): array
    {
        $areas = $this->getAreas();
        return $this->magicValidator($location, $data, self::fieldsValidator($areas), $actionType);
    }
}
