<?php

namespace App\Validators;

/**
 * @IMPORTANT - use the validator after the condition value was set
 *
 * Class OptionalValidator
 * @package App\Validators
 */
class OptionalValidator
{
    /**
     * Property of entity, will check the getter existence
     * @var string
     */
    private $fieldName;

    /**
     * Value, when validation will fired
     * @var string|array|integer|boolean
     */
    private $conditionValue;

    /**
     * Array of symfony Constraint
     * eg [new NotBlank(), new Time()]
     * @var array|null
     */
    private $constraints;

    /**
     * OptionalValidator constructor.
     * @param string $fieldName
     * @param $conditionValue
     * @param array $constraints
     */
    public function __construct(string $fieldName, $conditionValue, array $constraints = [])
    {
        $this->fieldName = $fieldName;
        $this->conditionValue = $conditionValue;
        $this->constraints = $constraints;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return mixed
     */
    public function getConditionValue()
    {
        return $this->conditionValue;
    }

    /**
     * @return mixed
     */
    public function getConstraints()
    {
        return $this->constraints;
    }
}
