<?php

namespace App\Validators;

use App\Entity\User;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class SignUpValidator
 * @package App\Validators
 */
class SignUpValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function signUpSellerRules() : array
    {
        return [
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'password' => [
                'constraints' => [
                    new Length([
                        'min' => '8',
                        'minMessage' => 'The password is too short. It should have 8 characters or more'
                    ]),
                    new NotBlank()
                ]
            ],
            'firstname' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'lastname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'phone' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'companyName' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'address' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'optionalAddress' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'town' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'postcode' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'invoiceEmail' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ]
        ];
    }

    /**
     * @param User $user
     * @param array $data
     * @return array
     */
    public function sellerSignUpValidator(User &$user, array $data)
    {
        return $this->magicValidator($user, $data, self::signUpSellerRules(), self::CREATE_ACTION);
    }

    /**
     * @return array
     */
    protected static function signUpBuyerRules() : array
    {
        return [
            'email' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Email()]
            ],
            'password' => [
                'constraints' => [
                    new Length([
                        'min' => '8',
                        'minMessage' => 'The password is too short. It should have 8 characters or more'
                    ]),
                    new NotBlank()
                ]
            ],
            'firstname' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'lastname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ]
        ];
    }

    /**
     * @param User $user
     * @param array $data
     * @return array
     */
    public function buyerSignUpValidator(User &$user, array $data) : array
    {
        return $this->magicValidator($user, $data, self::signUpBuyerRules(), self::CREATE_ACTION);
    }

    /**
     * @param string $password
     * @return array
     */
    public function validatePassword(string $password) : array
    {
        $errors = [];

        $this->validateProperty(
            'password',
            $password,
            [
                new Length([
                    'min' => '8',
                    'minMessage' => 'The password is too short. It should have 8 characters or more'
                ]),
                new NotBlank()
            ],
            $errors
        );

        return $errors;
    }
}
