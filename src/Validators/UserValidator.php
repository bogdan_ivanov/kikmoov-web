<?php

namespace App\Validators;

use App\Entity\User;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserValidator
 * @package App\Validators
 */
class UserValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function updateProfile() : array
    {
        return [
            'firstname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'lastname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'phone' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'companyName' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ]
        ];
    }

    /**
     * @param User $user
     * @param array $data
     * @return array
     */
    public function updateUserDetails(User &$user, array $data) : array
    {
        return $this->magicValidator($user, $data, self::updateProfile(), self::UPDATE_ACTION);
    }

    /**
     * @return array
     */
    protected static function updateSeller() : array
    {
        return [
            'firstname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'lastname' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'phone' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'companyName' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'address' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'optionalAddress' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'town' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'postcode' => [
                'required' => false,
                'constraints' => [new NotBlank()]
            ],
            'invoiceEmail' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Email()]
            ]
        ];
    }

    /**
     * @param User $user
     * @param array $data
     * @return array
     */
    public function updateSellerDetails(User &$user, array $data) : array
    {
        return $this->magicValidator($user, $data, self::updateSeller(), self::UPDATE_ACTION);
    }
}
