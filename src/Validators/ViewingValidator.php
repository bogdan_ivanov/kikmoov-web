<?php

namespace App\Validators;

use App\Entity\Viewing;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ViewingValidator
 * @package App\Validators
 */
class ViewingValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function viewingRules(): array
    {
        $nowTimestamp = (new \DateTime())->getTimestamp();

        return [
            'startTime' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'endTime' => [
                'required' => false,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ],
            'phone' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'email' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Email()]
            ]
        ];
    }

    /**
     * @param Viewing $viewing
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function viewingValidator(Viewing &$viewing, array $data, $actionType = self::CREATE_ACTION)
    {
        return $this->magicValidator($viewing, $data, self::viewingRules(), $actionType);
    }

    /**
     * @param array $emails
     * @param array $alreadyInvited
     * @return array
     */
    public function validateAttendees(array $emails, ?array $alreadyInvited) : array
    {
        $errorsList = [];

        foreach ($emails as $email) {
            if (!empty($alreadyInvited)) {
                if (in_array($email, $alreadyInvited)) {
                    $errorsList[] = [
                        'attribute' => "email",
                        'details' => sprintf('Email address already in list - %s', $email)
                    ];

                    continue;
                }
            }

            $errors = $this->getValidator()->validate($email, [new Email()]);

            if (0 !== count($errors)) {
                $errorsList[] = [
                    'attribute' => "email",
                    'details' => sprintf('Invalid email address - %s', $email)
                ];
            }
        }

        return $errorsList;
    }
}
