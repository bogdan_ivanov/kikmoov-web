<?php

namespace App\Validators;

use App\Entity\WorkSpace;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Validators\Constraints\Time;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class LocationValidator
 * @package App\Validators
 */
class WorkSpaceValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected static function meetingRoomValidator()
    {
        $nowTimestamp = (new \DateTime())->setTime(0,0)->getTimestamp();

        return [
            'quantity' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer"), new GreaterThanOrEqual(1)]
            ],
            'price' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("numeric")]
            ],
            'deskType' => [
                'required' => true,
                'constraints' => [
                    new Choice([
                        'choices' => [
                            WorkSpace::HOURLY_MEETING_ROOM,
                            WorkSpace::DAILY_MEETING_ROOM,
                        ],
                        'multiple' => false
                    ])
                ]
            ],
            'capacity' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer")]
            ],
            'description' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'opensFrom' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Time()]
            ],
            'closesAt' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Time()]
            ],
            'availableFrom' => [
                'required' => false,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected static function deskValidator()
    {
        $nowTimestamp = (new \DateTime())->setTime(0,0)->getTimestamp();

        return [
            'quantity' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer"), new GreaterThanOrEqual(1)]
            ],
            'price' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("numeric")]
            ],
            'deskType' => [
                'required' => true,
                'constraints' => [
                    new Choice([
                        'choices' => [
                            WorkSpace::HOURLY_HOT_DESK,
                            WorkSpace::MONTHLY_HOT_DESK,
                            WorkSpace::MONTHLY_FIXED_DESK,
                            WorkSpace::DAILY_HOT_DESK
                        ],
                        'multiple' => false
                    ])
                ]
            ],
            'description' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'opensFrom' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Time()]
            ],
            'closesAt' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Time()]
            ],
            'minContractLength' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Type("integer")]
            ],
            'availableFrom' => [
                'required' => false,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ]
        ];
    }

    /**
     * @return array
     */
    protected static function privateOfficeValidator()
    {
        $nowTimestamp = (new \DateTime())->setTime(0,0)->getTimestamp();

        return [
            'quantity' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer"), new GreaterThanOrEqual(1)]
            ],
            'price' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("numeric")]
            ],
            'size' => [
                'required' => false,
                'constraints' => [new NotBlank(), new Type("integer")]
            ],
            'capacity' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer")]
            ],
            'minContractLength' => [
                'required' => true,
                'constraints' => [new NotBlank(), new Type("integer")]
            ],
            'description' => [
                'required' => true,
                'constraints' => [new NotBlank()]
            ],
            'availableFrom' => [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => $nowTimestamp,
                        'message' => 'This value is not a valid date or date in past'
                    ])
                ],
                'checkGettersAndSetters' => false
            ]
        ];
    }

    /**
     * @param WorkSpace $workSpace
     * @param array $data
     * @param int $actionType
     * @return array
     */
    public function validateRequest(WorkSpace &$workSpace, array $data, $actionType = self::CREATE_ACTION): array
    {
        if ($workSpace->getType() === WorkSpace::MEETING_ROOM) {
            $validators = self::meetingRoomValidator();
        } elseif ($workSpace->getType() === WorkSpace::PRIVATE_OFFICE) {
            $validators = self::privateOfficeValidator();
        } else {
            $validators = self::deskValidator();
        }

        return $this->magicValidator($workSpace, $data, $validators, $actionType);
    }
}
